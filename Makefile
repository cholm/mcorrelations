# === General settings ===============================================
NAME		:= correlations
VERSION		:= master
MUTE		:= @
SHOW		= @printf "%-16s %s\n" "$(1)" "$(2)"
#SHOW		= $(shell echo "$(1) $(2)")
ifdef VERBOSE
MUTE		:=
SHOW		:=
endif

#DEBUG		:= 1

# --- Various compiler and linker flags ------------------------------
CXX		:= g++ -c
CXXFLAGS	:= -Wall -Wextra -ansi -pedantic -g -O3 -std=c++11 
#CXXFLAGS	:= -Wall -Wextra -ansi -pedantic -O3 -std=c++11 -ffast-math
CXXFLAGS	:= -Wall -Wextra -ansi -pedantic -std=c++11
CPP		:= g++
CPPFLAGS	:= -I.
LD		:= g++ 
LDFLAGS		:=
ifdef PROFILE
CXXFLAGS	+= -pg
LDFLAGS		+= -pg -lprofiler
endif
ifdef DEBUG
CXXFLAGS	:= $(filter-out -O3, $(CXXFLAGS))
endif
ifdef ADDRESS
CXXFLAGS	+= -fsanitize=address
LDFLAGS		+= -lasan
endif

PYTHON		:= python3

MAXP		:= 5
UNCER		:= Derivatives
ETA_GAP		:= 0.2

# === The source code and documentation ==============================
CORE		:= correlations/json.hh				\
		   correlations/Correlator.hh			\
		   correlations/FromQVector.hh			\
		   correlations/QVector.hh			\
		   correlations/QStore.hh			\
		   correlations/NestedLoops.hh			\
		   correlations/Result.hh			\
		   correlations/Types.hh			\
		   correlations/Partition.hh			\
		   correlations/MoreTypes.hh			\
		   correlations/Dbg.hh				\
		   correlations/Closed.hh			\
		   correlations/Recurrence.hh			\
		   correlations/Recursive.hh			\
		   correlations/RecursiveLoops.hh	
FLOW		:= correlations/flow/Calculations.hh		\
		   correlations/flow/ToValue.hh			\
		   correlations/flow/VNM.hh			\
		   correlations/flow/VPNM.hh			\
		   correlations/flow/Bin.hh			\
		   correlations/flow/BinT.hh			\
		   correlations/flow/Reference.hh		\
		   correlations/flow/OfInterest.hh		\
		   correlations/flow/explicit/VPN4.hh		\
		   correlations/flow/explicit/Reference4.hh	\
		   correlations/flow/explicit/OfInterest4.hh	
STAT		:= correlations/stat/Trait.hh			\
		   correlations/stat/Stat.hh			\
		   correlations/stat/West.hh			\
		   correlations/stat/Var.hh			\
		   correlations/stat/WelfordVar.hh		\
		   correlations/stat/WestVar.hh			\
		   correlations/stat/Cov.hh			\
		   correlations/stat/WelfordCov.hh		\
		   correlations/stat/WestCov.hh			\
		   correlations/stat/Estimator.hh		\
		   correlations/stat/Derivatives.hh		\
		   correlations/stat/SubSamples.hh		\
		   correlations/stat/Bootstrap.hh		\
		   correlations/stat/Jackknife.hh		
SYMBOLIC	:= correlations/symbolic/Types.hh		\
	      	   correlations/symbolic/Partition.hh		\
		   correlations/symbolic/QVector.hh		\
		   correlations/symbolic/Printer.hh		\
		   correlations/symbolic/Maxima.hh		\
		   correlations/symbolic/Maple.hh		\
		   correlations/symbolic/SymPy.hh		\
		   correlations/symbolic/Sage.hh		\
		   correlations/symbolic/Jupyter.hh		\
		   correlations/symbolic/JupyterSymPy.hh	\
		   correlations/symbolic/JupyterSage.hh
GENS		:= correlations/gen/EtaPtPhiW.hh		\
		   correlations/gen/EtaPhiW.hh			\
		   correlations/gen/Generator.hh		\
		   correlations/gen/Header.hh			\
		   correlations/gen/JetLike.hh			\
		   correlations/gen/MomJet.hh			\
		   correlations/gen/Momentum.hh			\
		   correlations/gen/OldPhiW.hh			\
		   correlations/gen/PhiW.hh			\
		   correlations/gen/PtPhiW.hh			\
		   correlations/gen/Sampler.hh		
ANAS		:= correlations/ana/StopWatch.hh		\
		   correlations/ana/Progress.hh			\
		   correlations/ana/Reader.hh			\
		   correlations/ana/Analyzer.hh			\
		   correlations/ana/Writer.hh			\
		   correlations/ana/Benchmark.hh		\
		   correlations/ana/Axis.hh			\
		   correlations/ana/Profile.hh			\
		   correlations/ana/Partitioner.hh		\
		   correlations/ana/Distributions.hh		\
		   correlations/ana/XBin.hh			\
		   correlations/ana/XBinned.hh			\
		   correlations/ana/Integrated.hh		\
		   correlations/ana/Differential.hh		\
		   correlations/ana/Explicit.hh			
EXAS		:= correlations/progs/tools.hh			\
		   correlations/progs/opt.hh
HEADERS		:= $(CORE) $(FLOW) $(STAT) $(SYMBOLIC) $(GENS) $(ANAS) $(EXAS)
DHEADERS	:= $(patsubst %.hh, %.hh.d, \
		     $(filter-out %/MoreTypes.hh %/Partition.hh %/ToValue.hh, \
		       $(HEADERS)))
PROGS		:= correlations/progs/writer.cc			\
		   correlations/progs/benchmark.cc		\
		   correlations/progs/distributions.cc		\
		   correlations/progs/integrated.cc		\
		   correlations/progs/differential.cc		\
		   correlations/progs/explicit.cc		\
		   correlations/progs/symbolic.cc
TESTS		:= tests/testEstimator.cc			\
		   tests/testOpt.cc				\
		   tests/testStat.hh				\
		   tests/testStat.cc				\
		   tests/testTrig.cc				\
		   tests/testQvector.cc				\
		   tests/testRecurrence.cc			\
		   tests/testTrig.py
MDS		:= README.md					\
		   doc/Tests.md					\
		   doc/Methods.md				\
		   doc/Overview.md				\
		   doc/Symbolic.md				\
		   doc/GF.md					\
		   doc/Sub.md					\
		   doc/Flow.md
DOCS		:= doc/style.css				\
		   doc/layout.xml	
EXTRA		:= Makefile 					\
		   doc/Doxyfile.in 				\
		   data/ante.mc					\
		   cas/Expand.nb				\
		   cas/P2correlator.nb				\
		   cas/P3correlator.nb				\
		   cas/P4correlator.nb				\
		   cas/P5correlator.nb				\
		   cas/P6correlator.nb				\
		   cas/P7correlator.nb				\
		   cas/P8correlator.nb				\
		   tests/examples.cc				\
		   COPYING

IMGS		:= gen_et0_medium_pint.png			\
		   gen_eta_medium_diff.png			\
		   gen_eta_medium_diff_bootstrap.png		\
		   gen_eta_medium_diff_jackknife.png		\
		   gen_eta_medium_dist.png			\
		   gen_eta_medium_edif.png			\
		   gen_eta_medium_pdif.png			\
		   gen_phi_medium_dist.png			\
		   gen_phi_medium_intg.png			\
		   gen_phi_medium_intg_uncr.png			\
		   gen_phi_small_corr.png			\
		   gen_pt_medium_dist.png			\
		   jet_eta_medium_diff.png			\
		   jet_eta_medium_dist.png			\
		   jet_eta_medium_edif.png			\
		   jet_eta_medium_pdif.png			\
		   jet_eta_medium_uedf.png			\
		   mom_eta_medium_dist.png			


# --- Derived settings -----------------------------------------------
TMDS		:= $(foreach m,$(MDS), $(dir $(m))_$(notdir $(m)))
EXEC		:= $(notdir $(basename $(PROGS)))
TEXEC		:= $(notdir $(basename $(filter %.cc, $(TESTS))))
EXEC_ARGS	:= -L

# === Pattern rules ==================================================
# With -pg output to gmon.out or ${GMON_OUT_PREFIX}.[PID] if set
%.o:correlations/progs/%.cc
	$(call SHOW, CXX, $@)
	$(MUTE)$(CXX) $(CPPFLAGS) $(CXXFLAGS) $<

%.o:tests/%.cc
	$(call SHOW, CXX, $@)
	$(MUTE)$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< 

%.hh.gch:%.hh
	$(call SHOW, CXX, $@)
	$(MUTE)$(CXX) $(CPPFLAGS) $(CXXFLAGS) $<

%.hh.d:%.hh
	$(call SHOW, CPP, $@)
	$(MUTE)$(CPP) $(CPPFLAGS) -MM -MT $<.gch -o $@ $<

%:%.o
	$(call SHOW, LD, $@)
	$(MUTE)$(LD) $(LDFLAGS) -o $@ $^

%.vlg:%
	$(call SHOW, VALGRIND, $@)
	$(MUTE)valgrind --tool=callgrind --callgrind-out-file=$@ ./$<

%.prof:main
	$(call SHOW, PROF, $@)
	$(MUTE)LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libprofiler.so	\
	CPUPROFILE=$@						\
		./$< -a $* $(FLOWFLAGS)

%_prof.dot:%.prof
	$(call SHOW, PROF-DOT, $@)
	$(MUTE)google-pprof -dot main $< > $@

maxima_%.mac sympy_%.py sage_%.sage maple_%.mw:symbolic
	$(call SHOW, SYMBOLIC, $@)
	$(MUTE)./$< -f $(word 1, $(subst _, , $@)) -n $* -l $* > $@

sympy_%.ipynb sage_%.ipynb:symbolic
	$(call SHOW, SYMBOLIC, $@)
	$(MUTE)./$< -f j$(word 1, $(subst _, , $@)) -n $* -l $* > $@

%.slides.html:doc/%.ipynb doc/slides.tpl
	$(call SHOW, NBCONVERT, $@)
	$(MUTE)jupyter nbconvert \
		--to slides \
		--no-input \
		--output-dir . \
		--SlidesExporter.reveal_theme=simple \
		--template doc/slides.tpl \
		$<


# === General targets ================================================
all:	$(EXEC)

dep:	$(HEADERS)
	$(MAKE) $(DHEADERS)
	$(call SHOW DEPEND,$@)
	$(MUTE)./correlations/scripts/depend.py $(DHEADERS) > $@
	$(MUTE)rm -f $(DHEADERS)


check:
	@for f in $(HEADERS); do \
	    if test ! -r $$f ; then echo "Missing $$i"; fi ; done
	@$(foreach f, $(shell find . -name "*.hh" -and -not -path "*/meta/*"), \
	     $(if $(findstring $(f:./%=%), $(HEADERS) $(TESTS)), , \
		echo "$(f) not mentioned";))
	@$(foreach f, $(shell find . -name "*.cc" -and -not -path "*/meta/*"), \
	     $(if $(findstring $(f:./%=%), $(PROGS) $(TESTS) $(EXTRA)), , \
		echo "$(f) not mentioned";))

install: 
ifeq ($(PREFIX),)
	$(error PREFIX is not defined)
endif
	$(foreach f, $(HEADERS), \
	  mkdir -p $(PREFIX)/$(dir $(f)); cp $(f) $(PREFIX)/$(f);)

doc:	html/index.html

clean:
	$(call SHOW,"CLEANING", "files")
	$(MUTE)find . -name "*~" | xargs rm -f

	$(MUTE)rm -f core.* TAGS *.o *.png *.vlg *.dat *.json
	$(MUTE)rm -f *.mac *.py *.sage *.ipynb *.mw *.dot *_MAS.bak
	$(MUTE)rm -f a.out gmon.out *.prof 

	$(MUTE)rm -f symbolic.md symbolic.py symbolic.sage

	$(MUTE)rm -f doc/*.log doc/*.aux doc/*.out doc/*.synctex* doc/*.toc
	$(MUTE)rm -f doc/diff.pdf
	$(MUTE)rm -f *.slides.html 

	$(MUTE)rm -rf .ipynb_checkpoints/ doc/auto dep
	$(MUTE)rm -rf correlations/progs/__pycache__ __pycache__

	$(MUTE)rm -f $(EXEC) $(TEXEC) $(TMDS) doc/Doxyfile
	$(MUTE)rm -rf html $(NAME)-$(VERSION)

	$(MUTE)rm -f $(HEADERS:%.hh=%.hh.gch)

distclean: clean 
	rm -rf $(NAME)-$(VERSION).tar.gz $(NAME)-$(VERSION).zip

# --- Testing --------------------------------------------------------
symtest: symbolic
	#./$< -f maxima | maxima --very-quiet
	$(call SHOW,SYMBOLIC,$@)
	$(MUTE)./$< -f sympy  | $(PYTHON) $(PYTHONFLAGS)
	$(MUTE)./$< -f sage   | sage -q


# === Documentation targets ==========================================
# We have to parse the markdown to turn Gitlab MD into Doxygen MD (sigh!)
_README.md:	README.md
	$(call SHOW,MD,$@)
	$(MUTE)sed -e 's/```math/@f[/'					\
	    -e 's/```/@f]/'						\
	    -e 's/\$$`/@f$$/g'						\
	    -e 's/`\$$/@f$$/g'						\
	    -e 's,(COPYING),(@ref COPYING),'				\
	    -e 's,doc/\(.*\).md,doc/_\1.md,'				\
	    -e 's,-master.tar.gz,-$(VERSION).tar.gz,'			\
	    -e 's,^# \(.*\),@mainpage\n[TOC]\n\n# \1 {#$*_top},'  	\
	    -e 's,^\(##*\) \([^[:space:]]*\)\(.*\),\1 \2\3 {#$*_\2},' 	\
	    < $< > $@

doc/_%.md:	doc/%.md
	$(call SHOW,MD,$@)
	$(MUTE)sed -e 's/```math/@f[/'					\
	    -e 's/```/@f]/'						\
	    -e 's/\$$`/@f$$/g'						\
	    -e 's/`\$$/@f$$/g'						\
	    -e 's,(correlations/progs/\(.*\)\.\(C\|cc\)),(@ref \1.\2),' \
	    -e 's,doc/\(.*\).md,doc/_\1.md,'				\
	    -e 's,^\(###*\) \([^[:space:]][^[:space:]]*\)\(.*\),echo -n "\1 \2\3 {#$*_";echo "\2\3" | tr "[[:space:]()]" "_"; echo "}",e' 	\
	    -e 's,^# \(.*\),[TOC]\n\n# \1 {#$*_top},'  			\
	    < $< > $@


doc/Doxyfile:	doc/Doxyfile.in 
	$(call SHOW,SED,$@)
	$(MUTE)sed -e 's/@PACKAGE@/${NAME}/' \
		   -e 's/@VERSION@/${VERSION}/' < $< > $@

html/index.html:doc/Doxyfile $(HEADERS) $(ANAS) $(PROGS) $(DOCS) $(TMDS)
	$(call SHOW,DOXYGEN,$@)
	$(MUTE)doxygen doc/Doxyfile
	$(MUTE)rm -f $(TMDS)

images:	$(IMGS)
doc_images:	$(IMGS:%=doc/%)

# === Distribution targets ===========================================
distdir: $(HEADERS) $(ANAS) $(EXTRA) $(PROGS) $(DOCS) $(MDS)
	$(call SHOW,DIST,$@)
	$(MUTE)rm -rf $(NAME)-$(VERSION)
	$(MUTE)mkdir -p $(NAME)-$(VERSION)
	$(MUTE)$(foreach f, $^, \
	  mkdir -p $(NAME)-$(VERSION)/$(dir $(f)); \
	  cp $(f) $(NAME)-$(VERSION)/$(f);)

dist:	distdir
	$(call SHOW,TAR,$@)
	$(MUTE)tar -czf $(NAME)-$(VERSION).tar.gz $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-$(VERSION)

zdist:	distdir
	$(call SHOW,ZIP,$@)
	$(MUTE)zip -r -l $(NAME)-$(VERSION).zip $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-$(VERSION)

distcheck:dist
	$(call SHOW,DIST,$@)
	$(MUTE)tar -xzf $(NAME)-$(VERSION).tar.gz
	$(MUTE)(cd $(NAME)-$(VERSION) && $(MAKE) $(MAKEFLAGS) test doc dist REPLOT=1)
	$(MUTE)rm -rf $(NAME)-$(VERSION)


# === Misc ===========================================================
%_small.dat:		NEV=100
%_medium.dat:		NEV=1000
%_large.dat:		NEV=10000

gen_%.dat:		GEN=default
jet_%.dat:		GEN=Jetlike
mom_%.dat:		GEN=Momentum

gen_phi_%.dat:		SAM=PhiW
gen_pt_%.dat:		SAM=PtPhiW
gen_eta_%.dat:		SAM=EtaPtPhiW
gen_et0_%.dat:		SAM=EtaPhiW

mom_pt_%.dat:		SAM=PtPhiW
mom_eta_%.dat:		SAM=EtaPtPhiW

jet_eta_%.dat:		SAM=EtaPtPhiW


%.dat: writer
	$(call SHOW,WRITER,$@)
ifdef PAR
	$(MUTE)seq 1 $(PAR) | \
	  parallel -t --eta -j 75% \
	    ./$< $(FLOWFLAGS) $(UFLOWFLAGS) -f -S \{\} | \
	    ./$<  $(FLOWFLAGS) $(UFLOWFLAGS) -l -i -
else
	$(MUTE)./$< -e $(NEV) -G $(GEN) -S $(SAM) -r 1000 -R 1000 -A -s 42 > $@
endif

%_clsd.json : %.dat benchmark
	$(call SHOW,BENCHMARK,$@)
	$(MUTE)./benchmark -i $< -s 42 -m $(MAXP) -a qvector_closed -o  $@

%_recs.json : %.dat benchmark
	$(call SHOW,BENCHMARK,$@)
	$(MUTE)./benchmark -i $< -s 42 -m $(MAXP) -a qvector_recursive -o  $@

%_recu.json : %.dat benchmark
	$(call SHOW,BENCHMARK,$@)
	$(MUTE)./benchmark -i $< -s 42 -m $(MAXP) -a qvector_recurrence -o $@

%_nest.json : %.dat benchmark
	$(call SHOW,BENCHMARK,$@)
	$(MUTE)./benchmark -i $< -s 42 -m $(MAXP) -a nested_closed -o $@

%_nstr.json : %.dat benchmark
	$(call SHOW,BENCHMARK,$@)
	$(MUTE)./benchmark -i $< -s 42 -m $(MAXP) -a nested_recursive -o $@

%_corr.png:%_clsd.json %_recs.json %_recu.json %_nest.json %_nstr.json
	$(call SHOW,BENCHMARK,$@)
	$(MUTE)./correlations/scripts/benchmark.py  $^ -o $@

%_dist.json:%.dat distributions
	$(call SHOW,DISTRIBUTION,$@)
	$(MUTE)./distributions -i $< -o $@ 

%_diff.json:%.dat differential
	$(call SHOW,DIFFERENTIAL,$@)
	$(MUTE)./differential -i $< -u $(UNCER) -o $@

%_intg.json:%.dat integrated
	$(call SHOW,INTEGRATED,$@)
	$(MUTE)./integrated -i $< -u $(UNCER) -o $@

%_pdif.json:%.dat differential
	$(call SHOW,DIFFERENTIAL,$@)
	$(MUTE)./differential -i $< -u $(UNCER) -g $(ETA_GAP) -o $@

%_pint.json:%.dat integrated
	$(call SHOW,INTEGRATED,$@)
	$(MUTE)./integrated -i $< -u $(UNCER) -g $(ETA_GAP) -o $@

%_edif.json:%.dat explicit
	$(call SHOW,EXPLICIT,$@)
	$(MUTE)./explicit -i $< -u $(UNCER) -g $(ETA_GAP) -o $@

%_uedf.json:%.dat explicit
	$(call SHOW,EXPLICIT,$@)
	$(MUTE)./explicit -i $< -u $(UNCER) -g $(ETA_GAP) -o $@ -U 

%.png:%.json
	$(call SHOW,PYTHON,$@)
	$(MUTE)$(PYTHON) $(PYTHONFLAGS) $(SCR) $< $@ $(FRACTION)

%_jackknife.json:
	$(call SHOW,JACKKNIFE,$@)
	$(MUTE)if test -f $*.json ; then mv $*.json $*_keep.json ; fi 
	$(MUTE)$(MAKE) $*.json UNCER=Jackknife
	$(MUTE)mv $*.json $@
	$(MUTE)if test -f $*_keep.json ; then mv $*_keep.json $*.json ; fi 

%_bootstrap.json:
	$(call SHOW,BOOSTRAP,$@)
	$(MUTE)if test -f $*.json ; then mv $*.json $*_keep.json ; fi 
	$(MUTE)$(MAKE) $*.json UNCER=Bootstrap
	$(MUTE)mv $*.json $@
	$(MUTE)if test -f $*_keep.json ; then mv $*_keep.json $*.json ; fi 

%_uncr.png:%.json %_jackknife.json %_bootstrap.json
	$(call SHOW,UNCERTAINTY,$@)
	$(MUTE)./correlations/scripts/uncertainty.py $^ -o $@

%_dist.png:		SCR=correlations/scripts/distributions.py

%_diff.png:		SCR=correlations/scripts/differential.py
%_pdif.png:		SCR=correlations/scripts/differential.py

%_intg.png:		SCR=correlations/scripts/integrated.py
%_pint.png:		SCR=correlations/scripts/integrated.py

%_edif.png:		SCR=correlations/scripts/differential.py
%_uedf.png:		SCR=correlations/scripts/differential.py

%_diff_jackknife.png:	SCR=correlations/scripts/differential.py
%_diff_bootstrap.png:	SCR=correlations/scripts/differential.py

%_edif_jackknife.png:	SCR=correlations/scripts/differential.py
%_edif_bootstrap.png:	SCR=correlations/scripts/differential.py

%_uedf_jackknife.png:	SCR=correlations/scripts/differential.py
%_uedf_bootstrap.png:	SCR=correlations/scripts/differential.py

doc/%.png:%.png
	$(call SHOW,CP,$@)
	$(MUTE)cp $< $@ 

# --- Spefific targets for data and so on ----------------------------
gen_phi_small.dat: writer
	$(call SHOW,WRITER,$@)
	$(MUTE)./$< -e $(NEV) -G $(GEN) -S $(SAM) -r 8 -R 10 -A -s 42 > $@

gen_eta_small.dat: writer
	$(call SHOW,WRITER,$@)
	$(MUTE)./$< -e 1 -G $(GEN) -S $(SAM) -r 8 -R 10 -A -s 42 > $@

testTrig.dat:	testTrig
	$(call SHOW,TESTTRIG,$@)
	$(MUTE)./$< > $@

testTrig.png:	tests/testTrig.py testTrig.dat
	$(call SHOW,PYTHON,$@)
	$(MUTE)$(PYTHON) $(PYTHONFLAGS) $< 


# Dependencies =======================================================
# Programs -----------------------------------------------------------
symbolic.o: 	correlations/progs/symbolic.cc 			\
		correlations/symbolic/JupyterSymPy.hh.gch	\
		correlations/symbolic/JupyterSage.hh.gch	\
		correlations/symbolic/Maxima.hh.gch		\
		correlations/symbolic/Maple.hh.gch		\
		correlations/Closed.hh.gch			\
		correlations/Recursive.hh.gch			\
		correlations/Recurrence.hh.gch			\
		correlations/RecursiveLoops.hh.gch		\
		correlations/progs/opt.hh.gch
symbolic.o:	CXXFLAGS+=-Wno-unused-function

writer.o:	correlations/progs/writer.cc			\
		correlations/progs/opt.hh.gch			\
		correlations/progs/tools.hh.gch			\
		correlations/ana/Writer.hh.gch			\
		correlations/gen/EtaPtPhiW.hh.gch		\
		correlations/gen/JetLike.hh.gch			\
		correlations/gen/MomJet.hh.gch			\
		correlations/gen/Momentum.hh.gch

distributions.o:correlations/progs/distributions.cc		\
		correlations/progs/opt.hh.gch			\
		correlations/progs/tools.hh.gch			\
		correlations/ana/Distributions.hh.gch		

benchmark.o:	correlations/progs/benchmark.cc			\
		correlations/ana/Benchmark.hh.gch		\
		correlations/progs/opt.hh.gch			\
		correlations/progs/tools.hh.gch			

integrated.o:	correlations/progs/integrated.cc		\
		correlations/progs/opt.hh.gch			\
		correlations/progs/tools.hh.gch			\
		correlations/ana/Integrated.hh.gch		

differential.o:	correlations/progs/differential.cc		\
		correlations/progs/opt.hh.gch			\
		correlations/progs/tools.hh.gch			\
		correlations/ana/Differential.hh.gch		

explicit.o:	correlations/progs/explicit.cc			\
		correlations/progs/opt.hh.gch			\
		correlations/progs/tools.hh.gch			\
		correlations/ana/Explicit.hh.gch		



# Tests --------------------------------------------------------------
tests:			$(TEXEC)

testStat.o:		tests/testStat.cc			\
			tests/testStat.hh.gch			\
			correlations/stat/WestVar.hh.gch	\
			correlations/stat/WestCov.hh.gch	\
			correlations/stat/WelfordVar.hh.gch	\
			correlations/stat/WelfordCov.hh.gch
testStat.o:		CXXFLAGS+=-Wno-unused-function

testWelfordVar.o:	tests/testWelfordVar.cc			\
			tests/testStat.hh			\
			correlations/stat/WelfordVar.hh.gch
testWestCov.o:		tests/testWestCov.cc			\
			tests/testStat.hh			\
			correlations/stat/WestCov.hh.gch
testWelfordCov.o:	tests/testWelfordCov.cc			\
			tests/testStat.hh			\
			correlations/stat/WelfordCov.hh.gch

testEstimator.o:	tests/testEstimator.cc			\
		        correlations/stat/Bootstrap.hh.gch	\
		        correlations/stat/Jackknife.hh.gch	\
		        correlations/stat/Derivatives.hh.gch

testOpt.o:		tests/testOpt.cc			\
			correlations/progs/opt.hh.gch

testTrig.o:		tests/testTrig.cc

testQvector.o:		correlations/QVector.hh.gch 		\
			correlations/ana/Analyzer.hh.gch 	\
			correlations/gen/Generator.hh.gch	\
			correlations/progs/opt.hh.gch 		


examples.o:		CPPFLAGS:=$(CPPFLAGS) -Icorrelations

# Dependencies automaticallly generated by target "dep" --------------
correlations/Closed.hh.gch:                                             \
	correlations/Closed.hh                                          \
	correlations/FromQVector.hh.gch                                 

correlations/Correlator.hh.gch:                                         \
	correlations/Correlator.hh                                      \
	correlations/Result.hh.gch                                      

correlations/Dbg.hh.gch:                                                \
	correlations/Dbg.hh                                             

correlations/FromQVector.hh.gch:                                        \
	correlations/FromQVector.hh                                     \
	correlations/QStore.hh.gch                                      \
	correlations/Correlator.hh.gch                                  

correlations/NestedLoops.hh.gch:                                        \
	correlations/NestedLoops.hh                                     \
	correlations/Correlator.hh.gch                                  

correlations/QStore.hh.gch:                                             \
	correlations/QStore.hh                                          \
	correlations/QVector.hh.gch                                     

correlations/QVector.hh.gch:                                            \
	correlations/QVector.hh                                         \
	correlations/Types.hh.gch                                       

correlations/Recurrence.hh.gch:                                         \
	correlations/Recurrence.hh                                      \
	correlations/FromQVector.hh.gch                                 

correlations/Recursive.hh.gch:                                          \
	correlations/Recursive.hh                                       \
	correlations/FromQVector.hh.gch                                 \
	correlations/Dbg.hh.gch                                         

correlations/RecursiveLoops.hh.gch:                                     \
	correlations/RecursiveLoops.hh                                  \
	correlations/NestedLoops.hh.gch                                 

correlations/Result.hh.gch:                                             \
	correlations/Result.hh                                          \
	correlations/Types.hh.gch                                       

correlations/Types.hh.gch:                                              \
	correlations/Types.hh                                           \
	correlations/MoreTypes.hh                                       \
	correlations/Partition.hh                                       

correlations/ana/Analyzer.hh.gch:                                       \
	correlations/ana/Analyzer.hh                                    \
	correlations/ana/Profile.hh.gch                                 \
	correlations/ana/Reader.hh.gch                                  \
	correlations/ana/Progress.hh.gch                                

correlations/ana/Axis.hh.gch:                                           \
	correlations/ana/Axis.hh                                        \
	correlations/gen/Generator.hh.gch                               

correlations/ana/Benchmark.hh.gch:                                      \
	correlations/ana/Benchmark.hh                                   \
	correlations/Closed.hh.gch                                      \
	correlations/Recursive.hh.gch                                   \
	correlations/Recurrence.hh.gch                                  \
	correlations/RecursiveLoops.hh.gch                              \
	correlations/ana/Analyzer.hh.gch                                

correlations/ana/Differential.hh.gch:                                   \
	correlations/ana/Differential.hh                                \
	correlations/ana/Analyzer.hh.gch                                \
	correlations/flow/OfInterest.hh.gch                             \
	correlations/ana/XBinned.hh.gch                                 \
	correlations/ana/Partitioner.hh.gch                             

correlations/ana/Distributions.hh.gch:                                  \
	correlations/ana/Distributions.hh                               \
	correlations/ana/Analyzer.hh.gch                                

correlations/ana/Explicit.hh.gch:                                       \
	correlations/ana/Explicit.hh                                    \
	correlations/ana/Analyzer.hh.gch                                \
	correlations/flow/explicit/OfInterest4.hh.gch                   \
	correlations/ana/XBinned.hh.gch                                 \
	correlations/ana/Partitioner.hh.gch                             

correlations/ana/Integrated.hh.gch:                                     \
	correlations/ana/Integrated.hh                                  \
	correlations/ana/Analyzer.hh.gch                                \
	correlations/flow/Reference.hh.gch                              \
	correlations/ana/Partitioner.hh.gch                             \
	correlations/ana/XBin.hh.gch                                    

correlations/ana/Partitioner.hh.gch:                                    \
	correlations/ana/Partitioner.hh                                 \
	correlations/Types.hh.gch                                       

correlations/ana/Profile.hh.gch:                                        \
	correlations/ana/Profile.hh                                     \
	correlations/ana/Axis.hh.gch                                    \
	correlations/stat/WelfordVar.hh.gch                             

correlations/ana/Progress.hh.gch:                                       \
	correlations/ana/Progress.hh                                    \
	correlations/ana/StopWatch.hh.gch                               

correlations/ana/Reader.hh.gch:                                         \
	correlations/ana/Reader.hh                                      \
	correlations/gen/Generator.hh.gch                               

correlations/ana/StopWatch.hh.gch:                                      \
	correlations/ana/StopWatch.hh                                   

correlations/ana/Writer.hh.gch:                                         \
	correlations/ana/Writer.hh                                      \
	correlations/ana/Analyzer.hh.gch                                

correlations/ana/XBin.hh.gch:                                           \
	correlations/ana/XBin.hh                                        \
	correlations/flow/Bin.hh.gch                                    

correlations/ana/XBinned.hh.gch:                                        \
	correlations/ana/XBinned.hh                                     \
	correlations/ana/XBin.hh.gch                                    \
	correlations/stat/WelfordVar.hh.gch                             

correlations/flow/Bin.hh.gch:                                           \
	correlations/flow/Bin.hh                                        \
	correlations/Result.hh.gch                                      \
	correlations/json.hh.gch                                        

correlations/flow/BinT.hh.gch:                                          \
	correlations/flow/BinT.hh                                       \
	correlations/flow/Bin.hh.gch                                    

correlations/flow/Calculations.hh.gch:                                  \
	correlations/flow/Calculations.hh                               \
	correlations/Types.hh.gch                                       \
	correlations/stat/Stat.hh.gch                                   

correlations/flow/OfInterest.hh.gch:                                    \
	correlations/flow/OfInterest.hh                                 \
	correlations/flow/Reference.hh.gch                              \
	correlations/flow/VPNM.hh.gch                                   

correlations/flow/Reference.hh.gch:                                     \
	correlations/flow/Reference.hh                                  \
	correlations/flow/VNM.hh.gch                                    \
	correlations/flow/BinT.hh.gch                                   \
	correlations/Closed.hh.gch                                      

correlations/gen/EtaPhiW.hh.gch:                                        \
	correlations/gen/EtaPhiW.hh                                     \
	correlations/gen/PhiW.hh.gch                                    

correlations/gen/EtaPtPhiW.hh.gch:                                      \
	correlations/gen/EtaPtPhiW.hh                                   \
	correlations/gen/PtPhiW.hh.gch                                  

correlations/gen/Generator.hh.gch:                                      \
	correlations/gen/Generator.hh                                   \
	correlations/gen/Sampler.hh.gch                                 

correlations/gen/Header.hh.gch:                                         \
	correlations/gen/Header.hh                                      \
	correlations/Types.hh.gch                                       \
	correlations/json.hh.gch                                        

correlations/gen/JetLike.hh.gch:                                        \
	correlations/gen/JetLike.hh                                     \
	correlations/gen/Generator.hh.gch                               

correlations/gen/MomJet.hh.gch:                                         \
	correlations/gen/MomJet.hh                                      \
	correlations/gen/Generator.hh.gch                               

correlations/gen/Momentum.hh.gch:                                       \
	correlations/gen/Momentum.hh                                    \
	correlations/gen/Generator.hh.gch                               

correlations/gen/OldPhiW.hh.gch:                                        \
	correlations/gen/OldPhiW.hh                                     \
	correlations/gen/Sampler.hh.gch                                 

correlations/gen/PhiW.hh.gch:                                           \
	correlations/gen/PhiW.hh                                        \
	correlations/gen/Sampler.hh.gch                                 

correlations/gen/PtPhiW.hh.gch:                                         \
	correlations/gen/PtPhiW.hh                                      \
	correlations/gen/PhiW.hh.gch                                    

correlations/gen/Sampler.hh.gch:                                        \
	correlations/gen/Sampler.hh                                     \
	correlations/gen/Header.hh.gch                                  

correlations/json.hh.gch:                                               \
	correlations/json.hh                                            

correlations/flow/ToValue.hh.gch:                                       \
	correlations/flow/ToValue.hh					\
	correlations/Types.hh.gch                                       

correlations/flow/VNM.hh.gch:                                           \
	correlations/flow/VNM.hh                                        \
	correlations/Result.hh.gch                                      \
	correlations/stat/Derivatives.hh.gch                            \
	correlations/flow/Calculations.hh.gch                           \
	correlations/flow/ToValue.hh                                    

correlations/flow/VPNM.hh.gch:                                          \
	correlations/flow/VPNM.hh                                       \
	correlations/flow/VNM.hh.gch                                    

correlations/progs/opt.hh.gch:                                          \
	correlations/progs/opt.hh                                       

correlations/progs/tools.hh.gch:                                        \
	correlations/progs/tools.hh                                     \
	correlations/ana/Analyzer.hh.gch                                \
	correlations/Closed.hh.gch                                      \
	correlations/Recursive.hh.gch                                   \
	correlations/Recurrence.hh.gch                                  \
	correlations/stat/Bootstrap.hh.gch                              \
	correlations/stat/Jackknife.hh.gch                              \
	correlations/stat/Derivatives.hh.gch                            \
	correlations/ana/Partitioner.hh.gch                             

correlations/stat/Bootstrap.hh.gch:                                     \
	correlations/stat/Bootstrap.hh                                  \
	correlations/stat/SubSamples.hh.gch                             

correlations/stat/Jackknife.hh.gch:                                     \
	correlations/stat/Jackknife.hh                                  \
	correlations/stat/SubSamples.hh.gch                             

correlations/flow/explicit/OfInterest4.hh.gch:                          \
	correlations/flow/explicit/OfInterest4.hh                       \
	correlations/flow/Reference.hh.gch                              \
	correlations/flow/explicit/Reference4.hh.gch                    \
	correlations/flow/explicit/VPN4.hh.gch                          

correlations/flow/explicit/Reference4.hh.gch:                           \
	correlations/flow/explicit/Reference4.hh                        \
	correlations/Closed.hh.gch                                      \
	correlations/flow/Bin.hh.gch                                    

correlations/flow/explicit/VPN4.hh.gch:                                 \
	correlations/flow/explicit/VPN4.hh                              \
	correlations/Result.hh.gch                                      \
	correlations/stat/Derivatives.hh.gch                            \
	correlations/flow/Calculations.hh.gch                           

correlations/stat/Cov.hh.gch:                                           \
	correlations/stat/Cov.hh                                        \
	correlations/stat/Stat.hh.gch                                   

correlations/stat/Derivatives.hh.gch:                                   \
	correlations/stat/Derivatives.hh                                \
	correlations/stat/Estimator.hh.gch                              \
	correlations/stat/WestCov.hh.gch                                

correlations/stat/Estimator.hh.gch:                                     \
	correlations/stat/Estimator.hh                                  \
	correlations/stat/Stat.hh.gch                                   

correlations/stat/Stat.hh.gch:                                          \
	correlations/stat/Stat.hh                                       \
	correlations/stat/Trait.hh.gch                                  \
	correlations/json.hh.gch                                        

correlations/stat/SubSamples.hh.gch:                                    \
	correlations/stat/SubSamples.hh                                 \
	correlations/stat/Estimator.hh.gch                              \
	correlations/stat/WestVar.hh.gch                                \
	correlations/stat/WelfordVar.hh.gch                             

correlations/stat/Trait.hh.gch:                                         \
	correlations/stat/Trait.hh                                      

correlations/stat/Var.hh.gch:                                           \
	correlations/stat/Var.hh                                        \
	correlations/stat/Stat.hh.gch                                   

correlations/stat/WelfordCov.hh.gch:                                    \
	correlations/stat/WelfordCov.hh                                 \
	correlations/stat/Cov.hh.gch                                    

correlations/stat/WelfordVar.hh.gch:                                    \
	correlations/stat/WelfordVar.hh                                 \
	correlations/stat/Var.hh.gch                                    

correlations/stat/West.hh.gch:                                          \
	correlations/stat/West.hh                                       \
	correlations/stat/Stat.hh.gch                                   

correlations/stat/WestCov.hh.gch:                                       \
	correlations/stat/WestCov.hh                                    \
	correlations/stat/Cov.hh.gch                                    \
	correlations/stat/West.hh.gch                                   \
	correlations/stat/WelfordVar.hh.gch                             

correlations/stat/WestVar.hh.gch:                                       \
	correlations/stat/WestVar.hh                                    \
	correlations/stat/Var.hh.gch                                    \
	correlations/stat/West.hh.gch                                   

correlations/symbolic/Jupyter.hh.gch:                                   \
	correlations/symbolic/Jupyter.hh                                \
	correlations/symbolic/Printer.hh.gch                            

correlations/symbolic/JupyterSage.hh.gch:                               \
	correlations/symbolic/JupyterSage.hh                            \
	correlations/symbolic/Jupyter.hh.gch                            \
	correlations/symbolic/Sage.hh.gch                               

correlations/symbolic/JupyterSymPy.hh.gch:                              \
	correlations/symbolic/JupyterSymPy.hh                           \
	correlations/symbolic/Jupyter.hh.gch                            \
	correlations/symbolic/SymPy.hh.gch                              

correlations/symbolic/Maple.hh.gch:                                     \
	correlations/symbolic/Maple.hh                                  \
	correlations/symbolic/Printer.hh.gch                            

correlations/symbolic/Maxima.hh.gch:                                    \
	correlations/symbolic/Maxima.hh                                 \
	correlations/symbolic/Printer.hh.gch                            

correlations/symbolic/Printer.hh.gch:                                   \
	correlations/symbolic/Printer.hh                                \
	correlations/symbolic/QVector.hh.gch                            \
	correlations/flow/Calculations.hh.gch                           \
	correlations/Recursive.hh.gch                                   \
	correlations/Recurrence.hh.gch                                  \
	correlations/Closed.hh.gch                                      

correlations/symbolic/QVector.hh.gch:                                   \
	correlations/symbolic/QVector.hh                                \
	correlations/symbolic/Types.hh.gch                              

correlations/symbolic/Sage.hh.gch:                                      \
	correlations/symbolic/Sage.hh                                   \
	correlations/symbolic/SymPy.hh.gch                              

correlations/symbolic/SymPy.hh.gch:                                     \
	correlations/symbolic/SymPy.hh                                  \
	correlations/symbolic/Printer.hh.gch                            

correlations/symbolic/Types.hh.gch:                                     \
	correlations/symbolic/Types.hh                                  \
	correlations/MoreTypes.hh                                       \
	correlations/symbolic/Partition.hh                              



.PRECIOUS:	%_clsd.json 	\
		%_nest.json 	\
		%_nstr.json 	\
		%_recs.json 	\
		%_recu.json	\
		%_diff.json	\
		%_intg.json	\
		%_pdif.json	\
		%_pint.json


#
# EOF
#

