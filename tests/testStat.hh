#ifndef TESTSTAT_HH
#define TESTSTAT_HH
#include <random>
#include <iostream>
#include <iomanip>
#include <complex>
#include <sstream>
#include <correlations/stat/Stat.hh>
#include <correlations/stat/Var.hh>
#include <correlations/stat/Cov.hh>
#include <correlations/stat/WelfordVar.hh>
#include <correlations/stat/WelfordCov.hh>
#include <correlations/stat/WestVar.hh>
#include <correlations/stat/WestCov.hh>
#include <correlations/ana/Progress.hh>

namespace std
{
  //==================================================================
  /**
   * Specialisation of a normal distribution for complex numbers.
   *
   * That is,
   *
   * @f[ Z\sim N(\mu,\sigma)\quad,@f$ 
   *
   * with the PDF
   *
   * @f[ P_\mathrm{N}(z) = 
   * \frac{1}{\sqrt{2\pi}\sigma}e^{-\frac12{(z-\mu)^2}{\sigma^2}}\quad,@f]
   *
   * for @f$z,\mu,\sigma\in\mathbb{C}@f$. 
   *
   * We do this by drawing @f$ Z=X+iY@f$ where 
   *
   * @f{eqnarray}{
   *    X &\sim& N\left(\mathcal{R}(\mu),\mathcal{R}(\sigma)\right)\\
   *    Y &\sim& N\left(\mathcal{I}(\mu),\mathcal{I}(\sigma)\right)\\
   * @f}
   */
  template <>
  struct normal_distribution<std::complex<double>>
  {
    /** type of real distribution */
    using double_dist=normal_distribution<double>;
    /** type of result */
    using result_type=std::complex<double>;
    /** type of parameters */
    using param_type=std::pair<double_dist::param_type,
			       double_dist::param_type>;
    /** 
     * Constructor 
     * 
     * @param mu @f$\mu@f$ 
     * @param sigma @f$\sigma@f$
     */
    normal_distribution(double mu, double sigma)
      : _r(mu, sigma),
	_i(mu+1, sigma)
    {
    }
    /** 
     * Constructor 
     * 
     * @param mu @f$\mu@f$ 
     * @param sigma @f$\sigma@f$
     */
    normal_distribution(result_type mu, result_type sigma)
      : _r(std::real(mu), std::real(sigma)),
	_i(std::imag(mu), std::imag(sigma))
    {
    }
    /** 
     * Reset state 
     */
    void reset()
    {
      _r.reset();
      _i.reset();
    }
    /** 
     * Sample 
     *
     * @param g Random number engine 
     *
     * @return The random variable @f$ Z@f$ 
     */
    template <class Generator>
    result_type operator()(Generator& g)
    {
      auto r = _r(g);
      auto i = _i(g);
      return result_type(r,i);
    }
    /** Get the mean (@f$\in\mathbb{C}@f$) */
    result_type mean() const { return result_type(_r.mean(),_i.mean()); }
    /** Get the standard deviation (@f$\in\mathbb{C}@f$) */
    result_type stddev() const { return result_type(_r.stddev(),_i.stddev()); }
    /** Get the parameters */
    param_type param() const {  return make_pair(_r.param(), _i.param()); }
    /** 
     * Set the parameters 
     *
     * @param p Parameters 
     */
    void param(const param_type& p) { _r.param(p.first); _i.param(p.second); }
    /** Get the least possible value (@f$\in\mathbb{C}@f$) */
    result_type min() const { return result_type(_r.min(),_i.min()); }
    /** Get the largest possible value (@f$\in\mathbb{C}@f$) */
    result_type max() const { return result_type(_r.max(),_i.max()); }
    /** Real part random distribution */
    double_dist _r;
    /** Imaginary part random distribution */
    double_dist _i;
  };
}

using namespace stat;

//====================================================================
template <typename T>
bool compareVar(const Stat<T>& ref,
		const Stat<T>& oth,
		double aeps,
		double reps)
{
  auto rm = ref.mean();
  auto om = oth.mean();
  auto rv = ref.variance();
  auto ov = oth.variance();
  bool re = true;
  std::cout << std::scientific << std::setprecision(8);

  for (size_t i = 0; i < rm.size(); i++) {
    bool cm = isclose(rm[i], om[i], aeps, reps);
    bool cv = isclose(rv[i], ov[i], aeps, reps);
    if (cm && cv) continue;
    std::cout << std::setw(3) << i << ' ';
    if (!cm) 
      std::cout << "M "
		<< std::setw(12) << rm[i] << " versus "
		<< std::setw(12) << om[i] << " -> "
		<< std::setw(12) << std::fabs(om[i]-rm[i]) / std::fabs(rm[i])
		<< ' ';
    if (!cv) 
      std::cout << "V "
		<< std::setw(12) << rv[i] << " versus "
		<< std::setw(12) << ov[i] << " -> "
		<< std::setw(12) << std::fabs(ov[i]-rv[i]) / std::fabs(rv[i])	
		<< ' ';
    std::cout << std::endl;
    re = false;
  }
  std::cout << std::defaultfloat << std::setprecision(0);
  return re;
}

//--------------------------------------------------------------------
template <typename T>
typename Cov<T>::ValueVector
transpose(const Cov<T>& c)
{
  auto m = c.covariance();
  for (size_t i = 0; i < c.size(); i++) 
    for (size_t j = i+1; j < c.size(); j++)
      std::swap(m[i*c.size()+j],m[j*c.size()+i]);
  return m;
}

//--------------------------------------------------------------------
template <typename T>
bool compareCov(const Cov<T>& ref,
		const Cov<T>& oth,
		double aeps,
		double reps)
{
  // std::cout << std::scientitic << std::setprecision(8);
  // std::cout << std::defaultfloat << std::setprecision(0);
  auto rm = ref.covariance();
  if (isasym(rm, aeps, reps)) {
    std::cout << "ref asymmetric" << std::endl;
    auto rt  =  transpose(ref);
    pm("rt ", rt);
    auto prd =  rm - rt;
    pm("prd ", prd);
    auto rd  =  std::abs(prd);
    pm("rd ", rd);
    auto rs  =  rd / std::abs(rm);
    pm("ref",rs,15,8);
  }
  else
    std::cout << "ref symmetric" << std::endl;
  auto om = oth.covariance();
  if (isasym(om, aeps, reps)) {
    auto ot =  transpose(oth);
    auto od =  std::abs(om-ot);
    auto os =  od / std::abs(om);
    pm("oth",os,15,8);
  }
  else
    std::cout << "oth symmetric" << std::endl;


  return true;
}

//====================================================================
template <typename T,
	  template <typename> class S>
bool compare(const S<T>& ref, const S<T>& oth,
	     double aeps=1e-12,
	     double reps=1e-15);
//____________________________________________________________________
template <>
bool compare<double,WelfordVar>(const WelfordVar<double>& ref,
				const WelfordVar<double>& oth,
				double aeps,
				double reps)
{
  return compareVar(ref, oth, aeps, reps);
}
//--------------------------------------------------------------------
template <>
bool compare<double,WestVar>(const WestVar<double>& ref,
			     const WestVar<double>& oth,
			     double aeps,
			     double reps)
{
  return compareVar(ref, oth, aeps, reps);
}
//____________________________________________________________________
template <>
bool compare<double,WelfordCov>(const WelfordCov<double>& ref,
				const WelfordCov<double>& oth,
				double aeps,
				double reps)
{
  bool v = compareVar(ref,oth,aeps,reps);
  bool c = compareCov(ref,oth,aeps,reps);
  return v && c;
}
//--------------------------------------------------------------------
template <>
bool compare<double,WestCov>(const WestCov<double>& ref,
			     const WestCov<double>& oth,
			     double aeps,
			     double reps)
{
  bool v = compareVar(ref,oth,aeps,reps);
  bool c = compareCov(ref,oth,aeps,reps);
  return v && c;
}
//____________________________________________________________________
template <>
bool compare<std::complex<double>,
	     WelfordVar>(const WelfordVar<std::complex<double>>& ref,
			 const WelfordVar<std::complex<double>>& oth,
			 double aeps,
			 double reps)
{
  return compareVar(ref, oth, aeps, reps);
}
//--------------------------------------------------------------------
template <>
bool compare<std::complex<double>,
	     WestVar>(const WestVar<std::complex<double>>& ref,
		      const WestVar<std::complex<double>>& oth,
		      double aeps,
		      double reps)
{
  return compareVar(ref, oth, aeps, reps);
}
//____________________________________________________________________
template <>
bool compare<std::complex<double>,
	     WelfordCov>(const WelfordCov<std::complex<double>>& ref,
			 const WelfordCov<std::complex<double>>& oth,
			 double aeps,
			 double reps)
{
  bool v = compareVar(ref,oth,aeps,reps);
  bool c = compareCov(ref,oth,aeps,reps);
  return v && c;
}
//--------------------------------------------------------------------
template <>
bool compare<std::complex<double>,
	     WestCov>(const WestCov<std::complex<double>>& ref,
		      const WestCov<std::complex<double>>& oth,
		      double aeps,
		      double reps)
{
  bool v = compareVar(ref,oth,aeps,reps);
  bool c = compareCov(ref,oth,aeps,reps);
  return v && c;
}

//====================================================================
/** 
 * Print out a statistics object 
 *
 * @param s Object to print 
 */
template <typename T>
void printStat(Stat<T>& s)
{
  std::cout << std::setw(12) << std::setprecision(8) << std::fixed;
  s.print(std::cout, 0x3);
}
/** 
 * Print out a statistics object - complex specialisation
 *
 * @param s Object to print 
 */
template <>
void printStat<std::complex<double>>(Stat<std::complex<double>>& s)
{
  std::cout << std::setw(17) << std::setprecision(4) << std::fixed;
  s.print(std::cout, 0x3);
}

//====================================================================
/** Non-implemented base template */
template <bool weighted,
	  typename T,
	  template <typename> class S> struct TestStat;
//--------------------------------------------------------------------
/** 
 * Basic test of statistics for non-weighted statistics  
 *
 * Here, we draw @f$ N@f$ random numbers @f$ X_i@f$ for each of @f$
 * M@f$ events and update the statstics with the observation
 *
 * @f$ X=[X_1,\ldots,X_N]\quad.@f$ 
 *
 * Here @f$ X\sim N@f$.  In the end we print out the result. 
 *
 * @tparam T Value type 
 * @tparam S Statistics type 
 */
template <typename T,
	  template <typename> class S>
struct TestStat<false,T,S>
{
  /** Type of statistics */
  using Stat=S<T>;
  /** Type of value vector  @f$ X@f$ */
  using ValueVector=typename Stat::ValueVector;
  /** The statistics */
  Stat _s;
  /** 
   * Constructor 
   * 
   * @param n The size of statistics to use 
   */
  TestStat(size_t n,bool) : _s(n) {}
  TestStat(TestStat&& o) : _s(std::move(o._s)) {}
  /** 
   * Run the test 
   *
   * @param nev  Number of events @f$ M@f$ 
   * @param seed Random number seed 
   */
  bool operator()(size_t nev=1000, unsigned int seed=123456)
  {
    std::default_random_engine       eng;
    std::normal_distribution<T>      xdist(0,1);
    ValueVector                      x(_s.size()); 
    
    eng.seed(seed);
    
    size_t         frq = std::max(nev/10,1ul);
    ana::Progress  pm(std::cout,nev);

    for (size_t i = 0; i < nev; i++) {
      if ((i+1) % frq == 0) pm.print(i+1);
      for (auto& xx : x) xx = xdist(eng);
      _s.fill(x);
    }
    pm.end(nev);
    
    json::JSON ojson = _s.toJson();
    std::stringstream s;
    s << std::scientific << std::setprecision(12)
      << ojson << std::endl;
    
    Stat o(0);
    json::JSON ijson;
    s >> ijson;
    o.fromJson(ijson);

    return compare(o, _s);
  }
};

//--------------------------------------------------------------------
/** 
 * Basic test of statistics for weighted statistics  
 *
 * Here, we draw @f$ N@f$ random numbers @f$ X_i@f$ and weights @f$
 * W_i@f$ for each of @f$ M@f$ events and update the statstics with
 * the observation
 *
 * @f$ X=[X_1,\ldots,X_N]\quad W=[W_1,\ldots,W_N]\quad.@f$ 
 *
 * Here @f$ X\sim N@f$ and @f$ W\sim U@f$.  In the end we print out
 * the result.
 */
template <typename T,
	  template <typename> class S>
struct TestStat<true,T,S>
{
  /** Type of the statistics */
  using Stat=S<T>;
  /** Type of value vector @f$ X@f$ */
  using ValueVector=typename Stat::ValueVector;
  /** Type of weight vector @f$ W@f$ */
  using WeightVector=typename Stat::WeightVector;
  /** The statistics */
  Stat _s;
  bool _same;
  /** 
   * Constructor 
   * 
   * @param n The size of statistics to use
   * @param same all component weights equal
   */
  TestStat(size_t n, bool same) : _s(n),_same(same) {}  
  TestStat(TestStat&& o) : _s(std::move(o._s)), _same(o._same) {}
  /** 
   * Run the test 
   *
   * @param nev  Number of events @f$ M@f$ 
   * @param seed Random number seed 
   */
  bool operator()(size_t nev=1000, unsigned int seed=123456)
  {
    std::default_random_engine        eng;
    std::normal_distribution<T>       xdist(0,1);
    std::uniform_real_distribution<>  wdist(.75, 1); 
    ValueVector                       x(_s.size()); 
    WeightVector                      w(_s.size()); 
    
    eng.seed(seed);

    size_t         frq = std::max(nev/10,1ul);
    ana::Progress  pm(std::cout,nev);

    for (size_t i = 0; i < nev; i++) {
      if ((i+1)%frq == 0) pm.print(i+1);
      auto cw = wdist(eng);
      for (auto& xx : x) xx = xdist(eng);
      for (auto& ww : w) ww = _same ? cw : wdist(eng);
      _s.fill(x,w);
    }
    pm.end(nev);

    std::stringstream s;
    s << std::scientific << std::setprecision(12);
    json::JSON j1 = _s.toJson();
    s << j1;

    Stat o(0);
    json::JSON j2;
    s >> j2;
    o.fromJson(j2);
    
    return compare(o, _s);
  }
};

//====================================================================
/** Non-implemented base template */
template <bool weighted,
	  typename T,
	  template <typename> class S> struct TestMerge;
//--------------------------------------------------------------------
/** 
 * Merging test of statistics for non-weighted statistics  
 *
 * Here, we draw @f$ N@f$ random numbers @f$ X_i@f$ for each of @f$
 * M@f$ events and update the statstics with the observation
 *
 * @f$ X=[X_1,\ldots,X_N]\quad.@f$ 
 *
 * Here @f$ X\sim N@f$.  
 *
 * We make @f$ J@f$ sub-samples by randomly selecting one of @f$ J@f$
 * statistics object to update on each event in addition to updating
 * the reference object. In the end we merge the @f$ J@f$ sub-samples
 * and compare to the reference statistics.  
 */
template <typename T,
	  template <typename> class S>
struct TestMerge<false,T,S>
{
  /** Type of statistics */
  using Stat=S<T>;
  /** Type of value vector  @f$ X@f$ */
  using ValueVector=typename Stat::ValueVector;
  /** Vector of stats */
  using StatVector=std::vector<Stat>;
  /** The statistics */
  Stat _s;
  /** 
   * Constructor 
   * 
   * @param n The size of statistics to use
   */
  TestMerge(size_t n, bool) : _s(n) {}
  TestMerge(TestMerge&& o) : _s(std::move(o._s)) {}
  /** 
   * Run the test 
   *
   * @param nev  Number of events @f$ M@f$ 
   * @param nsub Number of sub partitions 
   * @param seed Random number seed 
   */
  bool operator()(size_t nev=1000, size_t nsub=1, unsigned int seed=123456)
  {
    std::default_random_engine        eng;
    std::normal_distribution<T>       xdist(0,1);
    std::poisson_distribution<>       sdist(2);
    ValueVector                       x(_s.size());
    StatVector                        s;
    for (size_t i = 0; i < nsub; i++) s.emplace_back(_s.size());
    
    eng.seed(seed);

    size_t         frq = std::max(nev/10,1ul);
    ana::Progress  pm(std::cout,nev);

    for (size_t i = 0; i < nev; i++) {
      if ((i+1)%frq == 0) pm.print(i+1);
      for (auto& xx : x) xx = xdist(eng);
      _s.fill(x);
      // Random sub sample
      size_t j = sdist(eng) % nsub;
      s[j].fill(x);
    }
    pm.end(nev);
    
    Stat t(_s.size());
    for (auto& si : s) t.merge(si);
	
    std::cout << "Merged over " << nsub << " partitions:" << std::endl;
    for (auto& si : s)
      std::cout << si.count() << std::endl;

    bool ok = compare(t, _s);
    std::cout << (ok ? "OK" : "Bad") << std::endl;

    printStat(_s);
    printStat(t);

    return ok;
  }
};

//--------------------------------------------------------------------
/** 
 * Merging test of statistics for weighted statistics  
 *
 * Here, we draw @f$ N@f$ random numbers @f$ X_i@f$ and weights @f$
 * W_i@f$ for each of @f$ M@f$ events and update the statstics with
 * the observation
 *
 * @f$ X=[X_1,\ldots,X_N]\quad W=[W_1,\ldots,W_N]\quad.@f$ 
 *
 * Here @f$ X\sim N@f$ and @f$ W\sim U@f$.  
 *
 * We make @f$ J@f$ sub-samples by randomly selecting one of @f$ J@f$
 * statistics object to update on each event in addition to updating
 * the reference object. In the end we merge the @f$ J@f$ sub-samples
 * and compare to the reference statistics.  
 */
template <typename T,
	  template <typename> class S>
struct TestMerge<true,T,S>
{
  /** Type of the statistics */
  using Stat=S<T>;
  /** Type of value vector @f$ X@f$ */
  using ValueVector=typename Stat::ValueVector;
  /** Type of weight vector @f$ W@f$ */
  using WeightVector=typename Stat::WeightVector;
  /** Type of vector of statistics */
  using StatVector=std::vector<Stat>;
  /** The statistics */
  Stat _s;
  bool _same;
  /** 
   * Constructor 
   * 
   * @param n The size of statistics to use
   * @param same all component weights equal
   */
  TestMerge(size_t n, bool same) : _s(n), _same(same) {}  
  TestMerge(TestMerge&& o) : _s(std::move(o._s)), _same(o._same) {}
  /** 
   * Run the test 
   *
   * @param nev  Number of events @f$ M@f$ 
   * @param nsub Number of sub partitions 
   * @param seed Random number seed 
   */
  bool operator()(size_t nev=1000, size_t nsub=3, unsigned int seed=123456)
  {
    std::default_random_engine        eng;
    std::normal_distribution<T>       xdist(0,1);
    std::uniform_real_distribution<>  wdist(.75, 1); 
    std::poisson_distribution<>       sdist(2);
    ValueVector                       x(_s.size()); 
    WeightVector                      w(_s.size()); 
    StatVector                        s;
    for (size_t i = 0; i < nsub; i++) s.emplace_back(_s.size());
    
    eng.seed(seed);
    
    size_t         frq = std::max(nev/10,1ul);
    ana::Progress  pm(std::cout,nev);

    for (size_t i = 0; i < nev; i++) {
      if ((i+1) % frq == 0) pm.print(i+1);
      
      auto cw = wdist(eng);
      for (auto& xx : x) xx = xdist(eng);
      for (auto& ww : w) ww = _same ? cw : wdist(eng);
      _s.fill(x,w);
      // Random sub sample
      size_t j = sdist(eng) % nsub;
      s[j].fill(x,w);
    }
    pm.end(nev);
    
    Stat t(_s.size());
    for (auto& si : s) t.merge(si);
    
    std::cout << "Merged over " << nsub << " partitions:" << std::endl;
    for (auto& si : s)
      std::cout << si.count() << std::endl;

    bool ok = compare(t, _s, 0., 1./nev); // /sqrt(nev));
    std::cout << (ok ? "OK" : "Bad") << " to " << 1./nev << std::endl;

    printStat(_s);
    printStat(t);
    // for (auto& si : s) printStat(si);

    return ok;
  }
};

#endif

  
  
  
