#include <correlations/progs/opt.hh>
#include "testStat.hh"
#include <correlations/stat/WelfordVar.hh>
#include <correlations/stat/WelfordCov.hh>
#include <correlations/stat/WestVar.hh>
#include <correlations/stat/WestCov.hh>

//====================================================================
/** 
 * Generate a test of statistics 
 *
 * @tparam weighted If the statistics is weighted 
 * @tparam T        Value type 
 * @tparam S        Statistics type 
 *
 * @param nvar Size of samples 
 * @param nev  Number of events 
 * @param seed Random seed 
 *
 * @return Tester of statistics 
 */
template <bool weighted, typename T, template <typename> class S>
bool basic(size_t nvar, bool same, size_t nev, unsigned int seed)
{
  std::cout << "Same is " << same << std::endl;
  auto t = TestStat<weighted,T,S>(nvar,same);
  return t(nev, seed);
}

//--------------------------------------------------------------------
/** 
 * Generate a test of statistics 
 *
 * @tparam weighted If the statistics is weighted 
 * @tparam T        Value type 
 * @tparam S        Statistics type 
 *
 * @param nvar Size of samples
 * @param nsub Number of sub-samples
 * @param nev  Number of events
 * @param seed Random number seed 
 *
 * @return Tester of statistics 
 */
template <bool weighted, typename T, template <typename> class S>
bool merge(size_t nvar, bool same, size_t nsub, size_t nev, unsigned int seed)
{
  auto t = TestMerge<weighted,T,S>(nvar,same);
  return t(nev, nsub, seed);
}

//====================================================================
template <bool weighted,  typename T, template <typename> class S>
bool calc(size_t nvar, bool same, size_t nsub, size_t nev, unsigned seed)
{
  if (nsub>0) return merge<weighted,T,S>(nvar,same,nsub,nev,seed);
  else        return basic<weighted,T,S>(nvar,same,     nev,seed);
}

//--------------------------------------------------------------------
template<typename T>
bool weights(bool whts,bool unt, bool cov,
	     size_t nvar, bool same, size_t nsub, size_t nev, unsigned seed)
{
  if (whts) {
    if (unt) {
      if (cov) return calc<false, T, stat::WestCov>(nvar,same,nsub,nev,seed);
      else     return calc<false, T, stat::WestVar>(nvar,same,nsub,nev,seed);
    }
    else {
      if (cov) return calc<true, T, stat::WestCov>(nvar,same,nsub,nev,seed);
      else     return calc<true, T, stat::WestVar>(nvar,same,nsub,nev,seed);
    }
  }
  else {
    if (cov) return calc<false, T, stat::WelfordCov>(nvar,same,nsub,nev,seed);
    else     return calc<false, T, stat::WelfordVar>(nvar,same,nsub,nev,seed);
  }
}

//--------------------------------------------------------------------
bool type(bool cmplx, bool wht, bool unt, bool cov,
	  size_t nvar, bool same, size_t nsub, size_t nev, unsigned seed)
{
  if (cmplx)
    return weights<std::complex<double>>(wht,unt,cov,nvar,same,nsub,nev,seed);
  else
    return weights<double>              (wht,unt,cov,nvar,same,nsub,nev,seed);
}

template <typename T> using Option=exa::Option<T>;
using exa::CommandLine;


//====================================================================
int main(int argc, char** argv)
{
  Option<size_t>       nev('n',"Number of events",     1000, "NEV");
  Option<size_t>       nvr('v',"Number of variables",  3,    "NVAR");
  Option<size_t>       nsb('p',"Number of partitions", 0,    "NPART");
  Option<bool>         cov('c',"Compute covariance",   false);
  Option<bool>         wht('w',"Use weights",          false);
  Option<bool>         unt('u',"Use unit weights",     false);
  Option<bool>         sam('k',"Same weight",          false);
  Option<bool>         cmp('z',"Complex values",       false);
  Option<unsigned int> sed('s',"Random number seed",   123456,"SEED");
  CommandLine& cl = CommandLine::instance();
  
  if (!cl.process(argc, argv)) 
    return 0;

  cl.print(std::cout);

  return type(cmp,wht,unt,cov,nvr,sam,nsb,nev,sed) ? 0 : 1;
}
//
// EOF
//

  
  
