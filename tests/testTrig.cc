#include <iostream>
#include <iomanip>
#include <cmath>
namespace  fast
{
  // https://stackoverflow.com/questions/18662261/fastest-implementation-of-sine-cosine-and-square-root-in-c-doesnt-need-to-b#answer-28050328
  template<typename T>
  inline T cos(T x) noexcept
  {
    constexpr T tp = 1./(2.*M_PI);
    x *= tp;
    x -= T(.25) + std::floor(x + T(.25));
    x *= T(16.) * (std::abs(x) - T(.5));
    x += T(.225) * x * (std::abs(x) - T(1.));
    return x;
  }
  template <typename T>
  inline T sin(T x) noexcept
  {
    constexpr T of = M_PI / 2.;
    return cos(x-of);
  }
}

template <typename T>
void run()
{
  std::cout << std::scientific << std::setprecision(12);
  
  constexpr T p = 2. * M_PI;
  constexpr T d = p / 100.;
  constexpr size_t w = 18;
  for (T a = 0; a <= p; a += d) 
    std::cout << std::setw(w) << a << ' '
	      << std::setw(w) << std ::cos(a) << ' '
	      << std::setw(w) << std ::sin(a) << ' '
	      << std::setw(w) << fast::cos(a) << ' '
	      << std::setw(w) << fast::sin(a) << std::endl;
}

int main()
{
  run<double>();

  return 0;
}
