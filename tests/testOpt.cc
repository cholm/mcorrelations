#include <correlations/progs/opt.hh>

using exa::CommandLine;
template <typename T> using Option=exa::Option<T>;

int
main(int argc, char** argv)
{

  using RealVector=std::vector<double>;

  Option<bool>        b('b', "A booelan", false);
  Option<int>         i('i', "An integer", 0,    "NUMBER");
  Option<float>       f('f', "A float",    0.,   "NUMBER");
  Option<double>      d('d', "A double",   0.,   "NUMBER");
  Option<std::string> s('s', "A string",   "",   "STRING");
  Option<RealVector>  v('v', "A vector",   {0,}, "NUMBER[,...,...]");

  CommandLine& cl = CommandLine::instance();
  if (!cl.process(argc, argv)) return 0;

  cl.print(std::cout);
  
  return 0;
}
// Local Variables:
//    compile-command: "make -C ../ testOpt"
// End:


  
