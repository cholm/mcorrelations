#include <valarray>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <sstream>

template <typename Iterator>
void print(const Iterator first, const Iterator last,
	   const std::string& ind="",bool end=true)
{
  auto cur = first;
  std::cout << ind << "[";
  while (cur != last) {
    std::cout << (cur == first ? "" : ", ") << *cur;
    ++cur;
  }
  std::cout << "]";
  if (end) 
    std::cout << std::endl;
  else
    std::cout << std::flush;
}
  
/**
 * Return the next combination of @a k element from the range
 * [@a first, @a last).
 *
 * @param first  Start of the range
 * @param k      Number of elements to get
 * @param last   End of range
 *
 * @return true if there are more (unique) combinations
 */
template <typename Iterator>
bool nextCombination(const Iterator first,
		     Iterator       k,
		     const Iterator last)
{
  /* Credits: Mark Nelson http://marknelson.us 
   * http://www.dogma.net/markn/articles/Permutations/
   */
  if ((first == last) /* No range? */
      || (first == k) /* Nothing to select? */
      || (last == k)  /* Nothing to select? */
      || (last == first+1) /* No more to select from */)
    return false;

  // Point at the end of our selection
  Iterator i1 = k;
  // Point to the second to last
  Iterator i2 = last;
  --i2;

  // Then loop backward to the start
  while (first != i1) {
    if (*--i1 < *i2) {
      // If the value at marker is less than the last entry,
      // then point to our division
      Iterator j = k;

      // and if it's not smaller than current value, step into
      // unused territory
      while (!(*i1 < *j)) ++j;

      // Swap the greater value with our current object
      std::iter_swap(i1,j);
      // Increment one
      ++i1;
      ++j;

      // and go back to the divider
      i2 = k;

      // Rotate elements in the range i1->j, and put them at the end
      std::rotate(i1,j,last);

      // Then, while we haven't found our greater value,
      // increment into unused territory
      while (last != j) {
	++j;
	++i2;
      }

      // And rotate
      std::rotate(k,i2,last);
      return true;
    }
  }
  // And rotate
  std::rotate(first,k,last);
  return false;
}

using Real=double;
using Power=size_t;
using Size=size_t;
using SizeVector=std::valarray<Size>;

void testNC(Size k=2, Size n=4)
{
  SizeVector a(n);
  std::iota(std::begin(a),std::end(a),0);
  std::cout << "Test of combinations of size " << k << " of " << n
	    << std::endl;
  do 
    print(std::begin(a),std::end(a));
  while (nextCombination(std::begin(a),std::begin(a)+k,std::begin(a)+n-1));
}

std::string test(Size m, SizeVector n, const std::string& ind="")
{
  if (m==0) return "";
  if (m==1)
    return ind+(n[0]==0 ? "p" : "r")+"[h_"+std::to_string(n[0])+",1]";

  std::stringstream str;
  Power p = 1;
  Real  f = 1;
  Real  s = 1;
  for (Size i = m; i > 0; i--) {
    SizeVector nn(n);
    std::sort(std::begin(n), std::begin(n)+m-1);
	  
    Size k =  i-1;
    do {
      std::string t = test(k, nn, ind+" ");

      if (!str.str().empty())
	str << " + ";

      str << ind;
      if (!t.empty()) 
	str << "(\n" << t << "\n" << ind << ") * ";

      str << (s*f) << " * "
	  << (nn[k] == 0 ? (p == 1 ? "p" : "q") : "r") << "[";

      for (auto i = std::begin(nn)+k; i != std::begin(nn)+m; ++i)
	str << (i == std::begin(nn)+k ? "" : "+")
	    << "h_" << *i;
      str << "," << p << "]";

    } while (nextCombination(std::begin(nn),
			     std::begin(nn)+k,
			     std::begin(nn)+m-1));
    f       *= (m-k);
    s       *= -1;
    p++;
  }
  return str.str();
}

int
main(int argc, char** argv)
{
  Size m = 4;
  if (argc > 1) m = std::stoi(argv[1]);
  
  SizeVector n(m);
  std::iota(std::begin(n),std::end(n),0);
  std::cout << test(m,n) << std::endl;

  // combs(n);
  // testNC(0);
  // testNC(1);
  // testNC(2);
  // testNC(3);

  // testNC(1,5);
  // testNC(2,5);
  // testNC(3,5);
  // testNC(4,5);
  
  return 0;
}

    
