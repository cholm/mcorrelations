#include <correlations/QVector.hh>
#include <correlations/gen/Generator.hh>
#include <correlations/ana/Analyzer.hh>
#include <correlations/progs/opt.hh>
#include <correlations/stat/WelfordVar.hh>
#include <iomanip>
#include <numeric>
#include <cassert>
#include <chrono>

using correlations::QVector;
using correlations::HarmonicVector;
using correlations::RealVector;
using correlations::Complex;
using correlations::Real;
using ana::Analyzer;
using gen::Generator;

// template <typename T> using makeQ=correlations::makeQ<T>;

template <typename T> using Option=exa::Option<T>;
using exa::CommandLine;

struct Summer : public Analyzer
{
  Summer() : Analyzer(),
	     _h({-2,-2,2,2}),
	     _q1(correlations::makeQ(_h,true)),
	     _q2(correlations::makeQ(_h,true)),
	     _t(2)
  {}
  struct Timer
  {
    Timer() : _start(std::chrono::system_clock::now()) {}
    double count()
    {
      auto end = std::chrono::system_clock::now();
      std::chrono::duration<double> elap = end-_start; // seconds
      return elap.count();
    }
    std::chrono::time_point<std::chrono::system_clock> _start;
  };
      
  bool accept(Real) { return true; }
  void event(const ParticleVector& p)
  {
    stat::WelfordVar<>::RealVector t(_t.size());

    {
      Timer tt;
      for (auto& pp : p) {
	Real phi     = std::get<2>(pp);
	Real w       = std::get<3>(pp);
	_q1.fill(phi,w);
      }
      t[0] = tt.count();
    }

    {
      Timer tt;
      auto vecs    = Generator::toVectors(p);
      auto phis    = std::get<2>(vecs);
      auto weights = std::get<3>(vecs);
      _q2.fill(phis, weights);
      t[1] = tt.count();
    }
    _t.fill(t);
  }
  json::JSON end(bool)
  {
    _q1.print();
    _q2.print();

    std::cout << "Q-vectors 1 and 2 are" << (_q1 == _q2 ? "" : " NOT")
	      << " equal" << std::endl;

    auto oldf = std::cout.flags();
    size_t ws = 12;
    for (size_t i = 0; i < _t.size(); i++) {
      Real    t  = _t.mean()[i];
      Real    et = _t.sem()[i];
      std::cout << std::scientific
		<< i << ": " 
		<< std::setw(ws)      << t << " +/- "	
		<< std::setw(ws)      << et
		<< std::endl;
    }
    std::cout.setf(oldf);

    return json::JSON();
  }
  HarmonicVector _h;
  QVector _q1;
  QVector _q2;
  /** Timing (stores means) */
  stat::WelfordVar<> _t;
};

void run_fixed()
{
  Generator::ParticleVector p;
  p.push_back(std::make_tuple(0,0,M_PI/4,2));
  p.push_back(std::make_tuple(0,0,M_PI/3,2));
  
  Summer s;
  s.event(p);
  s.end(false);
}
  
void run_file(const std::string& filen)
{
  Summer s;
  s.run(filen);
}  

int
main(int argc, char** argv)
{
  Option<std::string> filen('i',"Input filename", "", "FILE");

  CommandLine& cl = CommandLine::instance();
  if (!cl.process(argc, argv)) return 0;
  

  run_fixed();
  try {
    run_file(filen);
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  
  return 0;
}
