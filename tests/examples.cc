#include <json.hh>
#include <iostream>
#include <sstream>
#include <valarray>
#include <vector>

/** Basic examples */
void basic()
{
  std::cout << "=== Basic stuff ===" << std::endl;
  // Create one of each type
  json::JSON null;
  json::JSON b(true);
  json::JSON s("RawString");
  json::JSON s2(std::string("C++String"));
  json::JSON i(42);
  json::JSON f(3.14);
  json::JSON a = json::Array();
  json::JSON o = json::Object();

  // Change types
  b = false;
  b = "rtew";
  b = 1;
  b = 1.1;
  b = std::string("asd");

  // Append to arrays
  a.append(1);
  a.append("test");
  a.append(false);

  // Access elements of an array
  auto& a0 = a[0];
  std::cout << a0 << std::endl;

  // Initialize array
  json::JSON a2 = json::Array(2, "Test", true);

  std::valarray<double> va = {0., 1., 2.,};
  json::JSON a3 = json::Array(va);
  std::cout << a2 << " and " << a3 << std::endl;

  // Object assigment
  o["key1"] = 1.;
  o["key2"] = "value";

  json::JSON o2 = json::Object();
  o2["key3"] = 1;
  o2["key4"] = a;
  o2["key5"] = a2;

  // Nested objects
  o["key6"] = o2;

  // Print object
  std::cout << o << std::endl;
}

/** As type */
void to()
{
  std::cout << "=== as types ===" << std::endl;
  json::JSON o;
  o = true;          std::cout << o.toBool()   << std::endl;
  o = "Test string"; std::cout << o.toString() << std::endl;
  o = 2.2;           std::cout << o.toFloat()  << std::endl;
  o = 3;             std::cout << o.toInt()    << std::endl;
}
  
/** Example of initializsation */
void init()
{
  std::cout << "=== Fancy initialisation ===" << std::endl;
  // Assigning using JSON-like syntax
  json::JSON o = {"key1", "value",
		  "key2", true,
		  "key3", { "key4", json::Array("This","is","an","array"),
			    "key5", { "boolean", true } } };

  std::cout << o << std::endl;
} // end_init

/** Example of iteration */
void iter()
{
  std::cout << "=== Iteration ===" << std::endl;
  json::JSON a = json::Array();
  json::JSON o = json::Object();

  for (size_t i = 0; i < 4; i++)
    a[i] = "Test"+std::to_string(i);

  o["key0"] = "value1";
  o["key1"] = a;
  o["key2"] = 123;

  std::cout << "Array: " << std::endl;
  for (auto& e : a.arrayRange())
    std::cout << e << std::endl;

  std::cout << "Object: "  << std::endl;
  for (auto& kv : o.objectRange())
    std::cout << "o[" << kv.first << "] = " << kv.second << std::endl;
}

void array()
{
  std::cout << "=== Arrays ===" << std::endl;

  json::JSON a;
  a[2] = "Test2"; std::cout << a << std::endl; 
  a[1] = "Test1"; std::cout << a << std::endl; 
  a[0] = "Test0"; std::cout << a << std::endl; 
  a[3] = "Test3"; std::cout << a << std::endl;

  json::JSON a2;

  a2[2][0][1] = true; std::cout << a2 << std::endl;
}

void load()
{
  std::cout << "=== Loading ===" << std::endl;
  std::string s("{\"key1\":\"string\","
		" \"key2\": true,"
		" \"key3\":     1234,"
		" \"key4\":null}");

  std::stringstream str(s);

  json::JSON j;
  str >> j;
  std::cout << j << std::endl;
  
  json::JSON j2 = json::JSON::load(s);
  std::cout << j2 << std::endl;
} // end_load

void nonfinite()
{
  std::cout << "=== Non-finite floats ===" << std::endl;
  json::JSON jnan = NAN;
  json::JSON jinf = INFINITY;

  std::cout << jnan << "\t" << jinf << std::endl;

  std::stringstream snan;  snan << jnan;
  std::stringstream sinf;  sinf << jinf;

  json::JSON jnan2;
  json::JSON jinf2;
  snan >> jnan2;
  sinf >> jinf2;
  std::cout << jnan2 << "\t" << jinf2 << std::endl;

  snan << jnan;
  sinf << jinf;
  try { jnan2.nonfinite_is_null = true; snan >> jnan2; }
  catch (std::exception& e) {
    std::cout << "Reading nan resulted in exception "
	      << e.what() << std::endl;
  }
  
  try { jinf2.nonfinite_is_null = true; sinf >> jinf2; }
  catch (std::exception& e) {
    std::cout << "Reading inf resulted in exception "
	      << e.what() << std::endl;
  }

  jinf.nonfinite_is_null = true;
  jnan.nonfinite_is_null = true;
  std::cout << jnan << "\t" << jinf << std::endl;
  
  snan << jnan;
  sinf << jinf;
  snan >> jnan2;
  sinf >> jinf2;
  std::cout << jnan2 << "\t" << jinf2 << std::endl;
}
  
void varray()
{
  std::cout << "=== More arrays ===" << std::endl;
  std::vector<double> v = {.1, .2, .3, .4 };

  json::JSON a;
  a = v;
  std::cout << a << std::endl;

  json::JSON a2 = v;
  std::cout << a2 << std::endl;

  json::JSON o = { "key1", "value",
		   "key2", a };
  std::cout << o << std::endl;

  std::valarray<double> v2 = {1.1, 1.2, 1.3, 1.4 };
  json::JSON o2;
  o2 = v2;
  std::cout << o2 << std::endl;

  std::valarray<double> v3;
  o2.toArray(v3);
  for (auto& x : v3) std::cout << x << ",";
  std::cout << std::endl;
  
}

void mobject()
{
  json::JSON o;
  std::map<std::string,int> m = {{"key1", 1}, {"key2", 2}};
  o = m; // {{"key1", 1}, {"key2", 2}};

  std::cout << o << std::endl;
}

    
int main()
{
  basic();
  to();
  init();
  iter();
  array();
  load();
  varray();
  mobject();
  nonfinite();
    
  return 0;
}

    
    
    
  
