#include <random>
#include <fstream>
#include <correlations/stat/Stat.hh>
#include <correlations/stat/Estimator.hh>
#include <correlations/stat/Jackknife.hh>
#include <correlations/stat/Bootstrap.hh>
#include <correlations/stat/Derivatives.hh>
#include <correlations/progs/opt.hh>

template <typename T>
using Option=exa::Option<T>;
using exa::CommandLine;

namespace std
{
  template <>
  struct normal_distribution<std::complex<double>>
  {
    using double_dist=normal_distribution<double>;
    using result_type=std::complex<double>;
    using param_type=std::pair<double_dist::param_type,
			       double_dist::param_type>;
    
    normal_distribution(double mu, double sigma)
      : _r(mu, sigma),
	_i(mu+1, sigma)
    {
    }
    void reset()
    {
      _r.reset();
      _i.reset();
    }
    template <class Generator>
    result_type operator()(Generator& g)
    {
      auto r = _r(g);
      auto i = _i(g);
      return result_type(r,i);
    }
    result_type mean() const { return result_type(_r.mean(),_i.mean()); }
    result_type stddev() const { return result_type(_r.stddev(),_i.stddev()); }
    param_type param() const {  return make_pair(_r.param(), _i.param()); }
    void param(const param_type& p) { _r.param(p.first); _i.param(p.second); }
    result_type min() const { return result_type(_r.min(),_i.min()); }
    result_type max() const { return result_type(_r.max(),_i.max()); }

    double_dist _r;
    double_dist _i;
  };
}

template <typename T> 
struct Test : public T
{
  using Base=T;
  using Value=typename Base::Value;
  using ValueVector=typename Base::ValueVector;
  
  Test() : Base(3) {}

  Value value(const ValueVector& means) const
  {
    auto a = means[0];
    auto b = means[1];
    auto c = means[2];

    return a+b*b+c*c*c;
  }
  ValueVector derivatives(const ValueVector& means) const
  {
    // auto a = means[0];
    auto b = means[1];
    auto c = means[2];
    return { 1, 2.*b, 3.*c*c };
  }
};

template <typename T>
struct Generator
{
  using Value=typename stat::Stat<T>::Value;
  using ValueVector=typename stat::Stat<T>::ValueVector;
  using WeightVector=typename stat::Stat<T>::WeightVector;

  Generator(unsigned int seed)
    : _eng(),
      _xdist(0.,1.),
      _wdist(.75,1.)
  {
    _eng.seed(seed);
  }
  void operator()(ValueVector& x, WeightVector& w)
  {
    x = 0;
    x[0] = _xdist(_eng) + 1.;
    x[1] = _xdist(_eng) / 10.;
    x[2] = _xdist(_eng) / 10. + x[0];
    for (auto& ww : w) ww = _wdist(_eng);
  }
  void run(const std::string& fname, size_t nev)
  {
    std::ofstream out(fname);
    ValueVector  x(3);
    WeightVector w(3);
    
    for (size_t i = 0; i < nev; i++) {
      this->operator()(x,w);
      
      for (auto& xx : x) out << xx << "\t";
      for (auto& ww : w) out << ww << "\t";
      out << "\n";
    }
    out.close();
  }	   
  std::default_random_engine       _eng;
  std::normal_distribution<Value>  _xdist;
  std::uniform_real_distribution<> _wdist;
};

template <typename T>
void testEstimatorIO(const Test<T>& t)
{
  json::JSON j1 = t.toJson();
  std::stringstream s;
  s << j1;
  
  json::JSON j2;
  s >> j2;

  Test<T> t2;
  t2.fromJson(j2);
}

template <typename T>
typename T::Result
testEstimator(size_t nev=1000, unsigned int seed=123456)
{
  using Value=typename Test<T>::Value;
  using ValueVector=typename Test<T>::ValueVector;
  using WeightVector=typename Test<T>::WeightVector;
  
  Test<T>           test;
  Generator<Value>  gen(seed);
  ValueVector       x(test.size()); 
  WeightVector      w(test.size()); 

  for (size_t i = 0; i < nev; i++) {
    gen(x,w);
    test.fill(x,w);
  }

  testEstimatorIO(test);
  
  return test.eval();
}

template <typename T>
typename T::Result
testEstimator(const std::string& fname, unsigned int seed=123456)
{
  using ValueVector=typename Test<T>::ValueVector;
  using WeightVector=typename Test<T>::WeightVector;

  std::default_random_engine eng;
  eng.seed(seed);
  
  Test<T>       test;
  ValueVector   x(test.size()); 
  WeightVector  w(test.size()); 

  std::ifstream in(fname);
  while (!in.eof()) {
    for (auto& xx : x) in >> xx;
    for (auto& ww : w) in >> ww;
    test.fill(x,w);
  }

  testEstimatorIO(test);
  
  return test.eval();
}

  

template <typename T>
void printRes(const std::string& what,
	      const std::pair<T,T>& r)
{
  std::cout << std::setprecision(4)
	    << std::setw(12) << what << ": "
	    << std::setw(8)  << r.first << " +/- "
	    << std::setw(8)  << r.second << std::endl;
}

template <typename T>
void run(const std::string& in,
	 const std::string& out,
	 unsigned int       nev,
	 unsigned int       seed)
{
  if (!out.empty()) {
    Generator<T> gen(seed);
    gen.run(out,nev);
  }
  if (!in.empty()) {
    printRes("Jackknife",  testEstimator<stat::Jackknife<T>>  (in,seed));
    printRes("Bootstrap",  testEstimator<stat::Bootstrap<T>>  (in,seed));
    printRes("Derivatives",testEstimator<stat::Derivatives<T>>(in,seed));
    return;
  }
  printRes("Jackknife",  testEstimator<stat::Jackknife<T>>  (nev,seed));
  printRes("Bootstrap",  testEstimator<stat::Bootstrap<T>>  (nev,seed));
  printRes("Derivatives",testEstimator<stat::Derivatives<T>>(nev,seed));
}

int
main(int argc, char** argv)
{
  Option<std::string>  in('i', "Input file",         "",     "FILE");
  Option<std::string>  out('o',"Output file",        "",     "FILE");
  Option<bool>         cpl('c',"Use complex numbers",false);
  Option<unsigned int> nev('n',"Number of events",   1000,   "NUMBER");
  Option<unsigned int> sdd('s',"Random number seed", 123456, "NUMBER");
  CommandLine&         cl = CommandLine::instance();
  if (!cl.process(argc, argv)) return 0;

  cl.print(std::cout,"");
  
  if (cpl)
    run<std::complex<double>>(in,out,nev,sdd);
  else
    run<double>              (in,out,nev,sdd);
  
  return 0;
}
  
  

    
