import numpy as np
import matplotlib.pyplot as plt

plt.ion()

dat = np.genfromtxt('testTrig.dat')
lbl = ['std::cos', 'std::sin', 'fast::cos', 'fast::sin']

fig, ax = plt.subplots(nrows=2,sharex=True,gridspec_kw={'hspace':0})
for i, l in enumerate(lbl):
    x = dat[:,0]
    y = dat[:,i+1]
    ax[0].plot(x,y,label=l)

    if i < 2: continue

    yr = dat[:,i+1-2]
    d  = y-yr
    rd = d[yr!=0] / yr[yr!=0]
    ax[1].plot(x[yr!=0],rd,label=l)

ax[0].legend()
ax[1].legend()
ax[1].set_xlabel(r'$\varphi$')
ax[0].set_ylabel(r'$\cos\varphi, \sin\varphi$')
ax[1].set_ylabel(r'$\Delta$')


