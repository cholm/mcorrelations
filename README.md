# Generic framework for anisotropic flow analyses with multi-particle azimuthal correlations

This is the reference implementation of the _Generic framework for
anisotropic flow analyses with multi-particle azimuthal
correlations_. 

This code calculates the generic multi-particle correlator

```math
C_{\mathbf{h}}\{n\} = \left\langle \exp\left[i\left(\sum_j^n h_j \varphi_j\right)\right]\right\rangle
```

where the vector $`\mathbf{h}=[h_1,\ldots,h_n]`$ is the harmonics of
each particle, and $`\varphi_j`$ is the observations of the azimuth
angle.  The code allows for both calculations of both the _integrated_
and _differential_ correlators, $`C_{\mathbf{h}}\{n\}`$ and
$`C_{\mathbf{h}}'\{n\}`$ respectively. 

See also [arXiv:1312.3572][arx] or [_Phys.Rev._*C89*(2014)064904][prc].

## Python implementation 

An implementation in Python is
[available](https://gitlab.com/cholmcc/pycorrelations).   Note, this -
the C++ implementation - is roughly 10 times faster than the Python
implementation. 

## Topics

- [Overview](doc/Overview.md)
- [Anisotropic Azimuthal Flow Measurements](doc/GF.md)
- [Practical use of the Generic Framework for Flow](doc/Flow.md)
- [Sub-events and pseudorapidity gap](doc/Sub.md)
- [Application Programming Interface (API)](https://cern.ch/cholm/mcorrelations)
- [Methods of computing correlators](doc/Methods.md)
- [Tests of the code](doc/Tests.md)
- [Symbolic expansion of cumulants](doc/Symbolic.md)
- [Download](#_Downloading)
- [Running tests and building documentation](#_Running)

## Downloading

The code is hosted on the [CERN Gitlab instance][git]. To check out,
do

    git clone https://gitlab.cern.ch/cholm/mcorrelations.git

Alternatively download [here][dnl] and extract. 

## Output 

The code generally outputs results as [JSON](https://json.org).  If
you need a small JSON library for C++, see
[here](https://gitlab.com/cholmcc/simplejson) (included in this
project). 

## Running tests and building documentation

All code of the project is contained in declaration (header) files.
That is, all code is 'inline'.  That means that there is no need to
compile a library or the like first.

The project does define a set of examples and tests that can be
build. To do that, go to the unpacked sources and do 

    make

To run tests, do

    make tests

To make the documentation (requires Doxygen and Python), do

    make doc

More information on the tests can be found in a [separate page](doc/Tests.md)

## Example programs 

The example programs [

## Compatibility

The code it self is portable as it is, and only uses ISO C++11.
However, the code and in particular the build system captured in the
top-level Makefile have only been tested on GNU/Linux with relatively
new versions of GCC.  To build the tests on other platforms it may
require a bit of tweaking on the users behalf.

## Copyright


Copyright (c) 2013, Kristjan Gulbrandsen, Ante Bilandzic, Christian
Holm Christensen

## License

GNU General Public License version 3 - see [COPYING](COPYING)

[arx]: http://arxiv.org/abs/1312.3572 
[prc]: https://journals.aps.org/prc/abstract/10.1103/PhysRevC.89.064904
[dox]: https://cern.ch/cholm/mcorrelations
[dnl]: http://www.cern.ch/cholm/mcorrelations/correlations-master.tar.gz
[git]: https://gitlab.cern.ch/cholm/mcorrelations.git

