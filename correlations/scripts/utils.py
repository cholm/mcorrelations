# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --- Load data ------------------------------------------------------
def load(file):
    """Load header and result from JSON file"""
    from json import load as jload
    # from correlations.gen import Header

    d = jload(file)
    p = file.name.split('_')
    i = {'generator': None if len(p) < 4 else p[0],
         'sampler':   None if len(p) < 4 else p[1],
         'analysis':  None if len(p) < 4 else p[3] }
    i.update({dk:dv for dk,dv in d.items() if dk not in ['header','result']})

    return d['header'], d['results'], i

# --- Title ---------------------------------------------------------- 
def gen_title(i,cut=None):
    st = {'phi': r'(\varphi,w)',
          'pt':  r'(p_{\mathrm{T}},\varphi,w)',
          'eta': r'(\eta,p_{\mathrm{T}},\varphi,w)',
          'et0': r'(\eta,\varphi,w)'   }
    gt = {'gen': 'Plain',
          'jet': 'Jet-like',
          'mom': 'Momentum-conserving' }

    gn = i.get("generator",None)
    sn = i.get("sampler",  None)
    cn = '' if cut is None else fr' $(\delta v/v < {100*cut:.0f}\%)$'
    if gn is None or sn is None:
        return ''
    
    return f'{gt[gn]} generator, sampling ${st[sn]}${cn}'

def annotate(ax,x,y,info,horiz=False):
    def mv(x,y):
        if horiz:
            x += .2
        else:
            y -= .06

        return x,y

    s = [info[k] for k in ['policy','correlator','corelator']
         if k in info]
    if horiz:
        s = ','.join(s)
    else:
        s = '\n'.join(s)
        
    ax.annotate(s,(x,y),xycoords='axes fraction')
        
        
# --- Get color for m ------------------------------------------------
def mcolor(m):
    cm    = {2*k: f'C{k-1}' for k in range(1,11) }
    cm[0] = 'k'
    
    return cm[m % 20]

# --- Get color for n ------------------------------------------------
def ncolor(n):
    return f'C{n-1 % 10}'

# --- Get marker style for given m -----------------------------------
def mmarker(m,cl):
    cm = { 2: 'o',
           4: 'o',
           6: 's',
           8: 's' }
    return {'marker': cm[m],
            'markerfacecolor': cl  if (m // 2) % 2 == 0 else 'none' }

# --- Get keywords for given n,m -------------------------------------
def nmkw(n,m):
    cl = mcolor(m) if n == 0 else ncolor(n)
    d  = {'color': cl,
          'label': fr'$m={int(m)}$' }

    
    if m != 0:
        d['ecolor']    = cl
        d['linestyle'] = 'none'
        d.update(mmarker(m,cl))
    if m == 0:
        d['ls']    = '--'
        d['label'] = 'Input'

    return d 

# --- Plot integrated flow for particular m --------------------------
def plot_vn(m,ns,vs,us=None,o=None,ax=None,marker=None,**kwargs):
    from matplotlib.lines import Line2D
    kw          = kwargs.copy()
    kw.update(nmkw(0,m))
    if marker is not None:
        kw['marker'] = marker
        
    if m == 0:
        ax.hlines(vs,ns-.5,ns+.5,**kw)
        return Line2D([],[],**kw)

    if 'lw'     in kw: del kw['lw']

    ax.errorbar(ns+o,vs,2*us,lw=1,**kw)
    ax.errorbar(ns+o,vs,us,  lw=3,**kw)

    if 'ecolor'     in kw: del kw['ecolor']
    if 'elinestyle' in kw: del kw['elinestyle']
    
    return Line2D([],[],ls='none',**kw)

# --- Get axes -------------------------------------------------------
def get_axes_with_ratio(ax):
    from matplotlib.pyplot import subplots

    if ax is None:
        return subplots(nrows=2,sharex=True,
                        gridspec_kw={'hspace':0,
                                     'height_ratios':(2,1)})

    return ax[0].figure, ax
                          
                           
# --- Plot differential flow for particular m ------------------------
def plot_vpn(m,vs,x=None,x_coef=None,w=None,ax=None,cut=.5,f=None,**kwargs):
    from matplotlib.lines import Line2D
    from numpy import asarray, arange,newaxis, linspace, unique, where, \
        logical_and

    fig, ax = get_axes_with_ratio(ax)
    l       = {}
    x       = asarray(x)
    
    if m == 0 and x_coef is not None:
        xm = (x[1:]+x[:-1])/2 
        f  = asarray(vs)[:,newaxis] * \
            (asarray(x_coef)
             * asarray(xm)[:,newaxis]**arange(len(x_coef))).sum(axis=1)
        x = linspace(x[0],x[-1],100)
        y = asarray(vs)[:,newaxis] * \
            (asarray(x_coef)
             * asarray(x)[:,newaxis]**arange(len(x_coef))).sum(axis=1)

        for i,yy in enumerate(y):
            kw = kwargs.copy()
            kw.update(nmkw(i+1,m))
            ax[0].plot(x,yy,**kw)

            l[i+1] = Line2D([],[],**kw)

        return l,f

    ns = unique(vs[:,0]).astype(int)
    for n in ns:
        kw = kwargs.copy()
        kw.update(nmkw(n,m))
        y  = vs[vs[:,0]==n][:,1]
        e  = vs[vs[:,0]==n][:,2]
        ee = e[y>0]
        yy = y[y>0]
        xx = x[y>0]
        ww = w[y>0]
        rr = logical_and(ee / yy < cut, yy < 10)
        xx = xx[rr]
        yy = yy[rr]
        ee = ee[rr]
        ww = ww[rr]
        ax[0].errorbar(xx,yy,ee,ww/2,**kw)

        if 'ecolor' in kw: del kw['ecolor']
        if n-1 < len(f):
            ff = f[n-1][y>0][rr]
            rf = (yy-ff)/ff
            ef = ee/ff
            ax[1].fill_between(xx,rf-ef,rf+ef,color=kw['color'],alpha=.3)
            ax[1].plot(xx,rf,**kw)
            
        l[n] = Line2D([],[],**kw)
        
    return l,None

# --- Plot integrated flow -------------------------------------------
def plot_v(results,ax,omin=-.3,omax=.3,marker=None):
    """Plot integrated flow results"""
    from matplotlib.pyplot import gca
    from numpy import unique, array, linspace, logical_or, logical_and, \
        true_divide, ones_like
    
    ax   = gca() if ax is None else ax
    data = array([(int(n),int(m),v,u)
                  for n,vn in results.items() for m,(v,u) in vn.items()])

    ms   = unique(data[:,1])
    ret  = []
    for i,(m,o) in enumerate(zip(ms,linspace(omin,omax,len(ms)))):
        msk   = data[:,1]==m
        n,v,u = data[msk,0],data[msk,2],data[msk,3]
        r     = true_divide(u,v,out=ones_like(u),where=v>0)
        msk   = r < 1
        ret.append(plot_vn(m,n[msk],v[msk],u[msk],o,ax,marker=marker))

    return ret

# --- Plot differential flow -----------------------------------------
def plot_vp(results,ax=None,cut=.5,f=None):
    from matplotlib.pyplot import subplots
    from numpy import unique, asarray, array

    fig, ax = get_axes_with_ratio(ax)
    edges   = asarray(results['edges'])
    values  = results['values']
    mid     = (edges[1:]+edges[:-1])/2
    width   = (edges[1:]-edges[:-1])
    data    = array([[int(m),int(n),v[0],v[1]]
                     for r in values
                     for n,vm in r.items()
                     for m,v in vm.items()])
    ms      = unique(data[:,0]).astype(int)

    r = {}
    for m in ms:
        r[m] = plot_vpn(m,data[data[:,0]==m][:,1:],mid,
                        w=width,cut=cut,ax=ax,f=f)[0]

    return r

# --- Plot distribution ----------------------------------------------
def plot_dist(d,ax,varname,logy=False,**kwargs):
    from matplotlib.pyplot import gca
    from numpy import asarray

    ax = gca() if ax is None else ax

    bins  = asarray(d['bins'])
    means = asarray(d['means'])
    uncer = asarray(d['uncer'])
    mid   = (bins[1:]+bins[:-1])/2
    width = (bins[1:]-bins[:-1])

    ax.bar(mid,means,width,yerr=uncer,alpha=.3,**kwargs)
    ax.set_xlabel(f'${varname}$')
    ax.set_ylabel(fr'$\mathrm{{d}}N/\mathrm{{d}}{varname}$')
    if logy:
        ax.set_yscale('log')

# ====================================================================
def isInteractive():
    """Check if the shell is interactive or not.  Interactive, in this
    case, can also mean that the option `-i` was passed.

    First, we check if `sys.flags.interactive` is true, and if so, we
    return true.

    Secondly, to deal with IPython, where `sys.flags.interactive` is
    _always_ false, we check if we're in IPython by trying to evaluate
    `get_ipython()`.  If this does not succeed, an exception is
    thrown, which we then ignore and return false.  If the call
    succeeds, then we check if the a configurable in the IPython shell
    of type `IPython.terminal.ipapp.TerminalIPythonApp` exists, and if
    so, if the `interact` flag is set to true, in which case we return
    true.

    In all other cases we return false. 

    Returns
    -------
    interactive : bool 
        True if the shell is interactive.

    """
    import sys

    # Test regular Python shell 
    if sys.flags.interactive:
        return True

    # Check for IPython 
    try:
        sh = get_ipython()

        import IPython
        for c in sh.configurables:
            if isinstance(c,IPython.terminal.ipapp.TerminalIPythonApp):
                return c.interact 
    except Exception as e:
        pass
        
    return False

# --------------------------------------------------------------------
def pltion():
    """Turn on interactive plotting if the shell is interactive"""
    from matplotlib.pyplot import ion
    if isInteractive():
        ion()

# --- Clean output on error ------------------------------------------
class CleanOutput:
    def __init__(self,out):
        self._o = out
        
    def __enter__(self):
        return self

    def __exit__(self,etype,eval,tb):
        if etype is None or self._o is None:
            return

        from os import unlink
        self._o.close()
        unlink(self._o.name)

        return False

# --- Customize matplotlib -------------------------------------------
def setup_style():
    from matplotlib import rcParams 
    rcParams['figure.max_open_warning'] = 0
    rcParams['font.serif'] = ['Palatino'] + rcParams['font.serif']
    rcParams['font.family'] = ['serif']
    rcParams['mathtext.fontset'] = 'dejavuserif'
    rcParams['axes.formatter.use_mathtext'] = True    

    
# --- Main for plotting ----------------------------------------------
def plot_main(desc,plot,cut=None):    
    from argparse import ArgumentParser, FileType
    from matplotlib.pyplot import ion

    ap = ArgumentParser('Plot integrated flow')
    ap.add_argument('input',type=FileType('r'),
                    help='Input filename')
    ap.add_argument('output',type=FileType('wb'),default=None,
                    nargs='?',help='Output file')

    if cut is not None:
        ap.add_argument('-c','--cut',default=cut,
                        help='Cut on relative uncertainty',
                        type=float)

    args = ap.parse_args()
    pltion()
    setup_style();
    
    with CleanOutput(args.output):
        try:
            if cut is None:
                fig = plot(args.input)
            else:
                fig = plot(args.input,args.cut)
        except:
            fig = None
            raise
    
    if args.output is not None and fig is not None:
        fig.savefig(args.output)
    
#
# EOF
#
