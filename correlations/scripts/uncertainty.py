#!/usr/bin/env python3
#
# Generic framework for multi-particle correlations 
# Copyright (C) 2020  Christian Holm Christensen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
sys.path.insert(0,'.')

def plot(inputs,ax=None):
    """Plot the difference between the three different strategies for
    estimating uncertainties:

    * Full error propagation using derivatives and the covariance
      matrix (correlations.stat.Derivatives)

    * Jackknife estimate using 10 samples calculating the variance
      from leave-on-out approach (correlations.stat.Jackknife)

    * Bootstrap estimate using 1000 samples from 10 sub-samples and
      calculating the variance over those 1000 samples
      (correlations.stat.Bootstrap).

    .. image:: static/gen_phi_medium_intg_uncr.png

    Parameters 
    ----------
    inputs : list of files 
        List of input files 
    ax : matplotlib.axes.Axes 
        Axes to plot in

    Returns 
    -------
    fig : matplotlib.figure.Figure 
        Figure plotted in 
    """

    from matplotlib.pyplot import gca
    from matplotlib.lines import Line2D
    from numpy import arange, unique, linspace
    from correlations.scripts.utils import load, plot_v, plot_vn

    ax = gca() if ax is None else ax 
    
    header, results, info = load(inputs[0])
    inp_vn                = header['flow_coef']
    ns                    = arange(1,len(inp_vn)+1)
    p                     = plot_vn(0,ns,inp_vn,ax=ax)    
    p.set_label('Input')

    inputs[0].seek(0)
    ni    = len(inputs)
    os,od = linspace(-.3,.3,3*ni-1,retstep=True)
    mks   = ['o','s','*']
    
    lp = [p] + [plot_v(load(inp)[1],ax,omin=o1,omax=o2,marker=mk)
                for inp,o1,o2,mk
                in zip(inputs,os[0::3],os[1::3],mks)][0]

    for inp,mk in zip(inputs,['o','s','*']):
        lbl='Full propagation'
        if 'jackknife' in inp.name.lower():
            lbl='Jackknife'
        elif 'bootstrap' in inp.name.lower():
            lbl='Bootstrap'
            
        lp.append(Line2D([],[],marker=mk,color='k',label=lbl,ls='none'))
        
    ax.legend(lp,[ll.get_label() for ll in lp])
    ax.set_xticks(ns)
    ax.set_xticklabels([fr'${n}$' for n in ns])
    ax.set_xlabel('$n$')
    ax.set_ylabel(r'$v_{n}\{m\}$')
    ax.set_title('Difference in uncertainty estimates')
    
    fig = ax.figure
    fig.tight_layout()
    return fig

if __name__ == "__main__":
    from argparse import ArgumentParser, FileType
    from correlations.scripts.utils import pltion

    ap = ArgumentParser('Plot difference from uncertainty type')
    ap.add_argument('input',help='Input files',nargs='+',
                    type=FileType('r'))
    ap.add_argument('-o','--output',help='Output file',
                    type=FileType('wb'))

    args = ap.parse_args()

    pltion()
    
    fig = plot(args.input)

    if fig is not None and args.output is not None:
        fig.savefig(args.output)


#
# EOF
#
