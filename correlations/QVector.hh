#ifndef CORRELATIONS_QVECTOR_HH
#define CORRELATIONS_QVECTOR_HH
/**
 * @file   correlations/QVector.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Container of the Q-vector
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/Types.hh>
#include <cmath>
#include <limits>
#include <iostream>
#include <iomanip>
#include <cassert>

namespace correlations
{
  namespace  fast {
    // https://stackoverflow.com/questions/18662261/fastest-implementation-of-sine-cosine-and-square-root-in-c-doesnt-need-to-b#answer-28050328
    template<typename T>
    inline T cos(T x) noexcept
    {
      constexpr T tp = 1./(2.*M_PI);
      x *= tp;
      x -= T(.25) + std::floor(x + T(.25));
      x *= T(16.) * (std::abs(x) - T(.5));
      x += T(.225) * x * (std::abs(x) - T(1.));
      return x;
    }
    template <typename T>
    inline T sin(T x) noexcept
    {
      constexpr T of = M_PI / 2.;
      return cos(x-of);
    }
  }
  //____________________________________________________________________
  /**
   * A structure to hold the @f$ Q@f$-vector.
   *
   * It provides member functions for reseting, filling and accessing
   * the individual @f$ Q@f$-vector components.
   *
   @code
   correlations::QVector q(3,2);
   
   // Fill vector
   q.reset();
   for (Int_t i = 0; i < nPhi; i++) q.fill(phi[i], weight[i])
   
   // Show components
   for (short n=-2, n <= 2; n++)
     for (unsigned short p = 0; p < 2; p++)
       std::cout << "Q{" << n << "," << p << "}=" << q(n,p) << std::endl;
   @endcode
   *
   * @headerfile ""  <correlations/QVector.hh>
   * @ingroup correlations 
   */
  struct QVector
  {
    /** The harmonics to use */
    using Harmonic=correlations::Harmonic;
    /** The complex type */
    using Complex=correlations::Complex;
    /** Vector of reals */
    using RealVector=correlations::RealVector;

    /** A constant null complex value */
    static constexpr Complex cnull{0,0};
    
    /**
     * Constructor - creates a QVector with the specified maximum
     * harmonic order and maximum power of the harmonics
     *
     * @param mN         Maximum harmonic order
     * @param mP         Maximum power/number of particles 
     * @param useWeights Whether to use weights or not
     *
     */
    QVector(Size mN, Size mP, bool useWeights)
      : _maxN(mN),
	_maxP(mP),
	_useWeights(useWeights),
	_q((2*sizeN()-1)*sizeP()),
	_ns(sizeN()),
	_ps(sizeP())
    {
      std::iota(std::begin(_ns),std::end(_ns), 0.);
      if (useWeights)
	std::iota(std::begin(_ps),std::end(_ps), 0.);
    }
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */
    QVector(QVector&& o)
      : _maxN(o._maxN),
	_maxP(o._maxP),
	_useWeights(o._useWeights),
	_q(std::move(o._q)),
	_ns(std::move(o._ns)),
	_ps(std::move(o._ps))
    {
    }
    /** 
     * Delete the copy constructor 
     */
    QVector(const QVector& o) = delete;
    /** 
     * Delete the assignment operator
     */
    QVector& operator=(const QVector& o) = delete;
    /**
     * Reset all @f$Q@f$ vector component to @f$0+i0@f$
     *
     */
    void reset(const Complex& q=Complex(0,0))
    {
      std::fill(std::begin(_q), std::end(_q), q);
    }
    /**
     * @brief Get @f$ Q_{h,p}@f$
     *
     * Get the @f$ Q_{h,p} = \sum_{i=1}^{M} w_i^{p}e^{i\phi_i\sum h}@f$.
     * Note that @f$ Q_{-h} = Q_{h}^*@f$
     *
     * @param h The harmonic @f$ h@f$ 
     * @param p The power @f$ p@f$ (or the number of terms in @f$ h@f$)
     *
     * @return @f$ Q_{h,p}@f$ 
     */
    const Complex operator()(Harmonic h, Power p) const
    {
      return _q[index(h,p)];
    }
    /**
     * Fill in an obersvation.  First, we calculate the vector 
     *
     * @f[ \vec\phi = \vec{n}\phi\quad\text{or}\quad \phi^n = n\phi\quad.@f]
     *
     * Then we calculate the complex vector 
     *
     * @f[ \vec z = \cos\vec\phi + i\sin\vec\phi\quad\text{or}\quad z^n = \cos n\phi + i\sin n\phi\quad.@f] 
     *
     * We calculate the weight vector 
     *
     * @f[\vec w = w^{\vec p}\quad\text{or}\quad w^p = w^p\quad,@f]
     *
     * and take the outer product 
     *
     * @f[ U = \vec w^T \vec \phi\quad\text{or}\quad U^{np} = w^p z^n\quad,@f]
     *
     * which is a matrix which we add to the current Q-vector
     *
     * @param phi     Phi observation.
     * @param weight  Weight of this observation.
     */
    void fill(Real phi, Real weight)
    {
      auto wp = std::pow(weight,_ps); // Powers of weight
      auto np = phi * _ns;            // Factors of angle
      auto cp = std::cos(np);         // Cosines of angles
      auto sp = std::sin(np);         // Sines of angles
      auto cu = _outer(wp, cp);       // Weights times cosines
      auto su = _outer(wp, sp);       // Weights times sines
      
      _update(cu,su);
    }
    /** 
     * Fill in many observations 
     *
     * This method first calculates the matrix 
     *
     * @f[ N = \vec{\varphi}^T\vec{h}\quad,@f]     
     *
     * which is the outer product of all angles with the possible
     * harmonics (i.e, a vector from 0 to the maximum harmonic
     * stored @f$H@f$).  That is, the @f$(i,n)@f$ element is given by 
     *
     * @f[ \Phi^{in} = n\phi_i\quad,@f]
     *
     * where @f$i@f$ labels the observation. 
     * 
     * Then, we take the cosine and sine of all these angles
     *
     * @f[ C = \cos(\\Phi)\quad S = \sin(\Phi)\quad,@f] 
     * 
     * and form the complex matrix 
     *
     * @f[ Z = C + iS\quad,@f]
     *
     * of size @f$(H+1)\times M@f$ where @f$M@f$ is the number of phi
     * observations. Again, the @f$ (i,n)@f$ element is given by 
     *
     * @f[ Z^{in} = \cos(N^{in})+i\sin(N^{in})\quad.@f]
     *
     * Next, we calculate the matrix 
     *
     * @f[ W = \vec{w}^{\vec{p}}\quad,@f] 
     *
     * That is, we take all weights and raise them to all possible
     * powers (from 0 to @f$ P @f$).  This matrix is of size
     * @f$ M\times(P+1)@f$, and the @f$(i,p)@f$ element is given by 
     *
     * @f[ W^{ip} = w_i^p\quad.@f]
     *
     * Finally, we calculate the matrix product 
     *
     * @f[U = ZW\quad\text{or}\quad U^{np} = Z_{ni}W^{ip}\quad,@f]
     *
     * of size @f$ (H+1)\times(P+1)@f$, which is our update to the
     * Q-vector.
     *
     * @param phis     Angles (in radians)
     * @param weights  Weights 
     *
     */
    void fill(const RealVector& phis, const RealVector& weights)
    {
      if (phis.size() <= 0) return;
      
      size_t     mN = sizeN();
      size_t     mP = sizeP();
      auto       wp = _allpow(weights,_ps);    // Powers of weights
      auto       np = _outer(phis, _ns);       // Factors of angles
      auto       cp = std::cos(np);            // Cosines
      auto       sp = std::sin(np);            // Sines
      auto       cu = _matmul(cp, wp, mN, mP); // Sum weights times cosines
      auto       su = _matmul(sp, wp, mN, mP); // Sum weights times sines 

      _update(cu,su);
    }
    /**
     * Fill in an obersvation
     *
     * @param q Q-vector of single observation
     */    
    void fill(const QVector& q)
    {
      _q += q._q;
    }
    /**
     * Get the maximum harmonic (minus 1)
     *
     * @return Maximum harmonic minus 1
     */
    Size maxN() const { return _maxN; }
    /**
     * Get the maximum power (minus 1)
     *
     * @return Maximum power (minus 1)
     */
    Power maxP() const { return _maxP; }
    /** 
     * Get wether we use weights or not. 
     *
     * @return true if weights are used 
     */
    bool useWeights() const { return _useWeights; }
    /** 
     * Number of unique harmonics 
     *
     * @return Number of unique harmonics 
     */
    Size sizeN() const { return _maxP / 2 *_maxN + 1; }
    /** 
     * Number of unique powers 
     *
     * @return Number of unique powers 
     */
    Size sizeP() const { return _maxP + 1; }
    /**
     * Verify that the negative orders are the conjugate of the
     * positive orders 
     *
     * @return true if the condition is met, false otherwise 
     */
    bool verify() const
    {
      bool ret = true;
      short mN = sizeN();
      short mP = sizeP();
      for (Harmonic i = -mN+1; i <= mN-1; i++) {
	if (i == 0) continue;
	for (Power j = 0; j <= mP-1; j++) {
	  const Complex& c1 = (*this)(i,j);
	  const Complex& c2 = (*this)(-i,j);
	  if (c1 != std::conj(c2)) {
	    std::cerr << "(" << i << "," << j << ") [" << index(i,j)
		      << "]=" << c1 << " is no the conjugate of ("
		      << -i << "," << j << ") [" << index(-i,j)
		      << "]= " << c2 << std::endl;
	    ret = false;
	  }
	}
      }
      return ret;
    }
    /** 
     * Print content of Q-vector 
     */
    void print(size_t precision=3,size_t width=9) const
    {
      std::ostream& out = std::cout;
      auto oldf         = out.flags();
      auto oldp         = out.precision();
      if (precision > 0) {
	out.setf(std::ios_base::fixed, std::ios_base::floatfield);
	out.precision(precision);
      }
      
      // if (!verify()) return;
      out << "[\n";
      for (Harmonic i = -sizeN()+1; i <= sizeN()-1; i++) {
	out << " [";
	for (Power j = 0; j <= _maxP; j++) {
	  auto z = this->operator()(i, j);
	  out << ' ' << std::setw(width) << z.real()
	      << '+' << std::setw(width) << z.imag() << "j,";
	}
	out << "], # " << std::setw(3) << i << std::endl;
      }
      out << "]" << std::endl;
      
      out.setf(oldf);
      out.precision(oldp);
    }
    /** 
     * Make a Q-vector of a single observation 
     *
     * @param phi Azimuthal angle 
     * @param weight Weight 
     *
     * @return A Q-vector for a single observation 
     */
    QVector one(Real phi, Real weight) const
    {
      QVector q(_maxN,_maxP,_useWeights);
      q.fill(phi,weight);
      return q;
    }
    /** 
     * Compare two Q-vectors for (approximate) equality 
     */
    bool operator==(const QVector& q)
    {
      if (q._q.size() != _q.size()) return false;

      bool ret = true;
      for (size_t i = 0; i < _q.size(); i++)
	if (!isclose(q._q[i],_q[i],1e-3,1e-3))
	  ret = false;

      return ret;
    }
  protected:
    /**
     * @brief Get @f$Q_{n,p}@f$
     *
     * Get the @f$Q_{n,p} = \sum_{i=1}^{M} w_i^pe^{in\phi_i}@f$.
     * Note that @f$Q_{-n,p} = Q_{n,p}^*@f$
     *
     * @param n The harmonic
     * @param p The power
     *
     * @return @f$ Q_{n,p}@f$ or @f$ Q^*_{-n,p}@f$ if @f$n<0@f$
     */
    Complex& ref(Harmonic n, Power p)
    {
      return _q[index(n,p)];
    }
    /**
     * Calculate index into internal storage for a given harmonic and
     * power.  @b NB No bounds check.
     *
     * @param n Harmonic
     * @param p Power
     *
     * @return Index into internal array
     */
    unsigned short index(Harmonic n, Power p) const
    {
      // return check(n,p);
      return ((n + sizeN()-1) * sizeP() + p);
    }
    /**
     * Calculate index into internal storage for a given harmonic and
     * power.  @b NB With bounds check.
     *
     * @param n Harmonic
     * @param p Power
     *
     * @return Index into internal array
     */
    unsigned short check(Harmonic n, Power p) const
    {
      int lN = sizeN()-1;
      if (n < -lN || n > lN)
	throw std::runtime_error("n="+std::to_string(n) + " out of bounds [-"
				 + std::to_string(lN) + ","
				 + std::to_string(lN) + "]");
      if (p > _maxP) 
	throw std::runtime_error("p="+std::to_string(p) + " out of bounds [0,"
				 + std::to_string(_maxP) + ")");
      unsigned short i = index(n,p);
      if (i >= _q.size())
	throw std::runtime_error("i="+std::to_string(i) + " ("
				 + std::to_string(n) + "," + std::to_string(p)
				 + ") out of bounds [0,"
				 + std::to_string(_q.size())
				 + "]");
      return i;
    }
    /** 
     * Check if two numbers are close 
     *
     */
    template <typename T>
    static bool isclose(const T& a, const T& b,
			double atol=std::numeric_limits<double>::epsilon(),
			double rtol=std::numeric_limits<double>::epsilon())
    {
      bool ret = std::fabs(a-b) <= (atol + rtol * std::fabs(b));
#if 0
      if (!ret) {
	auto oldf = std::cout.flags();
	std::cout << std::scientific
		  << a << " vs " << b << ": "
		  << std::fabs(a-b) << " > "
		  << atol << " + " << rtol << " * "
		  << std::fabs(b) << std::endl;
	std::cout.setf(oldf);
      }
#endif 
      return ret;
    }
    /** 
     * Updates Q-vector with powers of weights times cosines and sines
     * of scaled angles 
     *
     * @param wpcosnphi Matrix of @f$w^p\cos(n\phi)@f$ 
     * @param wpsinnphi Matrix of @f$w^p\sin(n\phi)@f$ 
     */
    void _update(const RealVector& wpcosnphi,
		 const RealVector& wpsinnphi)
    {
      size_t     mN  = sizeN();
      size_t     mP  = sizeP();
      size_t     off = (mN-1)*mP;
      for (size_t i = 0; i < wpcosnphi.size(); i++)   // Positive harmonics 
	_q[off+i] += Complex(wpcosnphi[i],wpsinnphi[i]);
      for (size_t n = 1; n < mN; n++)                 // Negative harmonics 
	for (size_t p = 0; p < mP; p++) {
	  auto z = this->operator()(n,p);
	  ref(-n,p) = Complex(z.real(),-z.imag());
	  // ref(-n,p) = std::conj(this->operator()(n,p));
	}
    }
    /** 
     * Calculate the outer product of two vectors @f$ u\in\mathbb{R}^N
     * @f$ and @f$ v\in\mathbb{R}^M@f$ resulting in a matrix @f$
     * m\in\mathbb{R}^N\times\mathbb{R}^M @f$.
     *
     * @f{eqnarray}{
     m &= uv^T\\
     &= \begin{bmatrix}u_1\\u_2\\\vdots\\u_N\end{bmatrix}
     \begin{bmatrix} v_1&v_2&\ldots& v_N\end{bmatrix}\\
     &= \begin{bmatrix} 
     u_1v_1 & u_1v_2 & \ldots &u_1v_N\\
     u_2v_1 & u_2v_2 & \ldots &u_2v_N\\
     \vdots & \vdots & \ddots & \vdots\\
     u_Nv_1 & u_Nv_2 & \ldots &u_Nv_N\\
     \end{bmatrix}
     @f}
     *
     * @param u the vector @f$ u@f$ 
     * @param v the vectir @f$ v@f$ 
     *
     * @return Outer product @f$ m@f$ 
     */
    static RealVector _outer(const RealVector& u, const RealVector& v)
    {
      size_t d1 = u.size();
      size_t d2 = v.size();
      
      RealVector r(d1*d2);
      for (size_t i = 0; i < d2; i++) r[std::slice(i*d1,d1,1)] = v[i]*u;
      return r;
    }
    /**
     * Calculates the matrix-matrix product of two matrices 
     *
     * @f[ A\in\mathbb{R}^{N\times P}\quad B\in\mathbb{R}^{P\times M}\quad.@f]
     *
     * resulting in the matrix 
     *
     * @f[ C\in\mathbb{R}^{N\times M}\quad.@f]
     *
     * @param m1    The matrix @f$ A@f$ 
     * @param m2    The matrix @f$ B@f$ 
     * @param rows1 Number of rows (@f$ N@f$) in @f$ A@f$
     * @param cols2 Number of columns (@f$ M@f$) in @f$ AB@f$
     *
     * @return @f$ C@f$ of size @f$N\times M@f$. 
     */
    static RealVector _matmul(const RealVector& m1, const RealVector& m2,
			      size_t rows1, size_t cols2)
    {
      size_t cols1 = m1.size() / rows1;
      size_t rows2 = m2.size() / cols2;

      assert(m1.size() % rows1 == 0);
      assert(m2.size() % cols2 == 0);
      assert(cols1 == rows2);

      RealVector m(rows1*cols2);

      for (size_t r = 0; r < rows1; r++) {
	for (size_t c = 0; c < cols2; c++) {
	  auto r1 = m1[std::slice(r * cols1, cols1, 1)];
	  auto c2 = m2[std::slice(c,         rows2, cols2)];
	  auto rc = r1 * c2;
	  m[r * cols2 + c] = rc.sum();
	}
      }
      return m;
    }
    /** 
     * Raises all bases (a vector of size @f$ M@f$) to all powers (a
     * vector of size @f$ P@f$), thus forming a @f$ M\times P@f$
     * matrix.
     *
     * @param base Bases 
     * @param ex   Exponenents 
     *
     * @return matrix 
     */
    static RealVector _allpow(const RealVector& base, const RealVector& ex)
    {
      size_t d2 = base.size();
      size_t d1 = ex.size();

      RealVector r(d1*d2);
      for (size_t i = 0; i < d2; i++)
	r[std::slice(i*d1,d1,1)] = std::pow(base[i],ex);

      return r;
    }
    /**
     * Fill in an obersvation
     *
     * @param phi     Phi observation.
     * @param weight  Weight of this observation.
     *
     * @deprecated Use QVector::fill instead (faster)
     */
    void fill_(Real phi, Real weight)
    {
      // Use single precision here to speed up calculation - at the
      // cost of precision (but not bad at all)
      using Flt=float;
      // Faster, but less precise implementations of cos and sin
      // using fast::cos;
      // using fast::sin;
      // Standard trig functions
      using std::cos;
      using std::sin;
      using std::pow;
      Size lN = sizeN()-1;
      static std::valarray<Flt> cs(lN+1);
      static std::valarray<Flt> sc(lN+1);
      const Flt fphi = phi;
      const Flt fw   = weight;
      for (Harmonic n = 0; n <= lN; n++) {
	cs[n] = cosf(n * fphi);
	sc[n] = sinf(n * fphi);
      }
      for (Power p=0; p <= _maxP; p++) {
	Real w = (!_useWeights || p == 0 ? 1 : powf(fw,p));
	for (Harmonic n = 0; n <= lN; n++) {
	  ref(n,p) += Complex(w * cs[n], w * sc[n]);
	  if (n == 0) continue;
	  // Update the conjugate as well
	  ref(-n,p) += Complex(w * cs[n], -w * sc[n]);
	}
      }
    }
    /** 
     * Calcuate needed size 
     *
     * @param mN         Maximum harmonic order
     * @param mP         Maximum power/number of particles 
     *
     * @return Needed storage in Q-vector
     */
    static size_t calcSize(Size mN, Size mP)
    {
      return (mP*mN+1)*(mP+1);
    }
    Size           _maxN;         /**< Maximum harmonic order  */
    Size           _maxP;         /**< Maximum power of harmonic */
    bool           _useWeights;   /**< Wheter to use weights or not */
    ComplexVector  _q;            /**< Internal storage of Q vector */
    RealVector     _ns;           /**< All harmonics */
    RealVector     _ps;           /**< All powers */
  };
  //==================================================================
  /** 
   * Get the maximum N from a HarmonicVector 
   *
   * @param hv Harmonic vector 
   */
  Size maxN(const HarmonicVector& hv)
  {
    Size ini = 0;
    return std::accumulate(std::begin(hv), std::end(hv), ini,
			   [](const Harmonic& h1, const Harmonic& h2) {
			     return Harmonic(std::abs(float(h1))+
					     std::abs(float(h2))); });
  }
  /** 
   * Get the maximum N from a HarmonicVector 
   *
   * @param hv Harmonic vector 
   */
  Size maxN(const PartitionHarmonicVector& hv)
  {
    std::vector<size_t> m(hv.size());
    std::transform(std::begin(hv), std::end(hv), m.begin(),
		   [](const PartitionHarmonic& hh) { return hh.max(); });
    Size ini = 0;
    return std::accumulate(std::begin(m),std::end(m),ini);
  }
  //==================================================================
  /** 
   * Make a Q-vector from harmonic vector  
   * 
   * @tparam Harmonics The harmonic vector type 
   *
   * @param h Harmonic vector
   * @param useWeights Whether to use weights
   */
  template <typename Harmonics>
  QVector makeQ(const Harmonics& h, bool useWeights)
  {
    return QVector(maxN(h), h.size(), useWeights);
  }
  //==================================================================
  /** A vector of @f$ Q@f$-vectors */
  using QContainer=std::vector<QVector>;

  //------------------------------------------------------------------
  /** 
   * Make a container of @f$ Q@f$-vectors 
   *
   * @param n       Number of @f$ Q@f$-vectors 
   * @param maxN    Largest @f$ n@f$ 
   * @param maxP    Largest power or @f$ m@f$ 
   * @param weights Whether to enable weights 
   *
   * @return Container of @a n @f$ Q@f$-vectors 
   */
  QContainer makeQC(size_t n, Size maxN, Size maxP, bool weights)
  {
    QContainer r;
    for (size_t i = 0; i < n; i++) r.emplace_back(maxN,maxP,weights);
    return r;
  }
  //------------------------------------------------------------------
  template <typename Harmonics>
  QContainer makeQC(size_t n, const Harmonics& h, bool weights)
  {
    Size mN = maxN(h);
    Size mP = h.size();
    return makeQC(n, mN, mP, weights);
  }
}
#endif
// Local Variables:
//  mode: C++
// End:
