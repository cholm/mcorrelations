#ifndef CORRELATIONS_PARTITION_HH
#define CORRELATIONS_PARTITION_HH
/**
 * @file   correlations/Partition.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Declarations of more types
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <numeric>

namespace correlations
{
  /** 
   * Type of partition identifier 
   *
   * A partition identifier sets which sub-event a given harmonic
   * belong to.  Suppose we want to calculate the 6-particle
   * correlator with harmonics
   *
   * @f[ \mathbf{h} = \{h_1,h_2,h_3,h_4,h_5,h_6\}\quad,@f] 
   *
   * and we want to do this in two sub-events corresponding to 
   *
   * @f[ \mathbf{h}_L=\{h_1,h_2,h_3\}\quad\mathbf{h}_R=\{h_4,h_5,h_6\}@f]
   * 
   * Then, we'd construct the partition vector 
   *
   * @f[ \mathbf{p} = \{1,1,1,2,2,2\}\quad,@f]
   *
   * and pass that along with the vector @f$ \mathbf{h}@f$ to the
   * calculate method.
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  using Partition=unsigned short;

  /** 
   * Conglomerate of harmonics 
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  struct PartitionHarmonic
  {
    /** Default constructor */
    PartitionHarmonic() : _h(), _p(), _s() {}
    /** Constructor from harmonic */
    PartitionHarmonic(const Harmonic) : _h(), _p(), _s() {}
    /** 
     * Constructor from harmonic and partition 
     *
     * @param h Harmonic
     * @param p Partition
     */
    PartitionHarmonic(const Harmonic h, const Partition p)
      : _h(h), _p(p), _s(1)
    {}
    /** 
     * Constructor from initializer list 
     *
     * @param i Initializer of harmonics 
     * @param p Initializer of partitions 
     */
    PartitionHarmonic(std::initializer_list<Harmonic>  i,
		      std::initializer_list<Partition> p)
      : _h(), _p(), _s(i.size())
    {
      Harmonic r;
      _h = std::accumulate(i.begin(), i.end(), r);
      _s = i.size();
      _p = *p.begin();
      if (!std::equal(p.begin() + 1, p.end(), p.begin()))
	_p = 0;      
    }
    /** 
     * Constructor from initializer list 
     *
     * @param i Initializer 
     */
    PartitionHarmonic(std::initializer_list<PartitionHarmonic> i)
      : _h(), _p(), _s(0)
    {
      _s = i.size();
      _p = i.begin()->partition();
      for (auto& ph : i) {
	_h += ph.sum();
	if (_p != ph.partition()) _p = 0;
      }
    }
    PartitionHarmonic& operator=(const std::pair<Harmonic,Partition>& hp)
    {
      _s = 1;
      _p = hp.second;
      _h = hp.first;
      return *this;
    }
    /** Get the partition */
    Partition partition() const { return _p; }
    /** Get the size */
    unsigned short size() const { return _p >= 1 ? _s : 0; }
    /** Get the sum of harmonics */
    Harmonic sum() const { return _h; }
    /** Get the maximum harmonics */
    Harmonic max() const { return _h; }
    /** Add another harmonic to this */
    PartitionHarmonic& operator+=(const PartitionHarmonic& other)
    {
      if (other._p != _p) _p = 0;
      _h += other._h;
      _s += other._s;

      return *this;
    }
  protected:
    /** Harmonic */
    Harmonic _h;
    /** Partition */
    Partition _p;
    /** Size */
    unsigned short _s;
  };

  /** 
   * Sum operator for harmonics calculations 
   *
   * @param lhs Left-hand side 
   * @param rhs Right-hand sid 
   *
   * @return Summed harmonics 
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  PartitionHarmonic operator+(const PartitionHarmonic& lhs,
			      const PartitionHarmonic& rhs)
  {
    PartitionHarmonic t(lhs); t += rhs;
    return t;
  }
}
#endif
//
// EOF
//

    
