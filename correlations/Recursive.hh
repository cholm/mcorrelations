#ifndef CORRELATIONS_RECURSIVE_HH
#define CORRELATIONS_RECURSIVE_HH
/**
 * @file   correlations/Recursive.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Cumulant correlator using recursion
 */
/*
 * Multi-particle correlations
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/FromQVector.hh>
#include <correlations/Dbg.hh>

/** 
 * @defgroup correlations_recursive Recursive implementations 
 *
 * The code in this module uses recursion to simplify the calculation
 * of the @f$ m@f$-particle correlator.  Essentially we form all
 * combinations first, and then recurse down to subtract off all
 * autocorrelations.
 * 
 * @ingroup correlations 
 */
namespace correlations
{
  /* 
   * Internal note: This was mainly done by Kris 
   */
  //____________________________________________________________________
  /**
   * Structure to calculate Cumulants of arbitrary order and power from
   * a given Q vector.
   *
   * This code used either generic recurssion to calculate the
   * correlator @f$ QC\{n\}@f$ or - for a limited number of @f$ n@f$
   * specific recursive functions.
   *
   * @tparam Harmonics Vector of harmonics to use 
   *
   * @headerfile ""  <correlations/Recursive.hh>
   * @ingroup correlations_recursive
   */
  template <typename Harmonics=HarmonicVector>
  struct Recursive : public FromQVector<Harmonics>
  {
    /** Base type */
    using Base=FromQVector<Harmonics>;
    /** The type of vectors of harmonics to use */
    using HarmonicVector=typename Base::HarmonicVector;
    /** The harmonics to use */
    using HarmonicElement=typename Base::HarmonicElement;
    /** The type of Q-vector storage */
    using QStore=typename Base::QStore;
    /** The Q-vector to use */
    using QVector=typename Base::QVector;
    /** The complex type */
    using Complex=typename Base::Complex;
    /** A vector of harmonic orders */
    using SizeVector=std::vector<Size>;

    /**
     * Constructor
     *
     * @param q Q vector storage 
     */
    Recursive(const QStore& q) : Base(q) {}
    /**
     * Constructor
     *
     * @param o Object to move from 
     */
    Recursive(Recursive&& o) : Base(std::move(o)) {}
    /** Deleted copy constructor */
    Recursive(const Recursive&) = delete;
    /** Deleted assignment operator */
    Recursive& operator=(const Recursive&) = delete;
    /**
     *  Destructor
     */
    virtual ~Recursive() {}
    /**
     * @return Name of the correlator
     */
    virtual const char* name() const { return "Recursive correlator"; }
  protected:
    using Base::_q;
    using Base::_r;
    using Base::_p;
    /**
     * @{
     * @name Arbitrary order calculations using recursion
     */
    /**
     * Calculate the multi-particle correlation
     *
     * The calculation is done using the following algorithm
     *
     * @verbatim
     *    C = q(h_{m-1}, cnt_{m-1});
     *    if m == 1 then return c;
     *
     *    C *= UN(m-1, h, cnt);
     *
     *    if (cnt[nm-1] > 1) return C;
     *
     *    for i from 0 upto m-2 do
     *       let h[i]   = h[i] + h[m-1];
     *       let cnt[i] = cnt[i]-1;
     *
     *       C -= (cnt[i]-1) * UN(m-1, h, cnt);
     *
     *       let cnt[i] = cnt[i]-1;
     *
     *       let h[i]   = h[i] - h[m-1];
     *    end for i
     * @endverbatim
     *
     * @param m Order of correlation (number of particles to correlate)
     * @param h PartitionHarmonic
     *
     * @return @f$ QC\{n\}@f$
     */
    Complex eval(const Size m, const HarmonicVector& h) const
    {
#if 0 // ndef _REENTRANT
      static HarmonicVector hh(0);
      static SizeVector     ss(0);
      Size mm =  std::max(hh.size(), h.size());
      ss.resize(std::max(m,Size(ss.size())));
      hh.resize(mm);
      hh[std::slice(0,m,1)] = h;
#else
      HarmonicVector hh(h);        // std::begin(h), std::end(h));
      SizeVector     ss(m);
#endif
      std::fill(ss.begin(), ss.end(), 1);
      
      return _eval(m, hh, ss);
    }
    /** 
     * Calculate the two particle correlator.  If @f$ h_1@f$ is
     * _not_ the sum of other harmonics (@f$ s_1 = 1@f$), then we
     * calculate
     *
     * @f[ C_{h_1,h_2}=p(h_1,s_1)r(h_2,s_2) - q(h_1+h_2,s_1+s_2)\quad,@f]
     *
     * otherwise we need to replace @f$ p @f$ with @f$ q@f$ to get 
     *
     * @f[ C_{h_1,h_2}=q(h_1,s_1)r(h_2,s_2) - q(h_1+h_2,s_1+s_2)\quad.@f]
     *
     * @param h1 @f$ h_1@f$ 
     * @param h2 @f$ h_2@f$ 
     * @param s1 @f$ s_1@f$ Number of terms in @f$ h_1@f$
     * @param s2 @f$ s_2@f$ Number of terms in @f$ h_2@f$
     * 
     * @return @f$ C_{h_1,h_2}@f$ - the two particle correlator 
     */
    Complex _two(const HarmonicElement& h1,
		 const HarmonicElement& h2,
		 Size                   s1,
		 Size                   s2) const
    {
      return ((s1 > 1) ? _q(h1,s1) : _p(h1,s1)) * _r(h2,s2) - _q(h1+h2,s1+s2);
    }
    /** 
     * Get the 1-particle correlator.
     *
     * @f[ C_{h_1} = p(h_1,s_1)\quad.@f]
     *
     * @param h1 @f$ h_1@f$ 
     * @param s1 @f$ s_1@f$ - number of terms in @f$ h_1@f$ 
     * 
     * @return @f$ C_{h_1}@f$ - the one particle correlator 
     */
    Complex _one(const HarmonicElement& h1, Size s1) const
    {
      return this->_p(h1,s1);
    }
    /**
     * Calculate the multi-particle correlation
     *
     *
     * @param m   Order of correlation (number of particles to correlate)
     * @param h   Harmonics
     * @param n   Vector of term counts
     *
     * @return @f$ QC\{n\}@f$
     */
    Complex _eval(const Size m,
		  const HarmonicVector& h,
		  SizeVector&           n) const
    {
      if (m == 1) return _one(h[0],n[0]);
      if (m == 2) return _two(h[0],h[1],n[0],n[1]);

      Size            j  =  m-1;
      Complex         c  = _r(h[j],n[j]) *  _eval(j, h,  n);

      if (n[j] > 1) return c;

      HarmonicVector t = zeroHV(h);
      
      for (Size i = 0; i < j; i++) {
	t[i] =  h[j];
	n[i] += 1;
	c    -= Real(n[i] - 1) * _eval(j, h+t, n);
	t[i] =  HarmonicElement();
	n[i] -= 1;
      }
      return c;
    }
  };
}
#endif
// Local Variables:
//  mode: C++
// End:

