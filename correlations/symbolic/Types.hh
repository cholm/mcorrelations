#ifndef CORRELATIONS_TYPES_HH
#define CORRELATIONS_TYPES_HH
// Note, we use the same header guard as in the normal header so we
// cannot include both definitions at once.
/**
 * @file   correlations/symbolic/Types.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Declarations of test types
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <complex>
#include <vector>
#include <string>
#include <sstream>
#include <streambuf>
#include <iostream>
#define SIMPLIFY_ADD

namespace std
{
  /* 
   * Convert a complex number into a string 
   */
  std::string to_string(const std::complex<double>& c)
  {
    return std::string("("+std::to_string(c.real())+
		       ","+std::to_string(c.imag())+")");
  }
}
namespace correlations
{
  /** 
   * @defgroup correlations_symbolic Symbolic evaluation 
   * @ingroup correlations 
   *
   * Code in this module allow us to evaluate the expression used in
   * the calculations symbolically.  
   * 
   */ 
  /** 
   * Namespace for symbolic evaluation code 
   *
   * @ingroup correlations_symbolic
   */
  namespace symbolic
  {
    //================================================================
    /** 
     * Base template class for numbers when evaluting the symbolic
     * expressions.
     * 
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    template <typename T>
    struct Number
    {
      /** 
       * Constructor 
       */
      Number() : _s(), _a(false) {}
      /** 
       * Constructor from a number 
       * 
       * @param n Number. 
       */
      Number(const Number& n) : _s(n.s()), _a(n._a)
      {
	// std::cout << "nCn:'" << n.s() << "' '" << _s << "'" << std::endl;
      };
      /** 
       * Constructor from a number - 
       * 
       * @param n Number. 
       */
      Number(Number&& n) : _s(), _a(n._a)
      {
	_s.swap(n._s);
	// std::cout << "nCm:'" << n.s() << "' '" << _s << "'" << std::endl;
      };
      /** 
       * Constructor from a string 
       * 
       * @param s String 
       * @param a Is this additive?
       */
      Number(const std::string& s, bool a=false) : _s(s), _a(a)
      {
	// std::cout << "nCs:'" << s << "' '" << _s << "'" << std::endl;
      }
      /** 
       * Constructor from a string 
       * 
       * @param s String 
       * @param a Is this additive?
       */
      Number(std::string&& s, bool a=false) : _s(), _a(a)
      {
	_s.swap(s);
	// std::cout << "nC2:'" << s << "' '" << _s << "'" << std::endl;
      }
      /** 
       * Constructor from primitive value 
       * 
       * @param v Value 
       */
      Number(const T& v) : _s(), _a(false)
      {
	std::stringstream s; s << v; _s = s.str();
	// _s = std::to_string(v);
      }
      /** 
       * Assignment operator from simple type 
       * 
       * @param x Value 
       * 
       * @return Reference to this object
       */
      Number& operator=(const T& x)
      {
	std::stringstream s; s << x; _s = s.str();
	return *this;
      }
      /** 
       * Assignment operator from simple type 
       * 
       * @param x Value 
       * 
       * @return Reference to this object
       */
      Number& operator=(const Number& x)
      {
	_s = x._s;
	_a = x._a;
	// std::cout << "nAs:'" << x._s << "' '" << _s << "'" << std::endl;
	return *this;
      }
      ~Number() = default;
      /** 
       * Assignment operator from simple type 
       * 
       * @param x Value 
       * 
       * @return Reference to this object
       */
      Number& operator=(Number&& x)
      {
	_s.swap(x._s);
	_a = x._a;
	// std::cout << "nAm:'" << x.s() << "' '" << _s << "'" << std::endl;
	return *this;
      }
      /** 
       * Increment assignment operator from other number 
       * 
       * @param x Other number 
       * 
       * @return Reference to this object 
       */
      Number& operator+=(const Number& x)
      {
	return (*this = aOp('+',x));
      }
      /** 
       * Decrement assignment operator from other number 
       * 
       * @param x Other number 
       * 
       * @return Reference to this object 
       */
      Number& operator-=(const Number& x)
      {
	return (*this = aOp('-',x));
      }
      /** 
       * Multiplication assignment operator from other number 
       * 
       * @param x Other number 
       * 
       * @return Reference to this object 
       */
      Number& operator*=(const Number& x)
      {
	// _s = formMOp('*',x);
	return (*this = mOp('*',x));
      }
      Number operator-()
      {
	return Number("-"+_s,_a);
      }
      /** 
       * Check if this is an additive expression 
       * 
       * @return true if the expression contains a + or -
       */
      bool isA() const
      {
	return _a;
      }
      std::string sp() const
      {
	return std::string("(" + _s + ")");
      }
      Number p() const
      {
	return Number(sp());
      }
	
      /** 
       * Format a multiplicative operation 
       * 
       * @param op Operator (one of @c '*' or @c '/')
       * @param n2 Second number 
       * 
       * @return String representation of operation 
       */
      template <typename T2>
      Number<T2> mOp(char op, const Number<T2>& n2) const
      {
	std::string sop = {op};
	return mOp(sop, n2);
      }
      /** 
       * Format a multiplicative operation 
       * 
       * @param op Operator (one of @c '*' or @c '/')
       * @param n2 Second number 
       * 
       * @return String representation of operation 
       */
      template <typename T2>
      Number<T2> mOp(const std::string& op, const Number<T2>& n2) const
      {
	if (_s == "0" || _s.empty()) return Number<T2>("0",true);
	if (_s == "1") {
	  if (!n2.isA()) return n2;
	  return n2.p();
	}
	// if (_s.empty() || _s == "0" || n2.s().empty() || n2.s() == "0")
	//   return Number<T2>("");

	return Number<T2>(std::string((isA() ? sp() : _s)
				      + " " + op + " "
				      + (n2.isA() ? n2.sp() : n2.s())));

      }
      /** 
       * Format an additive operation 
       * 
       * @param op Operator (one of @c '+' or @c '-')
       * @param n2 Second number 
       * 
       * @return String representation of operation 
       */
      template <typename T2>
      Number<T2> aOp(char op, const Number<T2>& n2) const
      {
	const char sop[] = { op, '\0' };
	if (_s.empty() || _s == "0") {
	  if (op == '+') return n2;
	  return Number<T2>(std::string(op + n2.s()),true);
	}
#ifdef SIMPLIFY_ADD
	size_t pos = 0;
	char   inv = op == '-' ? '+' : '-';
	do {
	  pos = _s.find(n2.s(),pos);
	  if (pos == std::string::npos) break;
	  if (pos == 0 && op == '-') {
	    std::string s = _s;
	    s.erase(0,n2.s().size());
	    return Number<T2>(s,true);
	  }
	  if (pos == 2 && op == '+') {
	    std::string s = _s;
	    s.erase(0,2+n2.s().size());
	    return Number<T2>(s,true);
	  }
	  if (pos > 3 && _s[pos-2] == inv) {
	    std::string s = _s;
	    s.erase(pos-3,3+n2.s().size());
	    return Number<T2>(s,true);
	  }
	  pos++;
	} while (pos < _s.size());
#endif
	if (op == '-' && n2.isA()) 
	  return Number<T2>(std::string(_s + " " + sop + " (" + n2.s() + ")"),
			    true);

	return Number<T2>(std::string(_s + " " + sop + " " + n2.s()),true);
      }
      /** 
       * Access the string 
       *
       * @return the string 
       */
      const std::string& s() const { return _s; }
      /** 
       * Evaluate the number into a stream 
       * 
       * @param o Output stream 
       * 
       * @return output stream
       */
      std::ostream& eval(std::ostream& o) const
      {
	return o << _s;
      }
      void setA(bool a) { _a = a; }
    protected:
      /** String representation */
      std::string _s;
      /** If this represents an additive calculation */
      bool _a;
    };
    //------------------------------------------------------------------
    /** 
     * Output stream operator for a number 
     * 
     * @param o Output stream 
     * @param n Number 
     * 
     * @return The output stream 
     */
    template <typename T>
    std::ostream& operator<<(std::ostream& o, const Number<T>& n)
    {
      return n.eval(o);
    }
    //------------------------------------------------------------------
    /** 
     * Input stream operator 
     * 
     * @param o Input stream 
     * @param n Number to read to 
     * 
     * @return Input stream 
     */
    template <typename T>
    std::istream& operator>>(std::istream& o, Number<T>& n)
    {
      T t;
      o >> t;
      n = t;
      return o;
    }
    //------------------------------------------------------------------
    /** 
     * Multiplication of two numbers 
     * 
     * @param n1 First number 
     * @param n2 Second number 
     * 
     * @return A new number 
     *
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    template <typename T1, typename T2, typename T3=T2>
    Number<T3> operator*(const Number<T1>& n1, const Number<T2>& n2)
    {
      return n1.mOp('*',n2);
    }
    template <typename T1>
    Number<T1> operator*(size_t a, const Number<T1>& n2)
    {
      return Number<T1>(std::to_string(a)).mOp('*',n2);
    }
    //------------------------------------------------------------------
    /** 
     * Division of two numbers 
     * 
     * @param n1 First number 
     * @param n2 Second number 
     * 
     * @return A new number 
     *
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    template <typename T1, typename T2, typename T3=T2>
    Number<T3> operator/(const Number<T1>& n1, const Number<T2>& n2)
    {
      return n1.mOp('/',n2);
    }
    template <typename T1>
    Number<T1> operator/(const Number<T1>& n1, double d)
    {
      return n1.mOp('/',Number<T1>(d));
    }
    //------------------------------------------------------------------
    /** 
     * Addition of two numbers 
     * 
     * @param n1 First number 
     * @param n2 Second number 
     * 
     * @return A new number 
     *
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    template <typename T1, typename T2, typename T3=T2>
    Number<T3> operator+(const Number<T1>& n1, const Number<T2>& n2)
    {
      return n1.aOp('+',n2);
    }
    //------------------------------------------------------------------
    /** 
     * Substraction of two numbers 
     * 
     * @param n1 First number 
     * @param n2 Second number 
     * 
     * @return A new number 
     *
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    template <typename T1, typename T2, typename T3=T2>
    Number<T3> operator-(const Number<T1>& n1, const Number<T2>& n2)
    {
      return n1.aOp('-',n2);
    }

    template <typename T1>
    Number<T1> power(const Number<T1>& n1, const double& p)
    {
      Number<T1> nt(n1);
      nt.setA(true);
      return nt.mOp("**",Number<T1>(p));
    }
    template <typename T1>
    bool isSmall(const Number<T1>&)
    {
      return false;
    }
    template <typename T1>
    bool isAbsSmall(const Number<T1>&)
    {
      return false;
    }
    
    //__________________________________________________________________
    /**
     * A real number used for symbol symbolic evaluation 
     * 
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    using Real=Number<double>;
    
    //------------------------------------------------------------------
    /** 
     * Specialisation of cosine for a real number representation 
     * 
     * @param r Real number representation 
     * 
     * @return A representation of the evaluation of cosine 
     *
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    Real cos(const Real& r)
    {
      return Real(std::string("cos(" + r.s() + ")"));
    }

    //------------------------------------------------------------------
    /** 
     * Specialisation of sine for a real number representation 
     * 
     * @param r Real number representation 
     * 
     * @return A representation of the evaluation of sine 
     *
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    Real sin(const Real& r)
    {
      return Real(std::string("sin(" + r.s() + ")"));
    }
    //------------------------------------------------------------------
    /** 
     * Compare a real to an integer 
     * 
     * 
     * @return 
     *
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    bool operator!=(const Real& /*r*/, int /*i*/)
    {
      return true;
    }
  
    //__________________________________________________________________
    /**
     * A representation of a complex number 
     * 
     *
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    struct Complex : public Number<std::complex<double>>
    {
      static std::string& i () { static std::string _i{"I"};   return _i; }
      static std::string& re() { static std::string _re{"re"}; return _re; }
      static std::string& im() { static std::string _im{"im"}; return _im; }
      /** 
       * Default constructor 
       */
      Complex()	: Number<std::complex<double>>("0")   {}
      /** 
       * Constructor from a string 
       * 
       * @param s string
       * @param a Is additive
       */
      Complex(const std::string& s, bool a=false)
	: Number<std::complex<double>>(s, a)
      {}
      /** 
       * Constructor from a string 
       * 
       * @param s string
       * @param a Is additive
       */
      Complex(std::string&& s, bool a=false)
	: Number<std::complex<double>>(s,a)
      {}
      /** 
       * Constructor from complex constant 
       * 
       * @param c Complex constant
       */
      Complex(const std::complex<double>& c)
	: Number<std::complex<double>>()
      {
	std::stringstream s; s << c; _s = s.str();
	_a = false;
      }
      /** 
       * Constructor from another complex representation 
       * 
       * @param c Complex representation 
       */
      Complex(const Number<std::complex<double>>& c)
	: Number<std::complex<double>>(c)
      {}      
      /** 
       * Constructor from another complex representation 
       * 
       * @param c Complex representation 
       */
      Complex(Number<std::complex<double>>&& c)
	: Number<std::complex<double>>(c)
      {}      
      /** 
       * Constructor from two real constants 
       * 
       * @param x Real part 
       * @param y Imaginary part
       */
      template <typename R, typename I>
      Complex(const R& x, const I& y)
	: Number<std::complex<double>>(std::complex<double>())
      {
	std::ostringstream s;
	s << "(" << x << "+" << i() << "*" << y << ")";
	_s = s.str();
	_a = false;
      }
      ~Complex() = default;
      /** 
       * Get the real value representation of the complex number
       * 
       * @return Real representation 
       */
      Real real() const { return Real(std::string(re() + "(" + _s + ")")); }
      /** 
       * Get the imaginary value representation of the complex number
       * 
       * @return Real representation 
       */
      Real imag() const { return Real(std::string(im() + "(" + _s + ")")); }
    };
    //------------------------------------------------------------------
    /** 
     * Specialisation of a complex number divided by a real number.
     * This generates a new complex number representing the operation.
     * 
     * @param c Complex number representation 
     * @param r Real number representation 
     * 
     * @return Complex repreentation of the operation 
     *
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    Complex operator/(const Complex& c, const Real& r)
    {
      return Complex(std::string("(" + c.s() + ") / " + r.s()));
    }
    //__________________________________________________________________
    /**
     * The type of a harmonic factor 
     *
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    using Harmonic=Number<short int>;
    //------------------------------------------------------------------
    /** 
     * Specialisation of comparison of harmonic to an integer value.
     * This just tests if the value isn't 0x7FFF since that what we
     * use this operation for.
     * 
     * @param n Number to check 
     *
     * @return true if @a n is not 0x7FFF
     *
     * @headerfile "" <correlations/symbolic/Types.hh>
     * @ingroup correlations_symbolic  
     */
    bool operator==(const Harmonic&, int n)
    {
      return n != 0x7FFF;
    }
    const Complex cnull;
  }
  using symbolic::Complex;
  using symbolic::Harmonic;
  using symbolic::Real;
  using symbolic::cnull;
}

#include <correlations/symbolic/Partition.hh>
#include <correlations/MoreTypes.hh>

namespace std
{
  /** 
   * Specialisation of absolute value for a real number representation 
   * 
   * @param x Real number representation 
   * 
   * @return A representation of the evaluation of absolute value 
   *
   * @headerfile "" <correlations/symbolic/Types.hh>
   * @ingroup correlations_symbolic  
   */
  correlations::Real abs(const correlations::Real& x)
  {
    return correlations::Real(std::string('|' + x.s() + '|'));
  }
  /** 
   * Specialisation of absolute value for a whole number representation 
   * 
   * @param x Whole number representation 
   * 
   * @return A representation of the evaluation of absolute value 
   *
   * @headerfile "" <correlations/symbolic/Types.hh>
   * @ingroup correlations_symbolic  
   */
  correlations::Harmonic abs(const correlations::Harmonic& x)
  {
    return correlations::Harmonic(std::string('|' + x.s() + '|'));
  }
  /** 
   * Specialisation of norm of a complex number representation 
   * 
   * @param x Complex number representation 
   * 
   * @return A representation of the evaluation of the norm of a
   * complex number
   *
   * @headerfile "" <correlations/symbolic/Types.hh>
   * @ingroup correlations_symbolic  
   */
  correlations::Real norm(const correlations::Complex& x)
  {
    return correlations::Real(std::string('|' + x.s() + '|'));
  }

  ostream& operator<<(ostream& o, correlations::PartitionHarmonic& h)
  {
    return o << h.s() << "(" << h.p() << ")" << h.ps();
  }
}

      
#endif
// Local Variables:
//   compile-command: make -C ../../ symbolic
// End:
//
// EOF
//
