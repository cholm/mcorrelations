#ifndef CORRELATIONS_QVECTOR_HH
#define CORRELATIONS_QVECTOR_HH
// Note, we use the same header guard as in the normal header so we
// cannot include both definitions at once.
/**
 * @file   correlations/symbolic/QVector.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Declarations of test Q-vector
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <string>
#include <correlations/symbolic/Types.hh>
#include <sstream>
#include <iostream>

namespace correlations
{
  namespace symbolic
  {
    /**
     * A QVector class for symbolic expression evalation 
     * 
     * @headerfile "" <correlations/symbolic/QVector.hh>
     * @ingroup correlations_symbolic  
     */
    struct QVector
    {
      using Complex=correlations::symbolic::Complex;
      /**
       * Constructor - creates a QVector with the specified maximum
       * harmonic order and maximum power of the harmonics
       *
       * @param mN         Maximum harmonic order
       * @param mP         Maximum power
       * @param s          Name of this Q vector
       */
      QVector(Size mN, Size mP, bool, const std::string& s="Q")
	:_maxP(mP), _maxN(mN), _s(s)
      {}
#if 0
      /**
       * Resize the QVector - note this clears all content
       *
       * @param mN New maximum harmonic
       * @param mP New maximum power
       */
      void resize(Size mN, Size mP)
      {
	_maxN = mN;
	_maxP = mP;
      }
#endif
      /**
       * Reset all @f$Q@f$ vector component to @f$0+i0@f$
       *
       */
      void reset(const Complex& c=Complex(0,0))
      {
	[&c]{}(); // Lambda to silence compiler 
      }
      /**
       * @brief Get @f$Q_{n,p}@f$
       *
       * Get the @f$Q_{n,p} = \sum_{i=1}^{M} w_i^pe^{in\phi_i}@f$.
       * Note that @f$Q_{-n,p} = Q_{n,p}^*@f$
       *
       * @param n The harmonic
       * @param p The power
       *
       * @return @f$ Q_{n,p}@f$ or @f$ Q^*_{-n,p}@f$ if @f$n<0@f$
       */
      const Complex operator()(const Harmonic& n, Power p) const
      {
	if (_s.empty()) return cnull;
	return Complex(std::string(_s + "(" + n.s() + ","
				   + std::to_string(p) + ")"));
      }
      /**
       * Fill in an observation
       *
       */
      void fill(Real, Real) {}
      void fill(const RealVector&,const RealVector&) {}
      /**
       * Get the maximum harmonic (minus 1)
       *
       * @return Maximum harmonic minus 1
       */
      Size maxN() const { return _maxN; }
      /**
       * Get the maximum power (minus 1)
       *
       * @return Maximum power (minus 1)
       */
      Power maxP() const { return _maxP; }
      /** 
       * Evaluate the expression to an output stream 
       *
       * @param o Output stream 
       *
       * @return the output stream 
       */
      std::ostream& eval(std::ostream& o) const
      {
	return o << _s;
      }
    protected:
#if 0
      /**
       * @brief Get @f$Q_{n,p}@f$
       *
       * Get the @f$Q_{n,p} = \sum_{i=1}^{M} w_i^pe^{in\phi_i}@f$.
       * Note that @f$Q_{-n,p} = Q_{n,p}^*@f$
       *
       * @param n The harmonic
       * @param p The power
       *
       * @return @f$ Q_{n,p}@f$ or @f$ Q^*_{-n,p}@f$ if @f$n<0@f$
       */
      Complex operator()(const Harmonic& n, Power p)
      {
	if (_s.empty()) return cnull;
	return Complex(std::string(_s + "(" + n.s() + ","
				   + std::to_string(p) + ")"));
      }
      /**
       * @brief Get @f$ Q_{h}@f$
       *
       * Get the @f$ Q_{h} = \sum_{i=1}^{M} w_i^{dim(h)}e^{i\phi_i\sum h}@f$.
       * Note that @f$ Q_{-h} = Q_{h}^*@f$
       *
       * @param h The harmonics
       *
       * @return @f$ Q_{h}@f$ 
       */
      Complex operator()(const PartitionHarmonic& h)
      {
	if (_s.empty()) return cnull;
	if (h.size() <= 0) return cnull; 
	return Complex(std::string(_s + "(" + h.s() + "," + h.p() + ")"));
      }
#endif
      /** 
       * Sanitize the string 
       *
       * @param n Harmonic 
       *
       * @return Sanitized string 
       */
      std::string sanitize(const Harmonic& n, Power) const
      {
	std::stringstream s;
	s << n;
	std::string t = s.str();
	Power p = 0;
	size_t pos = 0;
	while ((pos = t.find_first_of("+-", pos)) != std::string::npos) {
	  p++;
	  pos++;
	}
		
	for (Power q = 1; q <= p; q++) {
	  std::stringstream m;
	  m << " + h" << q << " - h" << q;
	  std::string x = m.str();
	  replaceAll(t, x, "");
	}
	return t;
      }
      /** 
       * Replace all occurences of @a from with @a to in @a s. 
       * 
       * @param s     String to search and replace in 
       * @param from  Search string 
       * @param to    Replacement string 
       */
      static void replaceAll(std::string& s,
			     const std::string& from,
			     const std::string& to)
      {
	size_t pos = 0;
	while ((pos = s.find(from, pos)) != std::string::npos) {
	  s.replace(pos, from.length(), to);
	  pos += to.length();
	}
      }
      /** Probably not needed */
      short _maxP;
      /** Probably not needed */
      short _maxN;
      /** Name of this */
      std::string _s;
    };
    /** 
     * Stream a @f$ Q@f$-vector to output 
     * 
     * @param o Output stream 
     * @param q The @f$ Q@f$-vector
     * 
     * @return the output stream
     */
    std::ostream& operator<<(std::ostream& o, const QVector& q)
    {
      return q.eval(o);
    }
  }
  using symbolic::QVector;
  /** A vector of @f$ Q@f$-vectors */
  using QContainer=std::vector<QVector>;
}
#endif
// Local Variables:
//   compile-command: make -C ../../ symbolic
// End:
//
// EOF
//

