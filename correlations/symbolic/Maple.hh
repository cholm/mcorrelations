#ifndef CORRELATIONS_SYMBOLIC_MAPLE_HH
#define CORRELATIONS_SYMBOLIC_MAPLE_HH
/**
 * @file   correlations/symbolic/Maple.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Print expression to Maple script 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/symbolic/Printer.hh>

namespace correlations
{
  namespace symbolic
  {
    //================================================================
    /** 
     * Printer of expression for Maple
     *
     * @headerfile "" <correlations/symbolic/Printer.hh>
     * @ingroup correlations_symbolic 
     */
    template <typename Harmonics=correlations::HarmonicVector>    
    struct Maple : public Printer<Harmonics>
    {
      using Base=Printer<Harmonics>;
      using HarmonicVector=typename Base::HarmonicVector;
      using QStore=typename Base::QStore;
      using Base::_o;
      using Base::replaceAll;
      
      size_t _n;
      /** 
       * Constructor
       *
       * @param o Output stream
       * @param maxM
       * @param overlap
       * @param npart
       * @param closed
       * @param recursive
       * @param recurrence 
       */
      Maple(std::ostream& o, Size maxM, bool overlap, Size npart,
	    bool closed, bool recursive, bool recurrence)
	: Base(o, maxM, overlap, npart, closed, recursive, recurrence)
      {}
      virtual void begin()
      {
	Complex::i()  = "I";
	Complex::re() = "Re";
	Complex::im() = "Im";
	_o << "<?xml version='1.0' encoding='UTF-8'?>\n"
	   << "<Worksheet>\n"
	   << "  <Version major='2017' minor='3'/>\n"
	   << "  <Label-Scheme value='2' prefix=''/>\n"
	   << "  <Startup-Code startupcode=''/>" << std::endl;
      }	
      /** 
       * Finish off output
       */
      virtual void end()
      {
	_o << "</Worksheet>" << std::endl;
      }
      /** 
       * Print out a comment 
       *
       * @param what The comment text 
       */
      virtual void comment(const std::string& what)
      {
	_o << "<!-- " << what << " -->" << std::endl;
      }
      /** 
       * Print out code
       *
       * @param what The code
       */
      virtual void code(const std::string& what)
      {
	_o << what << std::endl;
      }
      /** 
       * Assign a variable 
       *
       * @param var Variable
       * @param val Value
       *
       * @return String 
       */
      virtual std::string assign(const Real& var,const Real& val)
      {
	std::stringstream s;
	s << var << " := " << val;
	return s.str();
      }
      /** 
       * Assign a variable 
       *
       * @param var Variable
       * @param val Value
       *
       * @return String 
       */
      virtual std::string assign(const Complex& var,const Complex& val)
      {
	std::stringstream s;
	s << var << ":= " << val;
	return s.str();
      }
      /** 
       * Output test of an expression 
       *
       * @param p    Prefix to test 
       * @param q    Prefix to test against 
       */
      virtual void check(const Complex&     p,
			 const Complex&     q)
      {
	_o << "ifelse(evalb(expand(" << p << " - " << q 
	   << ") &lt;&gt; 0)," 
	   << "printf(&quot;" << p << " not equal to " << q 
	   << ": %a\\n&quot;,expand("
	   << p << "-" << q << ")),NULL);" << std::endl;
      }      
    protected:
    };
  }
}
#endif
// Local Variables:
//   compile-command: "make -C ../../ symbolic"
// End:
//
// EOF
//
