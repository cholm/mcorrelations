#ifndef CORRELATIONS_PARTITION_HH
#define CORRELATIONS_PARTITION_HH
/**
 * @file   correlations/symbolic/Partition.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Declarations of more types
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <numeric>
#include <forward_list>
#include <algorithm>

namespace correlations
{
  /** 
   * Type of partition identifier 
   *
   * A partition identifier sets which sub-event a given harmonic
   * belong to.  Suppose we want to calculate the 6-particle
   * correlator with harmonics
   *
   * @f[ \mathbf{h} = \{h_1,h_2,h_3,h_4,h_5,h_6\}\quad,@f] 
   *
   * and we want to do this in two sub-events corresponding to 
   *
   * @f[ \mathbf{h}_L=\{h_1,h_2,h_3\}\quad\mathbf{h}_R=\{h_4,h_5,h_6\}@f]
   * 
   * Then, we'd construct the partition vector 
   *
   * @f[ \mathbf{p} = \{1,1,1,2,2,2\}\quad,@f]
   *
   * and pass that along with the vector @f$ \mathbf{h}@f$ to the
   * calculate method.
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  using Partition=unsigned short;

  namespace symbolic
  {
    /** 
     * Conglomerate of harmonics 
     *
     * @headerfile "" <correlations/Types.hh>
     * @ingroup correlations_types 
     */
    struct PartitionHarmonic
    {
      /** Type of container */
      using Array = std::forward_list<Harmonic>;
      /** Type of container */
      using PArray = std::forward_list<Partition>;
      /** 
       * Constructor 
       */
      PartitionHarmonic() : _h(), _p(), _s(0), _same(true) { }
      PartitionHarmonic(const Harmonic) : _h(), _p(), _s(0), _same(true) {}
      /** 
       * Constructor 
       *
       * @param h Initial value 
       * @param p Partition 
       */
      PartitionHarmonic(const Harmonic h, const Partition p)
	: _h(), _p(), _s(0), _same(true)
      {
	push(h,p);      
      }
      /** 
       * Constructor from initializer list 
       *
       * @param i Initializer of harmonics 
       * @param p Initializer of partitions 
       */
      PartitionHarmonic(std::initializer_list<Harmonic>  i,
			std::initializer_list<Partition> p)
	: _h(i), _p(p), _s(i.size()), _same(calcSame())
      {}
      /** 
       * Constructor from initializer list 
       *
       * @param i Initializer 
       */
      PartitionHarmonic(std::initializer_list<PartitionHarmonic> i)
	: _h(), _p(), _s(0), _same(true)
      {
	for (auto h : i) push(h);
	_same = calcSame();
      }
      PartitionHarmonic& operator=(const std::pair<Harmonic,Partition>& hp)
      {
	_p.clear();
	push(hp.first,hp.second);
	_same = true;
	return *this;
      }
      /** 
       * Add an element to the list of harmonics 
       *
       * @param h Value 
       */
      size_t push(const PartitionHarmonic& h)
      {
	_h.insert_after(_h.before_begin(), h._h.cbegin(), h._h.cend());
	_p.insert_after(_p.before_begin(), h._p.cbegin(), h._p.cend());
	_s += h._s;
	_same = calcSame();
	return h._s;
      }
      /** 
       * Add an element to the list of harmonics 
       *
       * @param h Value 
       * @param p Partition 
       */
      void push(const Harmonic h, const Partition p=0)
      {
	_h.push_front(h);
	_p.push_front(p);
	if (p != _p.front()) _same = false;
	_s++;
      }
      /** 
       * Remove last @a n element from the list of harmonics 
       *
       * @param n of elements to remove 
       */
      void pop(size_t n=1)
      {
	if (n < 1) return;
	if (n == 1) {
	  _h.pop_front();
	  _p.pop_front();
	  _s--;
	}
	else {
	  auto hb= std::next(_h.before_begin() );
	  auto hl= std::next(hb, n);
	  _h.erase_after(hb, hl);
	  
	  auto pb= std::next(_p.before_begin() );
	  auto pl= std::next(pb, n);
	  _p.erase_after(pb, pl);
	  _s -= n;
	}
	_same = calcSame();
      }
      /** 
       * Get the sum of harmonics 
       *
       * @return Sum of harmonics 
       */
      Harmonic sum() const
      {
	Harmonic r = 0;
	return std::accumulate(_h.begin(), _h.end(), r);
      }
      /** 
       * Get the number harmonics 
       *
       * @return Number of harmonics 
       */
      unsigned short size() const
      {
	// Here, we could use partition information to return 0 in case
	// of mixed partitions.

	// Check to see if any two adjacent values are not equal 
	if (!same()) return 0;
	return _s;
      }
      /** 
       * Get the true size 
       */
      unsigned short real_size() const { return _s; }
      /** 
       * Check if all are from the same partition 
       */
      bool same() const { return _same; }
      /** 
       * Get partition if all harmonics are from the same partition,
       * otherwise zero.  Thus, partitions are numbered starting from 1.
       */
      unsigned short partition() const
      {
	return same() ? _p.front() : 0;
      }
      /** 
       * Get maximum harmonic 
       */
      size_t max() const
      {
	return _max<Harmonic>();
      }
      /** 
       * Concatenate lists of harmonics 
       *
       * @param other Other list 
       *
       * @return Reference to this 
       */
      PartitionHarmonic& operator+=(const PartitionHarmonic& other)
      {
	push(other);
	return *this;
      }
#if 0
      PartitionHarmonic& operator-=(const PartitionHarmonic& other)
      {
	pop(other.size());
	return *this;
      }
#endif
      /** 
       * Access the string 
       *
       * @return the string 
       */
      const std::string s() const 
      {
	std::stringstream str;
	bool first = true;
	for (auto& h : _h) {
	  _os(str,first,h);
	  first = false;
	}
	return str.str();
      }
      /** 
       * Get power of harmonics as a string 
       */
      const std::string p() const { return std::to_string(_s); }
      /** 
       * Get partitions as a string 
       */
      const std::string ps() const
      {
	std::stringstream str;
	str << "[";
	for (PArray::const_iterator i = _p.cbegin(); i != _p.cend(); ++i) {
	  if (i != _p.cbegin()) str << ",";
	  str << *i;
	}
	str << "]";
	return str.str();
      }
    protected:
      /** 
       * Check if all are from the same partition 
       */
      bool calcSame() const
      {
	return (_s <= 1 ||
		std::adjacent_find(_p.begin(),_p.end(),
				   std::not_equal_to<Partition>()) == _p.end());
      }
      template <typename T,
		typename
		std::enable_if<!std::is_integral<T>::value,int>::type = 0>
      void _os(std::ostream& o, bool first, const T& h) const
      {
	if (!first || h.s()[0] == '-') o << " + ";
	o << h.s();
      }
      template <typename T,
		typename
		std::enable_if<std::is_integral<T>::value,int>::type = 0>
      void _os(std::ostream& o, bool first, const T& h) const
      {
	if (!first || h < 0) o << " + ";
	o << h;
      }
      /** 
       * Maximum number of particles needed by harmonics 
       *
       * @return @f$ \sum_{i} |h_i|@f$ 
       */
      template <typename T,
		typename
		std::enable_if<std::is_integral<T>::value,int>::type = 0>
      size_t _max() const
      {
	size_t max = 0;
	return size_t(std::accumulate(std::begin(_h),std::end(_h), max,
				      [](const T& h1, const T& h2) {
					return short(habs(h1)+habs(h2)); }));
      }
      template <typename T,
		typename
		std::enable_if<!std::is_integral<T>::value,int>::type=0>
      size_t _max() const
      {
	return std::distance(_h.cbegin(),_h.cend());
      }
      /** The container */
      Array _h;
      /** Container of sub-partitions */
      PArray _p;
      /** Size count */
      unsigned short _s;
      bool _same;
    };

    /** 
     * Sum operator for harmonics calculations 
     *
     * @param lhs Left-hand side 
     * @param rhs Right-hand sid 
     *
     * @return Summed harmonics 
     *
     * @headerfile "" <correlations/Types.hh>
     * @ingroup correlations_types 
     */
    PartitionHarmonic operator+(const PartitionHarmonic& lhs,
				const PartitionHarmonic& rhs)
    {
      PartitionHarmonic t(lhs); t += rhs;
      return t;
    }
  }
  using symbolic::PartitionHarmonic;
}
#include <valarray>


#endif
//
// EOF
//
