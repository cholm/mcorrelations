#ifndef CORRELATIONS_SYMBOLIC_SYMPY_HH
#define CORRELATIONS_SYMBOLIC_SYMPY_HH
/**
 * @file   correlations/symbolic/SymPy.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Print expression to Sympy script 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/symbolic/Printer.hh>
#include <correlations/flow/Calculations.hh>

namespace correlations
{
  namespace symbolic
  {
    //================================================================
    /** 
     * Printer of expression for SymPy 
     *
     * @headerfile "" <correlations/symbolic/Printer.hh>
     * @ingroup correlations_symbolic 
     */
    template <typename Harmonics=correlations::HarmonicVector>    
    struct SymPy : public Printer<Harmonics>
    {
      using Base=Printer<Harmonics>;
      using HarmonicVector=typename Base::HarmonicVector;
      using QStore=typename Base::QStore;
      using Base::_o;
      using Base::gobble;
      using Base::replaceAll;
      /** 
       * Constructor 
       */
      SymPy(std::ostream& o, Size maxM, bool overlap, Size npart,
	    bool closed, bool recursive, bool recurrence)
	: Base(o, maxM, overlap, npart, closed, recursive, recurrence)
      {}
      /** 
       * Start of code 
       */
      virtual void begin()
      {
	Base::begin();
	Complex::i()  = "I";
	Complex::re() = "re";
	Complex::im() = "im";
	_import();
	_compare();
	_partition();
      }	
      /** 
       * Write a comment 
       * 
       * @param what What to put in the comment
       */
      virtual void comment(const std::string& what)
      {
	std::string t = replaceAll(gobble(what), "\n", "\n# ");
	_o << "# " << t << std::endl;
      }
      /** 
       * Write out code 
       *
       * @param source Source to put out 
       */
      virtual void code(const std::string& source)
      {
	_o << source << std::endl;
      }
      /** 
       * Set-up for output 
       *
       * @param maxM Largest correlator (number of particles)
       */
      virtual void setup(Size maxM)
      {
	auto h = this->makeH(maxM);
	_declareHQ(h);
      }
      /** 
       * Declare a variable 
       *
       * @param var Variable
       *
       * @return String 
       */
      virtual std::string declare(const Real& var)
      {
	std::stringstream s;
	s << var << " = symbols(\"" << var << "\",real=True)\n";
	return s.str();
      }
      /** 
       * Declare a variable 
       *
       * @param var Variable
       *
       * @return String 
       */
      virtual std::string declare(const Complex& var)
      {
	std::stringstream s;
	s << var << " = symbols(\"" << var << "\",complex=True)\n";
	return s.str();
      }
      /** 
       * Output test of an expression 
       *
       * @param p    Prefix to test 
       * @param q    Prefix to test against 
       */
      virtual void check(const Complex& p,
			 const Complex& q)
      {
	std::stringstream s;
	s << "compare('" << q << "','" << p << "',"
	  << q << "," << p << ")\n";
	code(s.str());
      }
      /** 
       * Output test of an expression 
       *
       */
      virtual void checkP(const Complex&,
			  const Complex&,
			  const HarmonicVector&)
      {
      }
      /** 
       * Print a progress message
       * 
       * @param p 
       */
      virtual void progress(const std::string& p)
      {
	code(p);
      }
    protected:
      /** 
       * @{ 
       * @name Specific initialisation methods 
       */
      /** Make import statement */
      virtual void _import()
      {
	comment("SECTION: Set-up");
	std::stringstream s;
	s << "from sympy import *\n"
	  << "init_printing()\n";
	code(s.str());
      }
      /** Create compare function */
      virtual void _compare()
      {
	std::stringstream s;
	comment("SUBSECTION: Comparison function");
	s << "def compare(n1,n2,c1,c2):\n"
	  << "    '''Compare expressions c1 and c2 (with names n1 and n2)'''\n"
	  << "    print('  {} vs {} '.format(n1,n2),end='')\n"
	  << "    \n"
	  << "    if expand(c1 - c2) == 0:\n"
	  << "        print('OK')\n"
	  << "        return\n"
	  << "    \n"
	  << "    print('Failed')\n"
	  << "    print('   {}:'.format(n1))\n"
	  << "    pprint(expand(c1))\n"
	  << "    print('   {}:'.format(n2))\n"
	  << "    pprint(expand(c2))\n"
	  << "    print('   {}-{} ->'.format(n1,n2))\n"
	  << "    pprint(expand(c1 - c2))\n"
	  << "    # raise\n";
	code(s.str());
      }
      /** Create partitioning function */
      virtual void _partition()
      {
	std::stringstream s;
	comment("SUBSECTION: Partition building function");
	s << "def makePar(expr,subs):\n"
	  << "    '''Build partitioned expression from full expression'''\n"
	  << "    def rep(*args):\n"
	  << "        has = [False]*len(subs)\n"
	  << "        n   = 0\n"
	  << "        for e in preorder_traversal(args):\n"
	  << "            for i,s in enumerate(subs):\n"
	  << "                has[i] = 1 if (e is s) or (e in s) else has[i]\n"
	  << "                if sum(has) > 1:\n"
	  << "                    return 0\n"
	  << "                n = i + 1 if has[i] == 1 else n\n"
	  << "        f = symbols(str(args[0].func)[:1]+str(n),cls=Function)\n"
	  << "        return f(*args[0].args)\n"
	  << "    \n"
	  << "    return expr.replace(lambda e: e.is_Function, rep)\n";
	code(s.str());
      }
      /** 
       * Specific functions for declaring stuff 
       */
      virtual std::string _declareQ(const QVector& q)
      {
	std::stringstream o;
	o << q << " = symbols(\"" << q << "\",cls=Function)";
	return o.str();
      }
      virtual std::string _declareH(const std::string& hs)
      {
	std::stringstream o;
	o << hs << " = symbols(\"" << hs << "\",integer=True)";
	return o.str();
      }
      virtual std::string _declareC(const std::string& hs)
      {
	std::stringstream o;
	o << hs << " = symbols(\"" << hs << "\")";
	return o.str();
      }
      /** 
       * Declare the variables and functions needed
       * 
       * @param h  Harmonic vector 
       */
      virtual void _declareHQ(const HarmonicVector& h)
      {
	std::stringstream hs;
	for (size_t i = 0; i < h.size(); i++) {
	  if (i != 0) hs << ",";
	  hs << h[i];
	}

	comment("SUBSECTION: Symbols used");
	std::stringstream s;
	s << _declareH(hs.str()) << "\n"
	  << _declareQ(this->_t.rvec()) << "\n"
	  << _declareQ(this->_t.pvec()) << "\n"
	  << _declareQ(this->_t.qvec());

	code(s.str());
      }
      
    };
    template <>
    void SymPy<PartitionHarmonicVector>::_declareHQ(const HarmonicVector& h)
    {
      std::stringstream hs;
      for (size_t i = 0; i < h.size(); i++) {
	if (i != 0) hs << ",";
	hs << h[i].sum();
      }
      comment("SUBSECTION: Symbols used");
      std::stringstream s;
      s << _declareH(hs.str()) << "\n";

      for (auto& r : this->_t.rvec())
	s << _declareQ(r.get()) << "\n";
      s << _declareQ(this->_t.pvec()[0].get()) << "\n"
	<< _declareQ(this->_t.qvec()[0].get());
      
      code(s.str());      
    }
    /** 
     * Output test of an expression 
     *
     * @param p    Prefix to test 
     * @param q    Prefix to test against 
     * @param h    Harmonic vector used 
     */
    template <>
    void SymPy<PartitionHarmonicVector>::checkP(const Complex&        p,
						const Complex&        q,
						const HarmonicVector& h)
    {
      size_t m = h.size();
      std::stringstream s;
      std::vector<std::vector<Harmonic>> o(m);
      for (size_t i = 0; i < h.size(); i++) 
	o[h[i].partition()-1].push_back(h[i].sum());

      comment("Partitions of " + p.s());
      s << p << "_part = [";
      for (auto& i : o) {
	s << "[";
	for (auto j : i) s << j << ",";
	s << "],";
      }
      s << "]";
      code(s.str());

      comment("Compare partitioned " + p.s() + " to " + q.s());
      s.str("");
      s << p << "_pp"   << " = makePar(" << p << "," << p << "_part)\n"
	<< "compare('" << p << "_pp','" << q << "',"
	<< p << "_pp" << "," << q << ")";
      code(s.str());
    }
  }
}
#endif
// Local Variables:
//   compile-command: "make -C ../../ symbolic"
// End:
//
// EOF
//
