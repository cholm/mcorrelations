#ifndef CORRELATIONS_SYMBOLIC_JUPYTER_HH
#define CORRELATIONS_SYMBOLIC_JUPYTER_HH
/**
 * @file   correlations/symbolic/Jupyter.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Base class for Jupyter notebooks 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/symbolic/Printer.hh>

namespace correlations
{
  namespace symbolic
  {
    //================================================================
    /**
     * Base class for jupyter notebook printers 
     */
    struct Jupyter
    {
      /** Output stream */
      std::ostream& _o;
      /** Cell counter */
      size_t _n;
      /** Vector of lines */
      typedef std::vector<std::string> Lines;
      /** 
       * Constructor 
       */
      Jupyter(std::ostream& o) : _o(o), _n(0) {}
      /** 
       * Destructor 
       */
      virtual ~Jupyter() {}
      /** 
       * Replace all occurences of @a from with @a to in @a s. 
       * 
       * @param s     String to search and replace in 
       * @param from  Search string 
       * @param to    Replacement string 
       * 
       * @return New string with all occurances of @a from replaced by
       * @a to
       */
      static std::string replaceAll(const std::string& s,
				    const std::string& from,
				    const std::string& to)
      {
	std::string r(s);
	size_t pos = 0;
	while ((pos = r.find(from, pos)) != std::string::npos) {
	  r.replace(pos, from.length(), to);
	  pos += to.length();
	}
	return r;
      }
      /** 
       * Expand headers to markdown 
       * 
       * @param s String 
       * 
       * @return New string 
       */
      static std::string expand(const std::string& s)
      {
	std::string r(s);
	if (r.substr(0,6)  == "TITLE:") {
	  r.replace(0,6,  "# ");
	  r += "<a class='tocSkip'></a>";
	}
	if (r.substr(0,8)  == "SECTION:")       r.replace(0,8,  "# ");
	if (r.substr(0,11) == "SUBSECTION:")    r.replace(0,11, "## ");
	if (r.substr(0,14) == "SUBSUBSECTION:") r.replace(0,14, "### ");
	if (r.substr(0,11) == "EOF")            r.replace(0,3,  "");
	return r;
      }
      /** 
       * Write out the beginning 
       */
      virtual void begin()
      {
	_o << "{\n"
	   << " \"cells\": [" << std::endl;
      }
      /** 
       * Write out the end 
       */
      virtual void end(const std::string& lang,
		       const std::string& kernel)
      {
	_o << "\n"
	   << " ],\n"
	   << " \"metadata\": {\n"
	   << "  \"kernelspec\": {\n"
	   << "   \"display_name\": \"" << lang << "\",\n" 
	   << "   \"language\": \"" << lang << "\",\n"
	   << "   \"name\": \"" << kernel << "\"\n"
	   << "  }\n"
	   << " },\n"
	   << " \"nbformat\": 4,\n"
	   << " \"nbformat_minor\": 2\n"
	   << "}" << std::endl;
      }
      /** 
       * write out a cell 
       * 
       * @param type Cell type 
       * @param what Body of cell 
       */
      virtual void cell(const std::string& type,
			const std::string& what)
      {	
	if (_n > 0)  _o << ",\n";

	bool        isCode = type == "code";
	std::string t;
	if (isCode) t = replaceAll(what, "\"", "\\\"");
	else        t = expand(what);
	Lines lines = split(t);
	
	_n++;
	_o << " {\n"
	   << "  \"cell_type\": \"" << type << "\",\n"
	   << "  \"metadata\": {},\n";
	if (isCode)
	  _o << "  \"execution_count\": null,\n"
	     << "  \"outputs\": [],\n";

	_o << "  \"source\": [";
	for (size_t i = 0; i < lines.size(); i++) {
	  if (i != 0) _o << ",";
	  _o << "\n    \"" << lines[i];
	  if (i != lines.size()-1) _o << "\\n";
	  _o << "\"";
	}
	_o << "\n  ]\n"
	   << " }" << std::flush;
      }
      /** 
       * Split lines 
       */
      static Lines split(const std::string& s)
      {
	Lines l;
	std::stringstream t(s);
	std::string       k;
	while (std::getline(t, k, '\n'))
	  l.push_back(k);
	return l;
      }
    };
  }
}
#endif
// Local Variables:
//   compile-command: "make -C ../../ symbolic"
// End:
//
// EOF
//
