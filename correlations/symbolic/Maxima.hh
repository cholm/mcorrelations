#ifndef CORRELATIONS_SYMBOLIC_MAXIMA_HH
#define CORRELATIONS_SYMBOLIC_MAXIMA_HH
// Note, we use the same header guard as in the normal header so we
// cannot include both definitions at once.
/**
 * @file   correlations/symbolic/Maxima.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Print expression to Maxima script 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/symbolic/Printer.hh>

namespace correlations
{
  namespace symbolic
  {
    //================================================================
    /** 
     * Printer of expression for Maxima 
     *
     * @headerfile "" <correlations/symbolic/Printer.hh>
     * @ingroup correlations_symbolic 
     */
    template <typename Harmonics=correlations::HarmonicVector>    
    struct Maxima : public Printer<Harmonics>
    {
      using Base=Printer<Harmonics>;
      using HarmonicVector=typename Base::HarmonicVector;
      using QStore=typename Base::QStore;
      using Base::_o;
      using Base::replaceAll;
      /** 
       * Constructor 
       */
      Maxima(std::ostream& o, Size maxM, bool overlap, Size npart,
	    bool closed, bool recursive, bool recurrence)
	: Base(o, maxM, overlap, npart, closed, recursive, recurrence)
      {}
      /** 
       * Generate comment 
       *
       * @param what Body of comment 
       */
      virtual void comment(const std::string& what)
      {
	std::stringstream s;
	s << "/* " << what << " */";
	_o << s.str() << std::endl;
      }
      /** 
       * Generate code 
       * 
       * @param what Body of code 
       */
      virtual void code(const std::string& what)
      {
	_o << what << "$" << std::endl;
      }
      virtual void begin()
      {
	comment("-*- mode: maxima -*-");
	comment("Evalulates expression of m-particle correlators\n"
		"   and compares to close form expression.");
	
	Complex::i()  = "%i";
	Complex::re() = "realpart";
	Complex::im() = "imagpart";
      }
      
      /** 
       * Set-up for output 
       *
       * @param maxM Largest harmonic to use 
       */
      virtual void setup(Size maxM)
      {
	auto h = this->makeH(maxM);
	std::stringstream hs;	
	hs << "declare(";
	for (size_t i = 0; i < maxM; i++) {
	  if (i != 0) hs << ",";
	  hs << h[i] << ",integer";
	}
	hs << ")";
	code(hs.str());
      }
      /** 
       * Declare a variable 
       *
       * @param var Variable
       *
       * @return String 
       */
      virtual std::string declare(const Real& var)
      {
	std::stringstream s;
	s << "declare(" << var << ",real)";
	return s.str();
      }
      /** 
       * Assign a variable 
       *
       * @param var Variable
       * @param val Value
       *
       * @return String 
       */
      virtual std::string assign(const Real& var,const Real& val)
      {
	std::stringstream s;
	s << var << ": " << val;
	return s.str();
      }
      /** 
       * Declare a variable 
       *
       * @param var Variable
       *
       * @return String 
       */
      virtual std::string declare(const Complex& var)
      {
	std::stringstream s;
	s << "declare(" << var << ",complex)";
	return s.str();
      }
      /** 
       * Assign a variable 
       *
       * @param var Variable
       * @param val Value
       *
       * @return String 
       */
      virtual std::string assign(const Complex& var,const Complex& val)
      {
	std::stringstream s;
	s << var << ": " << val;
	return s.str();
      }

      /** 
       * Output test of an expression 
       *
       * @param p    Prefix to test 
       * @param q    Prefix to test against 
       */
      virtual void check(const Complex&         p,
			 const Complex&         q) 
      {
	std::stringstream d;
	d << "expand(" << q << "-" << p << ")";

	std::stringstream s;
	s << "if " << d.str() << " # 0 then\n"
	  << "  error(\"" << p << " not equal to " << q << "\","
	  << d.str() << ")";
	code(s.str());
      }
    };
  }
}
#endif
// Local Variables:
//   compile-command: "make -C ../../ symbolic"
// End:
//
// EOF
//
