#ifndef CORRELATIONS_SYMBOLIC_SAGE_HH
#define CORRELATIONS_SYMBOLIC_SAGE_HH
// Note, we use the same header guard as in the normal header so we
// cannot include both definitions at once.
/**
 * @file   correlations/symbolic/SymPy.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Print expression to Sympy script 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/symbolic/SymPy.hh>

namespace correlations
{
  namespace symbolic
  {
    //================================================================
    /** 
     * Printer of expression for Sage 
     *
     * @headerfile "" <correlations/symbolic/Printer.hh>
     * @ingroup correlations_symbolic 
     */
    template <typename HarmonicVec=correlations::HarmonicVector>    
    struct Sage : public SymPy<HarmonicVec>
    {
      using Base=SymPy<HarmonicVec>;
      /** 
       * Constructor 
       */
      Sage(std::ostream& o, Size maxM, bool overlap, Size npart,
	    bool closed, bool recursive, bool recurrence)
	: Base(o, maxM, overlap, npart, closed, recursive, recurrence)
      {}
      /** 
       * Declare a variable 
       *
       * @param var Variable
       *
       * @return String 
       */
      virtual std::string declare(const Real& var)
      {
	std::stringstream s;
	s << var << " = var(\"" << var << "\")\n";
	return s.str();
      }
      /** 
       * Declare a variable 
       *
       * @param var Variable
       *
       * @return String 
       */
      virtual std::string declare(const Complex& var)
      {
	std::stringstream s;
	s << var << " = var(\"" << var << "\",domain=ZZ)\n";
	return s.str();
      }
      virtual void _declareQ(const QVector& q, std::ostream& o)
      {
	o << q << " = function(\"" << q << "\")\n";
      }
      virtual void _declareH(const std::string& hs, std::ostream& o)
      {
	o << hs << " = var(\"" << hs << "\",domain=ZZ)\n";
      }
      virtual void _import() {}
      virtual void _compare()
      {
	std::stringstream s;
	this->comment("SUBSECTION: Comparison function");
	s << "def compare(n1,n2,c1,c2):\n"
	  << "    '''Compare expressions c1 and c2 (with names n1 and n2)'''\n"
	  << "    print('  {} vs {} '.format(n1,n2))\n"
	  << "    if expand(c1 - c2) == 0:\n"
	  << "        print('OK')\n"
	  << "        return\n"
	  << "    print('Failed')\n"
	  << "    print('   {}:'.format(n1))\n"
	  << "    pprint(expand(c1))\n"
	  << "    print('   {}:'.format(n2))\n"
	  << "    pprint(expand(c2))\n"
	  << "    print('   {}-{} ->'.format(n1,n2))\n"
	  << "    pprint(expand(c1 - c2))\n"
	  << "    # raise\n";
	this->code(s.str());
      }
      virtual void _partition() {}
    };
  }
}
#endif
// Local Variables:
//   compile-command: "make -C ../../ symbolic"
// End:
//
// EOF
//
