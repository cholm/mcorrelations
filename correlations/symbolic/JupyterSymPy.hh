#ifndef CORRELATIONS_SYMBOLIC_JUPYTERSYMPY_HH
#define CORRELATIONS_SYMBOLIC_JUPYTERSYMPY_HH
/**
 * @file   correlations/symbolic/JupyterSymPy.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Print expression to Jupyter/Sympy script 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/symbolic/Jupyter.hh>
#include <correlations/symbolic/SymPy.hh>

namespace correlations
{
  namespace symbolic
  {
    //================================================================
    /** 
     * Printer of expression for JupyterSymPy 
     *
     * @headerfile "" <correlations/symbolic/Printer.hh>
     * @ingroup correlations_symbolic 
     */
    template <typename Harmonics=correlations::HarmonicVector>    
    struct JupyterSymPy : public SymPy<Harmonics>, public Jupyter
    {
      using Base=SymPy<Harmonics>;
      using QStore=typename Base::QStore;
      using HarmonicVector=typename Base::HarmonicVector;

      JupyterSymPy(std::ostream& o, Size maxM, bool overlap, Size npart,
	    bool closed, bool recursive, bool recurrence)
	: Base(o, maxM, overlap, npart, closed, recursive, recurrence),
	  Jupyter(o)
      {}
      /** 
       * Write out a comment 
       * 
       * @param what Comment body 
       */
      virtual void comment(const std::string& what)
      {
	cell("markdown", what);
      }
      /** 
       * Write out a code block 
       * 
       * @param what Code 
       */
      virtual void code(const std::string& what)
      {
	cell("code", what);
      }
      /** 
       * Print a progress message
       */
      virtual void progress(const std::string&){}
      /** 
       * Set-up for output 
       */
      virtual void begin()
      {
	Jupyter::begin();
	Base::begin();
	code("def show(name,expr):\n"
	     "    '''Shows value of expression'''\n"
	     "    from IPython.display import Latex\n"
	     "    display(Latex('$'+name+'='+latex(expr)+'$'))\n");
      }
      /** 
       * Finish off output
       */
      virtual void end()
      {
	Base::end();
	Jupyter::end("python","python3");
      }
    };
  }
}
#endif
// Local Variables:
//   compile-command: "make -C ../../ symbolic"
// End:
//
// EOF
//
