#ifndef CORRELATIONS_SYMBOLIC_PRINTER_HH
#define CORRELATIONS_SYMBOLIC_PRINTER_HH
/**
 * @file   correlations/symbolic/Printer.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Print expression 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/symbolic/QVector.hh>
#include <correlations/QStore.hh>
#include <correlations/Result.hh>
#include <correlations/Correlator.hh>
#include <correlations/flow/Calculations.hh>
#include <correlations/Recursive.hh>
#include <correlations/Recurrence.hh>
#include <correlations/Closed.hh>
#include <map>
#include <iostream>
#include <iomanip>
#include <sstream>
#ifndef NOTUSED
# define NOTUSED  __attribute__((unused))
#endif

namespace correlations
{
  namespace symbolic
  {
#if 0
    template <typename T>
    std::ostream& operator<<(std::ostream& o, const std::valarray<T>& v)
    {
      o << "[";
      for (size_t i = 0; i < v.size(); i++) o << (i != 0 ? ", " : "") << v[i];
      return o << "]";
    }
#endif
    /** 
     * @example symbolic.cc 
     *
     * Export symbolic expression to some CAS system 
     */
    //================================================================
    /** 
     * Base-base class for printers of expressions 
     */
    struct PrinterBase
    {
      using QVector=correlations::QVector;
      using Calculations=correlations::flow::Calculations<Real>;
      using ValueVector=typename Calculations::ValueVector;
      
      std::ostream& _o;
      /** 
       * Constructor 
       *
       * @param o Output stream 
       */
      PrinterBase(std::ostream& o) : _o(o) {}
      /** 
       * Do iniitial stuff for the output format 
       */
      virtual void begin()
      {
	comment("TITLE:m-particle correlations");
	comment("Evalulates expression of m-particle correlators \n"
		"and compares to close form expression.");
	comment("This is split into various sections.  First, we \n"
		"define correlators up to a given power. The \n"
		"correlators are defined for both the integrated and \n"
		"differential case.  In each instance, multiple \n"
		"correlator algorithms may have been used, in which case \n"
		"we compare them for equality.\n\n"
		"Next, we define cumulants and  flow expressions, as well \n"
		"as their derivatives.");
      }
      /** 
       * Set up for maximum @f$ m@f$ 
       *
       * @note Parameters
       * 
       * - maxM Largest correlator (number of parcticles)
       */
      virtual void setup(Size) {}
      /** 
       * Declare a variable 
       *
       * @param var Variable
       *
       * @return String 
       */
      virtual std::string declare(const Real& var)
      {
	return assign(var, Real("0"));
      }
      /** 
       * Assign a variable 
       *
       * @param var Variable
       * @param val Value
       *
       * @return String 
       */
      virtual std::string assign(const Real& var,const Real& val)
      {
	std::stringstream s;
	s << var << " = " << val;
	return s.str();
      }
      /** 
       * Declare a variable 
       *
       * @param var Variable
       *
       * @return String 
       */
      virtual std::string declare(const Complex& var)
      {
	return assign(var, Complex("0+0"+Complex::i()));
      }
      /** 
       * Assign a variable 
       *
       * @param var Variable
       * @param val Value
       *
       * @return String 
       */
      virtual std::string assign(const Complex& var,const Complex& val)
      {
	std::stringstream s;
	s << var << " = " << val;
	return s.str();
      }
      /** 
       * Output test of an expression 
       *
       * @param p    Prefix to test 
       * @param q    Prefix to test against 
       */
      virtual void check(const Complex& p,
			 const Complex& q) = 0;
      /** 
       * Print out a comment 
       *
       * @param what The comment text 
       */
      virtual void comment(const std::string& what) = 0;
      /** 
       * Print out code
       *
       * @param what The code
       */
      virtual void code(const std::string& what) = 0;
      /** 
       * Do final stuff for the output format 
       */
      virtual void end()
      {
	comment("EOF");
      }
      /* @} */
      /** 
       * @{ 
       * @name General utilities 
       */
      /** 
       * Make a @f$ Q@f$-Vector
       *
       * @param m    Harmonic power
       * @param name Name @f$ Q@f$-vector
       *
       * @return @f$ Q@f$-vector
       */
      static QVector makeQ(Size m, const std::string& name)
      {
	return QVector(m*m, m, true, name);
      }
      /** 
       * Replace all occurences of @a from with @a to in @a s. 
       * 
       * @param s     String to search and replace in 
       * @param from  Search string 
       * @param to    Replacement string 
       * 
       * @return New string with all occurances of @a from replaced by
       * @a to
       */
      static std::string replaceAll(const std::string& s,
				    const std::string& from,
				    const std::string& to)
      {
	std::string r(s);
	size_t pos = 0;
	while ((pos = r.find(from, pos)) != std::string::npos) {
	  r.replace(pos, from.length(), to);
	  pos += to.length();
	}
	return r;
      }
      /** 
       * Remove any sectioning marker 
       * 
       * @param s String to process
       * 
       * @return New string 
       */
      static std::string gobble(const std::string& s)
      {
	std::string r(s);
	if (r.substr(0,6)  == "TITLE:")          r.erase(0,6);
	if (r.substr(0,8)  == "SECTION:")        r.erase(0,8);
	if (r.substr(0,11) == "SUBSECTION:")     r.erase(0,11);
	if (r.substr(0,14) == "SUBSUBSECTION:")  r.erase(0,14);
	return r;
      }
      /* @} */
      /** 
       * @{ 
       * @name Specific tasks 
       */
      virtual void correlators(Size maxM) = 0;
      /** 
       * Write out code for the cumulants.  
       *
       * Note, this assumes that the average correlators of lower
       * power have been declared already.
       */
      void cumulant(Size m)
      {
	Size k =  m/2;
	comment("SUBSUBSECTION: "+std::to_string(m)+"-particle cumulant");

	ValueVector cs(k);
	ValueVector ds(k);

	for (size_t i = 0; i < k; i++) {
	  cs[i] = Real("Cbar"+std::to_string(2*(k-i)));
	  ds[i] = Real("Dbar"+std::to_string(2*(k-i)));
	}
	
	std::stringstream s;
	s << declare(cs[0]) << "\n"
	  << declare(ds[0]);
	code(s.str());

	comment("Cumulants");
	s.str("");
	Real cnm("c_n"+std::to_string(m));
	Real dnm("d_n"+std::to_string(m));
	s << assign(cnm, Calculations::cnm(cs)) << "\n"
	  << assign(dnm, Calculations::dnm(ds,cs[std::slice(1,k-1,1)]));
	code(s.str());

	comment("Derivatives of cumulants. `dx_dy` is $dx/dy$");
	s.str("");
	for (Size i = 0; i < k; i++) {
	  Real dcnm = Calculations::dcnm(cs,i);
	  Real ddnm = Calculations::ddnm(ds,cs[std::slice(1,k-1,1)],i);
	  if (i != 0) s << "\n";
	  s << assign(Real("d"+cnm.s()+"_d"+cs[i].s()), dcnm) << "\n"
	    << assign(Real("d"+dnm.s()+"_d"+ds[i].s()), ddnm);

	  if (i == 0) continue;

	  ddnm = Calculations::ddnm(ds,cs[std::slice(1,k-1,1)],-1,i-1);
	  s << "\n"
	    << assign(Real("d"+dnm.s()+"_d"+cs[i].s()), ddnm);
	}
	code(s.str());
      }
      /** 
       * Create expression for cumulants and their derivatives up to
       * power @a maxM
       */
      void cumulants(Size maxM)
      {
	comment("SUBSECTION: Cumulants");
	if (maxM % 2 == 1) maxM++;  // Force even 
	for (Size m = 2; m <= maxM; m += 2) cumulant(m);
      }
      /** 
       * Write sub-subsection of flow 
       *
       * Create expression for flow and derivatives.  This assumes
       * that the relevant cumulants have already been declared.
       *
       * @param m Power 
       */
      void flow(Size m)
      {
	comment("SUBSUBSECTION: "+std::to_string(m)+"-particle flow");
	
	Real cnm("c_n"+std::to_string(m));
	Real dnm("d_n"+std::to_string(m));
	Real vnm("v_n"+std::to_string(m));
	Real vpnm("vp_n"+std::to_string(m));

	comment("Flow");

	std::stringstream s;
	s << assign(vnm,  Calculations::vnm (m,cnm)) << "\n"
	  << assign(vpnm, Calculations::vpnm(m,vnm,dnm,cnm));
	code(s.str());
	
	comment("Derivatives of flow. `dx_dy` is $dx/dy$");
	s.str("");
	s << assign(Real("d"+vnm.s()+"_d"+cnm.s()),
		     Calculations::dvnm(m,vnm,cnm)) << "\n";
	auto dvpnm = Calculations::dvpnm(m,vpnm,vnm,dnm,cnm);
	s << assign(Real("d"+vpnm.s()+"_d"+dnm.s()),dvpnm[0]) << "\n"
	  << assign(Real("d"+vpnm.s()+"_d"+cnm.s()),dvpnm[1]);
	code(s.str());
      }
      /** 
       * Write subsection for flows
       *
       * @param maxM Largest power
       */
      void flows(Size maxM)
      {
	comment("SUBSECTION: Flow expressions");
	if (maxM % 2 == 1) maxM++;  // Force even 
	for (Size m = 2; m <= maxM; m += 2) flow(m);
      }
      /** 
       * Write subsection both flow an cumulants 
       *
       * @param m Power 
       */
      void cumulant_flow(Size m)
      {
	comment("SUBSECTION: "+std::to_string(m)+" particles");
	cumulant(m);
	flow(m);
      }
      /** 
       * Write out section of cumulants and flows 
       */
      void cumulants_flows(Size maxM)
      {
	comment("SECTION: Cumulant and flow expressions");
	if (maxM % 2 == 1) maxM++;  // Force even 
	for (Size m = 2; m <= maxM; m += 2) cumulant_flow(m);
      }
      /** 
       * Run everything 
       */
      void run(Size maxM) 
      {
	setup(maxM);
	correlators(maxM);
	cumulants_flows(maxM);
      }

      
    };

    //----------------------------------------------------------------
    template <typename HV>
    void fillMap(std::map<std::string,correlations::Correlator<HV>*>& cm,
		 correlations::QStore<HV>& s, const std::string& p,
		 bool closed, bool recursive, bool recurrence)
    {
      using Closed=correlations::Closed<HV>;
      using Recursive=correlations::Recursive<HV>;
      using Recurrence=correlations::Recurrence<HV>;

      if (closed)     cm[p+"C"] = new Closed    (s);
      if (recursive)  cm[p+"R"] = new Recursive (s);
      if (recurrence) cm[p+"U"] = new Recurrence(s);
    }
    //----------------------------------------------------------------
    /** Base template */
    template <typename Harmonics=correlations::HarmonicVector>
    struct Printer {};

    //----------------------------------------------------------------
    /** 
     * Base class for printer of expression.  Specialisation for
     * non-partitioned correlators.
     *
     * @headerfile "" <correlations/symbolic/Printer.hh>
     * @ingroup correlations_symbolic 
     */
    template <>
    struct Printer<correlations::HarmonicVector> : PrinterBase
    {
      using Base=PrinterBase;
      using HarmonicVector=correlations::HarmonicVector;
      using HarmonicElement=typename HarmonicVector::value_type;
      using QVector=Base::QVector;
      using QStore=correlations::QStore<HarmonicVector>;
      using Correlator=correlations::Correlator<HarmonicVector>;
      using CorrelatorMap=std::map<std::string,Correlator*>;
      using Calculations=Base::Calculations;
      using ValueVector=Base::ValueVector;

      QVector _rv;
      QVector _pv;
      QVector _qv;
      QStore  _s;
      QStore  _t;
      CorrelatorMap _cm;
      CorrelatorMap _dm;
      
      /** 
       * Constructor 
       *
       * @param o Output stream 
       * @param maxM
       * @param overlap
       * @param npart Not used
       * @param closed
       * @param recursive
       * @param recurrence 
       */
      Printer(std::ostream& o,
	      Size maxM,
	      bool overlap,
	      Size npart NOTUSED,
	      bool closed,
	      bool recursive,
	      bool recurrence)
	: Base(o),
	  _rv(makeQ(maxM, "r")),
	  _pv(makeQ(maxM, "p")),
	  _qv(makeQ(maxM, overlap ? "" : "q")),
	  _s(_rv, _rv, _rv),
	  _t(_rv, _pv, _qv),
	  _cm()
      {
	fillMap(_cm, _s, "C", closed, recursive, recurrence);
	fillMap(_dm, _t, "D", closed, recursive, recurrence);
      }
      /** 
       * Output test of an expression of partitions. 
       *
       * @note Parameters
       * 
       * - p    Prefix to test 
       * - q    Prefix to test against 
       * - h    Harmonic vector used 
       */
      virtual void checkP(const Complex& p NOTUSED,
			  const Complex& q NOTUSED, 
			  const HarmonicVector& h NOTUSED) {}
      /* @} */
      /** 
       * @{ 
       * @name Non-specific utilities 
       */
      /** 
       * Make a harmonic vector 
       */
      HarmonicVector makeH(Size m) const
      {
	HarmonicVector h(m);
	for (size_t i = 0; i < m; i++)
	  h[i] = Harmonic("h"+std::to_string(i+1));
	return h;
      }
      /* @} */
      /** 
       * @{ 
       * @name Specific tasks 
       */
      /** 
       * Write out sub-subsection of correlator using a specific
       * algorithm.
       *
       * @param h      Harmonics 
       * @param prefix Name prefix 
       * @param c      Correlator algorithm
       */
      Complex correlator(const HarmonicVector& h,
			 const std::string&    prefix,
			 const Correlator&     c)
      {
	comment("SUBSUBSECTION: "+prefix+" correlator");
	Complex z(prefix+std::to_string(h.size()));
	code(assign(z, c.calculate(h).sum()));
	return z;
      }
      /** 
       * Write out correlators and compare them.  The order of the map
       * dictates the order of comparison.
       *
       * @param h      Harmonics 
       * @param cm     Correlator methods map
       * @param kind   Kind of correlator to write
       */
      void correlators(const HarmonicVector& h,
		       CorrelatorMap&        cm,
		       const std::string&    kind="")
      {
	comment("SUBSECTION: "+std::to_string(h.size())
		+"-particle "+kind+"correlators");
	
	Complex ref("0");
	bool    hasref = false;
	for (auto& pc : cm) {
	  if (h.size() > pc.second->maxSize() && pc.second->maxSize() != 0)
	    continue;
	  
	  auto z = correlator(h, pc.first, *pc.second);
	  
	  if (!hasref) {
	    ref = z;
	    hasref = true;
	  }
	  else
	    check(z,ref);
	}
      }
      /** 
       * Make section of correlators 
       */
      void correlators(Size maxM)
      {
	comment("SECTION: Correlators");
	for (Size m=1; m <= maxM; m++) {
	  correlators(makeH(m),_cm);
	  correlators(makeH(m),_dm,"differential ");
	}
      }
    };
    //----------------------------------------------------------------
    /** 
     * Base class for printer of expression.  Specialisation for
     * partitioned correlators.
     *
     * @headerfile "" <correlations/symbolic/Printer.hh>
     * @ingroup correlations_symbolic 
     */
    template <>
    struct Printer<correlations::PartitionHarmonicVector> : PrinterBase
    {
      using Base=PrinterBase;
      using HarmonicVector=correlations::PartitionHarmonicVector;
      using HarmonicElement=typename HarmonicVector::value_type;
      using QVector=Base::QVector;
      using QContainer=correlations::QContainer;
      using QStore=correlations::QStore<HarmonicVector>;
      using Correlator=correlations::Correlator<HarmonicVector>;
      using CorrelatorMap=std::map<std::string,Correlator*>;
      using Calculations=Base::Calculations;
      using ValueVector=Base::ValueVector;

      QContainer _rv;
      QContainer _pv;
      QContainer _qv;
      QStore  _s;
      QStore  _t;
      CorrelatorMap _cm;
      CorrelatorMap _dm;
      
      static QContainer makeC(Size m, const std::string& n, Size s)
      {
	QContainer c;
	for (Size i = 1; i <= s; i++)
	  c.emplace_back(m*m,m,true,n+std::to_string(i));

	return c;
      }
      /** 
       * Constructor 
       *
       * @param o Output stream 
       * @param maxM
       * @param overlap Not used 
       * @param npart
       * @param closed
       * @param recursive
       * @param recurrence 
       */
      Printer(std::ostream& o,
	      Size maxM,
	      bool overlap NOTUSED,
	      Size npart,
	      bool closed,
	      bool recursive,
	      bool recurrence)
	: Base(o),
	  _rv(makeC(maxM,"r",npart)),
	  _pv(makeC(maxM,"p",1)),
	  _qv(makeC(maxM,"q",1)),
	  _s(_rv, _rv, _rv),
	  _t(_rv, _pv, _qv),
	  _cm()
      {
	fillMap(_cm, _s, "C", closed, recursive, recurrence);
	fillMap(_dm, _t, "D", closed, recursive, recurrence);
      }
      
      /** 
       * Output test of an expression of partitions. 
       *
       * @note Parameters
       * 
       * - p    Prefix to test 
       * - q    Prefix to test against 
       * - h    Harmonic vector used 
       */
      virtual void checkP(const Complex& p NOTUSED,
			  const Complex& q NOTUSED,
			  const HarmonicVector& h NOTUSED) {}
      /* @} */
      /** 
       * @{ 
       * @name Non-specific utilities 
       */
      /** 
       * Make a harmonic vector.  This distributed the partitions
       * evenly across the harmonics used.
       *
       * @param m Number of particles to correlate 
       */
      HarmonicVector makeH(Size m) const
      {
	size_t     nr = _rv.size();         // Number of ref vectors
	size_t     np = std::max(2lu,nr);   // Number of partitions 
	size_t     nh = std::max(1lu,m/np);  // Number of harmonics/partition

	Partitions p(m);                    // Partitions
	for (size_t i = 0; i < np; i++)     // Set nh partitions
	  p[std::slice(i*nh,nh,1)] = i+1;
	for (size_t i = np*nh; i < m; i++)  // Set remainder
	  p[i] = np;

	HarmonicVector h(m);
	for (size_t i = 0; i < m; i++) 
	  h[i] = PartitionHarmonic("h"+std::to_string(i+1),p[i]);
      
	return h;
      }
      // ---------------------------------------------------------------
      /**
       * Reduce a harmonic vector
       *
       * @param ph Harmonic vector
       *
       * @return Reduced harmonic vector
       */
      static HarmonicVector reduceH(const HarmonicVector& ph)
      {
	HarmonicVector h(ph.size());
	for (size_t i = 0; i < ph.size(); i++)
	  h[i] =  std::make_pair(ph[i].sum(),1);
	return h;
      }
      /* @} */
      /** 
       * @{ 
       * @name Specific tasks 
       */
      /** 
       * Write out sub-subsection of correlator using a specific
       * algorithm.
       *
       * @param h      Harmonics 
       * @param prefix Name prefix 
       * @param c      Correlator algorithm
       */
      Complex correlator(const HarmonicVector& h,
			 const std::string&    prefix,
			 const Correlator&     c)
      {
	comment("SUBSUBSECTION: "+prefix+" correlator");
	
	comment("Non-partitioned correlator");
	auto hr = reduceH(h);
	Complex z(prefix+std::to_string(h.size()));
	code(assign(z, c.calculate(hr).sum()));
	
	comment("Partitioned correlator");
	Complex zp(prefix+std::to_string(h.size())+"_p");
	code(assign(zp, c.calculate(h).sum()));

	return zp;
      }
      /** 
       * Write out correlators and compare them.  The order of the map
       * dictates the order of comparison.
       *
       * @param h      Harmonics 
       * @param cm     Map of Correlator algorithms
       * @param kind   What to write 
       */
      void correlators(const HarmonicVector& h,
		       CorrelatorMap& cm,
		       const std::string& kind="")
      {
	comment("SUBSECTION: "+std::to_string(h.size())
		+"-particle "+kind+"correlators");
	
	Complex ref("0");
	bool    hasref = false;
	for (auto& pc : cm) {
	  if (h.size() > pc.second->maxSize() && pc.second->maxSize() != 0)
	    continue;
	  
	  auto z = correlator(h, pc.first, *pc.second);
	  
	  if (!hasref) {
	    ref = z;
	    hasref = true;
	  }
	  else
	    check(z,ref);
	}

	if (!hasref) return;
	std::string nr(ref.s());
	checkP(Complex(nr.substr(0,nr.length()-2)),ref,h);
      }
      /** 
       * Make section of correlators 
       */
      void correlators(Size maxM)
      {
	comment("SECTION: Correlators");
	for (Size m=1; m <= maxM; m++) {
	  correlators(makeH(m),_cm);
	  correlators(makeH(m),_dm,"differential ");
	}
      }
    };
  }
}
#endif
// Local Variables:
//   compile-command: "make -C ../../ symbolic"
// End:
//
// EOF
//
