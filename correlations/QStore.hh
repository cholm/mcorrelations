#ifndef CORRELATIONS_QSTORE_HH
#define CORRELATIONS_QSTORE_HH
/**
 * @file   correlations/QStore.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Storage of Q-vectors 
 */
/*
 * Multi-particle correlations
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/QVector.hh>
#include <functional>

namespace correlations
{
  /** 
   * Base template.  Not implemented 
   *
   * @headerfile ""  <correlations/FromQVector.hh>
   * @ingroup correlations 
   */
  template <typename Harmonics> struct QStore;
  
  //____________________________________________________________________
  /**
   * Store of Q vectors.
   *
   * @headerfile ""  <correlations/FromQVector.hh>
   * @ingroup correlations 
   */
  template <>
  struct QStore<HarmonicVector>
  {
    /** The type of vectors of harmonics to use */
    using HarmonicVector=correlations::HarmonicVector;
    /** The harmonics to use */
    using HarmonicElement=typename HarmonicVector::value_type;
    /** The Q-vector to use */
    using QVector=correlations::QVector;
    /** The complex type */
    using Complex=typename QVector::Complex;

    /** Destructor */
    virtual ~QStore() {}
    /**
     * Constructor
     *
     * @param r Q-vector of reference particles
     * @param p Q-vector of particles of interest
     * @param q Q-vector of overlap of reference and particles of
     * interest.
     * @param ownr If true, own the reference Q-vector
     */
    QStore(QVector& r, QVector& p, QVector& q, bool ownr=true)
      : _r(r),
	_p(p),
	_q(q),
	_o(ownr)
    {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from
     */
    QStore(QStore&& o)
      : _r(o._r),
	_p(o._p),
	_q(o._q),
	_o(o._o)
    {}
    /** 
     * Copy constructor.  Since we store references to the external
     * q-vectors, and since we do not pass a QStore object around all
     * the time, it is OK to implement this.
     */
    QStore(const QStore& o)
      : _r(o._r),
	_p(o._p),
	_q(o._q),
	_o(o._o)
    {}
    /** 
     * Delete assignment operator 
     */
    QStore& operator=(const QStore&) = delete;

    /** 
     * Get @f$ Q@f$-vector component @f$ r_{h,p}@f$. 
     *
     * @param h @f$ h@f$ 
     * @param p @f$ p@f$ 
     *
     * @return @f$ r_{h,p}@f$. 
     */
    const Complex r(const HarmonicElement& h, Power p) const { return _r(h,p); }
    /** 
     * Get @f$ Q@f$-vector component @f$ p_{h,p}@f$. 
     *
     * @param h @f$ h@f$ 
     * @param p @f$ p@f$ 
     *
     * @return @f$ p_{h,p}@f$. 
     */
    const Complex p(const HarmonicElement& h, Power p) const { return _p(h,p); }
    /** 
     * Get @f$ Q@f$-vector component @f$ q_{h,p}@f$. 
     *
     * @param h @f$ h@f$ 
     * @param p @f$ p@f$ 
     *
     * @return @f$ q_{h,p}@f$. 
     */
    const Complex q(const HarmonicElement& h, Power p) const { return _q(h,p); }
    /** 
     * Access the Reference @f$ Q@f$-vector 
     * 
     * @return Reference @f$ Q@f$-vector 
     */
    const QVector& rvec() const { return _r; }
    /** 
     * Access the Particle-Of-Interest @f$ Q@f$-vector 
     * 
     * @return Particle-Of-Interest @f$ Q@f$-vector 
     */
    const QVector& pvec() const { return _p; }
    /** 
     * Access the Overlap @f$ Q@f$-vector 
     * 
     * @return Overlap @f$ Q@f$-vector 
     */
    const QVector& qvec() const { return _q; }
    /** 
     * Fill in an observation
     *
     * @param phi @f$\varphi@f$ observation 
     * @param weight @f$w@f$ of observation 
     * @param kind   Kind (REF, POI, or both) of observation 
     */
    void fill(Real phi, Real weight, Kind kind=(REF|POI), Partition=0)
    {
      if (kind & REF && _o)         _r.fill(phi,weight);
      if (kind & POI)               _p.fill(phi,weight);
      if (kind & POI && kind & REF) _q.fill(phi,weight);
    }
    /** 
     * Fill in observations 
     *
     * @param phi       @f$\varphi@f$ observations 
     * @param weight    @f$w@f$ of observations 
     * @param kind      Kinds (REF, POI, or both) of observations
     */
    void fill(const RealVector& phi,
	      const RealVector& weight,
	      const Kinds&      kind,
	      const Partitions& =Partitions())
    {
      auto ref = (kind &  Kinds::value_type(REF)) == REF;
      auto poi = (kind &  Kinds::value_type(POI)) == POI;
      auto bth = ref  && poi;
      if (_o) _r.fill(phi[ref],weight[ref]);
      _p.fill(phi[poi], weight[poi]);
      _q.fill(phi[bth], weight[bth]);
    }
    void reset()
    {
      if (_o) _r.reset();
      _p.reset();
      _q.reset();
    }
  protected:
    /** Q-vector of reference particles */
    QVector& _r;
    /** Q-vector of particles of interest */
    QVector& _p;
    /** Q-vector of overlap between reference and particles of interest */
    QVector& _q;
    bool _o;
  };
  //==================================================================
  /** 
   * Specialisation for correlator from @f$ Q@f$ vectors with explicit
   * partition information.
   */
  template <>
  struct QStore<PartitionHarmonicVector> 
  {
    /** The type of vectors of harmonics to use */
    using HarmonicVector=PartitionHarmonicVector;
    /** The harmonics to use */
    using HarmonicElement=typename HarmonicVector::value_type;
    /** The Q-vector to use */
    using QVector=correlations::QVector;
    /** The complex type */
    using Complex=typename QVector::Complex;
    /** Reference wrapper around a QVector */
    using QRef=std::reference_wrapper<QVector>;
    /** Vector of references to Q-vectors */
    using QReferences=std::vector<QRef>;
    /** Container of Q-vectors */
    using QContainer=correlations::QContainer;
    
    /** Destructor */
    virtual ~QStore() {}
    /**
     * Constructor
     *
     * @param r Q-vector of reference particles
     * @param p Q-vector of particles of interest
     * @param q Q-vector of overlap of reference and particles of
     * interest.
     * @param ownr If true, own the reference Q-vector
     */
    QStore(QVector& r, QVector& p, QVector& q, bool ownr=true)
      : _rv(1,std::ref(r)),
	_pv(1,std::ref(p)),
	_qv(1,std::ref(q)),
	_o(ownr)
    {}
    /**
     * Constructor
     *
     * @param rv Q-vectors of reference particles
     * @param pv Q-vectors of particles of interest
     * @param qv Q-vectors of overlap of reference and particles of
     * interest.
     * @param ownr If true, own the reference Q-vector 
     */    
    QStore(std::initializer_list<QRef> rv,
	   std::initializer_list<QRef> pv,
	   std::initializer_list<QRef> qv,
	   bool ownr=true)
      : _rv(rv),
	_pv(pv),
	_qv(qv),
	_o(ownr)
    {}
    /**
     * Constructor
     *
     * @param rv Q-vectors of reference particles
     * @param pv Q-vectors of particles of interest
     * @param qv Q-vectors of overlap of reference and particles of
     * interest.
     * @param ownr If true, own the reference Q-vector 
     */    
    QStore(QContainer& rv,
	   QContainer& pv,
	   QContainer& qv,
	   bool ownr=true)
      : _rv(),
	_pv(),
	_qv(),
	_o(ownr)
    {
      for (auto& rr : rv) _rv.push_back(std::ref(rr));
      for (auto& pp : pv) _pv.push_back(std::ref(pp));
      for (auto& qq : qv) _qv.push_back(std::ref(qq));
    }
    /**
     * Constructor
     *
     * @param rv Q-vectors of reference particles
     * @param pv Q-vectors of particles of interest
     * @param qv Q-vectors of overlap of reference and particles of
     * interest.
     * @param ownr If true, own the reference Q-vector 
     */    
    QStore(QReferences& rv,
	   QReferences& pv,
	   QReferences& qv,
	   bool ownr=true)
      : _rv(rv),
	_pv(pv),
	_qv(qv),
	_o(ownr)
    {
    }
    /** 
     * Move constructor 
     *
     * @param o Object to move from
     */
    QStore(QStore&& o)
      : _rv(std::move(o._rv)),
	_pv(std::move(o._pv)),
	_qv(std::move(o._qv)),
	_o(o._o)
    {}
    /** 
     * Delete copy constructor 
     */
    QStore(const QStore& o)
      : _rv(o._rv),
	_pv(o._pv),
	_qv(o._qv),
	_o(o._o)
    {}
    /** 
     * Delete assignment operator 
     */
    QStore& operator=(const QStore&) = delete;

    /** 
     * Get @f$ Q@f$-vector component @f$ r_{h,p}@f$. 
     *
     * @param h @f$ h@f$ 
     * @param p @f$ p@f$ 
     *
     * @return @f$ r_{h,p}@f$. 
     */
    const Complex r(const HarmonicElement& h, Power p) const
    {
      return _qc(h,_rv,p);
    }
    /** 
     * Get @f$ Q@f$-vector component @f$ p_{h,p}@f$. 
     *
     * @param h @f$ h@f$
     * @param p @f$ p@f$ 
     *
     * @return @f$ p_{h,p}@f$. 
     */
    const Complex p(const HarmonicElement& h, Power p) const
    {
      return _qc(h,_pv,p);
    }
    /** 
     * Get @f$ Q@f$-vector component @f$ q_{h,p}@f$. 
     *
     * @param h @f$ h@f$ 
     * @param p @f$ p@f$ 
     *
     * @return @f$ q_{h,p}@f$. 
     */
    const Complex q(const HarmonicElement& h, Power p) const
    {
      return _qc(h,_qv,p);
    }
    /** 
     * Access the Reference @f$ Q@f$-vectors 
     * 
     * @return Reference @f$ Q@f$-vectors 
     */
    const QReferences& rvec() const { return _rv; }
    /** 
     * Access the Particle-Of-Interest @f$ Q@f$-vectors 
     * 
     * @return Particle-Of-Interest @f$ Q@f$-vectors 
     */
    const QReferences& pvec() const { return _pv; }
    /** 
     * Access the Overlap @f$ Q@f$-vectors 
     * 
     * @return Overlap @f$ Q@f$-vectors 
     */
    const QReferences& qvec() const { return _qv; }
    /** 
     * Fill in an observation
     *
     * @param phi @f$\varphi@f$ observation 
     * @param weight @f$w@f$ of observation 
     * @param kind   Kind (REF, POI, or both) of observation 
     * @param partition Partition identifier of this observation 
     */
    void fill(Real phi, Real weight, Kind kind=(REF|POI), Partition partition=0)
    {
      if (partition < 1) return;
      if (kind & REF && _o)         _rv[partition-1].get().fill(phi,weight);
      if (kind & POI)               _pv[partition-1].get().fill(phi,weight);
      if (kind & POI && kind & REF) _qv[partition-1].get().fill(phi,weight);
    }
    /** 
     * Fill in observations 
     *
     * @param phi       @f$\varphi@f$ observations 
     * @param weight    @f$w@f$ of observations 
     * @param kind      Kinds (REF, POI, or both) of observations
     * @param partition Partitions 
     */
    void fill(const RealVector& phi,
	      const RealVector& weight,
	      const Kinds&      kind,
	      const Partitions& partition=Partitions())
    {
      auto ref = (kind & Kinds::value_type(REF)) == REF;
      auto poi = (kind & Kinds::value_type(POI)) == POI;
      for (size_t p = 0; p < _rv.size(); p++) {
	auto pref = ref && partition == Partitions::value_type(p+1);
	auto ppoi = poi && partition == Partitions::value_type(p+1);
	auto pbth = pref  && ppoi;
	if (_o) _rv[p].get().fill(phi[pref],weight[pref]);
	_pv[p].get().fill(phi[ppoi], weight[ppoi]);
	_qv[p].get().fill(phi[pbth], weight[pbth]);
      }
    }
    void reset()
    {
      if (_o) for (auto& r : _rv) r.get().reset();
      for (auto& p : _pv) p.get().reset();
      for (auto& q : _qv) q.get().reset();
    }
  protected:
    /** 
     * Get the appropriate complex value for a given harmonic and
     * partition. 
     */ 
    const Complex _qc(const HarmonicElement& h,
		      const QReferences&     ref,
		      Power                  pp) const
    {
      size_t p = h.partition();
      if (p < 1) return cnull;

      return ref[p-1](h.sum(),pp); // h.size());
    }
    /** Q-vector of reference particles */
    QReferences _rv;
    /** Q-vector of particles of interest */
    QReferences _pv;
    /** Q-vector of overlap between reference and particles of interest */
    QReferences _qv;
    bool _o;
  };
}
#endif
// Local Variables:
//  mode: C++
// End:
