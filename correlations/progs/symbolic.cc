/**
 * @file   correlations/progs/symbolic.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Test symbolic evaluated 
 *
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/symbolic/Maxima.hh>
#include <correlations/symbolic/Maple.hh>
#include <correlations/symbolic/SymPy.hh>
#include <correlations/symbolic/JupyterSymPy.hh>
#include <correlations/symbolic/Sage.hh>
#include <correlations/symbolic/JupyterSage.hh>
#include "opt.hh"
#include <algorithm>

// -------------------------------------------------------------------
/**
 * Downcase a string 
 *
 * @param s String to downcase 
 *
 * @return downcased copy of the string
 */
std::string downcase(const std::string& s)
{
  std::string t(s);
  std::transform(t.begin(), t.end(), t.begin(),
		 [](unsigned char c) { return std::tolower(c); });
  return t;
}


//--------------------------------------------------------------------
using correlations::HarmonicVector;
using correlations::PartitionHarmonicVector;
using correlations::QContainer;
using correlations::QVector;
using correlations::Size;
using correlations::Harmonic;
using correlations::PartitionHarmonic;
using correlations::Partitions;
template <typename HV> using Closed       = correlations::Closed<HV>;
template <typename HV> using Recursive    = correlations::Recursive<HV>;
template <typename HV> using Recurrence   = correlations::Recurrence<HV>;
template <typename HV> using QStore       = correlations::QStore<HV>;
template <typename HV> using Printer      = correlations::symbolic::Printer<HV>;
template <typename HV> using Correlator   = correlations::Correlator<HV>;
template <typename HV> using Maxima       = correlations::symbolic::Maxima<HV>;
template <typename HV> using Maple        = correlations::symbolic::Maple<HV>;
template <typename HV> using SymPy        = correlations::symbolic::SymPy<HV>;
template <typename HV> using Sage         = correlations::symbolic::Sage<HV>;
template <typename HV> using JupyterSymPy = correlations::symbolic::JupyterSymPy<HV>;
template <typename HV> using JupyterSage  = correlations::symbolic::JupyterSage<HV>;


template <typename T> using Option=exa::Option<T>;
using exa::CommandLine;

//--------------------------------------------------------------------
/** 
 * Create our printer based on string argument 
 *
 * @param f  Format name 
 * @param o  Output stream to use 
 *
 * @return Pointer to printer or null 
 */
template<typename HV, typename... Args>
Printer<HV>* select(const std::string& f, Args&...args)
{
  // Lower-case the format name
  std::string t = downcase(f);

  // Try to create our formatter
  if      (t == "maxima") return new Maxima      <HV>(args...);
  else if (t == "sympy")  return new SymPy       <HV>(args...);
  else if (t == "jsympy") return new JupyterSymPy<HV>(args...);
  else if (t == "sage")   return new Sage        <HV>(args...);
  else if (t == "jsage")  return new JupyterSage <HV>(args...);
  else if (t == "maple")  return new Maple       <HV>(args...);

  std::cerr << "Unknown formatter: " << f << std::endl;
  return 0;
}

template <typename HV>
void run(const std::string& format,
	 std::ostream&      output,
	 size_t             maxM,
	 bool               overlap,
	 size_t             npartitions,
	 bool               useClosed,
	 bool               useRecursive,
	 bool               useRecurrence)
{
  Printer<HV>* p = select<HV>(format, output, maxM, overlap, npartitions,
			      useClosed,useRecursive,useRecurrence);

  p->begin();
  p->run(maxM);
  p->end();
}
  
/** 
 * Entry point 
 * 
 * @param argc Number of arguments 
 * @param argv Vector of arguments 
 * 
 * @return 0 on success, 1 otherwise 
 */
int main(int argc, char** argv)
{
  // Import the namespace so we don't have to write long names.  This
  // is OK here because the scope is limited.  Never do this in
  // library code.
  std::ostream& o = std::cout;
  
  Option<std::string> f('f',"Output format",                 "sympy", "FORMAT");
  Option<size_t>      m('m',"Largest correlator",            5,       "M");
  Option<size_t>      s('s',"Number of sub-events",          1,       "NSUB");
  Option<bool>        c('c',"Use closed forms",              true);
  Option<bool>        r('r',"Use recursive algorithm",       true);
  Option<bool>        u('u',"Use recurrence algorithm",      true);
  Option<bool>        g('g',"No overlap for differential",   false);
  Option<bool>        v('v',"Verbose",                       false);

  CommandLine& cl = CommandLine::instance();
  if (!cl.process(argc, argv)) {
    std::cout << "FORMAT is one of\n\n"
	      << "- maxima:  Maxima\n"
	      << "- maple:   Maple\n"
	      << "- sympy:   SymPy\n"
	      << "- sage:    SageMath\n"
	      << "- jsympy:  SymPy in a Jupyter notebook\n"
	      << "- jsage:   SageMath in a Jupyter notebook\n"
	      << std::endl;
    return 1;
  }

  // cl.print(std::cerr);

  std::string format        = f;
  size_t      maxM          = m;
  bool        overlap       = g;
  size_t      npartitions   = s;
  bool        useClosed     = c;
  bool        useRecursive  = r;
  bool        useRecurrence = u;

  if (npartitions > 1)
    run<PartitionHarmonicVector>(format,o,maxM,overlap,npartitions,
				 useClosed,useRecursive,useRecurrence);
  else
    run<HarmonicVector>(format,o,maxM,overlap,npartitions,
			useClosed,useRecursive,useRecurrence);
    
  
  return 0;
}
// Local Variables:
//   compile-command: "make -C ../../ symbolic"
// End:
//
// EOF
//
