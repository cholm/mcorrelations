/**
 * @file   correlations/progs/benchmark.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Read in data (or generate on the fly) and calculate correlators
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <iostream>
#include <iomanip>
#include <correlations/ana/Benchmark.hh>
#include "tools.hh"
#include "opt.hh"

using correlations::RealVector;
using correlations::HarmonicVector;
template <typename T> using Option=exa::Option<T>;
using exa::CommandLine;

//--------------------------------------------------------------------
// using HV     = correlations::PartitionHarmonicVector;
using HV     = correlations::HarmonicVector;
using CNBenchmark = ana::CNBenchmark<HV>;
using RNBenchmark = ana::RNBenchmark<HV>;
using CQBenchmark = ana::CQBenchmark<HV>;
using RQBenchmark = ana::RQBenchmark<HV>;
using UQBenchmark = ana::UQBenchmark<HV>;

int main(int argc, char** argv)
{
  Option<std::string> mde   ('a', "Mode",           "qvector_closed", "MODE");
  Option<std::string> filen ('i', "Input file",                "",    "FILE");
  Option<std::string> outn  ('o', "Output file",               "",    "FILE");
  Option<size_t>      maxM  ('m',"Max particles to correlate", 5,     "M");
  Option<size_t>      nev   ('e',"Number of events",           10,    "NEV");
  Option<size_t>      seed  ('s',"Random number seed",         123456,"SEED");
  Option<bool>        usew  ('w',"Use weights",                true);
  Option<HarmonicVector> hn ('H',"Harmonic vector", {0},"H1[,H2[,H3[,...]]]");

  CommandLine& cl = CommandLine::instance();
  if (!cl.process(argc, argv)) {
    std::cout << "MODE is one of\n"
	      << "- nested_closed:      Nested loops, closed form\n"
	      << "- qvector_closed:     From Q-vector, closed form\n"
	      << "- qvector_recursive:  From Q-vector, recursive\n"
	      << "- qvector_recurrence: From Q-vector, recurrence\n"
	      << "- nested_recursive:   Nested Loops, recursive\n"
	      << "M is the largest numbor particles to correlate. Note,\n"
	      << "this is limited by the algorithm chosen.  E.g., \n"
	      << "'nested_closed' and 'qvector_closed' is limited to, at\n"
	      << "most 8 or 6 particle correlators, respectively."
	      << std::endl;
    return 0;
  }

  HarmonicVector h(hn);
  if (h.size() != maxM) {
    h.resize(maxM);

    std::random_device rd;
    gen::RandomEngine rnd(rd());
    if (seed > 0) rnd.seed(size_t(seed));

    std::uniform_int_distribution<> u(-6,6);
    
    for (auto& hh : h) hh = u(rnd);
  }
  
  
  std::string mode = downcase(mde);  
  std::unique_ptr<ana::Analyzer> ana = 0;

  if      (mode == "nested" || mode == "nested_closed" || mode == "nc")
    ana = mu<CNBenchmark>(h.size(), h, usew);
  else if (mode == "closed" || mode == "qvector_closed" || mode == "qc")
    ana = mu<CQBenchmark>(h.size(), h, usew);
  else if (mode == "recursive" || mode == "qvector_recursive" || mode == "qr")
    ana = mu<RQBenchmark>(h.size(), h, usew);
  else if (mode == "recurrence" || mode == "qvector_recurrence" || mode == "qu")
    ana = mu<UQBenchmark>(h.size(), h, usew);
  else if (mode == "rnested" || mode == "nested_recursive" || mode == "nr")
    ana = mu<RNBenchmark>(h.size(), h, usew);
  else {
    std::cerr << "Argh! Unknown mode: " << mode << std::endl;
    return 1;
  }

  json::JSON json = run(ana, filen, nev, false, true);
  output(json, outn);

  return 0;
}

    
// Local Variables:
//   compile-command: "make -C ../../ corr"
// End:

    
