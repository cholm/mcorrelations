/**
 * @file   correlations/progs/differential.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple program that calculates the differential flow
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/ana/Differential.hh>
#include "opt.hh"
#include "tools.hh"

/** Short-hand for differential bin */
template <template <typename> class Uncertainty,
	  template <typename> class Correlator,
	  typename Harmonics>
using Differential = ana::Differential<correlations::Real,
				       Uncertainty,
				       Correlator,
				       Harmonics>;

using correlations::RealVector;
using correlations::Real;
using correlations::Size;

using ana::EtaGap;

template <typename T> using Option=exa::Option<T>;
using exa::CommandLine;

int main(int argc, char** argv)
{
  Option<std::string> filen ('i',"Input filename",             "",    "FILE");
  Option<size_t>      nev   ('e',"Number of events",           10000, "N");
  Option<bool>        load  ('l',"Load state from input",      false);
  Option<bool>        fini  ('f',"Finalize calculations",      true);
  Option<std::string> outn  ('o',"Output filename",            "",    "FILE");
  Option<RealVector>  ptbin ('t',"pT bins",
			     {0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
			      1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.5, 3.0,
			      3.5, 4.0, 4.5, 5.0},"PT[,PT[,...]]");

  // Flow arguments
  Option<size_t>      maxH  ('n',"Maximum harmonic",           4,     "M");
  Option<size_t>      maxP  ('m',"Maximum correlator",         4,     "M");
  Option<bool>        usew  ('w',"Use weights",                true);
  Option<std::string> corr  ('c',"Correlator kind",            "closed","KIND");
  Option<std::string> unc   ('u',"Uncertainty kind",           "full","UNCER");
  Option<double>      deta  ('g',"Pseudorapidity gap",         -1,    "GAP");
  
  CommandLine& cl = CommandLine::instance();
  if (!cl.process(argc, argv)) return 0;

  std::string  uncer = uncertaintyName(downcase(unc));
  std::string  mth   = correlatorName(downcase(corr));
  std::string  fn    = filen; 
  unsigned int mH    = maxH;
  unsigned int mP    = maxP;
  bool         w     = usew;
  Real         de    = deta;
  RealVector   ptb   = ptbin;

  std::unique_ptr<ana::Analyzer> ana = 0;
  if (de >= 0) {
    auto part = EtaGap(de);
    ana       = analyzer<Differential>(uncer, mth, part, mH, mP, w, ptb);
  }
  else
    ana       = analyzer<Differential>(uncer, mth, mH, mP, w, ptb);

  json::JSON json     = run(ana, fn, nev, load, fini);
  json["correlator"]  = mth;
  json["policy"]      = uncer;
  json["eta_gap"]     = de;
  output(json,outn);
  
  return 0; 
}

  


