/**
 * @file   correlations/progs/opt.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Command line options 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef EXA_OPT_HH
#define EXA_OPT_HH
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <valarray>
#include <forward_list>
#include <functional>
#include <algorithm>

namespace exa
{
  namespace {
    /** Toggle value */
    template <typename T> T& toggle(T& v) { return v; }
    /** Toggle boolean value */
    template<> bool& toggle(bool& v) { v = !v; return v; }

    /** Convert a string to a value */
    template <typename T>
    void convert(const std::string& s, T& v)
    {
      std::stringstream str(s);
      str >> v;
    }
    /** Convert a string to vector of doubles */
    template <typename T>
    void convert(const std::string& s, std::vector<T>& v)
    {
      std::stringstream        str(s);
      std::string              sv;
      v.clear();
      while (std::getline(str, sv, ',')) {
	T vv;
	convert(sv,vv);
	v.push_back(vv);
      }
    }
    /** Convert a string to vector of doubles */
    template <typename T>
    void convert(const std::string& s, std::valarray<T>& v)
    {
      std::vector<T> vv;
      convert(s,vv);
      v.resize(vv.size());
      for (size_t i = 0; i < v.size(); i++) v[i] = vv[i];
    }
    template <>
    void convert(const std::string& s, std::string& v)
    {
      v = s;
    }
    template <typename T> std::string asStr(const T& v)
    {
      return std::to_string(v);
    }
    template <> std::string asStr(const std::string& v) { return v; }
    template <> std::string asStr(const bool& v) { return v ? "true" : "false";}
    template <typename T> std::string asStr(const std::vector<T>& v)
    {
      std::stringstream str;
      auto b = v.begin();
      for (auto i = b; i != v.end(); ++i) {
	if (i != b) str << ",";
	str << *i;
      }
      return str.str();
    }
    template <typename T> std::string asStr(const std::valarray<T>& v)
    {
      std::stringstream str;
      auto b = std::begin(v);
      for (auto i = b; i != std::end(v); ++i) {
	if (i != b) str << ",";
	str << *i;
      }
      return str.str();
    }
  }
  /** 
   * Base class for options 
   *
   * @ingroup exa 
   */
  struct OptionBase
  {
    char _c;
    std::string _desc;
    std::string _dum;
    bool        _set;
    /** 
     * Constructor 
     *
     * @param c       Short option 
     * @param dum     Dummy argument 
     * @param desc    Description 
     */
    OptionBase(char c, const std::string& desc, const std::string& dum="")
      : _c(c),
	_desc(desc),
	_dum(dum),
	_set(false)
    {}
    /** Destructor */
    virtual ~OptionBase() {}
    /** @return true if we need an argument */
    bool needArg() const { return !_dum.empty(); }
    /** @return true if set */
    bool isSet() const { return _set; }
    /** @return get value as a string */
    virtual std::string asString() const = 0;
    /** 
     * Possibly take an option 
     *
     * @param arg  Argument to check 
     * @param v    Possible value 
     *
     * @return true if we have taken the argument 
     */
    virtual bool take(const char arg, const std::string& v="") = 0;
    /** 
     * Print out the option 
     *
     * @param out Output stream 
     * @param wd  Width of dummy field
     *
     * @return Output stream @a out
     */
    std::ostream& help(std::ostream& out, size_t wd=10) const
    {
      out << "\t-" << _c << std::left;
      if (needArg()) out << ' ' << std::setw(wd) << _dum << ' ';
      else           out << ' ' << std::setw(wd+1) << " ";
      out << _desc << " (" << asString() << ")\n" << std::right;
      return out;
    }
    /**
     * Print out the option 
     *
     * @param out The output stream 
     * @param wd  Width of description field
     *
     * @return The output stream @a out
     */
    std::ostream& print(std::ostream& out, size_t wd=30) const
    {
      return out << std::left << std::setw(wd) << (_desc + ":")
		 << asString() << std::right << "\n";
    }
  };
  /** 
   * Manager of command line arguments 
   *
   * @include tests/testOpt.cc
   *
   * @ingroup exa 
   */
  struct CommandLine
  {
    /** Singleton accesss */
    static CommandLine& instance();
    /** 
     * Add an option 
     *
     * @param o  Option to add 
     */
    void add(OptionBase& o)
    {
      _opt.push_front(std::ref(o));
    }
    /** 
     * Process the command line 
     *
     * @param argc  Number of command line options 
     * @param argv  Command line arguments 
     */
    bool process(int argc, char** argv)
    {
      using Strings=std::vector<std::string>;
      Strings args(argv+1, argv+argc);
      auto   arg = args.begin();
      auto   val = arg+1;
      for (; arg != args.end(); ++arg, ++val) {
	bool taken = false;
	if ((*arg)[0] != '-')
	  throw std::runtime_error("Not an option: " + *arg);
	// continue;
	for (auto o : _opt) {
	  
	  if (o.get().take((*arg)[1], *val)) {
	    if (o.get().needArg()) {
	      // Eat one more
	      ++arg;
	      ++val;
	    }
	    taken = true;
	    break;
	  }
	}
	if (!taken)
	  throw std::runtime_error("Unknown argument " + *arg);
      }
      if (_help && _help->isSet()) {
	help(argv[0], std::cout);
	return false;
      }
      return true;
    }
    /**
     * Print out the options as a help output 
     *
     * @param o        Output stream 
     * @param progname Program invokation name 
     */
    void help(const char* progname, std::ostream& o)
    {
      o << "Usage: " << progname << "\n\n"
	<< "Options:" << std::endl;
      size_t od = 0;
      _opt.sort([](const OptionBase& o1, const OptionBase& o2) {
		  char l1 = std::tolower(o1._c);
		  char l2 = std::tolower(o2._c);
		  if (l1 == l2)  return l2 < l1;
		  return l1 < l2;
		});
      for (auto oo : _opt) od = std::max(oo.get()._dum.length(), od);
      for (auto oo : _opt) oo.get().help(o,od);
      o << std::flush;
    }
    /**
     * Print out the options
     *
     * @param o Output stream 
     * @param what Which options to print (all if empty)
     */
    void print(std::ostream& o, const std::string& what="")
    {
      Options fo;
      std::copy_if(_opt.begin(), _opt.end(), std::front_inserter(fo),
		   [what](const OptionBase& o) {
		     return what.empty() || what.find(o._c)!=std::string::npos;
		   });
      size_t od = 0;
      fo.sort([](const OptionBase& o1, const OptionBase& o2) {
		  char l1 = std::tolower(o1._c);
		  char l2 = std::tolower(o2._c);
		  if (l1 == l2)  return l2 < l1;
		  return l1 < l2;
		});
      for (auto oo : fo) od = std::max(oo.get()._desc.length(), od);
      for (auto oo : fo) oo.get().print(o,od+2);
      o << std::flush;
    }
  private:
    CommandLine()  : _opt(), _help(0) {}
    CommandLine(CommandLine&& o) : _opt(std::move(o._opt)), _help(o._help)  {}
    CommandLine(const CommandLine& o) = delete;
    CommandLine& operator=(const CommandLine& o) = delete;
    
    /** Map of options */
    using Options=std::forward_list<std::reference_wrapper<OptionBase>>;
    /** Options */
    Options _opt;
    /** Help option */
    OptionBase* _help;
  };
    
  /** 
   * A command line option 
   */
  template <typename T>
  struct Option : public OptionBase
  {
    Option(char               c,
	   const std::string& desc,
	   const T&           def,
	   const std::string& dum="")
      : OptionBase(c, desc, dum), 
	_val(def)
    {
      CommandLine::instance().add(*this);
    }
    std::string asString() const { return asStr(_val); }
    /** 
     * Get the value 
     */
    T value() const { return _val; }
    /** 
     * Conversion operator 
     */
    operator T() const { return _val; }
    /** 
     * Take an argument 
     */
    bool take(char arg, const std::string& v="")
    {
      if (arg != _c) return false;
      if (_dum.empty()) {
	toggle(_val);
	_set = true;
	return true;
      }
      if (v.empty()) 
	throw std::runtime_error("Missing argument " + _dum +
				 " to option -" + std::to_string(_c));

      convert(v, _val);
      _set = true;

      return true;
    }
    T _val;
  };
  CommandLine& CommandLine::instance()
  {
    static CommandLine*  _instance = 0;
    if (!_instance) {
      _instance         = new CommandLine;
      _instance->_help  = new Option<bool>('h',"Show help",false,"");
    }
    return *_instance;
  }
}
#endif
// Local Variables:
//   compile-command: "make -C ../../ testOpt"
// End:
//
// EOF
//

