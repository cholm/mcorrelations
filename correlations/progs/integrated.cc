/**
 * @file   correlations/progs/integrated.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple program that calculates the integrated flow
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/ana/Integrated.hh>
#include <correlations/Closed.hh>
#include <correlations/Recursive.hh>
#include <correlations/Recurrence.hh>
#include <correlations/stat/Bootstrap.hh>
#include <correlations/stat/Jackknife.hh>
#include <correlations/stat/Derivatives.hh>
#include "opt.hh"
#include "tools.hh"

/** Short-hand for integrated bin */
template <template <typename> class Uncertainty,
	  template <typename> class Correlator,
	  typename Harmonics>
using Integrated = ana::Integrated<correlations::Real,
				   Uncertainty,
				   Correlator,
				   Harmonics>;


template <typename T> using Option=exa::Option<T>;
using exa::CommandLine;
using correlations::RealVector;
using correlations::Real;
using correlations::Size;
using ana::EtaGap;


int main(int argc, char** argv)
{
  Option<std::string> filen ('i',"Input filename",             "",    "FILE");
  Option<size_t>      nev   ('e',"Number of events",           10000, "N");
  Option<bool>        load  ('l',"Load state from input",      false);
  Option<bool>        fini  ('f',"Finalize calculations",      true);
  Option<std::string> outn  ('o',"Output filename",            "",    "FILE");

  // Flow arguments
  Option<size_t>      maxH  ('n',"Maximum harmonic",           4,     "M");
  Option<size_t>      maxP  ('m',"Maximum correlator",         4,     "M");
  Option<bool>        usew  ('w',"Use weights",                true);
  Option<std::string> corr  ('c',"Correlator kind",            "closed","KIND");
  Option<std::string> unc   ('u',"Uncertainty kind",           "full","UNCER");
  Option<double>      deta  ('g',"Pseudorapidity gap",         -1,    "GAP");
  
  CommandLine& cl = CommandLine::instance();
  if (!cl.process(argc, argv)) return 0;

  std::string  uncer = uncertaintyName(downcase(unc));
  std::string  mth   = correlatorName(downcase(corr));
  std::string  fn    = filen; 
  unsigned int mH    = maxH;
  unsigned int mP    = maxP;
  bool         w     = usew;
  Real         de    = deta;

  std::unique_ptr<ana::Analyzer> ana = 0;
  if (de >= 0) {
    auto part = EtaGap(de);
    ana       = analyzer<Integrated>(uncer, mth, part, mH, mP, w);
  }
  else
    ana       = analyzer<Integrated>(uncer, mth, mH, mP, w);

  json::JSON json     = run(ana, fn, nev, load, fini);
  json["correlator"]  = mth;
  json["policy"]      = uncer;
  json["eta_gap"]     = de;
  output(json,outn);
  
  return 0; 
}

  


