/**
 * @file   correlations/progs/tools.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Tools for programs
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef PROGS_TOOLS_HH
#define PROGS_TOOLS_HH
#include <string>
#include <memory>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <correlations/ana/Analyzer.hh>
#include <correlations/Closed.hh>
#include <correlations/Recursive.hh>
#include <correlations/Recurrence.hh>
#include <correlations/stat/Bootstrap.hh>
#include <correlations/stat/Jackknife.hh>
#include <correlations/stat/Derivatives.hh>
#include <correlations/ana/Partitioner.hh>

// -------------------------------------------------------------------
/** 
 * Substitute for `std::make_unique<T>` not available until C++14
 *
 * @tparam T    Type of pointed to object 
 * @tparam Args Variadic argument types 
 *
 * @param args Variadic arguments
 */
template<typename T, typename... Args>
std::unique_ptr<T> mu(Args&&... args)
{
  return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
// ===================================================================
/** 
 * @{
 * @name String utilities 
 */
/** 
 * Check if a string begins with a specific string 
 *
 * @param s String to check 
 * @param t String to check for 
 *
 * @return true if @a s starts with @a t 
 */
bool beginsWith(const std::string& s, const std::string& t)
{
  return s.compare(0,t.length(),t) == 0;
}
// -------------------------------------------------------------------
/**
 * Downcase a string 
 *
 * @param s String to downcase 
 *
 * @return downcased copy of the string
 */
std::string downcase(const std::string& s)
{
  std::string t(s);
  std::transform(t.begin(), t.end(), t.begin(),
		 [](unsigned char c) { return std::tolower(c); });
  return t;
}
/** @} */

/** 
 * @{ 
 * @name Get canonical names 
 */
/**
 * Get the the name of the kind of uncertainty estimate used
 *
 * @param s Analysis name
 *
 * @return Kind of uncertainty estimate
 */
std::string uncertaintyName(const std::string& s)
{
  if (beginsWith(s,"ja")) return "Jackknife";
  if (beginsWith(s,"bo")) return "Bootstrap";
  return "Derivatives";
}
/**
 * Get the the name of the kind of correlator used
 *
 * @param s Analysis name
 *
 * @return Kind of correlator 
 */
std::string correlatorName(const std::string& s)
{
  if (beginsWith(s,"recurs")) return "Recursive";
  if (beginsWith(s,"recurr")) return "Recurrence";
  return "Closed";
}
/** @} */

/** 
 * @{ 
 * @name Function to deduce type from strings 
 */
/** 
 * Create analyzer given an uncertainty method and correlator
 * algorithm.
 *
 * This is overload in case we have a partitioner 
 *
 * @tparam A           Analyzer type 
 * @tparam Uncertainty Uncertainty method 
 * @tparam Correlator  Correlator algorithm
 * @tparam Args        Variadic argument types 
 *
 * @param args         Variadic arguments 
 * @param p            Partitioner 
 *
 * @return unique pointer to differential analysis 
 */
template <template <template <typename> class,
		    template <typename> class,
		    typename> class A,
	  template <typename> class Uncertainty,
	  template <typename> class Correlator,
	  typename P,
	  typename ... Args,
	  typename std::enable_if<std::is_base_of<ana::Partitioner,P>::value,
				  int>::type = 0>
std::unique_ptr<ana::Analyzer>
analyzer_UC(P& p, Args&&... args)
{
  using HV=correlations::PartitionHarmonicVector;
  return mu<A<Uncertainty,Correlator,HV>>(p, args...);
}

/** 
 * Create analyzer given an uncertainty method and correlator algorithm. 
 *
 * @tparam A           Analyzer type 
 * @tparam Uncertainty Uncertainty method 
 * @tparam Correlator  Correlator algorithm
 * @tparam Args        Variadic argument types 
 *
 * @param args         Variadic arguments 
 *
 * @return unique pointer to differential analysis 
 */
template <template <template <typename> class,
		    template <typename> class,
		    typename> class A,
	  template <typename> class Uncertainty,
	  template <typename> class Correlator,
	  typename ... Args>
std::unique_ptr<ana::Analyzer>
analyzer_UC(Args&&... args)
{
  using HV=correlations::HarmonicVector;
  return mu<A<Uncertainty,Correlator,HV>>(args...);
}

/** 
 * Trampoline based on string for correlator algorithm given an
 * uncertainty method.
 *
 * @tparam A           Analyzer type 
 * @tparam Uncertainty Uncertainty method 
 * @tparam Args        Variadic argument types 
 *
 * @param correlator Correlator algorithm 
 * @param args       Variadic arguments 
 *
 * @return unique pointer to differential analysis 
 */
template <template <template <typename> class,
		    template <typename> class,
		    typename> class A,
	  template <typename> class Uncertainty,
	  typename ... Args>
std::unique_ptr<ana::Analyzer>
analyzer_U(const std::string& correlator, Args&&... args)
{
  using correlations::Closed;
  using correlations::Recursive;
  using correlations::Recurrence;
  if (correlator == "Recursive")
    return analyzer_UC<A,Uncertainty,Recursive>(args...);
  if (correlator == "Recurrence")
    return analyzer_UC<A,Uncertainty,Recurrence>(args...);
  return analyzer_UC<A,Uncertainty,Closed>(args...);
}

template <typename T> using Jackknife=stat::Jackknife<T>; 
template <typename T> using Bootstrap=stat::Bootstrap<T>;

/** 
 * Trampoline based on string for uncertainty method
 *
 * @tparam A           Analyzer type 
 * @tparam Args        Variadic argument types 
 *
 * @param uncertainty Uncertainty method 
 * @param method      Correlator algorithm 
 * @param args        Variadic arguments 
 *
 * @return unique pointer to differential analysis 
 */
template <template <template <typename> class,
		    template <typename> class,
		    typename> class A,
	  typename ... Args>
std::unique_ptr<ana::Analyzer>
analyzer(const std::string& uncertainty,
	 const std::string& method,
	 Args&&... args)
{
  using stat::Derivatives;

  if (uncertainty == "Jackknife")
    return analyzer_U<A,Jackknife,Args...>(method,std::forward<Args>(args)...);
  if (uncertainty == "Bootstrap")
    return analyzer_U<A,Bootstrap,Args...>(method,std::forward<Args>(args)...);
  return analyzer_U<A,Derivatives,Args...>(method,std::forward<Args>(args)...);
}

/** @} */
/** 
 * @{ 
 * @name for running analyses 
 */
/**
 * Run the analysis
 *
 * @param ana      The analyser obejct
 * @param filename Name of file to read from
 * @param nev      Number of events
 * @param load     Wether to load results
 * @param fini     Wether to finialise results
 *
 * @return JSON formatted result of analysis 
 */
json::JSON run(std::unique_ptr<ana::Analyzer>& ana,
	       const std::string&              filename,
	       size_t                          nev=0,
	       bool                            load=false,
	       bool                            fini=true)
{
  if (filename.empty() or filename == "-")
    return ana->run(std::cin, nev, load, fini);
  return ana->run(filename, nev, load, fini);
}
/**
 * Write output to file
 *
 * @param json Results
 * @param outn Ouptut file name
 */
void output(const json::JSON& json, const std::string& outn="")
{
  if (json.isNull()) return;

  if (outn.empty() or outn == "-")
    std::cout << json << std::endl;
  else {
    std::ofstream out(outn);
    out << json << std::endl;
    out.close();
  }
}
/** @} */ 
  
#endif
//
// EOF
//
