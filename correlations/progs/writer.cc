/**
 * @file   correlations/progs/writer.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple program writes out observations 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/ana/Writer.hh>
#include <correlations/gen/EtaPtPhiW.hh>
#include <correlations/gen/PtPhiW.hh>
#include <correlations/gen/EtaPhiW.hh>
#include <correlations/gen/PhiW.hh>
#include <correlations/gen/JetLike.hh>
#include <correlations/gen/Momentum.hh>
#include <correlations/gen/MomJet.hh>
#include "opt.hh"
#include "tools.hh"

template <typename T> using Option=exa::Option<T>;
using exa::CommandLine;
using correlations::Real;
using correlations::RealVector;

// ===================================================================
/**						
 * @{ 
 * @name Tools for setting up a generator 
 */
// ===================================================================
/** 
 * Make sampler 
 *
 * @param s    Sampler name 
 * @param e    Random engine 
 * @param vn   Flow coefficients 
 * @param ptb  Transverse momentum bins 
 * @param ptc  Transverse momentum modulation coefficients 
 * @param mpt  Mean transverse momentum 
 * @param aeta Slope of pseudorapidity distribution 
 * @param nphi Number of sample points 
 *
 * @returns Sampler 
 */
std::unique_ptr<gen::Sampler> sampler(const std::string&               s,
				      gen::RandomEngine&               e,
				      const correlations::RealVector&  vn,
				      const correlations::RealVector&  ptb,
				      const correlations::RealVector&  ptc,
				      Real                             mpt,
				      Real                             aeta,
				      size_t                           nphi=100)
{
  if (s == "full" || s == "etaptphiw")
    return mu<gen::EtaPtPhiW>(e,vn,ptb,ptc,mpt,aeta,nphi);
  if (s == "eta" || s == "etaphiw")
    return mu<gen::EtaPhiW>  (e,vn,            aeta,nphi);
  if (s == "pt" || s == "ptphiw")
    return mu<gen::PtPhiW>   (e,vn,ptb,ptc,mpt,     nphi);
  return   mu<gen::PhiW>     (e,vn,                 nphi);
}
// ___________________________________________________________________
/** 
 * Make a generator 
 *
 * @param g Generator name 
 * @param e Random engine 
 * @param s Sampler 
 * @param n Least multiplicity 
 * @param m Largeest multiplicity 
 * @param f fixed multiplicity 
 * @param r Random Psi 
 * @param a Acceptance 
 *
 * @return Generator 
 */
std::unique_ptr<gen::Generator> generator(const std::string& g,
					  gen::RandomEngine& e,
					  gen::Sampler&      s,
					  size_t             n,
					  size_t             m,
					  bool               f,
					  bool               r,
					  bool               a)
{
  if (beginsWith(g,"momjet"))return mu<gen::MomJet>   (e,s,n,m,f,r,a);
  if (beginsWith(g,"mom"))   return mu<gen::Momentum> (e,s,n,m,f,r,a);
  if (beginsWith(g,"jet"))   return mu<gen::JetLike>  (e,s,n,m,f,r,a);
  return                            mu<gen::Generator>(e,s,n,m,f,r,a);
}
/* @} */

// ===================================================================
int main(int argc, char** argv)
{
  // Generator 
  Option<std::string> gen   ('G',"Generator kind",             "base","GEN");
  Option<size_t>      minN  ('r',"Least number of particles",  1000,  "N");
  Option<size_t>      maxN  ('R',"Largest number of particles",0,     "N");
  Option<bool>        hungry('F',"Fixed number of particles",  true);
  Option<bool>        ranpsi('Y',"Use random event plane",     false);
  Option<bool>        accep ('A',"Simulate acceptance",        true);
  // Sampler
  Option<std::string> sam   ('S',"Sampler kind",               "full","SAMPL");
  Option<RealVector>  vn    ('v',"Flow coefficents",  {0.02, .07, .03},
			     "V1[,V2[,V3[,...]]]");
  Option<double>      etaA  ('E',"Slope of pseudorapidity",    0.1,   "SLOPE");
  Option<RealVector>  ptcoef('V',"pT modulation coefficients",
			     {0, 1.77,-0.31},
			     // {0., 1.77157, -0.305679},
			     "P0[,P1[,P2[,...]]]");
  Option<RealVector>  ptbin ('t',"pT bins",
			     {0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
			      1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.5, 3.0,
			      3.5, 4.0, 4.5, 5.0},"PT[,PT[,...]]");
  Option<double>      meanpt('T',"Mean transverse momentum",   0.5,   "GEV/C");
  // General options
  Option<size_t>      nev   ('e',"Number of events",           10000, "N");
  Option<size_t>      seed  ('s',"Random number seed",         123456,"SEED");
  Option<std::string> outn  ('o',"Output file",                "-",   "FILE");

  CommandLine& cl = CommandLine::instance();
  if (!cl.process(argc, argv)) return 0;

  unsigned int       sd = seed;
  std::random_device rd;
  auto               rdseed = rd();
  gen::RandomEngine  rnd(rdseed);
  if (sd > 0) rnd.seed(sd);
  else        seed._val = rdseed;

  std::string sn = downcase(sam);
  std::string gn = downcase(gen);

  std::string    on   = outn;
  std::ofstream* fout = 0;
  if (on.empty() or on != "-") 
    fout = new std::ofstream(on);
  
  auto psampler   = sampler  (sn,rnd,vn,ptbin,ptcoef,meanpt,etaA);
  auto pgenerator = generator(gn,rnd,*psampler,minN,maxN,hungry,ranpsi,accep);
  auto analyzer   = ana::Writer(fout ? *fout : std::cout);

  analyzer.run(*pgenerator, nev);
  if (fout) fout->close();

  return 0;
}

  
