/**
 * @file   correlations/progs/distributions.cc
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple program that calculates the distributions flow
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/ana/Distributions.hh>
#include "opt.hh"
#include "tools.hh"

using correlations::RealVector;
using correlations::Real;
using correlations::Size;

using ana::EtaGap;

template <typename T> using Option=exa::Option<T>;
using exa::CommandLine;

int main(int argc, char** argv)
{
  Option<std::string> filen ('i',"Input filename",             "",    "FILE");
  Option<size_t>      nev   ('e',"Number of events",           10000, "N");
  Option<bool>        load  ('l',"Load state from input",      false);
  Option<bool>        fini  ('f',"Finalize calculations",      true);
  Option<std::string> outn  ('o',"Output filename",            "",    "FILE");
  Option<RealVector>  ptbin ('t',"pT bins",
			     {0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9,
			      1.0, 1.2, 1.4, 1.6, 1.8, 2.0, 2.5, 3.0,
			      3.5, 4.0, 4.5, 5.0},"PT[,PT[,...]]");
  
  CommandLine& cl = CommandLine::instance();
  if (!cl.process(argc, argv)) return 0;

  std::string  fn    = filen; 
  RealVector   ptb   = ptbin;

  std::unique_ptr<ana::Analyzer> ana = mu<ana::Distributions>(ptb);

  json::JSON json     = run(ana, fn, nev, load, fini);
  output(json,outn);
  
  return 0; 
}

  


