#ifndef CORRELATIONS_META_FROMQVECTOR_HH
#define CORRELATIONS_META_FROMQVECTOR_HH
#include <correlations/QVector.hh>
#include <correlations/Correlator.hh>

namespace correlations
{
  namespace meta
  {
    namespace {
      /** A vector of harmonic orders */
      typedef std::vector<Size> SizeVector;
#if 0
      //==============================================================
      template <size_t tst, size_t head, size_t ... TAIL>
      struct SameId
      {
	static const bool result = tst == head &&
	  SameId<tst,TAIL>::result;
      };
      //--------------------------------------------------------------
      template <size_t tst, class none=void>
      struct SameId
      {
	static const bool result = true;
      };
#endif 	
	  
      //==============================================================
      template <Harmonic ... HS, Harmonic lh, Size ... SV, Size ls
		//, typename std::enable_if<SameId<ls,HS>::result>::type=0
		>
      Complex calc(const QVector& p,
		   const QVector& r,
		   const QVector& q)
      {
	Complex c = r(lh,ls) * calc<HS...,SV...>(p,r,q);
	if (ls > 1) return c;
	
	for (size_t i = 0; i < sizeof...(HS); i++) {
	  // Add lh to HS[i]
	  // Add 1 to SV[i]
	  c -= Real(SV[i]-1) * calc<HS...,SV...>(p,r,q);
	  // Subtract lh from HS[i]
	  // Subtract 1 from SV[i]
	}
	return c;
      }
#if 0
      template <Harmonic ... HS, Harmonic lh, Size ... SV, Size ls,
		typename std::enable_if<!SameId<ls,HS>::result>::type=0>
      Complex calc(const QVector& p,
		   const QVector& r,
		   const QVector& q)
      {
	return 0;
      }
#endif 
      template <Harmonic H1, Harmonic H2, Size S1, Size S2>
      Complex calc(const QVector& p,
		   const QVector& r,
		   const QVector& q)
      {
	return _q(H1,S1) * _r(H2,S2)  - _q(H1+H2,S1+S2);
      }
      template <Harmonic H1, Harmonic H2, Size S2>
      Complex calc<H1,H2,1,S2>(const QVector& p,
			       const QVector& r,
			       const QVector& q)
      {
	return p(H1,1) * r(H2,S2) - q(H1+H2,1+S2);
      }
      template <Harmonic H1, Size S1>
      Complex calc(const QVector& p,
		   const QVector& r,
		   const QVector& q)
      {
	return p(H1,S1);
      }
    }
    
    struct FromQVector
    {
      FromQVector(QVector& r, QVector& p, QVector& q)
	: _r(r), _p(p), _q(q)
      {}
      template <Harmonic ...h, Size ....s>
      Complex calculate()
      {
	return calc<h...,s....>(_p,_r,_q);
      }
    protected:
      QVector& _r;
      QVector& _p;
      QVector& _q;
    };
  }
}
#endif
//
// EOF
//


    

      
