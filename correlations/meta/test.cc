#include <correlations/meta/FromQVector.hh>
#include <correlations/progs/gen.hh>

using correlations::RealVector;
using correlations::Real;

int main()
{
  size_t      minN      = 300;
  size_t      maxN      = 0;
  size_t      nev       = 10000;
  size_t      maxP      = 4;
  size_t      maxH      = 4;
  double      meanpt    = 0.5;
  bool        hungry    = true;
  bool        ranpsi    = false;
  bool        weights   = false;
  RealVector  vn        = {.02, .07, .03};
  RealVector  ptcoef    = {0., 1.77157, -0.305679};
  RealVector  ptbin     = {0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0,
			   1.2, 1.4, 1.6, 1.8, 2.0,
			   2.5, 3.0, 3.5, 4.0, 4.5, 5.0};
  
  std::random_device             rd;
  gen::RandomEngine              rnd(rd());
  gen::PtPhiW                    s(rnd,vn,ptbin,ptcoef,meanpt,100);
  gen::Generator                 g(rnd,s,minN,maxN,hungry,ranpsi);
  gen::Generator::ParticleVector pv;

  correlations::QVector           r(8,4,weights);
  correlations::QVector           p(8,4,weights);
  correlations::QVector           q(8,4,weights);
  correlations::meta::FromQVector f(r,p,q);

  
  for (size_t iev = 0; iev < nev; iev++) {
    size_t n = g.generate(pv);

    for (auto& pp : pv) {
      Real pt      = std::get<1>(pp);
      Real phi     = std::get<2>(pp);
      Real w       = std::get<3>(pp);

      r.fill(phi, w);
      p.fill(phi, w);
      q.fill(phi, w);

    }

    correlations::Complex c = f.calculate<4,4,-4,-4,1,1,1,1>();
  }
  return 0;
}

      
      
  
