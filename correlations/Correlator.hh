#ifndef CORRELATIONS_CORRELATOR_HH
#define CORRELATIONS_CORRELATOR_HH
/**
 * @file   correlations/Correlator.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Base class for correlators
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/Result.hh>
#include <iostream>

/** 
 * @defgroup correlations Top-level module of this framework 
 *
 * This is where most of the user callable code lives.  See also 
 * @ref doc/_Overview.md 
 */
namespace correlations
{
  //____________________________________________________________________
  /**
   * Base class for calculating the correlations.
   *
   * Pointer to objects of concrete implementations can be created
   * using the class function correlations::Factory::make.  The use of
   * this class is described in @ref doc/_Overview.md.
   *
   * @tparam Harmonics Type of vector of harmonics 
   *
   * @headerfile ""  <correlations/Correlator.hh>
   * @ingroup correlations 
   */
  template <typename Harmonics=HarmonicVector>
  struct Correlator
  {
    /** Type of a harmonic vector */
    using HarmonicVector=Harmonics;
    /** The harmonics to use */
    using HarmonicElement=typename HarmonicVector::value_type;
    /** Complex type */
    using Complex=correlations::Complex;
    
    /** Zero-constant */ 
    enum {
      /** Value to mark ignored harmonics */
      kInvalidN = 0x7FFF
    };

    /**
     * Calculate the multi-particle correlation.
     *
     * @param h Vector of harmonics.  The correlator size is the size
     *          of this vector.
     *
     * @return The correlation
     */
    Result calculate(const HarmonicVector& h) const
    {
      return calculate(h.size(), h);
    }
    /**
     * Calculate up the multi-particle  correlator.
     *
     * @param m    How many particles to correlate
     * @param h    PartitionHarmonic of each term
     *
     * @return The correlation
     */
    Result calculate(const Size m, const HarmonicVector& h) const
    {
      if (maxSize() > 0 && m > maxSize())
	throw std::runtime_error("Number of particles " + std::to_string(m)
				 + " too big (max "+std::to_string(maxSize())
				 + ")");
	  
      // If we're above our fixed cut, or above 8, use the generic
      // algorithms
      return cN(m, h);
    }
    /**
     * @return Name of the correlator
     */
    virtual const char* name() const { return "Correlator"; }
    /**
     * Destructor
     */
    virtual ~Correlator() {}
    /** 
     * Maximum number of particles we can correlate 
     * 
     * @return Maximum 
     */
    virtual Size maxSize() const { return 0; }
  protected:
    /** 
     * Constructor 
     */
    Correlator() {}
    /** Deleted copy constructor */
    Correlator(const Correlator&) = delete;
    /** Move copy constructor */
    Correlator(Correlator&&) {}
    /** Deleted assignment operator */
    Correlator& operator=(const Correlator&) = delete;
    /**
     * Calculate the @f$ m@f$=particle correlation using harmonics
     * @f$\mathbf{h}@f$.
     *
     * @f[
     * C_{\mathbf{h}}\{m\} = \langle\exp[i(\sum_j^m h_j\phi_j)]\rangle
     * @f]
     *
     * @param m How many particles to correlate
     * @param h @f$=h_1,\ldots,h_n@f$ Harmonic of each term
     *
     * @return The correlator and the summed weights
     */
    virtual Result cN(const Size m, const HarmonicVector& h) const = 0;
  };
}
#endif
// Local Variables:
//  mode: C++
// End:
