#ifndef CORRELATIONS_RECURRANCE_HH
#define CORRELATIONS_RECURRANCE_HH
/**
 * @file   correlations/Recurrence.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Cumulant correlator using recursion
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/FromQVector.hh>
#include <algorithm>
#include <numeric>
#include <iomanip>

/** 
 * @defgroup correlations_recurrence Recurrence implementations 
 *
 * The calculations done by code in this module exploit the recurrence
 * of terms in the expansion of the @f$ m@f$-particle correlator.
 * This allows us to expand the calculations to any number of
 * particles - or as the computing time and power allows.
 * 
 * @ingroup correlations 
 */
namespace correlations
{
  /* 
   * Internal note: This was mainly done by Christian 
   */
  
  //____________________________________________________________________
  /**
   * Structure to calculate Cumulants of arbitrary order and power from
   * a given Q vector.
   *
   * This code used either generic recurssion to calculate the
   * correlator @f$ C\{n\}@f$ or - for a limited number of @f$ n@f$
   * specific recursive functions.
   *
   * @tparam Harmonics Vector of harmonics to use 
   *
   * @headerfile ""  <correlations/Recurrence.hh>
   * @ingroup correlations_recurrence 
   */
  template <typename Harmonics=HarmonicVector>
  struct Recurrence : public FromQVector<Harmonics>
  {
    /** Base type */
    using Base=FromQVector<Harmonics>;
    /** The type of vectors of harmonics to use */
    using HarmonicVector=typename Base::HarmonicVector;
    /** The harmonics to use */
    using HarmonicElement=typename Base::HarmonicElement;
    /** The type of Q-vector storage */
    using QStore=typename Base::QStore;
    /** The Q-vector to use */
    using QVector=typename Base::QVector;
    /** The complex type */
    using Complex=typename Base::Complex;
    /** A vector of harmonic orders */
    using SizeVector=std::valarray<size_t>;
    
    /**
     * Constructor
     *
     * @param q Q vector store 
     */
    Recurrence(const QStore& q) : Base(q) {}
    /** Move constructor */
    Recurrence(Recurrence&& o) : Base(std::move(o)) {}
    /** Deleted copy constructor */
    Recurrence(const Recurrence&) = delete;
    /** Deleted assignment operator */
    Recurrence& operator=(const Recurrence&) = delete;
    /**
     * @return Name of the correlator
     */
    virtual const char* name() const { return "Recurrance cumulant"; }
  protected:
    using Base::_q;
    using Base::_r;
    using Base::_p;
    /**
     * @{
     * @name Arbitrary order calculations using recursion
     */
    /**
     * Return the next combination of @a k element from the range
     * [@a first, @a last).
     *
     * @param first  Start of the range
     * @param k      Number of elements to get
     * @param last   End of range
     *
     * @return true if there are more (unique) combinations
     */
    template <typename Iterator>
    static bool nextCombination(const Iterator first,
				Iterator       k,
				const Iterator last)
    {
      /* Credits: Mark Nelson http://marknelson.us 
       * http://www.dogma.net/markn/articles/Permutations/
       */
      if ((first == last) /* No range? */
	  || (first == k) /* Nothing to select? */
	  || (last == k)  /* Nothing to select? */
	  || (last == first+1) /* No more to select from */)
	return false;

      // Point at the end of our selection
      Iterator i1 = k;
      // Point to the second to last
      Iterator i2 = last;
      --i2;

      // Then loop backward to the start
      while (first != i1) {
	if (*--i1 < *i2) {
	  // If the value at marker is less than the last entry,
	  // then point to our division
	  Iterator j = k;

	  // and if it's not smaller than current value, step into
	  // unused territory
	  while (!(*i1 < *j)) ++j;

	  // Swap the greater value with our current object
	  std::iter_swap(i1,j);

	  // Increment one
	  ++i1;
	  ++j;

	  // and go back to the divider
	  i2 = k;

	  // Rotate elements in the range i1->j, and put them at the end
	  std::rotate(i1,j,last);

	  // Then, while we haven't found our greater value,
	  // increment into unused territory
	  while (last != j) {
	    ++j;
	    ++i2;
	  }

	  // And rotate
	  std::rotate(k,i2,last);
	  return true;
	}
      }
      // And rotate
      std::rotate(first,k,last);
      return false;
    }
    /**
     * Calculate the multi-particle correlation
     *
     * The calculation is done using the following algorithm
     *
     * @verbatim
     *    C = (0,0)
     *    for k from n-1 downto 0 do
     *       for each combination c of k harmonics except h_n do
     *         let m = sum of harmonics not in c
     *         let p = number of harmonics not in c
     *         let s = -1^(n-k)
     *         C += s * (n-1-k)! * QC{k}(combination) * Q(m,p)
     *       end for each c
     *    end for k
     * @endverbatim
     *
     * @param m Order of correlation (number of particles to correlate)
     * @param h Harmonic
     *
     * @return @f$ QC{n}@f$
     */
    Complex eval(const Size m,const HarmonicVector& h) const
    {
      SizeVector n(m); // Allocate cache here
      for (size_t i = 0; i < m; i++) n[i] = i;
      return _eval(m, h, n);
    }
    /**
     * Calculate the multi-particle correlation
     *
     * The calculation is done using the following algorithm
     *
     * @verbatim
     *    C = (0,0)
     *    for k from n-1 downto 0 do
     *       for each combination c of k harmonics except h_n do
     *         let m = sum of harmonics not in c
     *         let p = number of harmonics not in c
     *         let s = -1^(n-k)
     *         C += s * (n-1-k)! * QC{k}(combination) * Q(m,p)
     *       end for each c
     *    end for k
     * @endverbatim
     *
     * @param m Order of correlation (number of particles to correlate)
     * @param h Harmonic @f$ h_i@f$
     * @param n Count of terms in each @f$ h_i@f$ 
     *
     * @return @f$ QC{n}@f$
     */
    Complex _eval(const Size            m,
		  const HarmonicVector& h,
		  SizeVector            n) const
    {
      if (m == 0) return Complex(1,0);//
      if (m == 1) return (n[0] == 0 ? _p(h[n[0]],1) : _r(h[n[0]],1));

      Complex r; // = x;
      Real    f = 1;
      Real    s = 1;
      Power   p = 1;
      for (Size i = m; i > 0; i--) {
	// Reset indices for combinations - must be sorted 
	SizeVector nn(n);
	std::sort(std::begin(n), std::begin(n)+m-1);
	  
	Size k  =  i-1;
	do {
	  Complex t = _eval(k, h, nn);

	  // The calculation
	  HarmonicElement a(0);
	  // for (size_t i=k; i < n; i++) a.push(h[nn[i]]);
	  for (size_t j=k; j < m; j++) a += h[nn[j]];

	  Complex pp = (nn[k] == 0 ? (p == 1 ? _p(a,p) : _q(a,p)) : _r(a,p));
	  Complex x  = s * f * t * pp;
	  r += x;
	} while (nextCombination(std::begin(nn),
				 std::begin(nn)+k,
				 std::begin(nn)+m-1));
	f       *= (m-k);
	s       *= -1;
	p++;
      }
      return r;
    }
    /* @} */
    void _prints(const SizeVector& n,
		 Size k, Size m, const std::string& ind) const
    {
      std::cout << ind << "[";
      for (Size j=k; j < m; j++) std::cout << n[j] << ' ';
      std::cout << "]" << std::endl;
    }
#if 0
    /**
     * @{
     * @name Fixed expression
     */
    using QCall = Complex (Recurrence::*)(const HarmonicElement&,const Size) const;
    /**
     * 1-particle correlator.
     *
     * Calculate
     * @f[
     * QC\{1\} = \langle\exp[i(h_1\phi_1)]\rangle
     * @f]
     *
     * @param h1 Harmonic @f$ h_1@f$
     * @param q  Q-vector to use 
     *
     * @return @f$ QC\{1\}@f$
     */
    Complex c1(const HarmonicElement h1, QCall q) const
    {
      return (this->*q)(h1);
    }
    Complex c1(const HarmonicElement n1) const
    {
      return c1(n1,&Recurrence::_p);
    }
    /**
     * Do the 2-particle calculation.
     *
     * @param h1 1st Harmonic
     * @param h2 2nd Harmonic
     *
     * @return the correlator
     */
    Complex c2(const HarmonicElement h1,
	       const HarmonicElement h2) const
    {
      return c2(h1,h2,&Recurrence::_p);
    }
    /** 
     * Real calculation of 2-particle correlator. 
     * 
     * @param n1 List of harmonics 
     * @param n2 List of harmonics 
     * @param p  Q-vector to use 
     * 
     * @return The correlator
     */
    Complex c2(const HarmonicElement n1,
	       const HarmonicElement n2,
	       QCall           p) const
    {
      return (this->*p)(n1) * _r(n2) - _q(n1+n2);
    }
    /**
     * Do the 3-particle calculation.
     *
     * @param h1 1st Harmonic
     * @param h2 2nd Harmonic
     * @param h3 3rd Harmonic
     *
     * @return the correlator
     */
    Complex c3(const HarmonicElement h1,
	       const HarmonicElement h2,
	       const HarmonicElement h3) const
    {
      return c3(h1,h2,h3,&Recurrence::_p);
    }
    /** 
     * Real calculation of 3-particle correlator from specifc
     * Q-vector.
     * 
     * @param h1 Harmonic
     * @param h2 Harmonic
     * @param h3 Harmonic
     * @param p  Q-vector
     * 
     * @return 3-particle correlator 
     */
    Complex c3(const HarmonicElement h1,
	       const HarmonicElement h2,
	       const HarmonicElement h3,
	       QCall           p) const
    {
      // const Real k2 = 2;
      return (c2(h1,h2, p) * _r(h3)
	      - c2(h1, h2+h3, p)
	      - c2(h1+h3, h2, &Recurrence::_q));
    }
    /**
     * Do the 4-particle calculation.
     *
     * @f[
     * QC_{1234}\{4\} = QC_{123}\{3\} QC_{4}\{1\}
     *   - QC_{23}\{2\} QC_{1+4}\{2\}^2
     *   - QC_{13}\{2\} QC_{2+4}\{2\}^2
     *   - QC_{12}\{2\} QC_{3+4}\{2\}^2
     *   + 2 QC_{3}\{1\} QC_{1+2+4}\{1\}^3
     *   + 2 QC_{2}\{1\} QC_{1+3+4}\{1\}^3
     *   + 2 QC_{1}\{1\} QC_{2+3+4}\{1\}^3
     *   - 6 QC_{1+2+3+4}\{1\}^4
     * @f]
     *
     * @param h1 1st Harmonic
     * @param h2 2nd Harmonic
     * @param h3 3rd Harmonic
     * @param h4 4th Harmonic
     *
     * @return the correlator
     */
    Complex c4(const HarmonicElement h1,
	       const HarmonicElement h2,
	       const HarmonicElement h3,
	       const HarmonicElement h4) const
    {
      return c4(h1,h2,h3,h4,&Recurrence::_p);
    }
    /** 
     * Real calculation of 4-particle correlator from specifc
     * Q-vector.
     * 
     * @param h1 Harmonic
     * @param h2 Harmonic
     * @param h3 Harmonic
     * @param h4 Harmonic
     * @param p  Q-vector
     * 
     * @return 4-particle correlator 
     */
    Complex c4(const HarmonicElement h1,
	       const HarmonicElement h2,
	       const HarmonicElement h3,
	       const HarmonicElement h4,
	       QCall           p) const
    {
      return (c3(h1,h2,h3,p)*_r(h4)
	      - c3(h1,h2,h3+h4,p)
	      - c3(h1,h3,h2+h4,p)
	      - c3(h1+h4,h2,h3,&Recurrence::_q));
    }
    /**
     * 5-particle correlator. 
     *
     * @f[
     * QC_{12345}\{5\} = QC_{1234}\{4\} QC_5\{1\}
     *   - QC_{234}\{3\} QC_{1+5}\{1\}^2
     *   - QC_{134}\{3\} QC_{2+5}\{1\}^2
     *   - QC_{124}\{3\} QC_{3+5}\{1\}^2
     *   - QC_{123}\{3\} QC_{3+5}\{1\}^2
     *   + 2 QC_{34}\{2\} QC_{1+2+5}\{1\}^3
     *   + 2 QC_{24}\{2\} QC_{1+3+5}\{1\}^3
     *   + 2 QC_{14}\{2\} QC_{2+3+5}\{1\}^3
     *   + 2 QC_{23}\{2\} QC_{1+4+5}\{1\}^3
     *   + 2 QC_{13}\{2\} QC_{2+4+5}\{1\}^3
     *   + 2 QC_{12}\{2\} QC_{3+4+5}\{1\}^3
     *   - 6 QC_4\{1\} QC_{1+2+3+5}\{1\}^4
     *   - 6 QC_3\{1\} QC_{1+2+4+5}\{1\}^4
     *   - 6 QC_2\{1\} QC_{1+3+4+5}\{1\}^4
     *   - 6 QC_1\{1\} QC_{2+3+4+5}\{1\}^4
     *   + 24 QC_{1+2+3+4+5}\{1\}^5
     * @f]
     *
     *
     * @param h1 1st Harmonic
     * @param h2 2nd Harmonic
     * @param h3 3rd Harmonic
     * @param h4 4th Harmonic
     * @param h5 5th Harmonic
     *
     * @return The correlator
     */
    Complex c5(const HarmonicElement h1,
	       const HarmonicElement h2,
	       const HarmonicElement h3,
	       const HarmonicElement h4,
	       const HarmonicElement h5) const
    {
      const Real k2  = 2;
      const Real k6  = 6;
      const Real k24 = 24;
      return (c4(h1,h2,h3,h4)  * _r(h5)
	      - c3(h2,h3,h4)   * _r(h1+h5)
	      - c3(h1,h3,h4)   * _r(h2+h5)
	      - c3(h1,h2,h4)   * _r(h3+h5)
	      - c3(h1,h2,h3)   * _r(h4+h5)
	      + k2 * c2(h3,h4) * _r(h1+h2+h5)
	      + k2 * c2(h2,h4) * _r(h1+h3+h5)
	      + k2 * c2(h1,h4) * _r(h2+h3+h5)
	      + k2 * c2(h2,h3) * _r(h1+h4+h5)
	      + k2 * c2(h1,h3) * _r(h2+h4+h5)
	      + k2 * c2(h1,h2) * _r(h3+h4+h5)
	      - k6 * c1(h4)    * _r(h1+h2+h3+h5)
	      - k6 * c1(h3)    * _r(h1+h2+h4+h5)
	      - k6 * c1(h2)    * _r(h1+h3+h4+h5)
	      - k6 * c1(h1)    * _r(h2+h3+h4+h5)
	      + k24 * _r(h1+h2+h3+h4+h5));
    }
    /* @} */
#endif 
  };
}
#endif
// Local Variables:
//  mode: C++
// End:
