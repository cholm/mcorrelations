#ifndef CORRELATIONS_TYPES_HH
#define CORRELATIONS_TYPES_HH
/**
 * @file   correlations/Types.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Declarations of types
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <vector>
#include <complex>

/**
 * Namespace for correlations code
 *
 * @ingroup correlations 
 */
namespace correlations
{
  /**
   * @defgroup correlations_types Simple types 
   * 
   * Here, we define some simple types we will use throughout 
   *
   * @ingroup correlations
   */
  /**
   * Type of Real.
   *
   * Redefining to this to be long double say would make all of the code
   * use that precsion.
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  using Real=double;
  /** Type defintion of the kind of complex we use 
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  using Complex=std::complex<Real>;
  constexpr Complex cnull{0,0};
  /** Type of harmonics 
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  using Harmonic=short;

  /** 
   * Absolute value of harmonic 
   *
   * @param h Harmonic 
   *
   * @return Absolute value
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types  
   */
  Harmonic habs(const Harmonic& h)
  {
    return std::abs(float(h));
  }
  /** 
   * Check if real is positive and not small 
   */
  bool isSmall(const Real& v)
  {
    return v < 1e-9;
  }
  /** 
   * Check if absolute value of real is small 
   */
  bool isAbsSmall(const Real& v)
  {
    return isSmall(fabs(v));
  }
  Real power(const Real& r, const Real& p)
  {
    return std::pow(r, p);
  }
}
#include <correlations/Partition.hh>
#include <correlations/MoreTypes.hh>
#endif
// Local Variables:
//   mode: C++
// End:
