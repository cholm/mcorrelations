/**
 * @file   correlations/flow/ToValue.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2- and 4-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_FLOW_TOVALUE_HH
#define CORRELATIONS_FLOW_TOVALUE_HH
#include <correlations/Types.hh>
#include <complex>

namespace correlations
{
  /** 
   * Namespace for flow code 
   *
   * @ingroup flow 
   */
  namespace flow
  {
    /** 
     * Extract value from a correlator result 
     *
     * @tparam T The value type 
     */
    template <typename T>
    struct ToValue
    {
      static T eval(const Result& r)
      {
	return r.eval().real();
      }
    };
    /** 
     * Extract value from a correlator result 
     *
     * @tparam T The value type 
     */
    template <>
    struct ToValue<std::complex<correlations::Real>>
    {
      static std::complex<correlations::Real> eval(const Result& r)
      {
	return r.eval();
      }
    };
  }
}
#endif
//
// EOF
//

