/**
 * @file   correlations/flow/Bin.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A single bin in a flow analysis. 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_FLOW_BIN_HH
#define CORRELATIONS_FLOW_BIN_HH
#include <correlations/Result.hh>
#include <correlations/json.hh>
#include <iomanip>

namespace correlations
{
  namespace flow
  {
    // =================================================================
    /** 
     * A single bin for analysis.  This is a base class for bins of flow
     * coefficient calculations.  The derived class IBin calculates the
     * integrated flow coefficients, while sub-classes DBin and DBinI
     * calculates differentially. The interface is the same - e.g., 
     *
     * @code 
     Bin bin(MaxOrder(),MaxParticles()); // Really - sub-class
   
     while (MoreEvents()) {
       bin.reset();
       while (MoreObservations()) {
         unsigned int flags = ((IsReference()          ? Bin::REF : 0) | 
                               (IsParticleOfInterest() ? Bin::POI : 0));
         bin.fill(Phi(), Weight(), flags);
       }
       bin.update();
     }
 
     bin.write(output);
     @endcode
     *
     * @headerfile "" <correlations/flow/Bin.hh>
     * @ingroup flow 
     */
    struct Bin
    {
      /** Type of real values */
      using Real=correlations::Real;
      /** Type of real values */
      using RealVector=correlations::RealVector;
      /** Type of sizes */
      using Size=correlations::Size;
      /** Type of a correlator result @f$ C_{\mathbf{h}}\{m\}@f$ */
      using Correlation=correlations::Result;
      /** Type of a correlator results @f$ C_{\mathbf{h}}\{m\}@f$ */
      using Correlations=correlations::ResultVector;
      /** Kind of observation */
      using Kind=correlations::Kind;
      /** Kinds of observation */
      using Kinds=correlations::Kinds;
      /** Partition of observation */
      using Partition=correlations::Partition;
      /** Partitions of observation */
      using Partitions=correlations::Partitions;

      /** Destructor */
      virtual ~Bin() {}
      /** Move constructor */
      Bin(Bin&&) {}
      /** Deleted copy constructor */
      Bin(const Bin&) = delete;
      
      /** Deleted assignment operator */
      Bin& operator=(const Bin&) = delete;
      /**
       * Reset internal caches.  Note, derived classes should overload
       * the _clear method, not this method.
       */
      virtual void reset() = 0;
      /** 
       * Fill in an observation.
       *
       * @param phi    The observed @f$\varphi@f$ angle 
       * @param weight The weight of the observation 
       * @param kind   Type of observation (bit mask of types)
       * @param part   Partition 
       */
      virtual void fill(Real phi,
			Real weight,
			Kind kind=REF,
			Partition part=0) = 0;
      /** 
       * Fill in observations. 
       *
       * @param phi    The observed @f$\varphi@f$ angle 
       * @param weight The weight of the observation 
       * @param kind   Type of observation (bit mask of types)
       * @param part   Partition 
       */
      virtual void fill(const RealVector& phi,
			const RealVector& weight,
			const Kinds&      kind,
			const Partitions& part) = 0;
      /** 
       * Calculate per-event correlators 
       */
      virtual void update() = 0;
      /** 
       * Get map of results.  
       *
       * A derived class can override this method to prepend or append
       * additional results - e.g., the indendent variable can be
       * prepended, or additional information can be appended.  If a
       * derived class adds information in any way, it should also
       * override the member function headers to give the appropriate
       * header for the added information.  
       *
       * User defined columns should have @f$ m=0@f$, which makes the
       * default implementation of headers fill in blank headers.
       *
       * @return Results 
       */
      virtual json::JSON result() const = 0;
      /** 
       * Make a harmonic vector size @f$ m@f$ with elements 
       *
       * @f[h=\{\ldots,n,-n,\ldots\}\quad,@f]
       *
       * where @f$ n@f$ is the harmonic factor and there are @f$ m@f$
       * elements in @f$ h@f$
       */
      template <typename HV>
      HV makeNH(size_t m, typename HV::value_type n) const
      {
	HV h(n, m);
	for (size_t i = m/2; i < m; i++) h[i] *= -1;
	return h;
      }
      /** 
       * Dump state to JSON 
       *
       * @return JSON representation
       */
      virtual json::JSON toJson() const = 0;      
      /** 
       * Read state from JSON 
       *
       * @param json JSON object
       */
      virtual void fromJson(const json::JSON& json) = 0;
      /** 
       * Merge other bin in here 
       *
       * @param o Other bin to merge in 
       *
       * @return reference to this object 
       */
      virtual Bin& merge(const Bin& o) = 0;
      /** 
       * Merge state from JSON object 
       *
       * @param json JSON object to load from
       */
      virtual void mergeJson(const json::JSON& json) = 0;
    protected:
      /** 
       * A light-weight view of a container pointing from some element
       * to the end of the parent container
       */
      struct CorrelationView
      {
	/** Parent container type */
	using Container=Correlations;
	/** Constant iterator over container */
	using Iterator=typename Container::const_iterator;
	/** Value stored in container */
	using Value=Correlation;
	
	/** 
	 * Construct from a pair of iterators 
	 *
	 * @param b Start of range 
	 * @param e End of range 
	 */
	CorrelationView(Iterator b, Iterator e) : _b(b), _e(e) {}
	/** 
	 * Constructor from container and off-set from the beginning. 
	 *
	 * @param c Container 
	 * @param off Offset 
	 */
	CorrelationView(const Container& c, size_t off)
	  : _b(std::begin(c)+off), _e(std::end(c))
	{}
	/** 
	 * Move constructor */
	CorrelationView(CorrelationView&& o)
	  : _b(std::move(o._b)), _e(std::move(o._e))
	{}
	/** Deleted copy constructor */
	CorrelationView(const CorrelationView&) = delete;
	/** Deleted assignment operator */
	CorrelationView& operator=(const CorrelationView&) = delete;
	/** Get size of the view */
	size_t size() const { return std::distance(_b, _e); }
	/** 
	 * Access element in view,
	 * 
	 * @param i Index 
	 *
	 * @return Element 
	 */
	const Value& operator[](size_t i) const { return *(_b + i);}
	/** 
	 * Get iterator pointing to beginning 
	 *
	 * @return Iterator 
	 */
	const Iterator& begin() const { return _b; }
	/** 
	 * Get iterator pointing to just after last 
	 *
	 * @return Iterator 
	 */
	const Iterator& end() const { return _e; }
	/** Start of range */
	Iterator _b;
	/** End of range */
	Iterator _e;
      };
      /** Constructor */
      Bin() {}
    };
  }
}
#endif
//
// EOF
//
