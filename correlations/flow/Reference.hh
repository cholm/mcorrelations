/**
 * @file   correlations/flow/Reference.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2-particle differential
 * 2nd, 3rd, 4th, 5th, and 6th flow harmonics.
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_FLOW_REFERENCE_HH
#define CORRELATIONS_FLOW_REFERENCE_HH
#include <correlations/flow/VNM.hh>
#include <correlations/flow/BinT.hh>
#include <correlations/Closed.hh>
#include <correlations/QStore.hh>

namespace correlations
{
  namespace flow
  {
    /** 
     * Base class template for reference bins.  This contains the
     * common code independent of the actual harmonics used
     *
     * @tparam T The value type 
     * @tparam Uncertainties Type of estimator base-class 
     * 
     * @headerfile "" <correlations/flow/Reference.hh>
     * @ingroup flow 
     */
    template <typename T=correlations::Real,
	      template <typename> class Uncertainty=stat::Derivatives>
    struct ReferenceBase : public BinT<VNM<T,Uncertainty>>
    {
      /** Base type */
      using Base=BinT<VNM<T,Uncertainty>>;
      /** Type of correlator result vector */
      using Correlations=typename Base::Correlations;
      /** Type of correlator result */
      using Correlation=typename Base::Correlation;
      /** Kind of observation */
      using Kind=typename Base::Kind;
      /** Kinds of observation */
      using Kinds=typename Base::Kinds;
      /** Partition of observation */
      using Partition=typename Base::Partition;
      /** Partitions of observation */
      using Partitions=typename Base::Partitions;
      
      /** 
       * Constructor 
       *
       * @param maxN    Maximum order 
       * @param maxP    Number of particles to correlate 
       */
      ReferenceBase(size_t maxN=4, size_t maxP=4)
	: Base(maxN,maxP),
	  _x(maxN),
	  _updated(false)
      {
	for (auto& x : _x) x.resize(maxP/2);
      }
      /** 
       * Move constructor 
       *
       * @param o Object to move data from 
       */
      ReferenceBase(ReferenceBase&& o)
	: Base(std::move(o)),
	  _x(std::move(o._x)),
	  _updated(o._updated)
      {
      }
      /** 
       * Copy constructor (deleted)
       */
      ReferenceBase(const ReferenceBase&) = delete;
      /** 
       * Assignment operator (deleted)
       */
      ReferenceBase& operator=(const ReferenceBase&) = delete;
      /** 
       * Calculate per-event correlators 
       */
      virtual void update()
      {
	if (_updated) return;

	size_t mK = _vnm.size();
	for (size_t k = 0; k < _vnm.size(); k++) {
	  Size   m   = 2*(k+1);
	  auto&  vni = _vnm[k];
	  for (size_t l = 0; l < vni.size(); l++) {
	    Size   n = l + 1;
	    size_t j = mK - k - 1;
	    auto&  c = _x[l];
	    c[j]     =  calculate(m,n);
	    vni[l].update(CorrelationView(c,j));
	  }
	}
	_updated = true;
      }
      /** 
       * @{ 
       * @name Additional methods specific to integrated bins and
       * used by differential bins with external integrated bins
       */
      /** @return Wether this bin is using weights */
      virtual bool useWeights() const = 0;
      /** 
       * Get the correlator @f$ C_{\mathbf{n}}\{m\}@f$ of the last event. 
       *
       * The correlators are stored harmonic order first, then reverse
       * number of particles.
       *
       * @f[
       * \begin{bmatrix}
       *  \begin{bmatrix} 
       *    C_{\mathbf{1}}\{2\}&\ldots&C_{\mathbf{1}}\{2k\}
       *  \end {bmatrix}\\
       *  \begin{bmatrix} 
       *    \vdots             &\ddots&\vdots
       *  \end {bmatrix}\\
       *  \begin{bmatrix} 
       *    C_{\mathbf{n}}\{2\}&\ldots&C_{\mathbf{n}}\{2k\}
       *  \end {bmatrix}\\
       * \end{bmatrix}
       * @f]
       *
       * Note, the correlators are stored in reverse order, so that
       * for @f$\max(m)=8@f$ we have 
       *
       * @verbatim 
       * m=2 -> (max(m) - m) / 2 = (8 - 2) / 2 = 6 / 2 = 3
       * ...
       * m=8 -> (max(m) - m) / 2 = (8 - 8) / 2 = 0 / 2 = 0
       * @endverbatim
       *
       * Note @c max(m) is twice the size of the internal store @c
       * x[n-1], so in terms of code
       *
       * @verbatim 
       * m=2 -> (max(m) - m) / 2 = (2*x.size() - 2) / 2 = 6 / 2 = 3
       * ...
       * m=8 -> (max(m) - m) / 2 = (2*x.size() - 8) / 2 = 0 / 2 = 0
       * @endverbatim       
       *
       * or 
       *
       * @verbatim 
       * m=2 -> x.size() - m/2 = 4 - 1 = 3
       * ...
       * m=8 -> x.size() - m/2 = 4 - 4 = 0
       * @endverbatim
       *
       * @return m-particle correlator result of last event for a
       * harmonic order @f$ n@f$ and @f$m@f$-particle correlations
       * @f$C_{\mathbf{n}}\{m\}@f$
       */
      const Correlation& c(Size m, Size n) const
      {
	auto& x = c(n);
	return x[x.size()-m/2];
      }
      /**
       * Get correlators for a particular harmonic order @f$ n@f$. 
       *
       *
       * The correlators are stored harmonic order first, then reverse
       * number of particles.
       *
       * @f[
       * \begin{bmatrix}
       *  \begin{bmatrix} 
       *    C_{\mathbf{1}}\{2\}&\ldots&C_{\mathbf{1}}\{2k\}
       *  \end {bmatrix}\\
       *  \begin{bmatrix} 
       *    \vdots             &\ddots&\vdots
       *  \end {bmatrix}\\
       *  \begin{bmatrix} 
       *    C_{\mathbf{n}}\{2\}&\ldots&C_{\mathbf{n}}\{2k\}
       *  \end {bmatrix}\\
       * \end{bmatrix}
       * @f]
       *
       * @return m-particle correlators of last event for harmonic
       * order @f$n@f$
       */
      const Correlations& c(size_t n) const  { return _x[n-1];  }
      /* @} */
    protected:
      /** Clarify scope */
      using Base::_vnm;
      /** Vector of vectors of results */
      using CorrelationsVector=typename Base::CorrelationsVector;
      /** Type of correlator result vector */
      using CorrelationView=typename Base::CorrelationView;
      /**
       * Reset internal caches.
       */
      virtual void reset()
      {
	_updated = false;
      }
      /** 
       * Do the correlator calculation.  Derived classes must define
       * this.
       */
      virtual Correlation calculate(size_t m, size_t n) = 0;
      /** m-particle correlator results of last event */
      CorrelationsVector _x;
      /** Whether we have been updated */
      bool _updated;
    };
    	
    // _________________________________________________________________
    /** 
     * A single bin for analysis.  This is for integrated measurements.
     *
     * Objects of this class will calculate the integrated (reference)
     * flow coefficients.
     *
     * @code 
     Reference ref(MaxOrder(),MaxParticles());
   
     while (MoreEvents()) {
     ref.reset();
     while (MoreObservations()) 
     ref.fill(Phi(), Weight());
      
     ref.update();
     }
     @endcode
     *
     * One can sub-class this class for convenience, though it makes
     * less sense than for the differential bins. 
     *
     * @tparam T             The value type 
     * @tparam Uncertainties Type of estimator base-class 
     * @tparam Correlator    Algorithm for calculating correlators 
     * @tparam Harmonics     Harmonic type to use (decorated or not)
     * 
     * @headerfile "" <correlations/flow/Reference.hh>
     * @ingroup flow 
     */
    template <typename T=correlations::Real,
	      template <typename> class Uncertainty=stat::Derivatives,
	      template <typename> class Correlator=correlations::Closed,
	      typename Harmonics=correlations::HarmonicVector>
    struct Reference;

    /** 
     * Class template partial specialisation for undecorated harmonics 
     * (not in partitions). 
     *
     * @tparam T             The value type 
     * @tparam Uncertainties Type of estimator base-class 
     * @tparam Correlator    Algorithm for calculating correlators 
     * @tparam Harmonics     Harmonic type to use (decorated or not)
     * 
     * @headerfile "" <correlations/flow/Reference.hh>
     * @ingroup flow 
     */
    template <typename T,
	      template <typename> class Uncertainty,
	      template <typename> class Correlator>
    struct Reference<T,
		     Uncertainty,
		     Correlator,
		     correlations::HarmonicVector>
      : public ReferenceBase<T,Uncertainty>
    {
      /** Base type */
      using Base=ReferenceBase<T,Uncertainty>;
      /** Type of correlator to use */
      using FromQVector=Correlator<correlations::HarmonicVector>;
      /** Type of harmonic vectors */
      using HarmonicVector=typename FromQVector::HarmonicVector;
      /** QVector type */
      using QVector=typename FromQVector::QVector;
      /** QVector type */
      using QStore=typename FromQVector::QStore;
      /** Type of correlator result */
      using Correlation=typename Base::Correlation;
      /** Kind of observation */
      using Kind=typename Base::Kind;
      /** Kinds of observation */
      using Kinds=typename Base::Kinds;
      /** Partition of observation */
      using Partition=typename Base::Partition;
      /** Partitions of observation */
      using Partitions=typename Base::Partitions;
      
      /** 
       * Constructor 
       *
       * @param maxN    Maximum order 
       * @param maxP    Number of particles to correlate 
       * @param weights If true, use weights
       */
      Reference(size_t maxN=4, size_t maxP=4, bool weights=false)
	: Base(maxN,maxP),
	  _q(maxN,maxP,weights),
	  _s(_q,_q,_q),
	  _c(_s),
	  _h(maxN)
      {
	int n = 0;
	for (auto& h : _h) {
	  n++;
	  h.resize(maxP/2);
	  for (size_t k=0; k < maxP/2; k++) {
	    h[k].resize(2*(k+1)); // m 
	    std::fill(std::begin(h[k]), std::end(h[k]), n);
	    for (size_t j=k+1; j<2*k; j++) h[k][j] *= -1;
	  }
	}
      }
      
      /** 
       * Move constructor 
       *
       * @param o Object to move data from 
       */
      Reference(Reference&& o)
	: Base(std::move(o)),
	  _q(std::move(o._q)),
	  _s(_q,_q,_q), // Must recreate to set references correctly
	  _c(_s) // Must recreate to set references correctly
      {}
      /** 
       * Copy constructor (deleted)
       */
      Reference(const Reference&) = delete;
      /** 
       * Assignment operator (deleted)
       */
      Reference& operator=(const Reference&) = delete;
      /** 
       * Get the harmonic vector to use for m particles and harmonic n 
       *
       * @param m Number of particles to correlator 
       * @param n Harmonic order 
       *
       * @return Harmonic vector to use 
       */
      const HarmonicVector& h(size_t m, size_t n) const
      {
	return _h[n-1][m/2-1];
      }
      /**
       * Reset internal caches.
       */
      virtual void reset()
      {
	Base::reset();
	_s.reset();
      }
      /** 
       * Fill in an observation.
       *
       * @param phi    The observed @f$\varphi@f$ angle 
       * @param weight The weight of the observation 
       * @param kind   Kind 
       */
      virtual void fill(Real phi, Real weight, Kind kind=REF, Partition=0)
      {
	_s.fill(phi, weight, kind);
      }
      /** 
       * Fill in observations.  
       *
       * @param phi    The observed @f$\varphi@f$ angle 
       * @param weight The weight of the observation 
       * @param kind   Kind 
       */
      virtual void fill(const RealVector& phi,
			const RealVector& weight,
			const Kinds&      kind,
			const Partitions& =Partitions())
      {
	_s.fill(phi, weight, kind);
      }
      /** 
       * @{ 
       * @name Additional methods specific to integrated bins and
       * used by differential bins with external integrated bins
       */
      /** @return Reference to the used @f$ Q@f$-vector */
      QVector& q() { return _q; }
      /** @return Wether this bin is using weights */
      bool useWeights() const { return _q.useWeights(); }
      /* @} */
    protected:
      using HarmonicMap=std::vector<std::vector<HarmonicVector>>;
      /** 
       * Do the correlator calculation 
       */
      virtual Correlation calculate(size_t m, size_t n)
      {
	return _c.calculate(h(m,n));
      }
      /** Q-vector of overlap of reference and particles-of-interest */
      QVector _q;
      /** Q-vector storate */
      QStore _s;
      /** Integrated correlator that uses cumulants */
      FromQVector _c;
      /** Mapping of harmonics to use */
      HarmonicMap _h;
    };

    /** 
     * Class template partial specialisation for decorated harmonics 
     * (in partitions). 
     *
     * @tparam T             The value type 
     * @tparam Uncertainties Type of estimator base-class 
     * @tparam Correlator    Algorithm for calculating correlators 
     * 
     * @headerfile "" <correlations/flow/Reference.hh>
     * @ingroup flow 
     */
    template <typename T,
	      template <typename> class Uncertainty,
	      template <typename> class Correlator>
    struct Reference<T,
		     Uncertainty,
		     Correlator,
		     correlations::PartitionHarmonicVector>
      : public ReferenceBase<T,Uncertainty>
    {
      /** Base type */
      using Base=ReferenceBase<T,Uncertainty>;
      /** Type of correlator to use */
      using FromQVector=Correlator<correlations::PartitionHarmonicVector>;
      /** Type of harmonic vectors */
      using HarmonicVector=typename FromQVector::HarmonicVector;
      /** QVector type */
      using QVector=typename FromQVector::QVector;
      /** QVector type */
      using QStore=typename FromQVector::QStore;
      /** QContainer type */
      using QContainer=correlations::QContainer;
      /** Type of correlator result */
      using Correlation=typename Base::Correlation;
      /** Kind of observation */
      using Kind=typename Base::Kind;
      /** Kinds of observation */
      using Kinds=typename Base::Kinds;
      /** Partition of observation */
      using Partition=typename Base::Partition;
      /** Partitions of observation */
      using Partitions=typename Base::Partitions;
      
      /** 
       * Constructor 
       *
       * @param maxN       Maximum order 
       * @param maxP       Number of particles to correlate 
       * @param weights    If true, use weights
       * @param partitions Partition identifiers 
       */
      Reference(size_t            maxN=4,
		size_t            maxP=4,
		bool              weights=false,
		const Partitions& partitions={1})
	: Base(maxN,maxP),
	  _q(makeQC(partitions.max(),maxN,maxP,weights)),
	  _s(_q,_q,_q),
	  _c(_s),
	  _h(maxN)
      {
	size_t    np = partitions.size();
	Harmonic  n  = 0;
	for (auto& h : _h) {
	  n++;
	  h.resize(maxP/2);
	  for (size_t k = 0; k < maxP/2; k++) {
	    size_t m = 2*(k+1);
	    h[k].resize(m);
	    
	    for (size_t j = 0; j < k+1; j++) {
	      h[k][j]     = std::make_pair(n, partitions[j]);
	      h[k][m-1-j] = std::make_pair(-n,partitions[np-1-j]);
	    }
	  }
	}
      }
      /** 
       * Move constructor 
       *
       * @param o Object to move data from 
       */
      Reference(Reference&& o)
	: Base(std::move(o)),
	  _q(std::move(o._q)),
	  _s(_q,_q,_q), // Must recreate to set references correctly
	  _c(_s),       // Must recreate to set references correctly
	  _h(std::move(o._h)) 
      {}
      /** 
       * Copy constructor (deleted)
       */
      Reference(const Reference&) = delete;
      /** 
       * Assignment operator (deleted)
       */
      Reference& operator=(const Reference&) = delete;
      /** 
       * Get the harmonic vector to use for m particles and harmonic n 
       *
       * @param m Number of particles to correlator 
       * @param n Harmonic order 
       *
       * @return (Decorated) Harmonic vector to use 
       */
      const HarmonicVector& h(size_t m, size_t n) const
      {
	return _h[n-1][m/2-1];
      }
      /**
       * Reset internal caches.
       */
      virtual void reset()
      {
	Base::reset();
	_s.reset();
      }
      /** 
       * Fill in an observation.
       *
       * @param phi    The observed @f$\varphi@f$ angle 
       * @param weight The weight of the observation 
       * @param kind   Kind 
       * @param part   Partition
       */
      virtual void fill(Real phi, Real weight, Kind kind=REF, Partition part=0)
      {
	_s.fill(phi, weight, kind, part);
      }
      /** 
       * Fill in observations.  
       *
       * @param phi    The observed @f$\varphi@f$ angle 
       * @param weight The weight of the observation 
       * @param kind   Kind 
       * @param part   Partition
       */
      virtual void fill(const RealVector& phi,
			const RealVector& weight,
			const Kinds&      kind,
			const Partitions& part=Partitions())
      {
	_s.fill(phi, weight, kind, part);
      }
      /** 
       * @{ 
       * @name Additional methods specific to integrated bins and
       * used by differential bins with external integrated bins
       */
      /** @return Reference to the used @f$ Q@f$-vector */
      QContainer& q() { return _q; }
      /** @return Wether this bin is using weights */
      bool useWeights() const { return _q.front().useWeights(); }
      /* @} */
    protected:
      using HarmonicMap=std::vector<std::vector<HarmonicVector>>;
      /** 
       * Do the correlator calculation 
       */
      virtual Correlation calculate(size_t m, size_t n)
      {
	return _c.calculate(h(m,n));
      }
      /** Q-vector of overlap of reference and particles-of-interest */
      QContainer _q;
      /** Storage */
      QStore _s;
      /** Integrated correlator that uses cumulants */
      FromQVector _c;
      /** Mapping of harmonics to use */
      HarmonicMap _h;
    };
  }
}
#endif
//
// EOF
//
