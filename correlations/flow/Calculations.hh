/**
 * @file   correlations/flow/Calculations.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A utility class for performing various calculations
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_FLOW_CALCULATIONS_HH
#define CORRELATIONS_FLOW_CALCULATIONS_HH
#include <correlations/Types.hh>
#include <correlations/stat/Stat.hh>
#include <vector>
#include <stdexcept>
#include <string>
// #include <iostream>

namespace correlations
{
  namespace flow
  {
    /**
     * Helper class to calculate various quantities 
     *
     * @tparam T The value type 
     * @ingroup flow 
     */
    template <typename T=correlations::Real>
    struct Calculations
    {
      using Size=typename stat::Stat<T>::Size;
      using Value=typename stat::Stat<T>::Value;
      using ValueVector=typename stat::Stat<T>::ValueVector;
      using Real=correlations::Real;
      
      //--------------------------------------------------------------
      /** 
       * @{ 
       * @name Some general utilities 
       */
      /** 
       * Calculate the binomial coefficent.
       *
       * @f[\begin{pmatrix} n\\k\end{pmatrix} = \frac{n!}{(n-k)!k!}\quad.@f]
       *
       * Since we will only use this for realitively small values of
       * @f$n@f$ (and hence @f$k@f$) we do not use any approximation
       * for large numbers (e.g., via a @f$\beta@f$ function), and we
       * do integer arithmatic.
       *
       * @param n @f$n@f$ 
       * @param k @f$k@f$ 
       *
       * @return @f$\begin{pmatrix} n\\k\end{pmatrix}@f$ 
       */
      static size_t binom(size_t n, size_t k)
      {
	if (k == 0)    return size_t(1);
	if (k > n / 2) return binom(n,n-k);
	return n * binom(n-1,k-1) / k;
      }
      /** 
       * Return the factor @f$ M_k@f$ where @f$ m=2k@f$, from the
       * sequence
       *
       * @f[M=\{1,1,4,33,456,9460,274800,10643745,\ldots\}\quad,@f]
       *
       * via 
       *
       * @f{eqnarray}{
       * M_k &=& \sum_{i=1}^{k-1}\binom{k}{i}\binom{k}{k-i}M_{i}M_{k-i}\\
       * M_1 &=& 1\quad.
       * @f}
       * 
       * @sa 
       * - https://oeis.org/A002190
       *
       * On a 64-bit machine (most these days), the maximum @f$k@f$ is
       * 14, due to limited range of the @c size_t type.
       *
       * @param k @f$ k@f$ 
       *
       * @return @f$ M_k@f$ 
       */
      static size_t mk(size_t k)
      {
	// if (k > 14)
	//   throw std::runtime_error("Not enough precision for mk(k>14)");
	static std::vector<size_t> m = {1};
	size_t l = m.size();
	if (l < k) {
	  m.resize(k);
	  for (size_t n = l; n < k; n++) {
	    m[n] = 0;
	    for (size_t i = 0; i < n; i++)
	      m[n] += binom(n,i)*binom(n,n-i-1)*m[i]*m[n-i-1];
	  }
	}
	return m[k-1];
      }
      /* @} */
      //--------------------------------------------------------------
      /** 
       * @{ 
       * @name Flow harmonics and derivatives 
       */
      /** 
       * Calculate @f$ v_n\{m\}@f$ as a function of to @f$ c_n\{m\}@f$ 
       *
       * This calculates 
       *
       * @f[ 
       * v_n\{2k\} = \sqrt[{\textstyle 2k}]{(-1)^{k-1}\frac{1}{M_k}c_{n}\{2k\}}
       * \quad,
       * @f]
       *
       * where @f$ m = 2k@f$ and @f$ M_k@f$ is the @f$ k@f$-th element
       * of the sequence
       *
       * @f[M=\{1,1,4,33,456,9460,274800,10643745,\ldots\}\quad.@f]
       *
       * Note, 
       *
       * - If @f$ k @f$ is even (correspnding to @f$ m=4,8,12,\ldots@f$)
       *   and @f$ c_n\{m\}>0@f$ then there is no real solutions, and we
       *   return 0.
       *
       * - Similarly, if @f$ k@f$ is odd (corresponding to
       *   @f$ m=2,6,10,\ldots@f$) and @f$ c_n\{m\}<0@f$ then there is also
       *   no real soltions and we return 0.
       *
       * @param m @f$ m@f$ 
       * @param cnm @f$ c_n\{m\}@f$ 
       *
       * @return @f$ v_n\{m\}@f$
       */
      static Value vnm(Size m, Value cnm)
      {
	Size   k = m / 2;
	Value  s = (k % 2 == 1 ? 1 : -1);
	// std::cerr << "m=" << m << ' '
	//           << "k=" << k << ' '
	//           << "s=" << s << ' '
	//           << "Mk=" << mk(k) << std::endl;
	Value  i = s * cnm / mk(k);
	if (isSmall(i)) return 0;
	  
	return power(i, 1. / m);
      }
      /** 
       * The deriviative of @f$ v_n\{m\}@f$ with respeet to @f$ c_n\{m\}@f$ 
       *
       *
       * @f[ 
       * \frac{\partial v_n\{2k\}}{\partial c_n\{2k\}} 
       * = \frac{1}{2k\,c_n\{2k\}}\sqrt[2k]{(-1)^{k+1}m_k c_n\{2k\}}
       * = \frac{1}{m\,c_n\{m\}}v_n\{m\}\quad,
       * @f]
       *
       * where @f$ m=2k@f$, thus allowing to evaluate 
       *
       * @f[ 
       * \frac{\partial v_n\{m\}(c_n\{m\}(x))}{\partial x}
       * = \frac{\partial v_n\{m\}}{\partial c_n\{m\}}
       * \frac{\partial c_n\{m\}}{\partial x}\quad,
       * @f]
       * 
       * when, for example, we a propagating uncertainties. 
       *
       * @param m   @f$ m@f$ 
       * @param vnm @f$ v_n\{m\}@f$
       * @param cnm @f$ c_n\{m\}@f$ 
       *
       * @return \frac{\partial v_n\{m\}}{\partial c_n\{m\}} 
       */
      static Value dvnm(Size m, Value vnm, Value cnm)
      {
	return vnm / (m * cnm);
      }
      /** 
       * Calculate @f$ v'_{n}\{m\}@f$ as a function of @f$ d_{n}\{m\}@f$ and 
       * @f$ c_{n}\{m\}@f$
       *
       * @f[ 
       * v'_n\{2k\} = 
       * \frac{1}{M_k^{1/(2k)}}
       * \frac{(-1)^{k-1}d_n\{2k\}}
       *      {\left[(-1)^{k-1}c_{n}\{2k\}\right]^{1-1/(2k)}}
       * = \frac{d_n\{m\}}{c_{n}\{m\}}v_n\{m\}\quad,
       * @f]
       *
       * where @f$ m = 2k@f$ and @f$ M_k@f$ is the @f$ k@f$-th element
       * of the sequence
       *
       * @f[M=\{1,1,4,33,456,9460,274800,10643745,\ldots\}\quad.@f]
       *
       * Note, 
       *
       * - If @f$ |c_n\{m\}| <\epsilon@f$, we return 0
       *
       * - If @f$ k @f$ is even (correspnding to @f$ m=4,8,12,\ldots@f$)
       *   and @f$ c_n\{m\}>0@f$ then there is no real solutions, and we
       *   return 0.
       *
       * - Similarly, if @f$ k@f$ is odd (corresponding to
       *   @f$ m=2,6,10,\ldots@f$) and @f$ c_n\{m\}<0@f$ then there is also
       *   no real soltions and we return 0.
       *
       *
       * @param vnm  @f$ v_{n}\{m\}@f$ 
       * @param dnm  Differential cumulant @f$ d_n\{m\}@f$ 
       * @param cnm  Cumulant @f$ c_n\{m\}@f$ 
       *
       * @return @f$ v'_n\{m\}@f$
       */
      static Value vpnm(Size, Value vnm, Value dnm, Value cnm)
      {
	if (isAbsSmall(cnm) || isSmall(vnm)) return 0;

	return dnm / cnm * vnm;
      }
      /** 
       * Calculcate the derivative of @f$ v'_{n}\{m\}@f$ with respect to 
       * @f$ d_{n}\{m\}@f$ and @f$ c_{n}\{m\}@f$. 
       * 
       * @f{eqnarray*}{
       * \frac{\partial v'_n\{m\}}{\partial d_n\{m\}} 
       * &=& \frac{1}{c_{n}\{m\}}v_n\{m\}\\
       * \frac{\partial v'_n\{m\}}{\partial c_n\{m\}}
       * &=& 
       * -\frac{v'_n\{m\}}{c_n\{m\}}+\frac{d_n\{m\}}{c_n\{m\}}
       * \frac{\partial v_n\{m\}}{\partial c_n\{m\}}
       * @f}
       *
       * thus allowing to evaluate 
       *
       * @f{eqnarray*}{
       * \frac{\partial v'_n\{m\}(d_n\{m\}(x))}{\partial x}
       * = \frac{\partial v'_n\{m\}}{\partial d_n\{m\}}
       * \frac{\partial d_n\{m\}}{\partial x}\quad,
       * @f}
       * 
       * and similarly for @f$ c_n\{m\}@f$, when, for example, we a
       * propagating uncertainties.
       *
       * @param m    @f$ m@f$ 
       * @param vpnm @f$ v'_{n}\{m\}@f$ 
       * @param vnm  @f$ v_{n}\{m\}@f$ 
       * @param dnm  @f$ d_{n}\{m\}@f$ 
       * @param cnm  @f$ c_{n}\{m\}@f$ 
       *
       * @return  @f$
       * \begin{bmatrix}
       * \partial v'_n\{m\}/\partial d_n\{m\}\\
       * \partial v'_n\{m\}/\partial c_n\{m\}
       * \end{bmatrix}
       * @f$
       */
      static ValueVector dvpnm(Size m,
			       Value vpnm,
			       Value vnm,
			       Value dnm,
			       Value cnm)
      {
	Value dvnm_ = dvnm(m,vnm,cnm);
	return { vnm / cnm,
		 -vpnm / cnm + dnm / cnm * dvnm_ };
      }
      /* @} */
      //--------------------------------------------------------------
      /** 
       * @{ 
       * @name Generic cumulants 
       */
      /** 
       * Return the binomial factor.
       *
       * @f[\begin{pmatrix} n\\k\end{pmatrix}
       *    \begin{pmatrix} n-1\\k\end{pmatrix}\quad,@f]
       *
       * as used in the cumulant expression 
       *
       * @param n @f$ n@f$
       * @param k @f$ k@f$
       *
       * @return Factor 
       */
      static size_t binomFactor(size_t n, size_t k)
      {
	return binom(n,k) * binom(n-1,k);
      }
      /** 
       * Calculate the @f$ m@f$-particle cumulant (both integrated and
       * differential).
       *
       * @f{eqnarray}{
       * d_n\{2p\} &=& 
       *   \overline{C'}_{\mathbf{n}}\{2p\} - 
       *     \sum_{k=1}^{n-1}\begin{pmatrix} n\\k\end{pmatrix}
       *                     \begin{pmatrix} n-1\\k\end{pmatrix}
       *        d_n\{2(n-1)\}\overline{C}_{\mathbf{n}}\{2(n-k)\}\\
       * d_n\{2\} &=& \overline{C'}_{\mathbf{n}}\{2\}\quad,
       * @f}
       *
       * This method expects iterators over two vectors of average
       * multi-particle correlators in decending order
       *
       * @f{eqnarray}{
       *  d &=& \left\{\overline{C'}_{\mathbf{n}}\{2k\},
       *              \overline{C'}_{\mathbf{n}}\{2(k-1)\},
       *              \ldots
       *              \overline{C'}_{\mathbf{n}}\{2\}\right\}\\
       *  c &=& \left\{\overline{C}_{\mathbf{n}}\{2(k-1)\},
       *              \ldots
       *              \overline{C}_{\mathbf{n}}\{2\}\right\}\quad,\\
       * @f}
       *
       * with @f$m=2k@f$, and where @f$ c@f$ is one element shorter
       * then @f$ d@f$.  If
       *
       * @f[\overline{C'}_{\mathbf{n}}\{2(k-i)\}
       *    =\overline{C}_{\mathbf{n}}\{2(k-i)\}\quad,@f]
       *
       * for all @f$i@f$, then we are calculating the integrated
       * cumulant, otherwise we calculate the differential cumulant. 
       *
       * Note, these assumptions are _not_ checked. 
       *
       * @tparam ForwardIterator Iterator type 
       *
       * @param bd Iterator pointing to start of @f$ d@f$ 
       * @param ed Iterator pointing to end of @f$ d@f$ 
       * @param bc Iterator pointing to start of @f$ c@f$ 
       * @param ec Iterator pointing to end of @f$ c@f$ 
       *
       * @return The cumulant 
       */
      template <typename ForwardIterator>
      static Value cumulant(ForwardIterator bd,
			    ForwardIterator ed,
			    ForwardIterator bc,
			    ForwardIterator ec)
      {
	size_t n = std::distance(bd,ed);
	Value  t = 0;
	for (size_t k = 1; k < n; k++) {
	  size_t f =  binomFactor(n,k);
	  t        += f * cumulant(bd+k,ed,bc+k,ec) * *(ec-k);
	}
	return *bd - t;
      }
      /** 
       * Calculates the integrated cumulant from the average
       * multi-particle correlators given in descending order.
       *
       * @f{eqnarray}{
       *  c &= \left\{\overline{C}_{\mathbf{n}}\{2k\},
       *              \overline{C}_{\mathbf{n}}\{2(k-1)\},
       *              \ldots
       *              \overline{C}_{\mathbf{n}}\{2\}\right\}\quad.\\
       * @f}
       *
       * with @f$m=2k@f$, as 
       *
       * @f{eqnarray}{
       * c_n\{2n\} &=& \overline{C}_{\mathbf{n}}\{2n\} - 
       *      \sum_{k=1}^{n-1} \begin{pmatrix} n\\k\end{pmatrix}
       *                       \begin{pmatrix} n-1\\k\end{pmatrix}
       *          c_n\{2(n-1)\}\overline{C}_{\mathbf{n}}\{2(n-k)\}\\
       * c_n\{2\} &=& \overline{C}_{\mathbf{n}}\{2\}\quad.\\
       * @f}
       *
       * @param c Vector @f$ c@f$ of average multiparticle correlators 
       *
       * @return @f$ c_n\{m\}@f$
       */
      static Value cnm(const ValueVector& c)
      {
	return cumulant(std::begin(c), std::end(c),
			std::begin(c)+1, std::end(c));
      }
      /** 
       * Calculates the differential cumulant from the average
       * multi-particle correlators given in descending order.
       *
       * @f{eqnarray}{
       *  d &=& \left\{\overline{C'}_{\mathbf{n}}\{2k\},
       *              \overline{C'}_{\mathbf{n}}\{2(k-1)\},
       *              \ldots
       *              \overline{C'}_{\mathbf{n}}\{2\}\right\}\quad.\\
       *  c &=& \left\{\overline{C}_{\mathbf{n}}\{2(k-1)\},
       *              \ldots
       *              \overline{C}_{\mathbf{n}}\{2\}\right\}\quad.\\
       * @f}
       *
       * with @f$m=2k@f$, as 
       *
       * @f{eqnarray}{
       * d_n\{2n\} &=& \overline{C'}_{\mathbf{n}}\{2n\} - 
       *   \sum_{k=1}^{n-1}\begin{pmatrix} n\\k\end{pmatrix}
       *                   \begin{pmatrix} n-1\\k\end{pmatrix}
       *   d_n\{2(n-1)\}\overline{C}_{\mathbf{n}}\{2(n-k)\}\\
       * d_n\{2\} &=& \overline{C'}_{\mathbf{n}}\{2\}\quad.
       * @f}
       *
       * @param d Vector @f$ d@f$ of average differential multi-particle 
       *          correlators  
       * @param c Vector @f$ c@f$ of average integrated multi-particle 
       *          correlators 
       *
       * @return @f$ d_n\{m\}@f$
       */
      static Value dnm(const ValueVector& d,
		       const ValueVector& c)
      {
	return cumulant(std::begin(d), std::end(d),
			std::begin(c), std::end(c));
      }
      /** 
       * As function with same name, but the averages are packed and
       * the index is given into the packed vector.
       */
      static Value dnm(const ValueVector& dc)
      {
	size_t m = dc.size();
	size_t n = m/2 + 1;
	return cumulant(dc[std::slice(0,n,1)],dc[std::slice(n,m-n,1)]);
      }
      /** 
       * Calculate the differential of a cumulant with resect to one
       * average correlator.
       *
       * @f[ \frac{\partial d_n\{m\}}
       *          {\partial\overline{C'}_{\mathrm{n}}\{m\}}\quad.@f]
       * 
       * See the method `cumulant` for more on the arguments and formats. 
       *
       * @tparam ForwardIterator Iterator type 
       *
       * @param bd Iterator pointing to start of @f$ d@f$ 
       * @param ed Iterator pointing to end of @f$ d@f$ 
       * @param bc Iterator pointing to start of @f$ c@f$ 
       * @param ec Iterator pointing to end of @f$ c@f$ 
       * @param di Index into @f$ d@f$ pointing to the correlator 
       *           to diffentiate relative to. If negative, assume
       *           that @a ci is a valid index into @f$ c@f$.
       * @param ci Index into @f$ c@f$ point to the correlator to 
       *           differentiate relative to
       *
       * @return The derivative of the cumulant with respect to the
       * chosen average correlator.
       */
      template <typename ForwardIterator>
      static Value dcumulant(ForwardIterator bd,
			     ForwardIterator ed,
			     ForwardIterator bc,
			     ForwardIterator ec,
			     long            di,
			     long            ci)
      {
	if (di < 0 and ci < 1) return 0;
	size_t n = std::distance(bd,ed);
	Value  t = 0;
	for (size_t k = 1; k < n; k++) {
	  size_t f =  binomFactor(n,k);
	  Value  h =  dcumulant(bd+k, ed, bc+k, ec, di-k, ci-k) * *(ec-k);
	  if (ci == long(n-k)) h += cumulant(bd+k, ed, bc+k, ec);
	  t        += f*h;
	}
	// Below - aggressive type-casting for symbolic output
	if (di == 0 or ci == 0)
	  return Value(1) - t;
	return Value(-1)*t;
      }
      /** 
       * Calculates the partial derivative of the integrated cumulant
       * with respect to an average multi-particle correlator.
       *
       * @f[ \frac{\partial c_n\{2k\}}
       *                  {\partial\overline{C}_{\mathrm{n}}\{2(k-i)\}}@f]
       *
       * See method `cnm` for more on arguments. 
       *
       * @param c  Vector @f$ c@f$ of average multiparticle correlators 
       * @param i  Index @f$ i@f$ into @f$ c@f$ pointing to the average 
       *           correlator to differentian with respect to 
       *
       * @return @f$ \frac{\partial c_n\{2k\}}
       *                  {\partial\overline{C}_{\mathrm{n}}\{2(k-i)\}}@f$
       */
      static Value dcnm(const ValueVector& c, long i)
      {
	return dcumulant(std::begin(c), std::end(c),
			 std::begin(c)+1, std::end(c), i, i);
      }
      /** 
       * Calculate the gradient of the cumulant with respect to the
       * average multi-particle correlators
       */
      static ValueVector gcnm(const ValueVector& c)
      {
	ValueVector g(c.size());
	for (size_t i = 0; i < c.size(); i++) g[i] = dcnm(c, i);
	return g;
      }
      /** 
       * Calculates the partial derivative of the integrated cumulant
       * with respect to an average multi-particle correlator.
       *
       * @f{eqnarray}{
       * & \frac{\partial d_n\{2k\}}
       *      {\partial\overline{C'}_{\mathrm{n}}\{2(k-i)\}}&\\
       * & \frac{\partial c_n\{2k\}}
       *      {\partial\overline{C}_{\mathrm{n}}\{2(k-j)\}}&\\
       * @f}
       *
       * See method `dnm` for more on arguments.  Note, to take the
       * partial derivatives with respect to @f$
       * \overline{C}_{\mathbf{n}}\{2(k-j)\}@f$ one must pass -1 for
       * @a i
       *
       * @param d Vector @f$ d@f$ of average differential multi-particle 
       *          correlators  
       * @param c Vector @f$ c@f$ of average integrated multi-particle 
       *          correlators 
       * @param i Index @f$ i@f$ into @f$ d@f$ pointing to the average 
       *          correlator to differentian with respect to 
       * @param j Index @f$ j@f$ into @f$ c@f$ pointing to the average 
       *          correlator to differentian with respect to 
       *
       * @return Partial derivative of @f$ d_n\{m\}@f$
       */
      static Value ddnm(const ValueVector& d,
			const ValueVector& c,
			long i, long j=-2)
      {
	// for (auto& dd : d) std::cerr << dd << ' ';
	// for (auto& cc : c) std::cerr << cc << ' ';
	// std::cerr << std::endl;
	return dcumulant(std::begin(d), std::end(d),
			 std::begin(c), std::end(c),
			 i, j+1);
      }
      /** 
       * As function with same name, but the averages are packed and
       * the index is given into the packed vector.
       */
      static Value ddnm(const ValueVector& dc, long i)
      {
	size_t m = dc.size();
	size_t n = m/2 + 1;
	return dcumulant(dc[std::slice(0,n,1)],dc[std::slice(n,m-n,1)],
		 	i >= n ? -1 : i, i >= n ? i-n : -1);
      }
      /** 
       * Calculate the gradient of the cumulant with respect to the
       * average multi-particle correlators
       */
      static ValueVector gdnm(const ValueVector& dc)
      {
	ValueVector g(dc.size());
	for (size_t i = 0; i < dc.size(); i++) g[i] = ddnm(dc, i);
	return g;
      }
      /** 
       * Calculate the gradient of the cumulant with respect to the
       * average multi-particle correlators
       */
      static ValueVector gdnm(const ValueVector& d,
			      const ValueVector& c)
      {
	size_t      n = d.size();
	ValueVector g(n+c.size());
	for (size_t i = 0; i < d.size(); i++) g[i]   = ddnm(d, c, i);
	for (size_t i = 0; i < c.size(); i++) g[n+i] = ddnm(d, c, -1, i);
	
	return g;
      }
      /* @} */
    };
  }
}
#endif
//
// EOF
//

