/**
 * @file   correlations/flow/Bin.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A single bin in a flow analysis. 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_FLOW_BINT_HH
#define CORRELATIONS_FLOW_BINT_HH
#include <correlations/flow/Bin.hh>
#include <iomanip>

namespace correlations
{
  namespace flow
  {
    // =================================================================
    /** 
     * A single bin for analysis.  This is a base class for bins of flow
     * coefficient calculations.  The derived class IBin calculates the
     * integrated flow coefficients, while sub-classes DBin and DBinI
     * calculates differentially. The interface is the same - e.g., 
     *
     * @code 
     Bin bin(MaxOrder(),MaxParticles()); // Really - sub-class
   
     while (MoreEvents()) {
       bin.reset();
       while (MoreObservations()) {
         unsigned int flags = ((IsReference()  ? REF : 0) | 
                               (IsOfInterest() ? POI : 0));
         bin.fill(Phi(), Weight(), flags);
       }
       bin.update();
     }
 
     bin.result();
     @endcode
     *
     * @headerfile "" <correlations/flow/Bin.hh>
     * @ingroup flow 
     */
    template <typename T>
    struct BinT : public Bin
    {
      /** Type of vnm estimator */
      using VNM=T;
      /** Type of vector of VNM estimators */
      using VNIVector=std::vector<VNM>;
      /** Vector of vectors of VNM estimators  */
      using VNMVector=std::vector<VNIVector>;
      /** Type of sizes */
      using Size=Bin::Size;
      /** Kind of observation */
      using Kind=typename Bin::Kind;
      /** Kinds of observation */
      using Kinds=typename Bin::Kinds;
      /** Partition of observation */
      using Partition=typename Bin::Partition;
      /** Partitions of observation */
      using Partitions=typename Bin::Partitions;

      /** Destructor */
      virtual ~BinT() {}
      /** Move constructor */
      BinT(BinT&& o)
	:  Bin(std::move(o)),
      	  _vnm(std::move(o._vnm))
      {}
      /** Deleted copy constructor */
      BinT(const BinT&) = delete;
      /** Deleted assignment operator */
      BinT& operator=(const BinT&) = delete;

      /** 
       * Access a single flow estimator 
       *
       * @param m The number of particles 
       * @param n The harmonic order 
       */
      const VNM& vnm(Size n, Size m) const
      {
	return _vnm[m/2-1][n-1];
      }
      /** 
       * Get results as JSON.  The parent object maps harmonic order
       * to @f$m@f$ particle results.  That is, to access the
       * @f$v_n\{m\}@f$ result, one can subscript the returned object
       * @c r as
       *
       * @code 
       r[n][m]
       @endcode 
       *
       * Each of these elements consits of an array of two numbers: 
       *
       * - The value 
       * - The (statistical) uncertainty on the value 
       *
       * A derived class can override this method to prepend or append
       * additional results - e.g., the indendent variable can be
       * prepended, or additional information can be appended.  If a
       * derived class adds information in any way, it should also
       * override the member function headers to give the appropriate
       * header for the added information.
       *
       * @return Results encoded as JSON 
       */
      virtual json::JSON result() const
      {
	json::JSON json;
	for (size_t m = 2; m <= maxP(); m += 2) {
	  for (size_t n = 1; n <= maxN(); n++) {
	    auto vu = vnm(n,m).eval();
	    json[std::to_string(n)][std::to_string(m)]
	      = { json::Array(vu.first,  vu.second) };
	    
	  }
	}
	return json;
      }
      /** 
       * Dump state to JSON 
       *
       * @return JSON object 
       */
      virtual json::JSON toJson() const
      {
	json::JSON json;
	for (size_t m = 2; m <= maxP(); m += 2) {
	  for (size_t n = 1; n <= maxN(); n++) {
	    json[std::to_string(n)][std::to_string(m)] = vnm(n,m).toJson();
	  }
	}
	return json;
      }
      /** 
       * Load state from JSON
       *
       * @param json JSON Object to load from
       */
      virtual void fromJson(const json::JSON& json)
      {
	for (size_t m = 2; m <= maxP(); m += 2) 
	  for (size_t n = 1; n <= maxN(); n++) 
	    vnm(n,m).fromJson(json[std::to_string(n)][std::to_string(m)]);
      }
      /** 
       * Merge state from JSON
       *
       * @param json JSON Object to load from
       */
      virtual void mergeJson(const json::JSON& json)
      {
	for (size_t m = 2; m <= maxP(); m += 2) 
	  for (size_t n = 1; n <= maxN(); n++) 
	    vnm(n,m).mergeJson(json[std::to_string(n)][std::to_string(m)]);
      }
      /** @return Largest harmonic order */
      virtual Size maxN() const
      {
	size_t l = 0;
	for (auto& vni : _vnm) l = std::max(l, vni.size());
	return l;
      }
      /** @return Largest number of particles to correlate */
      virtual Size maxP() const
      {
	return 2*_vnm.size();
      }
      /** @return Whether we use weights */
      virtual bool useWeights() const = 0;
      /** 
       * Merge content of other bin into this 
       *
       * @param o Other bin to merge into this 
       *
       * @return Reference to this 
       */
      virtual Bin& merge(const Bin& o)
      {
	if (!dynamic_cast<const BinT<T>*>(&o))
	  throw std::runtime_error("Wrong type of bin");

	const BinT<T>& oo = dynamic_cast<const BinT<T>&>(o);
	Size  mN = maxN();
	Power mP = maxP();
	assert(mN == oo.maxN());
	assert(mP == oo.maxP());

	for (size_t k = 0; k < _vnm.size(); k++) {
	  // m = 2(k+1)
	  auto& vni = _vnm[k];
	  auto& vno = oo._vnm[k];
	  for (size_t l = 0; l < vni.size(); l++)
	    // n = l+1
	    vni[l].merge(vno[l]);
	}
	return *this;
      }
    protected:
      /** Result vector */
      using CorrelationView=typename Bin::CorrelationView;
      /** Result vector */
      using Correlations=typename Bin::Correlations;
      /** Vector of vectors of results */
      using CorrelationsVector=std::vector<Correlations>;
      /** 
       * Constructor 
       *
       * @param maxN Largest harmonic order 
       * @param maxP Largest number of particles to correlate 
       */
      BinT(Size maxN=4, Size maxP=4)
	: Bin(),
	  _vnm(maxP/2)
      {
	for (size_t k = 1; k <= maxP/2; k++) 
	  for (size_t n=1; n <= maxN; n++) 
	    _vnm[k-1].emplace_back(n,2*k);
      }
      /** 
       * Access a single flow estimator 
       *
       * @param m The number of particles 
       * @param n The harmonic order 
       */
      VNM& vnm(Size n, Size m)
      {
	return _vnm[m/2-1][n-1];
      }
      /** Vector of vectors of flow estimators */
      VNMVector _vnm;
    };
  }
}
#endif
//
// EOF
//
