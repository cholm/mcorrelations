/**
 * @file   correlations/flow/VNM.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2- and 4-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_FLOW_VNM_HH
#define CORRELATIONS_FLOW_VNM_HH
#include <correlations/Result.hh>
#include <correlations/stat/Derivatives.hh>
#include <correlations/flow/Calculations.hh>
#include <correlations/flow/ToValue.hh>

namespace correlations
{
  /** 
   * @defgroup flow Estimating flow harmonics 
   * 
   * Code in this module implements methods for calculating flow for
   * data using the prescriptions of the Generic Framework.  See also
   * [here](doc/_Flow.md).
   */
  /** 
   * Namespace for flow code 
   *
   * @ingroup flow 
   */
  namespace flow
  {
    /** 
     * @example Integrated.hh
     *
     * Calculate @f$m@f$-particle integrated flow
     *
     * @image html gen_phi_medium_intg.png
     */
    // =================================================================
    /** 
     * Base class for flow calculations 
     *
     *
     * @tparam T The value type 
     * @tparam Uncertainties Type of estimator base-class 
     * 
     * @ingroup flow
     * @headerfile "" <correlations/flow/VNM.hh>
     */
    template <typename T=correlations::Real,
	      template <typename> class Uncertainties=stat::Derivatives>
    struct VNM : public Uncertainties<T>
    {
      /** Estimator type */
      using Estimator=Uncertainties<T>;
      /** Conversion type */
      using ToValue=flow::ToValue<T>;
      /** Calculations */
      using Calculations=flow::Calculations<T>;
      /** Real value type */
      using Value=typename Estimator::Value;
      /** Real value type */
      using ValueVector=typename Estimator::ValueVector;
      /** Size value type */
      using Size=typename Estimator::Size;
      /** Result of correlator calculation */
      using Correlation=correlations::Result;
      /** Results of correlator calculations */
      using Correlations=correlations::ResultVector;
      
      /** Destructor */
      virtual ~VNM() {}
      /** 
       * @param n  The order of the flow coeffient to calculate 
       * @param m  Number of terms we need for calculating the flow coefficient 
       */
      VNM(Size n, Size m) :  Estimator(m/2), _n(n), _m(m), _cnm(0), _vnm(0)
      {
	if (m % 2 != 0 or m <= 0)
	  throw std::runtime_error("Number of particles m=" + std::to_string(m)
				   + " not even or 0 or less");
      }
      /** 
       * Move constructor 
       *
       * @param o Object to move from 
       */
      VNM(VNM&& o)
	: Estimator(std::move(o)), _n(o._n), _m(o._m), _cnm(o._cnm), _vnm(0)
      {}
      /** 
       * Event update.
       *
       * This method expects one vector of the integrated m-particle
       * correlators in descending order.
       *
       * @f{eqnarray}{
       * c = \{C_{\mathbf{n}}\{2k\},
       *       C_{\mathbf{n}}\{2(k-i)\},
       *       \ldots,C_{\mathbf{n}}\{2\}\}\\
       * @f}
       *
       * for @f$ m = 2k@f$ and @f$i=1,\ldots,k-1@f$
       *
       * @param c Integrated correlator @f$ C_{\mathbf{n},i}@f$
       */
      template <typename C>
      void update(const C& c)
      {
	assert(c.size() == this->Estimator::size());

	ValueVector v(c.size());
	ValueVector w(c.size());
	for (size_t i = 0; i < v.size(); i++) {
	  v[i] = ToValue::eval(c[i]);
	  w[i] = c[i].weights();
	}
	_update(v, w);
      }
      /** 
       * Calculate the value of @f$ v_n\{m\}@f$. 
       *
       * @f[ 
       * v_n\{2k\} = \sqrt[{\textstyle 2k}]{(-1)^{k-1}\frac{1}{M_k}c_{n}\{2k\}}
       * \quad,
       * @f]
       *
       * where @f$m=2k@f$, and 
       *
       * @f[
       * c_{n}\{2k\} = f(\overline{C}_{\mathbf{n}}\{2k\},\ldots,
       *                 \overline{C}_{\mathbf{n}}\{2\})\quad. @f]
       *
       * @sa 
       * - Calculations<T>::vnm 
       * - Calculations<T>::cnm 
       *
       * @param means Mean values of correlators 
       *              @f$\overline{C}_{\mathbf{n}}\{2j\}@f$ for 
       *              @f$j=1,\ldots,k@f$ and @f$m=2k@f$ 
       *
       * @return @f$ v_n\{m\}@f$ 
       */
      virtual Value value(const ValueVector& means) const
      {
	_cnm = Calculations::cnm(means);
	_vnm = Calculations::vnm(_m, _cnm);
	return _vnm;
      }
      /** 
       * Get the derivatives of @f$ v_n\{m\}@f$ with respect to the
       * correlators used.
       *
       * @f[ 
       * \frac{\partial v_n\{2k\}}{\partial\overline{C}_{\mathbf{n}}\{2j}\quad,
       * @f]
       *
       * for @f$ m=2k@f$ and @f$ j=1,\ldots k@f$. 
       *
       * This member function then returns 
       *
       * @f{eqnarray}{
       * \nabla v_{n}\{2k\} &=& 
       * \begin{bmatrix}
       *  \frac{\partial v_n\{2k\}}{\partial\overline{C}_{\mathbf{n}}\{2k\}}\\
       *  \ldots 
       *  \frac{\partial v_n\{2k\}}{\partial\overline{C}_{\mathbf{n}}\{2\}}\\
       * \end{bmatrix}\\
       * &=& \frac{\mathrm{d} v_n\{2k\}}{\mathrm{d} c_{n}\{2k\}}
       * \begin{bmatrix}
       *  \frac{\partial c_n\{2k\}}{\partial\overline{C}_{\mathbf{n}}\{2k\}}\\
       *  \ldots 
       *  \frac{\partial c_n\{2k\}}{\partial\overline{C}_{\mathbf{n}}\{2\}}\\
       * \end{bmatrix}\\
       * @f}
       * 
       *
       * @sa
       * - Calculations<T>::dvnm  Derivative of @f$v_{n}\{m\}@f$ 
       * - Calculations<T>::gcnm  Gradient of @f$c_{n}\{m\}@f$
       *
       * @param means Mean values of correlators 
       *              @f$\overline{C}_{\mathbf{n}}\{2j\}@f$ for 
       *              @f$j=1,\ldots,k@f$ and @f$m=2k@f$ 
       *
       * @return Derivatives (Gradient) of @f$ v_n\{m\}@f$ wrt. correlators
       */
      virtual ValueVector derivatives(const ValueVector& means) const
      {
	Value       dvnm = Calculations::dvnm(_m, _vnm, _cnm);
	ValueVector gcnm = Calculations::gcnm(means);
	return dvnm * gcnm;
      }
      /** @return Harmonic order @f$n@f$ of @f$v_n\{m\}@f$ */
      Size n() const { return _n; }
      /** @return Particle count @f$m@f$ of @f$v_n\{m\}@f$ */
      Size m() const { return _m; }
    protected:
      /** Harmonic order */
      Size _n;
      /** Number of particles */
      Size _m;
      /** Cached value of cumulant */
      mutable Value _cnm;
      /** Cached value of flow coefficent */
      mutable Value _vnm;
      /** 
       * Event update 
       *
       * @param v Vector of values 
       * @param w Vector if weights 
       */
      void _update(const ValueVector& v, const ValueVector& w)
      {
	assert(v.size() == this->Estimator::size());
	assert(w.size() == this->Estimator::size());
	// Clear cached values 
	_cnm = 0;
	_vnm = 0;
	this->fill(v, w);
      }
    };
  }
}
#endif
//
// EOF
//
