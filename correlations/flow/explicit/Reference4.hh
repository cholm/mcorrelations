/**
 * @file   correlations/flow/explicit/Reference4.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Explicit 4-particle differential flow in two partitions.
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_FLOW_EXPLICIT_REFERENCE4_HH
#define CORRELATIONS_FLOW_EXPLICIT_REFERENCE4_HH
#include <correlations/Closed.hh>
#include <correlations/flow/Bin.hh>

namespace correlations
{
  namespace flow
  {
    namespace expl
    {
      //================================================================
      /** 
       * this is merely a convinience class to simplify things a bit 
       */
      template <template <typename> class Correlator=correlations::Closed>
      struct Reference4Base : public Bin
      {
	/** Base class */
	using Base=Bin;
	/** Harmonic vectors */
	using HarmonicVector=correlations::HarmonicVector;
	/** type of correlator algorithm */
	using FromQVector=Correlator<correlations::HarmonicVector>;
	/** QVector type */
	using QVector=typename FromQVector::QVector;
	/** Q-vector storage */
	using QStore=typename FromQVector::QStore;
	/** Correlation type */
	using Correlation=typename Base::Correlation;
	/** Correlation type */
	using Correlations=typename Base::Correlations;
	/** Mapping of harmonic vectors */
	using HarmonicMap=std::vector<HarmonicVector>;
	/** Correlation view */
	using CorrelationView=typename Base::CorrelationView;
	/** 
	 * Constructor 
	 */
	Reference4Base(Size maxN, bool useWeights=true)
	  : Base(),
	    _h12(maxN),                 // Harm. for 12
	    _r12(2*maxN,2,useWeights),  // Ref Q for 12
	    _s12(_r12,_r12,_r12),       // Store of Int. Q for 12 
	    _c12(_s12),
	    _x12(maxN),
	    _updated(false)
	{
	  assert(&_r12 == &_s12.qvec());
	  for (size_t i = 0; i < maxN; i++) {
	    Harmonic n = i+1;
	    _h12[i] = {  n,  n };
	  }
	}
	/** 
	 * Move constructor 
	 */
	Reference4Base(Reference4Base&& o)
	  : Base(std::move(o)),
	    _h12(std::move(o._h12)),
	    _r12(std::move(o._r12)),
	    _s12(_r12, _r12, _r12),
	    _c12(_s12),
	    _x12(std::move(o._x12)),
	    _updated(o._updated)
	{
	}	
	Reference4Base(const Reference4Base& o) = delete;
	Reference4Base& operator=(const Reference4Base& o) = delete;

	/** 
	 * Harmonics 
	 */
	const HarmonicVector& h12(Harmonic n) const { return _h12[n-1]; }
      
	/** 
	 * Get the Q-vector for partition 12 
	 *
	 * @return @f$ Q\{12\}@f$
	 */
	QVector& r12() { return _r12; }

	/** 
	 * Get the correlator for harmonic @f$ n@f$ of last event 
	 *
	 * @return @f[C_{n,n}\{12\} @f]
	 */
	const Correlation& c12(Harmonic n) const { return _x12[n-1]; }

	/** 
	 * Reset cache 
	 */
	void reset()
	{
	  _r12.reset();
	  _updated = false;
	}

	/** 
	 * Fill in values 
	 */
	virtual void fill(Real phi, Real weight, Kind kind, Partition part)
	{
	  if (part != 1) return;
	  if (kind & correlations::REF) _r12.fill(phi,weight);
	}
	virtual void fill(const RealVector& phi,
			  const RealVector& weight,
			  const Kinds&      kind,
			  const Partitions& part)
	{
	  for (size_t i = 0; i < phi.size(); i++)
	    fill(phi[i], weight[i], kind[i], part[i]);
	}
	/** 
	 * Calculates needed correlators and updates statistics 
	 */
	virtual void update() = 0;
      
	/** Ge result (none) */
	virtual json::JSON result() const { return json::JSON(); }

	/** 
	 * Dump state to JSON  
	 *
	 * @return JSON object 
	 */
	virtual json::JSON toJson() const { return json::JSON(); }

	/** Load state from JSON */
	virtual void fromJson(const json::JSON&) {}

	/** Merge state from JSON */
	virtual void mergeJson(const json::JSON&) {}

	/** @return Largest harmonic order */
	virtual Size maxN() const  { return _x12.size(); }

	/** @return Largest number of particles to correlate */
	virtual Size maxP() const { return 4; }

	virtual Bin& merge(const Bin&) { return *this; }
      protected:
	/** Harmonic vector map */
	HarmonicMap _h12;
	/** Reference Q-vector for 12 */
	QVector _r12;                   
	/** Storage of Q-vector for 12 for integrated correlator */
	QStore _s12;
	/** Integrated correlator for 12 */ 
	FromQVector _c12;
	/** Correlations */
	Correlations _x12;
	/** Whether we have been updated or not */
	bool _updated;
      };
    
      //================================================================
      /** 
       * Explict 4-particle differential flow calculations in two sub-events. 
       *
       * This uses the expression 
       *
       * @f[
       v'_n\{12,34\} &= 
       \frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}v_n\{4\}
       @f]
       *
       * This class simply calculates 
       *
       * @f[ \overline{C_{n,n}\{12\}} @f]
       *
       */
      template <template <typename> class Correlator=correlations::Closed>
      struct Reference4U : public Reference4Base<Correlator>
      {
	using Base=Reference4Base<Correlator>;
	/** Harmonic vectors */
	using HarmonicVector=typename Base::HarmonicVector;
	/** type of correlator algorithm */
	using FromQVector=typename Base::FromQVector;
	/** Correlation type */
	using Correlation=typename Base::Correlation;
	/** Correlation type */
	using Correlations=typename Base::Correlations;
	/** Mapping of harmonic vectors */
	using HarmonicMap=typename Base::HarmonicMap;
	/** Correlation view */
	using CorrelationView=typename Base::CorrelationView;


	/** 
	 * Constructor 
	 */
	Reference4U(Size maxN, bool weight=true) : Base(maxN,weight) {}
	Reference4U(Reference4U&& o) : Base(std::move(o)) {}
	Reference4U(const Reference4U& o) = delete;
	Reference4U& operator=(const Reference4U& o) = delete;
	/** 
	 * Calculates needed correlators and updates statistics 
	 */
	virtual void update()
	{
	  if (_updated) return;
	  for (size_t l = 0; l < this->_x12.size(); l++) 
	    this->_x12[l] = this->_c12.calculate(this->_h12[l]);

	  _updated = true;
	}
      protected:
	using Base::_updated;      
      };

      //----------------------------------------------------------------
      /** 
       * Explict 4-particle differential flow calculations in two sub-events. 
       *
       * This uses the expression 
       *
       * @f[v'_n\{12|34\} = \frac{\overline{C'_{n,n}\{12\} C_{-n,-n}\{34\}}
       -2\overline{C'_{n,n}\{12\}}\,\overline{C_{n,n}\{34\}}}{
       \overline{C_{n,n}\{12\} C{-n,-n}\{34\}} 
       -2\overline{C_{n,n}\{12\}}\,\overline{C_{-n,-n}\{34\}}}@f]
       * 
       * This class simply calculates 
       *
       * @f[ \overline{C_{n,n}\{12\}}\quad \overline{C_{n,n}\{34\}}
       \quad \overline{C_{n,n}\{12\} C{-n,-n}\{34\} @f]
       *
       */
      template <template <typename> class Correlator=correlations::Closed>
      struct Reference4 : public Reference4U<Correlator>
      {
	using Base=Reference4U<Correlator>;
	/** Harmonic vectors */
	using HarmonicVector=typename Base::HarmonicVector;
	/** type of correlator algorithm */
	using FromQVector=typename Base::FromQVector;
	/** QVector type */
	using QVector=typename Base::QVector;
	/** Q-vector storage */
	using QStore=typename Base::QStore;
	/** Correlation type */
	using Correlation=typename Base::Correlation;
	/** Correlation type */
	using Correlations=typename Base::Correlations;
	/** Mapping of harmonic vectors */
	using HarmonicMap=typename Base::HarmonicMap;
	/** Correlation view */
	using CorrelationView=typename Base::CorrelationView;

	/** 
	 * Constructor 
	 */
	Reference4(Size maxN, bool useWeights=true)
	  : Base(maxN),
	    _h34(maxN),                   // Harm. for 34
	    _r34(2*maxN,2,useWeights),    // Ref Q for 34	   
	    _s34(_r34, _r34, _r34),       // Store Q for Int. 34
	    _c34(_s34),                   // Int. Corr for 34
	    _x34(maxN)
	{
	  for (Harmonic i = 0; i < maxN; i++) {
	    Harmonic n = i+1;
	    _h34[i] = HarmonicVector(-n, 2);
	  }
	}
	/** 
	 * Move constructor 
	 */
	Reference4(Reference4&& o)
	  : Base(std::move(o)),
	    _h34(std::move(o._h34)),
	    _r34(std::move(o._r34)),
	    _s34(_r34, _r34, _r34),
	    _c34(_s34),                          // Int. Corr for 34
	    _x34(std::move(o._x34))
	{
	}	
	Reference4(const Reference4& o) = delete;
	Reference4& operator=(const Reference4& o) = delete;

	/** 
	 * Harmonics 
	 */
	const HarmonicVector& h34(Harmonic n) const { return _h34[n-1]; }

	/** 
	 * Get the Q-vector for partition 34 
	 *
	 * @return @f$ Q\{34\}@f$
	 */
	QVector& r34() { return _r34; }
      
	/** 
	 * Get the correlator for harmonic @f$ n@f$ of last event 
	 *
	 * @return @f[C_{-n,-n}\{34\} @f]
	 */
	const Correlation& c34(Harmonic n) const { return _x34[n-1]; }

	/** 
	 * Get the correlator for harmonic @f$ n@f$ of last event 
	 *
	 * @return @f[C_{n,n}\{12\}\,C_{-n,-n}\{34\} @f]
	 */
	Correlation c1234(Harmonic n) const
	{
	  return Correlation(this->c12(n).sum()    *_x34[n-1].sum(),
			     this->c12(n).weights()*_x34[n-1].weights());
	}
      
	/** 
	 * Reset cache 
	 */
	void reset()
	{
	  Base::reset();
	  _r34.reset();
	}
	/** 
	 * Fill in values 
	 */
	virtual void fill(Real phi, Real weight, Kind kind, Partition part)
	{
	  if (part == 1) {
	    Base::fill(phi,weight,kind,part);
	    return;
	  }
	  if (part != 2) return;
			 
	  bool ref = kind & correlations::REF;
	  if (ref)         _r34.fill(phi,weight);
	}
	virtual void fill(const RealVector& phi,
			  const RealVector& weight,
			  const Kinds&      kind,
			  const Partitions& part)
	{
	  for (size_t i = 0; i < phi.size(); i++)
	    fill(phi[i], weight[i], kind[i], part[i]);
	}
	/** 
	 * Calculates needed correlators
	 *
	 * @f{eqnarray*}{
	 * 0 &:& \overline{C_{n,n}\{12\}}\\
	 * 1 &:& \overline{C_{-n,-n}\{34\}}\\
	 * 2 &:& \overline{C_{n,n}\{12\} C{-n,-n}\{34\}}\\
	 * @f}
	 *
	 */
	virtual void update()
	{
	  if (_updated) return;
	  Base::update();
	  for (size_t l = 0; l < this->_x34.size(); l++)
	    _x34[l] = _c34.calculate(_h34[l]); 
	}
      protected:
	using Base::_updated;
	/** Harmonic vector maps for 34 */
	HarmonicMap _h34;
	/**  Reference Q-vector for 34 */
	QVector _r34;
	/** Storage of Q-vector for 34 for integrated correlator */
	QStore _s34;
	/** Integrated correlator for 34 */
	FromQVector _c34;
	/** Correlations in 34 */
	Correlations _x34;
      };
    }
  }
}
#endif
//
// EOF
//

      
