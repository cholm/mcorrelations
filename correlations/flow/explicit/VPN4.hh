/**
 * @file   correlations/flow/explicit/VPN4.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Explicit calculations of 4-particle differential flow in two
 * partitions
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_FLOW_EXPLICIT_VPN4_HH
#define CORRELATIONS_FLOW_EXPLICIT_VPN4_HH
#include <correlations/Result.hh>
#include <correlations/stat/Derivatives.hh>
#include <correlations/flow/Calculations.hh>
#include <correlations/flow/ToValue.hh>

namespace correlations
{
  namespace flow
  {
    namespace expl
    {
      //----------------------------------------------------------------
      /** 
       * An estimator of 4-particle differential flow in two partitions
       * where the expression is explicitly written out and zero terms
       * removed.  
       *
       * @f[v'_n\{12|34\} = \frac{\overline{C'_{n,n}\{12\} C_{-n,-n}\{34\}}
       -2\overline{C'_{n,n}\{12\}}\,\overline{C_{n,n}\{34\}}}{
       \overline{C_{n,n}\{12\} C{-n,-n}\{34\}} 
       -2\overline{C_{n,n}\{12\}}\,\overline{C_{-n,-n}\{34\}}}@f]
       * 
       * Furthermore, we assume the two sides are uncorrelated so that 
       *
       * @f[
       \overline{C'_{n,n}\{12\}C_{-n,-n}\{34\}} &= 
       \overline{C'_{n,n}\{12\}}\,\overline{C_{-n,-n}\{34\}}\\
       \overline{C_{n,n}\{12\}C_{-n,-n}\{34\}} &= 
       \overline{C_{n,n}\{12\}}\,\overline{C_{-n,-n}\{34\}}\quad,
       @f] 
       *
       * and the above reduces further to 
       *
       * @f[
       v'_n\{12,34\} &= 
       \frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}v_n\{4\}
       @f]
       *
       * Note, this class relies on numeric computation of derivatives. 
       *
       * @tparam T Value type 
       * @tparam Uncertainty Uncertainty policy 
       */
      template <typename T=correlations::Real,
		template <typename> class Uncertainty=stat::Derivatives>
      struct VPN4U : public Uncertainty<T>
      {
	/** Estimator type */
	using Estimator=Uncertainty<T>;
	/** Conversion type */
	using ToValue=flow::ToValue<T>;
	/** Calculations */
	using Calculations=flow::Calculations<T>;
	/** Real value type */
	using Value=typename Estimator::Value;
	/** Real value type */
	using ValueVector=typename Estimator::ValueVector;
	/** Size value type */
	using Size=typename Estimator::Size;


	/** 
	 * Constructor 
	 *
	 * @param n    Harmonic
	 * @param nvar Number of variables
	 */
	VPN4U(Size n, size_t nvar=4)
	  : Estimator(nvar), _n(n)
	{}
	/** 
	 * Move constructor 
	 *
	 * @param o Object to move from 
	 */
	VPN4U(VPN4U&& o)
	  : Estimator(std::move(o)), _n(o._n)
	{}
	/** 
	 * Update from a container 
	 */
	template <typename C>
	void update(const C& c)
	{
	  assert(c.size() == this->Estimator::size());

	  ValueVector v(c.size());
	  ValueVector w(c.size());
	  for (size_t i = 0; i < c.size(); i++) {
	    v[i] = ToValue::eval(c[i]);
	    w[i] = c[i].weights();
	  }
	  this->fill(v,w);
	}
	/** 
	 * Calculates the four-particle differential flow coefficients. 
	 * 
	 * The input are the average correlators as follows 
	 *
	 * @f{eqnarray*}{
	 * 0 &:& \overline{C_n\{4\}}\\
	 * 1 &:& \overline{C_n\{2\}}\\
	 * 2 &:& \overline{C'_{n,n}\{12\}\\
	 * 3 &:& \overline{C_{n,n}\{12\}}\\
	 * @f}
	 *
	 * and calculates 
	 *
	 * @f[
	 v'_n\{12,34\} &= 
	 \frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}v_n\{4\}
	 @f]
	 *
	 */
	virtual Value value(const ValueVector& means) const
	{
	  Value cnm4    = Calculations::cnm(means[std::slice(0,2,1)]);
	  Value vnm4    = Calculations::vnm(4,cnm4);
	  Value cp12    = means[3];
	  Value c12     = means[6];

	  return cp12 / c12 * vnm4;
	}
	/** @return Harmonic order @f$n@f$ of @f$v_n\{m\}@f$ */
	Size n() const { return _n; }
	/** @return Particle count @f$m@f$ of @f$v_n\{m\}@f$ */
	Size m() const { return 4; }
      protected:
	/** Harmonic order */
	Size _n;
      };
      //================================================================
      /** 
       * An estimator of 4-particle differential flow in two partitions
       * where the expression is explicitly written out and zero terms
       * removed.
       *
       * @f[v'_n\{12|34\} = \frac{\overline{C'_{n,n}\{12\} C_{-n,-n}\{34\}}
       -2\overline{C'_{n,n}\{12\}}\,\overline{C_{n,n}\{34\}}}{
       \overline{C_{n,n}\{12\} C{-n,-n}\{34\}} 
       -2\overline{C_{n,n}\{12\}}\,\overline{C_{-n,-n}\{34\}}}@f]
       * 
       * Note, this class relies on numeric computation of derivatives. 
       *
       * @tparam T Value type 
       * @tparam Uncertainty Uncertainty policy 
       */
      template <typename T=correlations::Real,
		template <typename> class Uncertainty=stat::Derivatives>
      struct VPN4 : public VPN4U<T,Uncertainty>
      {
	/** Estimator type */
	using Estimator=VPN4U<T,Uncertainty>;
	/** Conversion type */
	using ToValue=flow::ToValue<T>;
	/** Calculations */
	using Calculations=flow::Calculations<T>;
	/** Real value type */
	using Value=typename Estimator::Value;
	/** Real value type */
	using ValueVector=typename Estimator::ValueVector;
	/** Size value type */
	using Size=typename Estimator::Size;
	/** 
	 * Constructor 
	 *
	 * @param n Harmonic 
	 */
	VPN4(Size n) : Estimator(n,7)   {}
	/** 
	 * Move constructor 
	 *
	 * @param o Object to move from 
	 */
	VPN4(VPN4&& o) : Estimator(std::move(o)) {}
	/** 
	 * Calculates the four-particle differential flow coefficients. 
	 * 
	 * The input are the average correlators as follows 
	 *
	 * @f{eqnarray*}{
	 % 0 &:& \overline{C_n\{4\}}\\
	 % 1 &:& \overline{C_n\{2\}}\\
	 % 2 &:& \overline{C'_{n,n}\{12\} C_{-n,-n}\{34\}}\\
	 % 3 &:& \overline{C'_{n,n}\{12\}}\\
	 % 4 &:& \overline{C_{-n,-n}\{34\}}\\
	 % 5 &:& \overline{C_{n,n}\{12\} C{-n,-n}\{34\}}\\
	 % 6 &:& \overline{C_{n,n}\{12\}}\\
	 % @f}
	 *
	 * and calculates 
	 *
	 * @f[v'_n\{12|34\} = \frac{\overline{C'_{n,n}\{12\} C_{-n,-n}\{34\}}
	 -2\overline{C'_{n,n}\{12\}}\,\overline{C_{n,n}\{34\}}}{
	 \overline{C_{n,n}\{12\} C{-n,-n}\{34\}} 
	 -2\overline{C_{n,n}\{12\}}\,\overline{C_{-n,-n}\{34\}}}@f]
	 *
	 */
	virtual Value value(const ValueVector& means) const
	{
	  Value cnm4    = Calculations::cnm(means[std::slice(0,2,1)]);
	  Value vnm4    = Calculations::vnm(4,cnm4);
	  Value cp12c34 = means[2];
	  Value cp12    = means[3];
	  Value c34     = means[4];
	  Value c12c34  = means[5];
	  Value c12     = means[6];

	  return (cp12c34 - 2 * cp12 * c34) / (c12c34 - 2 * c12 * c34) * vnm4;
	}
      };
    }
  }
}
#endif
//
// EOF
//
