/**
 * @file   correlations/flow/explicit/OfInterest4.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Explicit 4-particle differential flow in two partitions.
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_FLOW_EXPLICIT_OFINTEREST4_HH
#define CORRELATIONS_FLOW_EXPLICIT_OFINTEREST4_HH
#include <correlations/flow/Reference.hh>
#include <correlations/flow/explicit/Reference4.hh>
#include <correlations/flow/explicit/VPN4.hh>

namespace correlations
{
  namespace flow
  {
    namespace expl
    {
      //================================================================
      /** 
       * Wrapper around estimators 
       *
       * @param T Estimator type 
       */
      template <typename T>
      struct BinT4 : public Bin
      {
	/** Type of vnm estimator */
	using VNM=T;
	/** Vector of VNM estimators  */
	using VNMVector=std::vector<VNM>;
	/** Type of sizes */
	using Size=typename Bin::Size;
	/** Kind of observation */
	using Kind=typename Bin::Kind;
	/** Kinds of observation */
	using Kinds=typename Bin::Kinds;
	/** Partition of observation */
	using Partition=typename Bin::Partition;
	/** Partitions of observation */
	using Partitions=typename Bin::Partitions;
	/** Correlation view */
	using CorrelationView=Bin::CorrelationView;

	/** Destructor */
	virtual ~BinT4() {}
	/** Move constructor */
	BinT4(BinT4&& o)
	  :  Bin(std::move(o)),
	     _vnm(std::move(o._vnm))
	{}
	/** Deleted copy constructor */
	BinT4(const BinT4&) = delete;
	/** Deleted assignment operator */
	BinT4& operator=(const BinT4&) = delete;

	/** 
	 * Access a single flow estimator 
	 *
	 * @param n The harmonic order 
	 */
	const VNM& vnm(Size n) const
	{
	  return _vnm[n-1];
	}
	/** 
	 * Get results as JSON.  The parent object maps harmonic order
	 * to @f$m@f$ particle results.  That is, to access the
	 * @f$v_n\{m\}@f$ result, one can subscript the returned object
	 * @c r as
	 *
	 * @code 
	 r[n][m]
	 @endcode 
	 *
	 * Each of these elements consits of an array of two numbers: 
	 *
	 * - The value 
	 * - The (statistical) uncertainty on the value 
	 *
	 * A derived class can override this method to prepend or append
	 * additional results - e.g., the indendent variable can be
	 * prepended, or additional information can be appended.  If a
	 * derived class adds information in any way, it should also
	 * override the member function headers to give the appropriate
	 * header for the added information.
	 *
	 * @return Results encoded as JSON 
	 */
	virtual json::JSON result() const
	{
	  json::JSON json;
	  for (size_t n = 1; n <= maxN(); n++) {
	    auto vu = vnm(n).eval();
	    json[std::to_string(n)]["4"]
	      = { json::Array(vu.first,  vu.second) };
	  }
	  return json;
	}
	/** 
	 * Dump state to JSON 
	 *
	 * @return JSON object 
	 */
	virtual json::JSON toJson() const { return json::JSON(); }
	/** 
	 * Load state from JSON
	 */
	virtual void fromJson(const json::JSON&) {}
	/** 
	 * Merge state from JSON
	 */
	virtual void mergeJson(const json::JSON&) {}
	/** @return Largest harmonic order */
	virtual Size maxN() const  { return _vnm.size(); }
	/** @return Largest number of particles to correlate */
	virtual Size maxP() const { return 4; }
	virtual Bin& merge(const Bin&) { return *this; }
      protected:
	/** 
	 * Constructor 
	 *
	 * @param maxN Largest harmonic order 
	 */
	BinT4(Size maxN=4)
	  : Bin(),
	    _vnm()
	{
	  for (size_t n=1; n <= maxN; n++) 
	    _vnm.emplace_back(n);
	}
	/** 
	 * Access a single flow estimator 
	 *
	 * @param n The harmonic order 
	 */
	VNM& vnm(Size n)  { return _vnm[n-1]; }
	/** Vector of vectors of flow estimators */
	VNMVector _vnm;
      };
      //================================================================
      template <template <typename,template <typename> class> class VNM,
		typename T=correlations::Real,
		template <typename> class Uncertainty=stat::Derivatives,
		template <typename> class Correlator=correlations::Closed>
      struct OfInterest4Base : public BinT4<VNM<T,Uncertainty>>
      {
	/** Base class */
	using Base=BinT4<VNM<T,Uncertainty>>;
	/** Harmonic vectors */
	using HarmonicVector=correlations::HarmonicVector;
	/** type of correlator algorithm */
	using FromQVector=Correlator<correlations::HarmonicVector>;
	/** QVector type */
	using QVector=typename FromQVector::QVector;
	/** Q-vector storage */
	using QStore=typename FromQVector::QStore;
	/** Reference flow bin */
	using R = Reference<T,Uncertainty,Correlator,HarmonicVector>;
	/** Reference flow bin */
	using R4 = Reference4Base<Correlator>;
	/** Correlation type */
	using Correlation=typename Base::Correlation;
	/** Correlation type */
	using Correlations=typename Base::Correlations;
	/** Mapping of harmonic vectors */
	using HarmonicMap=std::vector<HarmonicVector>;
	/** Correlation view */
	using CorrelationView=typename Base::CorrelationView;
	/** 
	 * Constructor 
	 */
	OfInterest4Base(R& r, R4& r4)
	  : Base(r.maxN()),
	    _r(r),
	    _r4(r4),
	    _p12(2*r.maxN(),2,r.useWeights()),    // POI Q for 12
	    _q12(2*r.maxN(),2,r.useWeights()),    // Ref & POI for 12
	    _t12(_r4.r12(),_p12,_q12,false),      // Store of Diff. Q for 12 
	    _d12(_t12)                            // Diff. Corr for 12
	{}
	/** 
	 * Move constructor 
	 */
	OfInterest4Base(OfInterest4Base&& o)
	  : Base(std::move(o)),
	    _r(o._r),
	    _r4(o._r4),
	    _p12(std::move(o._p12)),
	    _q12(std::move(o._q12)),
	    _t12(_r4.r12(),_p12,_q12,false),    // Store of Diff. Q for 12 
	    _d12(_t12)                          // Diff. Corr for 12	  
	{
	}	
	OfInterest4Base(const OfInterest4Base& o) = delete;
	OfInterest4Base& operator=(const OfInterest4Base& o) = delete;
	/** 
	 * Reset cache 
	 */
	void reset()
	{
	  _r.reset();
	  _r4.reset();
	  _p12.reset();
	  _q12.reset();
	}
	/** 
	 * Fill in values 
	 */
	virtual void fill(Real phi, Real weight, Kind kind, Partition part)
	{
	  if (part != 1) return;
	
	  bool ref = kind & correlations::REF;
	  bool poi = kind & correlations::POI;
	  if (poi)         _p12.fill(phi,weight);
	  if (ref and poi) _q12.fill(phi,weight);
	}
	virtual void fill(const RealVector& phi,
			  const RealVector& weight,
			  const Kinds&      kind,
			  const Partitions& part)
	{
	  for (size_t i = 0; i < phi.size(); i++)
	    fill(phi[i], weight[i], kind[i], part[i]);
	}
	/** 
	 * Calculates needed correlators and updates statistics 
	 */
	virtual void update() = 0;
      protected:
	/** Reference bin */
	R& _r;
	/** Reference utility bin */
	R4& _r4;
	/** Of interest Q-vector for 12 */
	QVector _p12;
	/** Overlap Q-vector for 12 */
	QVector _q12;
	/** Storage of Q-vector for 12 for differential correlator */
	QStore _t12;
	/** Differential correlator for 12 */
	FromQVector _d12;       // Diff. Corr for 12
      };
    
      //================================================================
      /** 
       * Explict 4-particle differential flow calculations in two sub-events. 
       *
       * This uses the expression 
       *
       * @f[
       v'_n\{12,34\} &= 
       \frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}v_n\{4\}
       @f]
      */
      template <typename T=correlations::Real,
		template <typename> class Uncertainty=stat::Derivatives,
		template <typename> class Correlator=correlations::Closed>
      struct OfInterest4U :
	public OfInterest4Base<VPN4U,T,Uncertainty,Correlator>
      {
	using Base=OfInterest4Base<VPN4U,T,Uncertainty,Correlator>;
	/** Reference flow bin */
	using R = typename Base::R;
	/** Reference flow bin */
	using R4 = Reference4U<Correlator>;
	/** Correlation type */
	using Correlation=typename Base::Correlation;
	/** Correlation type */
	using Correlations=typename Base::Correlations;

	/** 
	 * Constructor 
	 */
	OfInterest4U(R& r, R4& r4) : Base(r, r4), _ref4(r4) {}
	OfInterest4U(OfInterest4U&& o) : Base(std::move(o)), _ref4(o._ref4) {}
	OfInterest4U(const OfInterest4U& o) = delete;
	OfInterest4U& operator=(const OfInterest4U& o) = delete;
	/** 
	 * Calculates needed correlators and updates statistics 
	 */
	virtual void update()
	{
	  this->_r.update();
	  _ref4.update();
	
	  for (size_t l = 0; l < this->_vnm.size(); l++) {
	    size_t       n     = l+1;
	    Correlation  c4    = this->_r.c(n,4);
	    Correlation  c2    = this->_r.c(n,2);
	    Correlation  c12   = _ref4.c12(n);
	    Correlation  d12   = this->_d12.calculate(_ref4.h12(n));
	    Correlations   c   = {c4,c2,d12, c12};
	    this->_vnm[l].update(c);
	  }
	}
      protected:
	R4& _ref4;
      };

      //----------------------------------------------------------------
      /** 
       * Explict 4-particle differential flow calculations in two sub-events. 
       *
       * This uses the expression 
       *
       * @f[v'_n\{12|34\} = \frac{\overline{C'_{n,n}\{12\} C_{-n,-n}\{34\}}
       -2\overline{C'_{n,n}\{12\}}\,\overline{C_{n,n}\{34\}}}{
       \overline{C_{n,n}\{12\} C{-n,-n}\{34\}} 
       -2\overline{C_{n,n}\{12\}}\,\overline{C_{-n,-n}\{34\}}}@f]
      */
      template <typename T=correlations::Real,
		template <typename> class Uncertainty=stat::Derivatives,
		template <typename> class Correlator=correlations::Closed>
      struct OfInterest4 : public OfInterest4Base<VPN4,T,Uncertainty,Correlator>
      {
	using Base=OfInterest4Base<VPN4,T,Uncertainty,Correlator>;
	/** Reference flow bin */
	using R = typename Base::R;
	/** Reference flow bin */
	using R4 = Reference4<Correlator>;
	/** Correlation type */
	using Correlation=typename Base::Correlation;
	/** Correlation type */
	using Correlations=typename Base::Correlations;

	/** 
	 * Constructor 
	 */
	OfInterest4(R& r, R4& r4) : Base(r, r4), _ref4(r4) {}
	/** 
	 * Move constructor 
	 */
	OfInterest4(OfInterest4&& o) : Base(std::move(o)), _ref4(o._ref4) {}	
	OfInterest4(const OfInterest4& o) = delete;
	OfInterest4& operator=(const OfInterest4& o) = delete;
	/** 
	 * Calculates needed correlators and updates statistics.
	 *
	 * @f{eqnarray*}{
	 % 0 &:& \overline{C_n\{4\}}\\
	 % 1 &:& \overline{C_n\{2\}}\\
	 % 2 &:& \overline{C'_{n,n}\{12\} C_{-n,-n}\{34\}}\\
	 % 3 &:& \overline{C'_{n,n}\{12\}}\\
	 % 4 &:& \overline{C_{-n,-n}\{34\}}\\
	 % 5 &:& \overline{C_{n,n}\{12\} C{-n,-n}\{34\}}\\
	 % 6 &:& \overline{C_{n,n}\{12\}}\\
	 % @f}
	 *
	 */
	void update()
	{
	  this->_r.update();
	  for (size_t l = 0; l < this->_vnm.size(); l++) {
	    size_t      n     = l+1;
	    Correlation  c4     = this->_r.c(n,4);
	    Correlation  c2     = this->_r.c(n,2);
	    Correlation  c12    = _ref4.c12(n);
	    Correlation  c34    = _ref4.c34(n);
	    Correlation  c12c34 = _ref4.c1234(n);
	    Correlation  d12    = this->_d12.calculate(_ref4.h12(n)); 
	    Correlation  d12c34(d12.sum()*c34.sum(),
				d12.weights()*c34.weights());
	    Correlations c      = {c4, c2, d12c34, d12, c34, c12c34, c12};

	    this->_vnm[l].update(c);
	  }
	}
      protected:
	R4& _ref4;
      };
    }
  }
}
#endif
//
// EOF
//

      
