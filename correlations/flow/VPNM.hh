/**
 * @file   correlations/flow/VPNM.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Base class for differential flow measurements
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_FLOW_VPNM_HH
#define CORRELATIONS_FLOW_VPNM_HH
#include <correlations/flow/VNM.hh>

namespace correlations
{
  namespace flow
  {
    /** 
     * @example Differential.hh
     *
     * Calculate @f$m@f$-particle @f$ p_{\mathrm{T}}@f$-differential
     * flow with or without @f$\eta@f$ gap
     *
     * @image html gen_eta_medium_diff.png
     * @image html gen_eta_medium_pdif.png
     */
    // =================================================================
    /** 
     * Base class for differential flow calculations 
     *
     * @tparam T The value type 
     * @tparam Uncertainties Type of estimator base-class 
     * 
     * @ingroup flow 
     * @headerfile "" <correlations/flow/VPNM.hh>
     */
    template <typename T,
	      template <typename> class Uncertainty=stat::Derivatives>
    struct VPNM : public VNM<T,Uncertainty> 
    {
      /** Base type */
      using Base=VNM<T,Uncertainty>;
      /** Conversion type */
      using ToValue=typename Base::ToValue;
      /** Type of calculations */
      using Calculations=typename Base::Calculations;
      /** Value type */
      using Value=typename Base::Value;
      /** Vector of values type */
      using ValueVector=typename Base::ValueVector;
      /** Result of correlator */
      using Correlation=typename Base::Correlation;
      /** Result of correlator */
      using Correlations=typename Base::Correlations;

      /** 
       * Constructor 
       *
       * @param n  The order of the flow coeffient to calculate 
       * @param m  Number of terms we need for calculating the flow coefficient 
       */
      VPNM(Size n, size_t m) : Base(n,2*m), _dnm(0), _vpnm(0) { this->_m = m; }
      /** 
       * Move constructor 
       *
       * @param o Object to move from 
       */
      VPNM(VPNM&& o) : Base(std::move(o)), _dnm(o._dnm), _vpnm(o._vpnm) {}
      /** 
       * Event update.
       *
       * This method expects two vectors of the differential and
       * integrated m-particle correlators in descending order.
       *
       * @f{eqnarray}{
       * d = \{C'_{\mathbf{n}}\{2k\},
       *       C'_{\mathbf{n}}\{2(k-i)\},
       *       \ldots,C'_{\mathbf{n}}\{2\}\}\\
       * c = \{C_{\mathbf{n}}\{2k\},
       *       C_{\mathbf{n}}\{2(k-i)\},
       *       \ldots,C_{\mathbf{n}}\{2\}\}\\
       * @f}
       *
       * for @f$ m = 2k@f$ and @f$i=1,\ldots,k-1@f$
       *
       * @param d Differential correlators @f$ C'_{\mathbf{n}_i}@f$
       * @param c Integrated correlator @f$ C_{\mathbf{n}_i}@f$
       */
      template <typename C>
      void update(const C& d, const C& c)
      {
	assert(d.size() == c.size());
	assert(d.size() + c.size() == this->Estimator::size());

	size_t      m = d.size();
	ValueVector v(2*m);
	ValueVector w(2*m);
	for (size_t i = 0; i < m; i++) {
	  v[i] = ToValue::eval(d[i]);
	  w[i] = d[i].weights();
	}
	for (size_t i = 0; i < m; i++) {
	  v[m+i] = ToValue::eval(c[i]);
	  w[m+i] = c[i].weights();
	}
	Base::_update(v, w);
      }
      template <typename C>
      void update(const C& dc)
      {
	Base::update(dc);
      }
      /** 
       * Calculate the value of @f$ v'_n\{m\}@f$. 
       *
       * @f[ 
       * v_n'\{m\} = \frac{d_n\{m\}}{c_n\{m\}}v_n\{m\}\quad,
       * @f]
       * 
       * where, with @f$m=2k@f$, 
       *
       * @f[ 
       * v_n\{2k\} = \sqrt[{\textstyle 2k}]{(-1)^{k-1}\frac{1}{M_k}c_{n}\{2k\}}
       * \quad,
       * @f]
       *
       * and 
       *
       * @f{eqnarray}{
       * c_{n}\{2k} &=& f(\overline{C}_{\mathbf{n}}\{2k},\ldots,
       *                  \overline{C}_{\mathbf{n}}\{2})\\ 
       * d_{n}\{2k} &=& f(\overline{C'}_{\mathbf{n}}\{2k},\ldots,
       *                  \overline{C'}_{\mathbf{n}}\{2},
       *                  \overline{C}_{\mathbf{n}}\{2(k-1)},\ldots,
       *                  \overline{C}_{\mathbf{n}}\{2})\\ 
       * @f}
       *
       *
       * @sa 
       * - Calculations<T>::vpnm 
       * - Calculations<T>::dnm 
       *
       * @param means Mean values of correlators 
       *              @f$\overline{C}_{\mathbf{n}}\{2j\}@f$ for 
       *              @f$j=1,\ldots,k@f$ and @f$m=2k@f$ 
       *
       * @return @f$ v_n\{m\}@f$ 
       */
      virtual Value value(const ValueVector& means) const
      {
	size_t m = this->Estimator::size()/2;
	auto   d = means[std::slice(0,m,1)];
	auto   c = means[std::slice(m,m,1)];
	Base::value(c);
	_dnm  = Calculations::dnm(d,c[std::slice(1,m-1,1)]);
	_vpnm = Calculations::vpnm(_m, _vnm, _dnm, _cnm);
	return _vpnm;
      }
      /** 
       * Get the derivatives of @f$ v'_n\{m\}@f$ with respect to the
       * correlators used.
       *
       * @f{eqnarray}{
       * \frac{\partial v'_n\{2k\}}{\partial\overline{C'}_{\mathbf{n}}\{2j}\\
       * \frac{\partial v'_n\{2k\}}{\partial\overline{C'}_{\mathbf{n}}\{2l}
       * \quad,
       * @f}
       *
       * for @f$ m=2k@f$ and @f$ j=1,\ldots k@f$ and @f$l=1,\ldots,k-1@f$.
       *
       * This member function then returns 
       *
       * @f{eqnarray}{
       * \Nabla v_{n}\{2k\} &=& 
       * \begin{bmatrix}
       * \frac{\partial v'_n\{2k\}}{\partial\overline{C'}_{\mathbf{n}}\{2k\}}\\
       * \ldots 
       * \frac{\partial v'_n\{2k\}}{\partial\overline{C'}_{\mathbf{n}}\{2\}}\\
       * \frac{\partial v'_n\{2k\}}{\partial\overline{C}_{\mathbf{n}}\{2k)\}}\\
       * \ldots 
       * \frac{\partial v'_n\{2k\}}{\partial\overline{C}_{\mathbf{n}}\{2\}}\\
       * \end{bmatrix}\\
       * &=& 
       * \begin{bmatrix}
       * \frac{\partial v'_n\{2k\}}{\partial d_n\{2k\}}
       * \frac{\partical d_n\{2k\}}{\partical\overline{C'}_\mathbf{n}\{2k\}}\\
       * \ldots 
       * \frac{\partial v'_n\{2k\}}{\partial d_n\{2k\}}
       * \frac{\partical d_n\{2k\}}{\partical\overline{C'}_\mathbf{n}\{2\}}\\
       * \frac{\partial v'_n\{2k\}}{\partial c_n\{2k\}}
       * \frac{\partical d_n\{2k\}}{\partical\overline{C}_\mathbf{n}\{2k\}}\\
       * \frac{\partial v'_n\{2k\}}{\partial d_n\{2k\}}
       * \frac{\partical d_n\{2k\}}{\partical\overline{C}_\mathbf{n}\{2(k-1)\}}
       * +
       * \frac{\partial v'_n\{2k\}}{\partial c_n\{2k\}}
       * \frac{\partical c_n\{2k\}}{\partical\overline{C}_\mathbf{n}\{2(k-1)\}}+
       * \ldots 
       * \frac{\partial v'_n\{2k\}}{\partial d_n\{2k\}}
       * \frac{\partical d_n\{2k\}}{\partical\overline{C}_\mathbf{n}\{2\}}
       * +
       * \frac{\partial v'_n\{2k\}}{\partial c_n\{2k\}}
       * \frac{\partical c_n\{2k\}}{\partical\overline{C}_\mathbf{n}\{2\}}+
       * \end{bmatrix}
       * @f}
       * 
       *
       * @sa
       * - Calculations<T>::dvpnm  Derivative of @f$v_{n}\{m\}@f$ 
       * - Calculations<T>::gdnm  Gradient of @f$c_{n}\{m\}@f$
       *
       * @param means Mean values of correlators 
       *              @f$\overline{C'}_{\mathbf{n}}\{2j\}@f$ and
       *              @f$\overline{C}_{\mathbf{n}}\{2j\}@f$ for 
       *              @f$j=1,\ldots,k@f$ and @f$m=2k@f$ 
       *
       * @return Derivatives (Gradient) of @f$ v'_n\{m\}@f$ wrt. correlators
       */
      virtual ValueVector derivatives(const ValueVector& means) const
      {
	size_t      m     = this->Estimator::size()/2;
	auto        d     = means[std::slice(0,m,1)];
	auto        c     = means[std::slice(m,m,1)];
	ValueVector dvpnm = Calculations::dvpnm(_m,_vpnm, _vnm, _dnm, _cnm);
	ValueVector gdnm  = Calculations::gdnm(d,c[std::slice(1,m-1,1)]);
	ValueVector gcnm  = Calculations::gcnm(c);
	ValueVector g(2*m);
	// dv'_n{2k}/dd_n{2k} * dd_n{2k} / dC'_{2k} for k=1..m/2
	for (size_t k = 0; k < m; k++) g[k] = dvpnm[0] * gdnm[k];
	// dv'_n{2k}/dd_n{2k} * dd_n{2k} / dC_{2k} +
	// dv'_n{2k}/dc_n{2k} * dc_n{2k} / dC_{2k} +
	// for k=1..m/2 with first term 0 for k=0
	for (size_t k = 0; k < m; k++) {
	  g[m+k] = dvpnm[1] * gcnm[k];
	  if (k > 0) g[m+k] += dvpnm[0] * gdnm[m+k-1];
	}
	return g;
      }
    protected:
      /** Estimator type */
      using Estimator=typename Base::Estimator;
      /** Clarify scope */
      using Base::_m;
      /** Clarify scope */
      using Base::_cnm;
      /** Clarify scope */
      using Base::_vnm;      
      /** Cache of correlator */
      mutable Value _dnm; 
      /** Cache of harmonic coefficient */
      mutable Value _vpnm; 
    };    
  }
}
#endif
//
// EOF
//
