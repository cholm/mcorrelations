/**
 * @file   correlations/flow/OfInterest.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2- and 4-particle
 * differential 2nd, 3rd, 4th, 5th, and 6th flow harmonics in a bin.
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_FLOW_OFINTEREST_HH
#define CORRELATIONS_FLOW_OFINTEREST_HH
#include <correlations/flow/Reference.hh>
#include <correlations/flow/VPNM.hh>

namespace correlations
{
  namespace flow
  {
    // _________________________________________________________________
    /** 
     * A single bin for analysis.  This is for differntial measurements.
     *
     * This class provided a mechanism for calculating the differential
     * flow coefficients, but using an external bin for the reference
     * particles.  This in principle can speed up the computations,
     * since we do not need to calculate the reference cumulants more
     * than once.
     * 
     * One must still fill the reference @f$ Q@f$-vector (via the
     * reference bin) separatly from this bin, but other operations,
     * such as clearing and computations are done on-demand by objects
     * of this class.
     *
     * @code 
     Reference  ref(MaxOrder(),MaxParticles());
     OfInterest diff(ref);
   
     while (MoreEvents()) {
       diff.reset();
       ref.reset();
       while (MoreObservations()) {
         unsigned int flags = ((IsReference()          ? Bin::REF : 0) | 
                               (IsParticleOfInterest() ? Bin::POI : 0));
         if (flags & Bin::REF) ref.fill(Phi(),Weight())
         diff.fill(Phi(), Weight(), flags);
       }
       diff.update();
     }
     @endcode
     *
     * The size of the differential @f$ Q@f$-vectors is determined by
     * the size of the @f$ Q@f$-vector of the reference bin.
     *
     * One can sub-class this class for convenience, for example as a
     * PtBin.
     *
     * @tparam T The value type 
     * @tparam Uncertainties Type of estimator base-class 
     * @tparam Correlator Algorithm for calculating correlators 
     * 
     * @headerfile "" <correlations/flow/OfInterest.hh>
     * @ingroup flow 
     */
    template <typename T=correlations::Real,
	      template <typename> class Uncertainty=stat::Derivatives>
    struct OfInterestBase : public BinT<VPNM<T,Uncertainty>>
    {
      /** Base type */
      using Base=BinT<VPNM<T,Uncertainty>>;
      /** Type of reference to integrated bin */
      using RBase=ReferenceBase<T,Uncertainty>;
      /** View */
      using CorrelationView=typename Base::CorrelationView;
      /** Kind of observation */
      using Kind=typename RBase::Kind;
      /** Kinds of observation */
      using Kinds=typename RBase::Kinds;
      /** Partition of observation */
      using Partition=typename RBase::Partition;
      /** Partitions of observation */
      using Partitions=typename RBase::Partitions;
      
      /** 
       * Constructor 
       *
       * @param i       External reference bin 
       */
      OfInterestBase(RBase& i)
	: Base(i.maxN(), i.maxP()),
	  _i(i),
	  _x(i.maxN())
      {
	for (auto& xx : _x) xx.resize(i.maxP()/2);
      }
      /** 
       * Move constructor 
       *
       * @param o Object to move data from 
       */
      OfInterestBase(OfInterestBase&& o)
	: Base(std::move(o)),
	  _i(o._i),
	  _x(std::move(o._x))
      {
      }
      /** 
       * Copy constructor (deleted)
       */
      OfInterestBase(const OfInterestBase&) = delete;
      /** 
       * Assignment operator (deleted)
       */
      OfInterestBase& operator=(const OfInterestBase&) = delete;
      /** @return Wether this bin is using weights */
      bool useWeights() const { return _i.useWeights(); }
    protected:
      /** Clarify scope */
      using Base::_vnm;
      /** Type of correlator result vector */
      using Correlations=typename Base::Correlations;
      /** Type of correlator result */
      using Correlation=typename Base::Correlation;
      /** Vector of vector of correlator results */
      using CorrelationsVector=typename Base::CorrelationsVector;

      virtual Correlation calculate(size_t m, size_t n) = 0;
      /** 
       * Calculate per-event correlators 
       */
      virtual void update()
      {
	_i.update();

	size_t mK = _vnm.size();
	for (size_t k = 0; k < _vnm.size(); k++) {
	  size_t  m   = 2*(k+1);
	  auto&   vni = _vnm[k];
	  for (size_t l = 0; l < vni.size(); l++) {
	    Size   n = l + 1;
	    size_t j = mK - k - 1;
	    auto& c  = _i.c(n);
	    auto& d  = _x[l];
	    d[j]     = calculate(m,n);
	    vni[l].update(CorrelationView(d,j),CorrelationView(c,j));
	  }
	}
	// std::cout << "Differential correlators\n"
	// 	     << o(_x) << std::endl;
      }
      /** Reference bin  */
      RBase& _i;
      /** m-particle correlator results of last event */
      CorrelationsVector _x;
    };      
    // =================================================================
    /** 
     * A single bin for analysis.  This is for differntial
     * measurements of 2- and 4-particle correlators.
     *
     * Objects of this class will calculate the differential flow
     * coefficients.
     *
     * @code 
     OfInterest diff(MaxOrder(),MaxParticles());
   
     while (MoreEvents()) {
       diff.reset();
       while (MoreObservations()) {
         unsigned int flags = ((IsReference()          ? Bin::REF : 0) | 
                             (IsParticleOfInterest() ? Bin::POI : 0));
         diff.fill(Phi(), Weight(), flags);
       }
       diff.update();
     }
     @endcode
     *
     * One can sub-class this class for convenience, for example as a
     * PtBin.  Note, if the same reference particles are used for
     * multiple differential bins, one may increase performance using
     * the alternative class OfInterestI.
     * 
     * @tparam T             The value type 
     * @tparam Uncertainties Type of estimator base-class 
     * @tparam Correlator    Algorithm for calculating correlators 
     * @tparam Harmonics     Type of harmonics 
     * 
     * @headerfile "" <correlations/flow/OfInterest.hh>
     * @ingroup flow 
     */
    template<typename T=correlations::Real,
	     template <typename> class Uncertainty=stat::Derivatives,
	     template <typename> class Correlator=correlations::Closed,
	     typename Harmonics=correlations::HarmonicVector>
    struct OfInterest;


    // _________________________________________________________________
    /** 
     * A single bin for differential analysis with undecorated
     * harmonics (not in partitions).
     *
     * This class provided a mechanism for calculating the differential
     * flow coefficients, but using an external bin for the reference
     * particles.  This in principle can speed up the computations,
     * since we do not need to calculate the reference cumulants more
     * than once.
     * 
     * One must still fill the reference @f$ Q@f$-vector (via the
     * reference bin) separatly from this bin, but other operations,
     * such as clearing and computations are done on-demand by objects
     * of this class.
     *
     * @code 
     Reference   ref(MaxOrder(),MaxParticles());
     OfInterestI diff(ref);
   
     while (MoreEvents()) {
       ref.reset();
       diff.reset();
       while (MoreObservations()) {
         unsigned int flags = ((IsReference()          ? REF : 0) | 
                               (IsParticleOfInterest() ? POI : 0));
         if (flags & REF) ref.fill(Phi(),Weight())
         diff.fill(Phi(), Weight(), flags);
       }
       diff.update();
     }
     @endcode
     *
     * The size of the differential @f$ Q@f$-vectors is determined by
     * the size of the @f$ Q@f$-vector of the reference bin.
     *
     * @tparam T             The value type 
     * @tparam Uncertainties Type of estimator base-class 
     * @tparam Correlator    Algorithm for calculating correlators 
     * 
     * @headerfile "" <correlations/flow/OfInterest.hh>
     * @ingroup flow 
     */
    template<typename T,
	     template <typename> class Uncertainty,
	     template <typename> class Correlator>
    struct OfInterest<T,
		      Uncertainty,
		      Correlator,
		      correlations::HarmonicVector>
      : public OfInterestBase<T,Uncertainty>
    {
      /** Base type */
      using Base=OfInterestBase<T,Uncertainty>;
      /** type of correlator algorithm */
      using FromQVector=Correlator<correlations::HarmonicVector>;
      /** QVector type */
      using QVector=typename FromQVector::QVector;
      /** Q-vector storage */
      using QStore=typename FromQVector::QStore;
      /** Reference bin type */
      using R=Reference<T,Uncertainty,Correlator,
				correlations::HarmonicVector>;
      /** Kind of observation */
      using Kind=typename Base::Kind;
      /** Kinds of observation */
      using Kinds=typename Base::Kinds;
      /** Partition of observation */
      using Partition=typename Base::Partition;
      /** Partitions of observation */
      using Partitions=typename Base::Partitions;
      
      /** 
       * Constructor 
       *
       * @param i       External reference bin 
       */
      OfInterest(R& i)
	: Base(i),
	  _ref(i),
	  _p(i.maxN(), i.maxP(), i.useWeights()),
	  _q(i.maxN(), i.maxP(), i.useWeights()),
	  _s(i.q(), _p, _q, false),
	  _d(_s)
      {}
      /** 
       * Move constructor 
       *
       * @param o Object to move data from 
       */
      OfInterest(OfInterest&& o)
	: Base(std::move(o)),
	  _ref(o._ref),
	  _p(std::move(o._p)),
	  _q(std::move(o._q)),
	  _s(_ref.q(), _p, _q,false),//Recreate to set refs correctly 
	  _d(_s)                     //Recreate to set refs correctly
      {}
      /** 
       * Copy constructor (deleted)
       */
      OfInterest(const OfInterest&) = delete;
      /** 
       * Assignment operator (deleted)
       */
      OfInterest& operator=(const OfInterest&) = delete;
      /** @return Wether this bin is using weights */
      bool useWeights() const { return _p.useWeights(); }
    protected:
      /**
       * Reset internal caches.  Note, derived classes should overload
       * the _clear method, not this method.
       */
      virtual void reset()
      {
	// Reset reference bin if not done already 
	_ref.reset();
	_s.reset();
      }
      /** 
       * Fill in an observation.  
       *
       * @note Reference particles should never be filled here.
       * Instead, we should fill the referenced integrated bin.
       *
       * @param phi    The observed @f$\varphi@f$ angle 
       * @param weight The weight of the observation 
       * @param kind   Type of observation (bit mask of types)
       * @param part   Partition identifier 
       */
      virtual void fill(Real phi, Real weight, Kind kind, Partition part=0)
      {
	_s.fill(phi,weight,kind,part);
      }
      /** 
       * Fill in an observation.  
       *
       * @note Reference particles should never be filled here.
       * Instead, we should fill the referenced integrated bin.
       *
       * @param phi    The observed @f$\varphi@f$ angle 
       * @param weight The weight of the observation 
       * @param kind   Type of observation (bit mask of types)
       * @param part   Partition identifiers 
       */
      virtual void fill(const RealVector& phi,
			const RealVector& weight,
			const Kinds&      kind,
			const Partitions& part=Partitions())
      {
	_s.fill(phi,weight,kind,part);
      }
      /** Type of correlator result */
      using Correlation=typename Base::Correlation;
      /** Vector of vector of correlator results */
      using CorrelationsVector=typename Base::CorrelationsVector;
      
      virtual Correlation calculate(size_t m, size_t n)
      {
	return _d.calculate(_ref.h(m,n));
      }
      /** Reference bin (again) */
      R& _ref;
      /** Q-vector of particles-of-interest */
      QVector _p;
      /** Q-vector of overlap of reference and particles-of-interest */
      QVector _q;
      /** Store */
      QStore _s;
      /** Differential correlator that uses Q-vectors */
      FromQVector _d;
    };      

    // _________________________________________________________________
    /** 
     * A single bin for differential analysis with decorated
     * harmonics (in partitions).
     *
     * This class provided a mechanism for calculating the differential
     * flow coefficients, but using an external bin for the reference
     * particles.  This in principle can speed up the computations,
     * since we do not need to calculate the reference cumulants more
     * than once.
     * 
     * One must still fill the reference @f$ Q@f$-vector (via the
     * reference bin) separatly from this bin, but other operations,
     * such as clearing and computations are done on-demand by objects
     * of this class.
     *
     * @code 
     Reference   ref(MaxOrder(),MaxParticles(),UseWeights(),Partitions());
     OfInterestI diff(ref);
   
     while (MoreEvents()) {
       ref.reset();
       diff.reset();
       while (MoreObservations()) {
         Kind kind = ((IsReference()          ? REF : 0) | 
                      (IsParticleOfInterest() ? POI : 0));
         if (kind & REF) ref.fill(Phi(),Weight(), kind, GetPartition())
         diff.fill(Phi(), Weight(), kind, GetPartition());
       }
       diff.update();
     }
     @endcode
     *
     * The size of the differential @f$ Q@f$-vectors is determined by
     * the size of the @f$ Q@f$-vector of the reference bin.
     *
     * @tparam T             The value type 
     * @tparam Uncertainties Type of estimator base-class 
     * @tparam Correlator    Algorithm for calculating correlators 
     * 
     * @headerfile "" <correlations/flow/OfInterest.hh>
     * @ingroup flow 
     */
    template<typename T,
	     template <typename> class Uncertainty,
	     template <typename> class Correlator>
    struct OfInterest<T,
		      Uncertainty,
		      Correlator,
		      correlations::PartitionHarmonicVector>
      : public OfInterestBase<T,Uncertainty>
    {
      /** Base type */
      using Base=OfInterestBase<T,Uncertainty>;
      /** type of correlator algorithm */
      using FromQVector=Correlator<correlations::PartitionHarmonicVector>;
      /** QVector type */
      using QVector=typename FromQVector::QVector;
      /** QVector type */
      using QContainer=correlations::QContainer;
      /** Q-vector storage */
      using QStore=typename FromQVector::QStore;
      /** Reference bin type */
      using R=Reference<T,Uncertainty,Correlator,
				correlations::PartitionHarmonicVector>;
      /** Kind of observation */
      using Kind=typename Base::Kind;
      /** Kinds of observation */
      using Kinds=typename Base::Kinds;
      /** Partition of observation */
      using Partition=typename Base::Partition;
      /** Partitions of observation */
      using Partitions=typename Base::Partitions;
      
      /** 
       * Constructor 
       *
       * @param i       External reference bin 
       */
      OfInterest(R& i)
	: Base(i),
	  _ref(i),
	  _p(makeQC(i.q().size(),i.maxN(), i.maxP(), i.useWeights())),
	  _q(makeQC(i.q().size(),i.maxN(), i.maxP(), i.useWeights())),
	  _s(i.q(), _p, _q, false),
	  _d(_s)
      {}
      /** 
       * Move constructor 
       *
       * @param o Object to move data from 
       */
      OfInterest(OfInterest&& o)
	: Base(std::move(o)),
	  _ref(o._ref),
	  _p(std::move(o._p)),
	  _q(std::move(o._q)),
	  _s(_ref.q(), _p, _q,false),//Recreate to set refs correctly 
	  _d(_s)                     //Recreate to set refs correctly
      {}
      /** 
       * Copy constructor (deleted)
       */
      OfInterest(const OfInterest&) = delete;
      /** 
       * Assignment operator (deleted)
       */
      OfInterest& operator=(const OfInterest&) = delete;
      /** @return Wether this bin is using weights */
      bool useWeights() const { return _p.front().useWeights(); }
    protected:
      /**
       * Reset internal caches.  Note, derived classes should overload
       * the _clear method, not this method.
       */
      virtual void reset()
      {
	// Reset reference bin if not done already 
	_ref.reset();
	_s.reset();
      }
      /** 
       * Fill in an observation.  
       *
       * @note Reference particles should never be filled here.
       * Instead, we should fill the referenced integrated bin.
       *
       * @param phi    The observed @f$\varphi@f$ angle 
       * @param weight The weight of the observation 
       * @param kind   Type of observation (bit mask of types)
       * @param part   Partition for this observation
       */
      virtual void fill(Real phi, Real weight, Kind kind, Partition part)
      {
	_s.fill(phi,weight,kind,part);
      }
      /** 
       * Fill in an observation.  
       *
       * @note Reference particles should never be filled here.
       * Instead, we should fill the referenced integrated bin.
       *
       * @param phi    The observed @f$\varphi@f$ angles 
       * @param weight The weight of the observations 
       * @param kind   Type of observations (bit mask of types)
       * @param part   Partition for these observations
       */
      virtual void fill(const RealVector& phi,
			const RealVector& weight,
			const Kinds&      kind,
			const Partitions& part)
      {
	_s.fill(phi,weight,kind,part);
      }
      /** Type of correlator result */
      using Correlation=typename Base::Correlation;
      /** Vector of vector of correlator results */
      using CorrelationsVector=typename Base::CorrelationsVector;

      /** 
       * Calculate the correlator 
       */
      virtual Correlation calculate(size_t m, size_t n)
      {
	return _d.calculate(_ref.h(m,n));
      }
      /** Reference bin (again) */
      R& _ref;
      /** Q-vector of particles-of-interest */
      QContainer _p;
      /** Q-vector of overlap of reference and particles-of-interest */
      QContainer _q;
      /** Store */
      QStore _s;
      /** Differential correlator that uses Q-vectors */
      FromQVector _d;
      /** m-particle correlator results of last event */
      CorrelationsVector _x;
    };      
    
#if 0    
    // =================================================================
    /** 
     * A single bin for differential analysis with own reference bin.
     *
     * Objects of this class will calculate the differential flow
     * coefficients.
     *
     * @code 
     OfInterestWithRef diff(MaxOrder(),MaxParticles());
   
     while (MoreEvents()) {
       diff.reset();
       while (MoreObservations()) {
         Kind kind = ((IsReference()          ? REF : 0) | 
                      (IsParticleOfInterest() ? POI : 0));
         diff.fill(Phi(), Weight(), kind);
       }
       diff.update();
     }
     @endcode
     *
     * One can sub-class this class for convenience, for example as a
     * PtBin.  Note, if the same reference particles are used for
     * multiple differential bins, one may increase performance using
     * the alternative class OfInterestI.
     * 
     * @tparam T             The value type 
     * @tparam Uncertainties Type of estimator base-class 
     * @tparam Correlator    Algorithm for calculating correlators 
     * @tparam Harmonics     Type of harmonics 
     * 
     * @headerfile "" <correlations/flow/OfInterest.hh>
     * @ingroup flow 
     */
    template<typename T=correlations::Real,
	     template <typename> class Uncertainty=stat::Derivatives,
	     template <typename> class Correlator=correlations::Closed,
	     typename Harmonics=correlations::HarmonicVector>
    struct OfInterestWithRef :
      public OfInterest<T,Uncertainty,Correlator,Harmonics>
    {
      /** Base type */
      using Base=OfInterest<T,Uncertainty,Correlator,Harmonics>;
      /** type of correlator algorithm */
      using FromQVector=typename FromQVector;
      /** QVector type */
      using QVector=typename Base::QVector;
      /** Q-vector storage */
      using QStore=typename FromQVector::QStore;
      /** Reference bin type */
      using R=typename Base::Reference;

      OfInterestWithRef(Size maxN=4,
			Size maxP=2,
			bool weights=false,
			size npart=1)
	: Base(_ref(maxN,maxP,weights,npart))
      {}
      
	       
      
    protected:
      /** 
       * Fill in an observation.  
       *
       * @note Reference particles should never be filled here.
       * Instead, we should fill the referenced integrated bin.
       *
       * @param phi    The observed @f$\varphi@f$ angle 
       * @param weight The weight of the observation 
       * @param type   Type of observation (bit mask of types)
       */
      virtual void fill(Real phi, Real weight, Kind kind, Partition part)
      {
	_ref.fill(phi,weight,kind,part);
	_s.fill(phi,weight,kind,part);
      }
      /** 
       * Fill in an observation.  
       *
       * @note Reference particles should never be filled here.
       * Instead, we should fill the referenced integrated bin.
       *
       * @param phi    The observed @f$\varphi@f$ angle 
       * @param weight The weight of the observation 
       * @param type   Type of observation (bit mask of types)
       */
      virtual void fill(const RealVector& phi,
			const RealVector& weight,
			const Kinds&      kind,
			const Partitions& part)
      {
	_ref.fill(phi,weight,kind,part);
	_s.fill(phi,weight,kind,part);
      }
      R _ref;
    };
#endif
  }
}
#endif
// Local Varibles:
//   compile-command: "make -C ../../ "
// End:
//
// EOF
//

