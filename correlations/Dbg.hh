#ifndef CORRELATIONS_DBG_HH
#define CORRELATIONS_DBG_HH
/**
 * @file   correlations/Dbg.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Debug guard
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <iostream>
#include <iomanip>

namespace correlations
{
  /** 
   * A debug guard that automatically indents output 
   *
   * Use as 
   *
   * @code 
   void func()
   {
     Dbg g;
     g.out() << "Entered function" << std::endl;
     
     for (size_t i = 0; i < 10; i++) { 
       Dbg gg;
       gg.out() << "At iteration " << i << std::endl;
     }
   }
   @endcode 
   */
  struct Dbg
  {
    /** Output stream */
    std::ostream& _out;
    /** 
     * Constructor.
     *
     * Increments indendention 
     *
     * @param out Output to write to 
     */
    Dbg(std::ostream& out=std::cerr)
      : _out(out)
    {
      inc();
    }
    /** 
     * Destructor.
     *
     * Decrements indention 
     */
    ~Dbg() { dec(); }
    /** 
     * Get current indention 
     *
     * @param off Possible change to indention 
     *
     * @return Current indention level 
     */
    size_t current(int off=0)
    {
      static int _i = 0;
      if      (off < 0) _i--;
      else if (off > 0) _i++;
      return _i;
    }
    /** Increments indention */
    void inc() { current(+1); }
    /** Decrements indention */
    void dec() { current(-1); }
    /** 
     * Get the output stream 
     * 
     * @param ind If true (default), print indention first 
     *
     * @return The output stream 
     */
    std::ostream& o(bool ind=true)
    {
      if (!ind || current() < 1) return _out;
      return _out << std::setw(current()) << " ";
    }
  };
}
#endif
//
// EOF
//
