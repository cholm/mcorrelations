/**
 * @file   correlations/ana/Benchmark.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Read in data and calculate correlators
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_BENCHMARK_HH
#define ANA_BENCHMARK_HH
#include <correlations/Closed.hh>
#include <correlations/Recursive.hh>
#include <correlations/Recurrence.hh>
#include <correlations/NestedLoops.hh>
#include <correlations/RecursiveLoops.hh>
#include <correlations/ana/Analyzer.hh>
#include <iostream>
#include <iomanip>
#include <chrono>
/** 
 * @defgroup test Testing code 
 *
 * Some code to test the calculations 
 *
 * @image html gen_phi_small_corr.png 
 */
namespace ana
{
  //====================================================================
  /** 
   * @ingroup test 
   */
  namespace {
    constexpr const char _NC[] = "nested_closed";
    constexpr const char _NR[] = "nested_recursive";
    constexpr const char _QC[] = "qvector_closed";
    constexpr const char _QR[] = "qvector_recursive";
    constexpr const char _QU[] = "qvector_recurrence";
  }

  //====================================================================
  /** 
   * Base class for correlator test 
   *
   *
   * @ingroup test 
   * @headerfile "" <correlations/progs/corr.hh>
   * @example benchmark.cc
   */
  template <const char* OUT, typename Harmonics=correlations::HarmonicVector>
  struct Benchmark : public ana::Analyzer
  {
    /** Type of harmonics */
    using HarmonicVector  = Harmonics;
    /** Type of size */
    using Size            = correlations::Size;
    /** Type of correlator */
    using Correlator      = correlations::Correlator<HarmonicVector>;
    /** Type of correlation */
    using Result          = correlations::Result;
    /** Type of correlation */
    using ResultVector    = correlations::ResultVector;
    /** Complex type */
    using Complex         = typename Correlator::Complex;
    /** Base class */
    using Base            = Analyzer;
    
    /** Destructor */
    virtual ~Benchmark() {}
    /** 
     * Analyze one event 
     *
     * @param p List of particles 
     */
    void event(const ParticleVector& p)
    {
      clear(p.size());

      auto start = std::chrono::system_clock::now();
      takePhis(p);
      auto end = std::chrono::system_clock::now();
      std::chrono::duration<double> elap = end-start; // seconds

      this->calculate(correlator(),elap.count());
    }
    /** Maximum number of particles */
    virtual size_t max() const = 0;
  protected:
    /** 
     * Constructor 
     *
     * @param maxN    Maximum number of particles to correlate 
     * @param h       Harmonics
     */
    Benchmark(Size maxN, const HarmonicVector& h)
      : Analyzer(),
	_h(h),
	_r(maxN),
	_t(maxN)
    {
      std::cerr << "Harmonics:";
      for (auto& hh : _h) std::cerr << std::setw(3) << hh;
      std::cerr << std::endl;
    }
    /** 
     * Move copy constructor 
     *
     * @param o Object to move from 
     */
    Benchmark(Benchmark&& o)
      : Analyzer(o),
	// _u(std::move(o._u)),
	_h(std::move(o._h)),
	_r(std::move(o._r)),
	_t(std::move(o._t))
    {}
    /** 
     * Perform the correlator calculations for 2-N particle
     * correlations.
     *
     * @param c Correlator to use 
     * @param fillt Time to fill Q-vector of phi store 
     */
    void calculate(Correlator& c, double fillt)
    {
      stat::WelfordVar<>::RealVector t(_r.size());
      t[0]  = fillt;
      _r[0] = 0;
      for (size_t i = 1; i < _r.size(); i++) {
	Size n = i + 1;

	auto start = std::chrono::system_clock::now();
	_r[i] = c.calculate(n, _h);
	auto end = std::chrono::system_clock::now();
	std::chrono::duration<double> elap = end-start; // seconds
	t[i] = elap.count();
      }
      _t.fill(t,t);
    }
    /** 
     * Clears 
     */
    virtual void clear(size_t) {}
    /** 
     * Store phis 
     */
    virtual void takePhis(const ParticleVector& p)
    {
      for (auto& pp : p) {
	Real phi     = std::get<2>(pp);
	Real w       = std::get<3>(pp);

	takePhi(phi,w);
      }
    }
    /**
     * Register an observation 
     *
     * @param phi    The azimuthal angle @f$\varphi@f$
     * @param weight The weight 
     */
    virtual void takePhi(Real phi, Real weight) = 0;
    /** Get the correlator */
    virtual Correlator& correlator() = 0;
    /** Get state as JSON */
    json::JSON toJson() const { return json::JSON(); }
    /** Decode state from JSON */
    void fromJson(const json::JSON&) {}
    /** Merge state from JSON */
    void mergeJson(const json::JSON&) {}
    /** 
     * Write results to as JSON 
     */
    json::JSON result() const
    {
      json::JSON json  = Base::result();
      json::JSON times = json::Object();
      for (size_t i = 0; i < _r.size(); i++) {
	Size    n  = i + 1;
	Complex c  = _r[i].eval();
	Real    t  = _t.mean()[i];
	Real    et = _t.sem()[i];
	if (n==1) n = 0;
	times[std::to_string(n)] = { "real", c.real(),
				     "imag", c.imag(),
				     "t",    t,
				     "dt",   et };
	
      }
      json["results"] = { "mode", std::string(OUT),
			  "times", times };
      return json;
    }
      
    /** Harmonic vector */
    HarmonicVector _h;
    /** Result vector */
    ResultVector _r;
    /** Timing (stores means) */
    stat::WelfordVar<> _t;
  };
  
  //====================================================================
  /** 
   * Nested loops correlator test template  
   *
   * @ingroup test 
   * @headerfile "" <correlations/progs/corr.hh>
   */
  template <typename N, const char* OUT>
  struct NBenchmark : public Benchmark<OUT,typename N::HarmonicVector>
  {
    /** Type of correlator */
    using NestedLoops = N;
    /** Base class */
    using Base           = Benchmark<OUT,typename N::HarmonicVector>;
    /** Size type */
    using Size           = typename Base::Size;
    /** Value type */
    using Real           = typename Base::Real;
    /** Array of values type */
    using RealArray      = typename NestedLoops::RealArray;
    /** Observations type */
    using ParticleVector = typename Base::ParticleVector;
    /** Harmonics type */
    using HarmonicVector = typename Base::HarmonicVector;
    /** Correlator type */
    using Correlator     = typename Base::Correlator;
    
    /** 
     * Constructor 
     *
     * @param maxN    Maximum number of particles to correlate 
     * @param h       Harmonics
     * @param weights Whether to use weights 
     */
    NBenchmark(Size maxN, const HarmonicVector& h, bool weights)
      : Base(maxN,h),
	_phiss(),
	_weights(),
	_c(_phiss,_weights, weights)
    {}
    /** Maximum number of particles */
    virtual size_t max() const { return _c.maxSize(); }
  protected:
    Correlator& correlator() { return _c; }
#if 0
    /** 
     * Store phis 
     */
    virtual void takePhis(const ParticleVector& p)
    {
      auto data = gen::Generator::toVectors(p);
      _phiss    = std::get<2>(data);
      _weights  = std::get<3>(data);
    }
#endif
    /**
     * Register an observation - not used 
     *
     * @param phi The azimuthal angle @f$\varphi@f$
     * @param w   The weight 
     */
    void takePhi(Real phi, Real w)
    {
      _phiss  .push_back(phi);
      _weights.push_back(w);
    }
    /** 
     * Clear and prep internal caches 
     *
     * @param n  How many observations to prepare for 
     */
    void clear(size_t n)
    {
      _phiss  .clear();
      _weights.clear();
      _phiss  .reserve(n);
      _weights.reserve(n);
    }
    /** Cache of phis */
    RealArray   _phiss;
    /** Cache of weights */
    RealArray   _weights;
    /** Correlator */
    NestedLoops _c;
  };

  //--------------------------------------------------------------------
  template <typename H>
  using CNBenchmark = NBenchmark<correlations::NestedLoops<H>,    _NC>;
  template <typename H>
  using RNBenchmark = NBenchmark<correlations::RecursiveLoops<H>, _NR>;

  //====================================================================
  /** 
   * QVector correlator test template 
   *
   * @ingroup test 
   * @headerfile "" <correlations/progs/corr.hh>
   */
  template <typename F, const char* OUT>
  struct QBenchmark : public Benchmark<OUT,typename F::HarmonicVector>
  {
    /** Correlator type */
    using FromQVector    = F;
    /** Store of Q-vectors */
    using QStore         = typename F::QStore;
    /** Q-vector */
    using QVector        = typename F::QVector;
    /** Base class */
    using Base           = Benchmark<OUT,typename F::HarmonicVector>;
    /** Size type */
    using Size           = typename Base::Size;
    /** Value type */
    using Real           = typename Base::Real;
    /** Observations type */
    using ParticleVector = typename Base::ParticleVector;
    /** Harmonics type */
    using HarmonicVector = typename Base::HarmonicVector;
    /** Correlator type */
    using Correlator     = typename Base::Correlator;
  
    /** 
     * Constructor 
     *
     * @param maxH    Maximum number of particles to correlate 
     * @param h       Harmonics
     * @param weights Whether to use weights 
     */
    QBenchmark(Size maxH, const HarmonicVector& h, bool weights)
      : Base(maxH, h),
	_q(correlations::makeQ(Base::_h, weights)),
	_c(QStore(_q,_q,_q))
    {}
    /** Clear cache */
    virtual void pre()
    {
      _q.reset();
    }
    /** Maximum number of particles */
    virtual size_t max() const { return _c.maxSize(); }
  protected:
    Correlator& correlator() { return _c; }
    // #define FILL_VECTORS
#ifndef FILL_VECTORS
    void takePhis(const ParticleVector& p)
    {
      auto data    = gen::Generator::toVectors(p);
      auto phis    = std::get<2>(data);
      auto weights = std::get<3>(data);
      _q.fill(phis,weights);
    }
#endif
    /**
     * Register an observation 
     *
     * @param phi The azimuthal angle @f$\varphi@f$
     * @param w   The weight 
     */
    void takePhi(Real phi, Real w)
    {
      _q.fill(phi, w);
    }
    /** Q-vector */
    QVector _q;
    /** Correlator */
    FromQVector _c;
  };

  //--------------------------------------------------------------------
  template <typename H>
  using CQBenchmark = QBenchmark<correlations::Closed<H>,     _QC>;
  template <typename H>
  using RQBenchmark = QBenchmark<correlations::Recursive<H>,  _QR>;
  template <typename H>
  using UQBenchmark = QBenchmark<correlations::Recurrence<H>, _QU>;
}
#endif
//
// EOF
//
