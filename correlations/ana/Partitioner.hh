/**
 * @file   correlations/ana/Partitioner.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A partitioner utility 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_PARTITIONER_HH
#define ANA_PARTITIONER_HH
#include <correlations/Types.hh>

namespace ana
{
  /** 
   * Base class for partitioners.  
   *
   * A partitioner partitions an event into partitions (or
   * sub-events).  The partitions are numbered sequentially starting
   * from one (@c 1).  Observations that are given the partition
   * number zero (@c 0) are ignored.
   */
  struct Partitioner
  {
    using Real=correlations::Real;
    using RealVector=correlations::RealVector;
    using Partition=correlations::Partition;
    using Partitions=correlations::Partitions;
    /** 
     * Must be overloaded.  Must return a list of partition
     * identifiers.  That is, for harmonic vector 
     *
     * @f[\mathbf{h} = \{h_1, \ldots, h_n\}\quad,@f] 
     *
     * where harmonic @f$h_i@f$ will be calculated from observations
     * in partition @f$ p_i@f$, then this must return
     *
     * @f[\mathbf{p} = \{p_1, p_2, \ldots, p_n\}\quad.@f]
     *
     * @return List of partition identifers. 
     */
    virtual const Partitions partitions() const = 0;
    /** 
     * Partition observations 
     *
     * @param eta Pseuorapidity @f$\eta@f$ of observations 
     * @param pt  Transverse momentum @f$p_{\mathrm{T}}@f$ of observations 
     * @param phi Azimuth angle @f$\varphi@f$ of observations 
     *
     * @return A list of partition identifiers (of same size as @a
     * eta) for each observation.  Observations with partition zero @c
     * 0 are ignored.
     */
    virtual Partitions partition(const RealVector& eta,
				 const RealVector& pt,
				 const RealVector& phi) const
    {
      Partitions p(eta.size());
      for (size_t i = 0; i < p.size(); i++) 
	p[i] = partition(eta[i],pt[i],phi[i]);

      return p;
    }
    /** 
     * Partition an observation
     *
     * @param eta Pseuorapidity @f$\eta@f$ of observation 
     * @param pt  Transverse momentum @f$p_{\mathrm{T}}@f$ of observation 
     * @param phi Azimuth angle @f$\varphi@f$ of observation 
     *
     * @return Partition identifiers for observation.  Observations
     * with partition zero @c 0 are ignored.
     */
    virtual Partition partition(Real eta,
				Real pt,
				Real phi) const = 0;
  };

  // _________________________________________________________________
  struct EtaGap : public Partitioner
  {
    using Base=Partitioner;
    using Real=Base::Real;
    using RealVector=Base::RealVector;
    using Partition=Base::Partition;
    using Partitions=Base::Partitions;

    /** 
     * Construct a @f$\Delta\eta@f$ Eta partitioner 
     *
     * @param deltaEta @f$\Delta\eta@f$
     */
    EtaGap(Real deltaEta=0) : _deta(deltaEta) {}
    /** Return partition identifiers */
    virtual const Partitions partitions() const { return {1,2}; }
    /** Return partition identifiers for each observation */
    virtual Partitions partition(const RealVector& eta,
				 const RealVector&,
				 const RealVector&) const
    {
      Partitions p(Partition(),eta.size());
      p[eta < -_deta/2] = 1;
      p[eta > +_deta/2] = 2;
      return p;
    }
    /** Return partition identifier for observation */
    virtual Partition partition(Real eta,
				Real,
				Real) const
    {
      return eta < _deta / 2 ? 1 : eta > _deta / 2 ? 2 : 0;
    }
  protected:
    Real _deta;
  };
}
#endif
//
// EOF
//
