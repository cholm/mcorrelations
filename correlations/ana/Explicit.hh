/**
 * @file   correlations/ana/Explicit.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief An differential analyzer 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_EXPLICIT_HH
#define ANA_EXPLICIT_HH
#include <correlations/ana/Analyzer.hh>
#include <correlations/flow/Reference.hh>
#include <correlations/flow/explicit/OfInterest4.hh>
#include <correlations/ana/XBinned.hh>
#include <correlations/ana/Partitioner.hh>

namespace ana
{
  namespace {
    static constexpr size_t M = 4;
  }    
  /** 
   * Explict 4-particle differential flow calculations in two sub-events. 
   *
   * This uses the expression 
   *
   * @f[v'_n\{12|34\} = \frac{\overline{C'_{n,n}\{12\} C_{-n,-n}\{34\}}
   -2\overline{C'_{n,n}\{12\}}\,\overline{C_{n,n}\{34\}}}{
   \overline{C_{n,n}\{12\} C{-n,-n}\{34\}} 
   -2\overline{C_{n,n}\{12\}}\,\overline{C_{-n,-n}\{34\}}}@f]
   *
   * @code 
   * Explicit<> d(MaxN(), MaxP(), UseWeights());
   *
   * while (MoreEvents())  {
   *   d.pre();
   *   d.event(Particles());
   *   d.post();
   * }
   * 
   * d.result();
   * @endcode
   *
   * This does _not_ do partitioned (or sub-event) flow.  Use the
   * correlations::PartitionHarmonicVector specialisation for that.
   *
   * @tparam T           Value type 
   * @tparam Uncertainty Type of uncertainty to use 
   * @tparam Correlator  Algorithm for calculating correlators
   * @example explicit.cc
   */
  template <typename T,
	    template <typename> class Uncertainty,
	    template <typename> class Correlator>
  struct Explicit : public Analyzer
  {
    /** Base class short hand */
    using Base=Analyzer;
    /** Reference bin */
    using Reference=correlations::flow::Reference<T,Uncertainty,
						  Correlator,
						  correlations::HarmonicVector>;
    /** Reference bin */
    using Reference4=correlations::flow::expl::Reference4<Correlator>;
    /** Of interest bins */
    using OfInterest=
      correlations::flow::expl::OfInterest4<T,Uncertainty,Correlator>;
    /** Binned analysis */
    using XB=XBinned<OfInterest>;
    using IB=LowHigh<Reference>;
    using IB4=LowHigh<Reference4>;
    using Real=typename XB::Real;
    using RealVector=typename XB::RealVector;
    /** Kind of observation */
    using Kind=typename Reference::Kind;
    /** Kinds of observation */
    using Kinds=typename Reference::Kinds;
    /** Partition of observation */
    using Partition=typename Reference::Partition;
    /** Partitions of observation */
    using Partitions=typename Reference::Partitions;
    /** 
     * Constructor 
     *
     * @param partitioner The partitioner object to use 
     * @param maxN        Maximum order 
     * @param weights     If true, use weights
     * @param edges       Bin edges 
     */
    Explicit(Partitioner&      partitioner,
	     size_t            maxN,
	     bool              weights,
	     const RealVector& edges)
      : _r(edges[0],
	   edges[edges.size()-1],
	   correlations::REF,
	   maxN,M,weights),
	_r4(edges[0], edges[edges.size()-1],correlations::REF,maxN,weights),
	_b(edges, correlations::POI, _r, _r4),
	_p(partitioner)
    {}
    /** Clear caches */
    void pre()
    {
      _r.reset();
      _r4.reset();
      _b.reset();
    }
    /** Process a single event */
    void event(const ParticleVector& p)
    {
      using correlations::REF;
#ifdef FILL_VECTORS
      auto data    = gen::Generator::toVectors(p);
      auto etas    = std::get<0>(data);
      auto pts     = std::get<1>(data);
      auto phis    = std::get<2>(data);
      auto weights = std::get<3>(data);
      auto parts   = _p.partition(etas,pts,phis);
      Kinds kinds(REF, phis.size());
      
      _r. fill(pts, phis, weights, kinds, parts);
      _r4.fill(pts, phis, weights, kinds, parts);
      _b. fill(pts, phis, weights, kinds, parts);
#else
      for (auto& pp : p) {
	Real eta       = std::get<0>(pp);
	Real pt        = std::get<1>(pp);
	Real phi       = std::get<2>(pp);
	Real weight    = std::get<3>(pp);
	Kind kind      = REF;
	Partition part = _p.partition(eta,pt,phi);
	_r. fill(pt, phi, weight, kind, part);
	_r4.fill(pt, phi, weight, kind, part);
	_b. fill(pt, phi, weight, kind, part);
      }
#endif
    }
    /** Updates statistics */
    void post()
    {
      _r .update();
      _r4.update();
      _b .update();
    }
    /** Get state as JSON */
    virtual json::JSON toJson() const { return json::JSON(); }
    /** Read state from json */
    virtual void fromJson(const json::JSON&) {}
    /** Merge state from JSON */
    virtual void mergeJson(const json::JSON&) {}
    /** Get results as JSON object */
    json::JSON result() const
    {
      json::JSON json = Base::result();
      json["results"] = _b.result();
      return json;
    }
    /** Reference bin */
    IB _r;
    /** Reference bin */
    IB4 _r4;
    /** Bins */
    XB _b;
    /** Partitioner */
    Partitioner& _p;
  };
    
  /** 
   * Explicit 4-particle differential flow analysis in two partitions. 
   *
   * This uses the expression 
   *
   * @f[
   v'_n\{12,34\} &= 
   \frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}v_n\{4\}
   @f]
   * 
   *
   * @code 
   * ExplicitU<> d(MaxN(), MaxP(), UseWeights(),Partitioner());
   *
   * while (MoreEvents())  {
   *   d.pre();
   *   d.event(Particles());
   *   d.post();
   * }
   * 
   * d.result();
   * @endcode
   *
   * This does do partitioned (or sub-event) flow.  Use the
   * correlations::HarmonicVector specialisation if that is not
   * wanted.
   *
   * @tparam T           Value type 
   * @tparam Uncertainty Type of uncertainty to use 
   * @tparam Correlator  Algorithm for calculating correlators 
   */
  template <typename T,
	    template <typename> class Uncertainty,
	    template <typename> class Correlator>
  struct ExplicitU : public Analyzer
  {
    /** Base class short hand */
    using Base=Analyzer;
    /** Reference bin */
    using Reference=
      correlations::flow::Reference<T,Uncertainty,
				    Correlator,
				    correlations::HarmonicVector>;
    /** Reference bin */
    using Reference4=correlations::flow::expl::Reference4U<Correlator>;
    /** Of interest bins */
    using OfInterest=
      correlations::flow::expl::OfInterest4U<T,Uncertainty,Correlator>;
    /** Binned analysis */
    using XB=XBinned<OfInterest>;
    using IB=LowHigh<Reference>;
    using IB4=LowHigh<Reference4>;
    using Real=typename XB::Real;
    using RealVector=typename XB::RealVector;
    /** Kind of observation */
    using Kind=typename Reference::Kind;
    /** Kinds of observation */
    using Kinds=typename Reference::Kinds;
    /** Partition of observation */
    using Partition=typename Reference::Partition;
    /** Partitions of observation */
    using Partitions=typename Reference::Partitions;

    /** 
     * Constructor 
     *
     * @param maxN        Maximum order 
     * @param weights     If true, use weights
     * @param edges       Bin edges 
     * @param partitioner Partitioner to use 
     * 
     */
    ExplicitU(Partitioner&      partitioner,
	      size_t            maxN,
	      bool              weights,
	      const RealVector& edges)
      : _r(edges[0],edges[edges.size()-1],correlations::REF,
	   maxN,M,weights,partitioner.partitions()),
	_r4(edges[0],edges[edges.size()-1],correlations::REF,maxN,weights),
	_b(edges, correlations::POI, _r, _r4),
	_p(partitioner)
    {}
    /** Clears caches */
    void pre()
    {
      _r .reset();
      _r4.reset();
      _b .reset();
    }
    /** Process a single event */
    void event(const ParticleVector& p)
    {
      std::cout << "\n\nProcessing event" << std::endl;
      using correlations::REF;
#ifdef FILL_VECTORS
      auto  data     = gen::Generator::toVectors(p);
      auto  etas     = std::get<0>(data);
      auto  pts      = std::get<1>(data);
      auto  phis     = std::get<2>(data);
      auto  weights  = std::get<3>(data);
      auto  parts    = _p.partition(etas,pts,phis);
      Kinds kinds(REF, phis.size());
      
      _r .fill(pts, phis, weights, kinds, parts);
      _r4.fill(pts, phis, weights, kinds, parts);
      _b .fill(pts, phis, weights, kinds, parts);
#else
      for (auto& pp : p) {
	Real      eta  = std::get<0>(pp);
	Real      pt   = std::get<1>(pp);
	Real      phi  = std::get<2>(pp);
	Real      w    = std::get<3>(pp);
	Partition part = _p.partition(eta,pt,phi);
	Kind      kind = REF;
	std::cout << "Calling fill on reference and binned" << std::endl;
	_r .fill(pt, phi, w, kind, part);
	_r4.fill(pt, phi, w, kind, part);
	_b .fill(pt, phi, w, kind, part);
      }
#endif
    }
    /** Updates statistics */
    void post()
    {
      _r .update();
      _r4.update();
      _b .update();
    }
    /** Get state as JSON */
    virtual json::JSON toJson() const { return json::JSON(); }
    /** Read state from JSON */
    virtual void fromJson(const json::JSON&) {}
    /** Merge state from JSON */
    virtual void mergeJson(const json::JSON&) {}
    /** Get results as JSON object */
    json::JSON result() const
    {
      json::JSON json = Base::result();
      json["results"] = _b.result();
      return json;
    }
    /** Reference bin */
    IB _r;
    /** Reference bin */
    IB4 _r4;
    /** Bins */
    XB _b;
    /** Partitioner */
    Partitioner& _p;
  };
}
#endif
//
// EOF
//
