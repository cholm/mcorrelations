/**
 * @file   correlations/ana/Reader.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Read data
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_READER_HH
#define ANA_READER_HH
#include <correlations/gen/Generator.hh>
namespace ana
{
  //====================================================================
  /**
   * Reader of events 
   *
   * @ingroup ana 
   * @headerfile "" <correlations/ana/Reader.hh>
   */
  struct Reader
  {
    /** Real type */
    using Real=correlations::Real;
    /** Type of particle vector */
    using ParticleVector=gen::Generator::ParticleVector;

    /** Constructor */
    Reader() {}
    
    /**
     * Read in one event 
     *
     * @param in  Input stream 
     * @param p   Vector to store particles in 
     *
     * @return Number of particles read, or negative if no more input 
     */
    int event(std::istream& in, ParticleVector& p)
    {
      if (in.eof()) return 0;
      
      if (!in.good())
	throw std::runtime_error("Bad input stream for reading data");
      
      // Clear previous values 
      p.clear();

      // Read in number of particles 
      size_t n = 0;
      in >> n;

      // Reserve space
      p.reserve(n);

      // Read in particles
      size_t i = 0;
      for (; i < n; i++) {
	Real eta, pt, phi, w;
	in >> eta >> pt >> phi >> w;
	if (in.eof()) break;
	if (!in.good()) {
	  std::cerr << "Failed to read particle " << i+1 << " of " << n
		    << std::endl;
	  return -1;
	}
	p.push_back(std::make_tuple(eta, pt, phi, w));
      }
      return i;
    }
  };
}
#endif
//
// EOF
//
