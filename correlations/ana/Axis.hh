/**
 * @file   correlations/ana/Axis.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Context of I/O
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_AXIS_HH
#define ANA_AXIS_HH
#include <correlations/gen/Generator.hh>
#include <correlations/json.hh>

namespace ana
{
  //====================================================================
  /** 
   * Base class for axes 
   *
   * @ingroup ana 
   * @headerfile "" <correlations/ana/Axis.hh>
   */
  struct Axis
  {
    /** Real type */
    using Real = gen::Generator::Real;
    /** Type of array of reals */
    using RealVector = gen::Generator::RealVector;
    /** Destructor */
    virtual ~Axis() {}
    /** 
     * Get the size of this axes 
     *
     * @return Number of bins 
     */
    virtual size_t size() const = 0;
    /** 
     * Find the bin number.  Returns negative on out of bound 
     *
     * @param x Value 
     *
     * @return Bin number or negative in case of out of bounds
     */
    virtual short bin(Real x) const = 0;
    /** 
     * Get mid point of bin 
     *
     * @param bin Bin number (zero based)
     */
    virtual Real mid(size_t bin) const = 0;
    /** 
     * Get mid width of bin 
     *
     * @param bin Bin number (zero based)
     */
    virtual Real width(size_t bin) const = 0;
    /**
     * Dump state to JSON 
     *
     * @return JSON representation
     */
    virtual json::JSON toJson() const = 0;
    /**
     * Load state from JSON 
     *
     * @param json JSON representation
     */
    virtual void fromJson(const json::JSON& json) = 0;
    /** 
     * Get bin boundaries 
     *
     * @return Array of bin boundaries 
     */
    virtual RealVector boundaries() const = 0;
  protected:
    Axis() {}
    Axis(const Axis&) {}
    Axis& operator=(const Axis&) = delete;
  };
  //====================================================================
  /** 
   * Axis with equidistant binning 
   *
   * @ingroup ana 
   * @headerfile "" <correlations/ana/Axis.hh>
   */
  struct Equidistant : public Axis
  {
    /** Real type */
    using Real = Axis::Real;
    using RealVector = Axis::RealVector;
    /** 
     * Constructor 
     *
     * @param n Number of bins 
     * @param l Least value to record 
     * @param h Largest value to record 
     */
    Equidistant(size_t n, Real l, Real h)
      : Axis(),
	_l(l),
	_h(h),
	_n(n),
	_d((_h-_l) / _n)
    {}
    /** 
     * Copy constructor 
     *
     * @param o Object to copy from 
     */
    Equidistant(const Equidistant& o)
      : Axis(o),
	_l(o._l),
	_h(o._h),
	_n(o._n),
	_d(o._d)
    {}
    /** Deleted assignement operator */
    Equidistant& operator=(const Equidistant& o) = delete;
    /** 
     * Get the size of this axes 
     *
     * @return Number of bins 
     */
    size_t size() const { return _n; }
    /** 
     * Find the bin number.  Returns negative on out of bound 
     *
     * @param x Value 
     *
     * @return Bin number or negative in case of out of bounds
     */
    short bin(Real x) const
    {
      if (x < _l || x > _h) return -1;
      size_t idx = (x - _l) / _d;
      if (idx == _n) idx--;
      return idx;
    }
    /** 
     * Get mid point of bin 
     *
     * @param bin Bin number (zero based)
     */
    Real mid(size_t bin) const { return _l + _d * (bin+.5); }
    /** 
     * Get mid width of bin 
     *
     */
    Real width(size_t) const { return _d; }
    /**
     * Dump state to output stream 
     *
     * @param out Output stream 
     */
    /**
     * Dump state to JSON 
     *
     * @return JSON representation
     */
    virtual json::JSON toJson() const
    {
      json::JSON json = {"min", _l,
			 "max", _h,
			 "nbin", _n };
      return json;
    }
    /**
     * Load state from JSON 
     *
     * @param json JSON representation
     */
    virtual void fromJson(const json::JSON& json)
    {
      _n = json["nbin"].toInt();
      _l = json["min"] .toFloat();
      _h = json["max"] .toFloat();
      _d = (_h - _l) / _n;
    }
    /** 
     * Get bin boundaries 
     *
     * @return Array of bin boundaries 
     */
    RealVector boundaries() const
    {
      RealVector b(_n+1);
      b[0] = _l;
      for (size_t i = 1; i < b.size(); i++) b[i] = b[i-1] + _d;
      return b;
    }
  protected:
    Real _l;
    Real _h;
    size_t _n;
    Real _d;
  };
  //------------------------------------------------------------------
  /** 
   * An axis with variable binning 
   *
   * @ingroup ana 
   * @headerfile "" <correlations/ana/Axis.hh>
   */
  struct Variable : public Axis
  {
    /** Real type */
    using Real = Axis::Real;
    /** Type of array of reals */
    using RealVector = Axis::RealVector;
    /** 
     * Constructor 
     *
     * @param i Initializer list of boundaries 
     */
    Variable(std::initializer_list<Real> i) : Axis(), _b(i) {}
    /** 
     * Constructor 
     *
     * @param b Array of boundaries
     */
    Variable(const RealVector& b) : Axis(), _b(b) {}
    /** 
     * Copy constructor 
     *
     * @param o Object to copy from 
     */
    Variable(const Variable& o) : Axis(o), _b(o._b) {}
    /** Deleted assignment operator */
    Variable& operator=(const Variable& o) = delete;
    /** 
     * Get the size of this axes 
     *
     * @return Number of bins 
     */
    size_t size() const { return _b.size()-1; }
    /** 
     * Find the bin number.  Returns negative on out of bound 
     *
     * @param x Value 
     *
     * @return Bin number or negative in case of out of bounds
     */
    short bin(Real x) const
    {
      auto b = std::begin(_b);
      auto e = std::end  (_b);

      auto it = std::lower_bound(b,e,x); // Search
      if (it != (e-1) && *it == x) it++;  // On boundary
      if (it == e) return -1;             // Out of bounds

      return std::max(std::distance(b, it)-1,0l);
    }
    /** 
     * Get mid point of bin 
     *
     * @param bin Bin number (zero based)
     */
    Real mid(size_t bin) const { return (_b[bin]+_b[bin+1])/2; }
    /** 
     * Get mid width of bin 
     *
     * @param bin Bin number (zero based)
     */
    Real width(size_t bin) const { return (_b[bin+1]-_b[bin]); }
    /**
     * Dump state to JSON 
     *
     * @return JSON representation
     */
    virtual json::JSON toJson() const
    {
      json::JSON json = {"nbin", _b.size(),
			 "edges", _b };
      return json;
    }
    /**
     * Load state from JSON 
     *
     * @param json JSON representation
     */
    virtual void fromJson(const json::JSON& json)
    {
      // size_t n = json["nbin"].toInt();      
      json.toArray(_b);
    }
    /** 
     * Get bin boundaries 
     *
     * @return Array of bin boundaries 
     */
    RealVector boundaries() const
    {
      return _b;
    }
  protected:
    RealVector _b;
  };
}
#endif
//
// EOF
//
