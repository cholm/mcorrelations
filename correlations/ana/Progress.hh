/**
 * @file   correlations/ana/Progress.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A progress meter
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_PROGRESS_HH
#define ANA_PROGRESS_HH
#include <correlations/ana/StopWatch.hh>
#include <iostream>
#include <iomanip>
#include <cmath>

namespace ana
{
  //====================================================================
  /** 
   * Guard (context manager) on IO attributes 
   *
   * @ingroup ana
   * @headerfile "" <correlations/ana/Progress.hh>
   */
  struct IOGuard
  {
    IOGuard(std::ostream&      out,
	    size_t             precision,
	    size_t             width)
      : _o(out),
	_p(out.precision()),
	_w(out.width()),
	_f(out.flags())
    {
      _o.precision(precision);
      _o.width(width);
    }
    ~IOGuard()
    {
      _o.precision(_p);
      _o.width(_w);
      _o.setf(_f);
    }
  protected:
    std::ostream&      _o;
    size_t             _p;
    size_t             _w;
    std::ios::fmtflags _f;
  };
  /** 
   * Progress meter 
   *
   * @ingroup ana
   */
  struct Progress
  {
    /** Output stream */
    std::ostream& _o;
    /** Stop watch */
    StopWatch     _w;
    /** Total number of events */
    size_t        _nev;
    /** Bar size */
    size_t        _b;
    /** Size of fields */
    size_t        _l;
    /** Last progress */
    mutable double _x;
    /** Constructor */
    Progress(std::ostream& o,size_t nev)
      : _o(o), _w(), _nev(nev), _b(30), _l(size_t(ceil(log10(_nev)+.5))), _x(0)
    {
    }
    /** format a duration */
    void format(double s) const
    {
      auto t = _w.parts(s);
      IOGuard g(_o, 3, 0);
      _o << std::fixed
	 << std::get<0>(t) << ":"
	 << std::setfill('0')
	 << std::setw(2) << std::get<1>(t) << ":"
	 << std::setprecision(3)
	 << std::setw(6) << std::get<2>(t) << std::flush;
      _o << std::setfill(' ');
    }
    /** Print progress */
    void print(size_t now) const
    {
      double prog = double(now) / _nev;
      if (prog < 1 and prog < _x + 0.01) return;
      
      _x          = prog;
      double elap = _w.split();
      double avg  = elap / now;
      double eta  = _nev * avg - elap;
      size_t blk  = _b  * prog;
      _o << '\r'
	 << std::string(blk,'#')
	 << std::string(_b-blk,'-') << ' '
	 << std::setw(_l) << now   << " / "
	 << std::setw(_l) << _nev
	 << " " << std::setw(3) << int(100*prog) << "% "
	 << (prog < 1 ? " ETA " : " took ");
      format(prog < 1 ? eta : elap);
      _o << std::flush;

      if (prog >= 1) {
	_o << " average ";
	format(elap/now);
	_o << " per event" << std::endl;
      }
    }
    void end(size_t) { }
  };
}
#endif
//
// EOF
//
