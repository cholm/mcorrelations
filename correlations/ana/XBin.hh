/**
 * @file   correlations/ana/XBin.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A wrapper for differential measurement bins with explicit
 * independent variable value and width
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef XBIN_HH
#define XBIN_HH
#include <correlations/flow/Bin.hh>
#include <valarray>
#include <numeric>

namespace ana
{
  /** 
   * A single bin that wraps a flow measurement bin. 
   */
  template <typename BaseBin>
  struct XBin : public BaseBin
  {
    /** Base class */
    using Base=BaseBin;
    /** Real value */
    using Real=typename Base::Real;
    /** Real value */
    using RealVector=typename Base::RealVector;
    /** Kind of observation */
    using Kind=typename Base::Kind;
    /** Kinds of observation */
    using Kinds=typename Base::Kinds;
    /** Partition of observation */
    using Partition=typename Base::Partition;
    /** Partitions of observation */
    using Partitions=typename Base::Partitions;
    /** Mask array */
    using Mask=std::valarray<bool>;
    /** 
     * Constructor 
     *
     * @param args Parameters for base class (bin) constructor 
     * @param kind The kind of observation to label observations with 
     */
    template <typename ...Args>
    XBin(Kind kind, Args& ...args)
      : Base(args...), _n(0), _w(0), _kind(kind)
    {
      // std::cout << "Bin with kind=" << _kind << std::endl;
    }
    /** 
     * Move constructor 
     */
    XBin(XBin&& o)
      : Base(std::move(o)),
	_n(o._n),
	_w(o._w),
	_kind(o._kind)
    {}
    /** 
     * Deleted copy constructor 
     */
    XBin(const XBin&) = delete;
    /** 
     * Deleted assignment operator 
     */
    XBin& operator=(const XBin&) = delete;
    /** Destructor */
    virtual ~XBin() {}
    /** 
     * Select an observation.  Based on the value of the independent
     * variable @a x, decide whether to include this observation or
     * not.
     *
     * Derived classes can overload this member function.  The default
     * implementation selects all observations.
     *
     * @return true if observation is selected. 
     */
    virtual bool select(Real) const
    {
      return true;
    }
    /** 
     * Select observations.  Based on the values of the independent
     * variable @a x, decide which observations to include or not.
     *
     * Derived classes can overload this member function.  The default
     * implementation selects all observations.
     *
     * @param x The independent variable values 
     *
     * @return Array of booleans, one for each of the values in @a x,
     * which are true if the corresponding observation is selected.
     */
    virtual Mask select(const RealVector& x) const
    {
      return Mask(true,x.size());
    }
    /** 
     * Resets internal cache 
     */
    virtual void reset()
    {
      Base::reset();
      _n = 0;
      _w = 0;
    }
    /** 
     * Updates parent bin 
     */
    virtual void update()
    {
      Base::update();
    }
    /** 
     * Fill in an observation.
     *
     * @param x      The independent variable value 
     * @param phi    The observed @f$\varphi@f$ angle 
     * @param weight The weight of the observation 
     * @param kind   Kind 
     * @param part   Partition
     */
    virtual void fill(Real       x,
		      Real       phi,
		      Real       weight,
		      Kind       kind=correlations::REF,
		      Partition  part=0)
    {
#if 0
      std::cout << "x="<< x << ":"
      		<< " phi=" << phi
      		<< " weight=" << weight
      		<< " kind=" << kind << "|" << _kind
      		<< " -> ";
#endif
      if (!select(x)) return;

      _n++;
      _w += weight;
      Base::fill(phi,weight,kind|_kind,part);
    }
    /** 
     * Fill in observations.  
     *
     * @param x      The independent variable values 
     * @param phi    The observed @f$\varphi@f$ angle 
     * @param weight The weight of the observation 
     * @param kind   Kind 
     * @param part   Partition
     */
    virtual void fill(const RealVector& x,
		      const RealVector& phi,
		      const RealVector& weight,
		      const Kinds       kind=0,
		      const Partitions& part=0)
    {
      auto msk = select(x);

      unsigned short n = 0;
      n = std::accumulate(std::begin(msk),std::end(msk),n);
      if (n <= 0) return;

      _n += n;
      _w += weight[msk].sum();
      
      Base::fill(phi,weight,kind|_kind,part);
    }
    /** Get number of included observations */
    unsigned short count() const { return _n; }
    /** Get sum of included weights */
    Real sumWeights() const { return _w; }
  protected:
    /** Count of observations */
    unsigned short _n;
    /** Sum of weights */
    Real           _w;
    /** Mark to add to selected observations */
    Kind _kind;
  };

  template <typename BaseBin>
  struct LowHigh : public XBin<BaseBin>
  {
    /** Base class */
    using Base=XBin<BaseBin>;
    /** Real value */
    using Real=typename Base::Real;
    /** Real value */
    using RealVector=typename Base::RealVector;
    /** Mask of observations */
    using Mask=typename Base::Mask;
    /** Kind of observation */
    using Kind=typename Base::Kind;
    
    template <typename ...Args>
    LowHigh(Real low, Real high, Kind kind, Args& ...args)
      : Base(kind, args...), _low(low), _high(high)
    {}
    /** 
     * Move constructor 
     */
    LowHigh(LowHigh&& o)
      : Base(std::move(o)),
	_low(o._low),
	_high(o._high)
    {}
    /** 
     * Deleted copy constructor 
     */
    LowHigh(const LowHigh&) = delete;
    /** 
     * Deleted assignment operator 
     */
    LowHigh& operator=(const LowHigh&) = delete;
    /** Destructor */
    virtual ~LowHigh() {}
    
    /** 
     * Select an observation.  Based on the value of the independent
     * variable @a x, decide whether to include this observation or
     * not.
     *
     * This selects observations where 
     *
     * @f[l \le x \lt h\quad.@f] 
     *
     * @param x The independent variable value 
     *
     * @return true if observation is selected. 
     */
    virtual bool select(Real x) const
    {
      bool ret = x >= _low and x < _high;
#if 0
      std::cout << (ret ? "selected" : "ignored")
       		<< " [" << _low << "," << _high << "]" << std::endl;
#endif
      return ret;
    }
    /** 
     * Select observations.  Based on the values of the independent
     * variable @a x, decide which observations to include or not.
     *
     * This selects observations where 
     *
     * @f[l \le x \lt h\quad.@f] 
     *
     * @param x The independent variable values 
     *
     * @return Array of booleans, one for each of the values in @a x,
     * which are true if the corresponding observation is selected.
     */
    virtual Mask select(const RealVector& x) const
    {
      return x >= _low && x < _high;
    }
    Real _low;
    Real _high;
  };
}
#endif
//
// EOF
//

