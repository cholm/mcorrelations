/**
 * @file   correlations/ana/Distributions.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief An distributions analyzer
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_DISTRIBUTIONS_HH
#define ANA_DISTRIBUTIONS_HH
#include <correlations/ana/Analyzer.hh>
#include <correlations/ana/Profile.hh>

namespace ana
{
  /**
   * Create distributions of variables in input file
   *
   * @example distributions.cc
   */
  struct Distributions : public Analyzer
  {
    using Base=Analyzer;
    using RealVector=stat::Stat<Real>::RealVector;
    /**
     * @param ptbins Vector of transverse momentum bins
     */
    Distributions(const RealVector& ptbins)
      : Analyzer(),
	_aeta(20,-1,1),
	_apt(ptbins),
	_aphi(30,0,2*M_PI),
	_aw(100,0,1),
	_dndeta(_aeta),
	_dndpt(_apt),
	_dndphi(_aphi),
	_dndw(_aw)
    {}
    /**
     *  Executed before start of event
     */
    void pre()
    {
      _dndeta.reset();
      _dndphi.reset();
      _dndpt .reset();
      _dndw  .reset();
    }
    /**
     *  Process an event
     *
     * @param p Vector of observations
     *
     * Each observation consist of
     *
     * - @f$\eta=\log\frac{p+p_z}{p-p_z}@f$ pseudorapidity
     * - @f$p_T=\sqrt{p_x^2+p_y^2}@f$ transverse momentum
     * - @f$\varphi = \tan^{-1}\frac{p_y}{p_x}@f$ azimuthal angle (radians)
     * - @f$w@f$ observation weight 
     */
    void event(const ParticleVector& p)
    {
      for (auto& pp : p) {
	Real eta     = std::get<0>(pp);
	Real pt      = std::get<1>(pp);
	Real phi     = std::get<2>(pp);
	Real w       = std::get<3>(pp);

	_dndeta.fill(eta);
	_dndpt .fill(pt);
	_dndphi.fill(phi);
	_dndw  .fill(w);
      }
    }
    /**
     * After each event
     */
    void post()
    {
      _dndeta.update();
      _dndphi.update();
      _dndpt .update();
      _dndw  .update();
    }
    /**
     * Get the result of the analysis as a JSON object
     */
    json::JSON result() const
    {
      json::JSON json = Base::result();
      json["results"] = { "dndeta", _dndeta.result(),
			  "dndpt",  _dndpt .result(),
			  "dndphi", _dndphi.result(),
			  "dndw",   _dndw  .result() };
      return json;
    }
    /**
     * Convert parameters to JSON object
     */
    json::JSON toJson() const
    {
      json::JSON json = { "header", _header.toJson(),
			  "dndeta", _dndeta.toJson(),
			  "dndpt",  _dndpt .toJson(),
			  "dndphi", _dndphi.toJson(),
			  "dndw",   _dndw  .toJson() };
      return json;
    }
    /**
     * Read state from JSON object
     *
     * @param json Object to read state from 
     */
    void fromJson(const json::JSON& json)
    {
      _header.fromJson(json["header"]);
      _dndeta.fromJson(json["dndeta"]);
      _dndpt .fromJson(json["dndpt"]);
      _dndphi.fromJson(json["dndphi"]);
      _dndw  .fromJson(json["dndw"]);
    }
    /**
     * Merge other state into this state
     *
     * @param json Object to merge into this
     */
    void mergeJson(const json::JSON& json)
    {
      _header.mergeJson(json["header"]);
      _dndeta.mergeJson(json["dndeta"]);
      _dndpt .mergeJson(json["dndpt"]);
      _dndphi.mergeJson(json["dndphi"]);
      _dndw  .mergeJson(json["dndw"]);
    }      
      
    Equidistant _aeta;
    Variable    _apt;
    Equidistant _aphi;
    Equidistant _aw;
    Profile     _dndeta;
    Profile     _dndpt;
    Profile     _dndphi;
    Profile     _dndw;
  };
}
#endif
//
// EOF
//

    
