/**
 * @file   correlations/ana/Integrated.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief An integrated analyzer
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_INTEGRATED_HH
#define ANA_INTEGRATED_HH
#include <correlations/ana/Analyzer.hh>
#include <correlations/flow/Reference.hh>
#include <correlations/ana/Partitioner.hh>
#include <correlations/ana/XBin.hh>

namespace ana
{
  /** 
   * Base template for integrated flow analyzer. 
   *
   * This class represents an analysis for integrated flow.  It determines 
   *
   * @f[v_n\{m\}\quad,@f]
   *
   * for 
   *
   * @f[n\in\{1,2,\ldots,\max n\}\quad m\in{2,4,\ldots,\max m\}\quad.@f]
   *
   * The default template does _not_ do partitioned (or sub-event) flow. 
   * Use the correlations::PartitionHarmonicVector specialisation for that. 
   *
   * @tparam T           Value type 
   * @tparam Uncertainty Type of uncertainty to use 
   * @tparam Correlator  Algorithm for calculating correlators 
   * @tparam Harmonics   Type of harmonics to use 
   * @example integrated.cc
   */
  template <typename T=correlations::Real,
	    template <typename> class Uncertainty=stat::Derivatives,
	    template <typename> class Correlator=correlations::Closed,
	    typename Harmonics=correlations::HarmonicVector>
  struct Integrated;

  /** 
   * Integrated flow analysis (without partitions)
   *
   * @code 
   * Integrated<> i(MaxN(), MaxP(), UseWeights());
   *
   * while (MoreEvents())  {
   *   i.pre();
   *   i.event(Particles());
   *   i.post();
   * }
   * 
   * i.result();
   * @endcode
   *
   * This does _not_ do partitioned (or sub-event) flow.  Use the
   * correlations::PartitionHarmonicVector specialisation for that.
   *
   * @tparam T           Value type 
   * @tparam Uncertainty Type of uncertainty to use 
   * @tparam Correlator  Algorithm for calculating correlators 
   */
  template <typename T,
	    template <typename> class Uncertainty,
	    template <typename> class Correlator>
  struct Integrated<T,Uncertainty,Correlator,correlations::HarmonicVector>
    : public Analyzer
  {
    /** Base class short hand */
    using Base=Analyzer;
    /** Reference bin */
    using Reference=correlations::flow::Reference<T,Uncertainty,
						  Correlator,
						  correlations::HarmonicVector>;
    /** Bin type */
    using XB=ana::XBin<Reference>;
    /** Kind of observation */
    using Kind=typename Reference::Kind;
    /** Kinds of observation */
    using Kinds=typename Reference::Kinds;
    /** Partition of observation */
    using Partition=typename Reference::Partition;
    /** Partitions of observation */
    using Partitions=typename Reference::Partitions;

    /** 
     * Constructor 
     *
     * @param maxN    Maximum order 
     * @param maxP    Number of particles to correlate 
     * @param weights If true, use weights
     */
    Integrated(size_t maxN, size_t maxP, bool weights)
      : _r(correlations::REF,maxN,maxP,weights)
    {}

    /** Called before event processing. Resets internal cache */
    void pre()
    {
      _r.reset();
    }
    /** Process a single event */
    void event(const ParticleVector& p)
    {
      using correlations::REF;
#ifdef FILL_VECTORS
      auto data    = gen::Generator::toVectors(p);
      auto phis    = std::get<2>(data);
      auto weights = std::get<3>(data);
      _r.fill(phis,phis,weights);
#else
      for (auto& pp : p) {
	Real phi     = std::get<2>(pp);
	Real w       = std::get<3>(pp);
	_r.fill(phi,phi,w);
      }
#endif
    }
    /** Called after event processing. Updates calculations */
    void post()
    {
      _r.update();
    }
    /** Get state as JSON object */
    json::JSON toJson() const
    {
      json::JSON r = { "bins", _r.toJson() };
      return r;
    }
    /** Read state from JSON object */
    void fromJson(const json::JSON& json)
    {
      _r.fromJson(json["bins"]);
    }
    /** Merge state from JSON object */
    void mergeJson(const json::JSON& json)
    {
      _r.mergeJson(json["bins"]);
    }
    /** Get results as JSON object */
    json::JSON result() const
    {
      json::JSON json = Base::result();
      json["results"] = _r.result();
      return json;
    }
    /** Reference bin */
    XB _r;
  };

  /** 
   * Integrated flow analysis (without partitions)
   *
   * @code 
   * Integrated<> i(MaxN(), MaxP(), UseWeights(), Partitions());
   *
   * while (MoreEvents())  {
   *   i.pre();
   *   i.event(Particles());
   *   i.post();
   * }
   * 
   * i.results();
   * @endcode
   *
   * This does _not_ do partitioned (or sub-event) flow.  Use the
   * correlations::PartitionHarmonicVector specialisation for that.
   *
   * @tparam T           Value type 
   * @tparam Uncertainty Type of uncertainty to use 
   * @tparam Correlator  Algorithm for calculating correlators 
   */
  template <typename T,
	    template <typename> class Uncertainty,
	    template <typename> class Correlator>
  struct Integrated<T,Uncertainty,Correlator,
		    correlations::PartitionHarmonicVector>
    : public Analyzer
  {
    /** Base class short hand */
    using Base=Analyzer;
    /** Reference bin */
    using Reference=
      correlations::flow::Reference<T,Uncertainty,
				    Correlator,
				    correlations::PartitionHarmonicVector>;
    /** Bin type */
    using XB=ana::XBin<Reference>;
    /** Kind of observation */
    using Kind=typename Reference::Kind;
    /** Kinds of observation */
    using Kinds=typename Reference::Kinds;
    /** Partition of observation */
    using Partition=typename Reference::Partition;
    /** Partitions of observation */
    using Partitions=typename Reference::Partitions;
						  
    /** 
     * Constructor 
     *
     * @param maxN        Maximum order 
     * @param maxP        Number of particles to correlate 
     * @param weights     If true, use weights
     * @param partitioner Partitioner to use 
     */
    Integrated(Partitioner&      partitioner,
	       size_t            maxN,
	       size_t            maxP,
	       bool              weights)
      : _r(correlations::REF,maxN,maxP,weights,partitioner.partitions()),
	_p(partitioner)
    {}

    /** Called before event processing. Resets internal cache */
    void pre()
    {
      _r.reset();
    }
    /** Process a single event */
    void event(const ParticleVector& p)
    {
      using correlations::REF;
#ifdef FILL_VECTORS
      auto       data    = gen::Generator::toVectors(p);
      auto       etas    = std::get<0>(data);
      auto       pts     = std::get<1>(data);
      auto       phis    = std::get<2>(data);
      auto       weights = std::get<3>(data);
      auto       parts   = _p.partition(etas,pts,phis);
      Kinds      kinds(REF, phis.size());
      _r.fill(phis,phis,weights,kinds,parts);
#else
      for (auto& pp : p) {
	Real      eta     = std::get<0>(pp);
	Real      pt      = std::get<1>(pp);
	Real      phi     = std::get<2>(pp);
	Real      w       = std::get<3>(pp);
	Partition part    = _p.partition(eta,pt,phi);
	Kind      kind    = REF;
	_r.fill(phi,phi,w,kind,part);
      }
#endif
    }
    /** Called after event processing. Updates calculations */
    void post()
    {
      _r.update();
    }
    /** Get state as JSON object */
    json::JSON toJson() const
    {
      json::JSON r = { "bins", _r.toJson() };
      return r;
    }
    /** Read state from JSON object */
    void fromJson(const json::JSON& json)
    {
      _r.fromJson(json["bins"]);
    }
    /** Merge state from JSON object */
    void mergeJson(const json::JSON& json)
    {
      _r.mergeJson(json["bins"]);
    }
    /** Get results as JSON object */
    json::JSON result() const
    {
      json::JSON json = Base::result();
      json["results"] = _r.result();
      return json;
    }
    /** Reference bin */
    XB _r;
    /** Partitioner to use */
    Partitioner& _p;
  };
}
#endif
//
// EOF
//
