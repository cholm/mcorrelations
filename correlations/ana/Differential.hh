/**
 * @file   correlations/ana/Differential.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief An differential analyzer 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_DIFFERENTIAL_HH
#define ANA_DIFFERENTIAL_HH
#include <correlations/ana/Analyzer.hh>
#include <correlations/flow/Reference.hh>
#include <correlations/flow/OfInterest.hh>
#include <correlations/ana/XBinned.hh>
#include <correlations/ana/Partitioner.hh>

namespace ana
{
  /** 
   * Base template for differential flow analyzer. 
   *
   * This class represents an analysis for differential flow.  It determines 
   *
   * @f[v'_n\{m\}\quad\mathrm{versus}\quad p_{\mathrm{T}},@f]
   *
   * for 
   *
   * @f[n\in\{1,2,\ldots,\max n\}\quad m\in{2,4,\ldots,\max m\}\quad.@f]
   *
   * The default template does _not_ do partitioned (or sub-event) flow. 
   * Use the correlations::PartitionHarmonicVector specialisation for that. 
   *
   * @tparam T           Value type 
   * @tparam Uncertainty Type of uncertainty to use 
   * @tparam Correlator  Algorithm for calculating correlators 
   * @tparam Harmonics   Type of harmonics to use 
   * @example differential.cc
   */
  template <typename T=correlations::Real,
	    template <typename> class Uncertainty=stat::Derivatives,
	    template <typename> class Correlator=correlations::Closed,
	    typename Harmonics=correlations::HarmonicVector>
  struct Differential;

  /** 
   * Differential flow analysis (without partitions)
   *
   * @code 
   * Differential<> d(MaxN(), MaxP(), UseWeights());
   *
   * while (MoreEvents())  {
   *   d.pre();
   *   d.event(Particles());
   *   d.post();
   * }
   * 
   * d.result();
   * @endcode
   *
   * This does _not_ do partitioned (or sub-event) flow.  Use the
   * correlations::PartitionHarmonicVector specialisation for that.
   *
   * @tparam T           Value type 
   * @tparam Uncertainty Type of uncertainty to use 
   * @tparam Correlator  Algorithm for calculating correlators 
   */
  template <typename T,
	    template <typename> class Uncertainty,
	    template <typename> class Correlator>
  struct Differential<T,Uncertainty,Correlator,correlations::HarmonicVector>
    : public Analyzer
  {
    /** Base class short hand */
    using Base=Analyzer;
    /** Reference bin */
    using Reference=correlations::flow::Reference<T,Uncertainty,
						  Correlator,
						  correlations::HarmonicVector>;
    /** Of interest bins */
    using OfInterest=
      correlations::flow::OfInterest<T,Uncertainty,
				     Correlator,
				     correlations::HarmonicVector>;
    /** Binned analysis */
    using XB=XBinned<OfInterest>;
    using IB=LowHigh<Reference>;
    using Real=typename XB::Real;
    using RealVector=typename XB::RealVector;
    /** Kind of observation */
    using Kind=typename Reference::Kind;
    /** Kinds of observation */
    using Kinds=typename Reference::Kinds;
    /** Partition of observation */
    using Partition=typename Reference::Partition;
    /** Partitions of observation */
    using Partitions=typename Reference::Partitions;

    /** 
     * Constructor 
     *
     * @param maxN    Maximum order 
     * @param maxP    Number of particles to correlate 
     * @param weights If true, use weights
     * @param edges   Bin edges 
     */
    Differential(size_t maxN, size_t maxP, bool weights,
		 const RealVector& edges)
      : _r(edges[0],edges[edges.size()-1],correlations::REF,
	   maxN,maxP,weights),
	_b(edges, correlations::POI, _r)
    {}
    /** Clear caches */
    void pre()
    {
      _r.reset();
      _b.reset();
    }
    /** Process a single event */
    void event(const ParticleVector& p)
    {
      using correlations::REF;
#ifdef FILL_VECTORS
      auto data    = gen::Generator::toVectors(p);
      // auto etas = std::get<0>(data);
      auto pts     = std::get<1>(data);
      auto phis    = std::get<2>(data);
      auto weights = std::get<3>(data);
      Kinds kinds(REF, phis.size());
      
      _r.fill(phis,weights,kinds);
      _b.fill(pts, phis, weight, kinds);
#else
      for (auto& pp : p) {
	// Real eta  = std::get<0>(pp);
	Real pt      = std::get<1>(pp);
	Real phi     = std::get<2>(pp);
	Real w       = std::get<3>(pp);
	_r.fill(pt, phi, w, REF);
	_b.fill(pt, phi, w, REF);
      }
#endif
    }
    /** Updates statistics */
    void post()
    {
      _r.update();
      _b.update();
    }
    /** Get state as JSON */
    virtual json::JSON toJson() const
    {
      json::JSON r = { "reference", _r.toJson(),
		       "bins",      _b.toJson() };
      return r;
    }
    /** Read state from json */
    virtual void fromJson(const json::JSON& json)
    {
      _r.fromJson(json["reference"]);
      _b.fromJson(json["bins"]);
    }
    /** Merge state from JSON */
    virtual void mergeJson(const json::JSON& json)
    {
      _r.mergeJson(json["reference"]);
      _b.mergeJson(json["bins"]);
    }
    /** Get results as JSON object */
    json::JSON result() const
    {
      json::JSON json = Base::result();
      json["results"] = _b.result();
      return json;
    }
    /** Reference bin */
    IB _r;
    /** Bins */
    XB _b;
  };
    
  /** 
   * Differential flow analysis (with partitions)
   *
   * @code 
   * Differential<> d(MaxN(), MaxP(), UseWeights(),Partitioner());
   *
   * while (MoreEvents())  {
   *   d.pre();
   *   d.event(Particles());
   *   d.post();
   * }
   * 
   * d.result();
   * @endcode
   *
   * This does do partitioned (or sub-event) flow.  Use the
   * correlations::HarmonicVector specialisation if that is not
   * wanted.
   *
   * @tparam T           Value type 
   * @tparam Uncertainty Type of uncertainty to use 
   * @tparam Correlator  Algorithm for calculating correlators 
   */
  template <typename T,
	    template <typename> class Uncertainty,
	    template <typename> class Correlator>
  struct Differential<T,Uncertainty,Correlator,
		      correlations::PartitionHarmonicVector>
    : public Analyzer
  {
    /** Base class short hand */
    using Base=Analyzer;
    /** Reference bin */
    using Reference=
      correlations::flow::Reference<T,Uncertainty,
				    Correlator,
				    correlations::PartitionHarmonicVector>;
    /** Of interest bins */
    using OfInterest=
      correlations::flow::OfInterest<T,Uncertainty,
				     Correlator,
				     correlations::PartitionHarmonicVector>;
    /** Binned analysis */
    using XB=XBinned<OfInterest>;
    using IB=LowHigh<Reference>;
    using Real=typename XB::Real;
    using RealVector=typename XB::RealVector;
    /** Kind of observation */
    using Kind=typename Reference::Kind;
    /** Kinds of observation */
    using Kinds=typename Reference::Kinds;
    /** Partition of observation */
    using Partition=typename Reference::Partition;
    /** Partitions of observation */
    using Partitions=typename Reference::Partitions;

    /** 
     * Constructor 
     *
     * @param maxN        Maximum order 
     * @param maxP        Number of particles to correlate 
     * @param weights     If true, use weights
     * @param edges       Bin edges 
     * @param partitioner Partitioner to use 
     * 
     */
    Differential(Partitioner&      partitioner,
		 size_t            maxN,
		 size_t            maxP,
		 bool              weights,
		 const RealVector& edges)
      : _r(edges[0],edges[edges.size()-1],correlations::REF,
	   maxN,maxP,weights,partitioner.partitions()),
	_b(edges, correlations::POI, _r),
	_p(partitioner)
    {}
    /** Clears caches */
    void pre()
    {
      _r.reset();
      _b.reset();
    }
    /** Process a single event */
    void event(const ParticleVector& p)
    {
      using correlations::REF;
#ifdef FILL_VECTORS
      auto  data     = gen::Generator::toVectors(p);
      auto  etas     = std::get<0>(data);
      auto  pts      = std::get<1>(data);
      auto  phis     = std::get<2>(data);
      auto  weights  = std::get<3>(data);
      auto  parts    = _p.partition(etas,pts,phis);
      Kinds kinds(REF, phis.size());
      kind[parts==2] = REF;
      
      _r.fill(pts, phis, weights, kinds, parts);
      _b.fill(pts, phis, weights, kinds, parts);
#else
      for (auto& pp : p) {
	Real      eta  = std::get<0>(pp);
	Real      pt   = std::get<1>(pp);
	Real      phi  = std::get<2>(pp);
	Real      w    = std::get<3>(pp);
	Partition part = _p.partition(eta,pt,phi);
	Kind      kind = REF;
	_r.fill(pt, phi, w, kind, part);
	_b.fill(pt, phi, w, kind, part);
      }
#endif
    }
    /** Updates statistics */
    void post()
    {
      _r.update();
      _b.update();
    }
    /** Get state as JSON */
    virtual json::JSON toJson() const
    {
      json::JSON r = { "reference", _r.toJson(),
		       "bins",      _b.toJson() };
      return r;
    }
    /** Read state from JSON */
    virtual void fromJson(const json::JSON& json)
    {
      _r.fromJson(json["reference"]);
      _b.fromJson(json["bins"]);
    }
    /** Merge state from JSON */
    virtual void mergeJson(const json::JSON& json)
    {
      _r.mergeJson(json["reference"]);
      _b.mergeJson(json["bins"]);
    }
    /** Get results as JSON object */
    json::JSON result() const
    {
      json::JSON json = Base::result();
      json["results"] = _b.result();
      return json;
    }
    /** Reference bin */
    IB _r;
    /** Bins */
    XB _b;
    /** Partitioner */
    Partitioner& _p;
  };
}
#endif
//
// EOF
//
