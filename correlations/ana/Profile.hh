/**
 * @file   correlations/ana/Profile.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Profile (binned means of data)
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_PROFILE_HH
#define ANA_PROFILE_HH
#include <correlations/ana/Axis.hh>
#include <correlations/stat/WelfordVar.hh>

namespace ana
{
  //====================================================================
  /** 
   * A profile of means 
   */
  struct Profile
  {
    using Real           = gen::Generator::Real;

    /** 
     * Constructor 
     *
     * @param a   Axis to use 
     */
    Profile(const Axis& a)
      : _a(a),
	_s(_a.size()),
	_c(_a.size())
    {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */
    Profile(Profile&& o)
      : _a(o._a),
	_s(std::move(o._s)),
	_c(std::move(o._c))
    {}
    /** Deleted copy constructor */
    Profile(const Profile&) = delete;
    /** Deleted assignment operator */
    Profile& operator=(const Profile&) = delete;
    /** 
     * Get axis 
     *
     * @return Reference to axis 
     */
    const Axis& axis() const { return _a; }
    /** 
     * Clear cache 
     */
    void reset()
    {
      _c = 0;
    }
    /** 
     * Update cache with observation @a x 
     *
     * @param x Observation 
     */
    void fill(Real x) 
    {
      short b = _a.bin(x);
      if (b < 0) return;
      _c[b]++;
    }
    /** 
     * Update means of observations. Resets cache 
     */
    void update()
    {
      _s.fill(_c);
      _c = 0;
    }
    /** 
     * Get result - the means and uncertainties - as JSON
     *
     */
    json::JSON result() const
    {
      json::JSON json = {"bins", _a.boundaries(),
			 "means", _s.mean(),
			 "uncer", _s.sem() };
      return json;
    }
    /**
     * Dump state to JSON 
     *
     * @return JSON representation
     */
    json::JSON toJson() const
    {
      json::JSON json = { "profile", _s.toJson() };
      return json;
    }
    /**
     * Load state from JSON 
     *
     * @param json JSON representation
     */
    void fromJson(const json::JSON& json)
    {
      _s.fromJson(json["profile"]);
    }
    /** 
     * Merge from other profile 
     *
     * @param o Other profile 
     *
     * @return Reference to self 
     */
    Profile& merge(const Profile& o)
    {
      _s.merge(o._s);
      return *this;
    }
    /** 
     * Merge from JSON 
     *
     * @param json To merge from 
     */
    void mergeJson(const json::JSON& json)
    {
      _s.mergeJson(json["profile"]);
    }
  protected:
    /** Axis used */
    const Axis& _a;
    /** Statistics */
    stat::WelfordVar<Real> _s;
    /** Cache of count of X's */
    stat::Stat<Real>::RealVector _c;
  };
}
#endif
//
// EOF
//
