/**
 * @file   correlations/ana/XBinned.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A wrapper for differential measurement bins with explicit
 * independent variable value and width
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef XBINNED_HH
#define XBINNED_HH
#include <correlations/ana/XBin.hh>
#include <correlations/stat/WelfordVar.hh>

namespace ana
{
  /** 
   * Bins that wraps a flow measurement bin. 
   */
  template <typename BinType>
  struct XBinned
  {
    /** Bin type */
    using Bin=BinType;
    /** Wrapped bin type */
    using LHBin=LowHigh<Bin>;
    /** type of value */
    using Real=typename LHBin::Real;
    /** type of value vector */
    using RealVector=typename LHBin::RealVector;
    /** Kind of observation */
    using Kind=typename LHBin::Kind;
    /** Kinds of observation */
    using Kinds=typename LHBin::Kinds;
    /** Partition of observation */
    using Partition=typename LHBin::Partition;
    /** Partitions of observation */
    using Partitions=typename LHBin::Partitions;
    /** Mask array */
    using Mask=typename LHBin::Mask;
    /** 
     * Constructor 
     *
     * @param boundaries Bin boundaries 
     * @param kind The kind of observation to label observations with 
     * @param args additional arguments for contained bin constructors 
     */
    template <typename ...Args>
    XBinned(const RealVector& boundaries, Kind kind=0, Args&...args)
      : _bins(),
	_bounds(boundaries),
	_s(boundaries.size()-1,false)
    {
      for (size_t i = 0; i < boundaries.size()-1; i++)
	_bins.emplace_back(boundaries[i],boundaries[i+1],kind,args...);
    }
    /** 
     * Move constructor 
     */
    XBinned(XBinned&& o)
      : _bins(std::move(o._bins)),
	_bounds(std::move(o._bounds)),
	_s(std::move(o._s))
    {}
    /** 
     * Deleted copy constructor 
     */
    XBinned(const XBinned&) = delete;
    /** 
     * Deleted assignment operator 
     */
    XBinned& operator=(const XBinned&) = delete;
    /** Destructor */
    virtual ~XBinned() {}

    /** Reset cache of bins */
    virtual void reset()
    {
      for (auto& b : _bins) b.reset();
    }
    /** Update statistics of bins */
    virtual void update()
    {
      RealVector x(_s.size());
      for (size_t i = 0; i < _s.size(); i++) {
	x[i] = _bins[i].count();
	if (x[i] <= 0) continue;
	_bins[i].update();
      }
      _s.fill(x);
    }
    /** 
     * Fill in an observation.
     *
     * @param x      The independent variable value 
     * @param phi    The observed @f$\varphi@f$ angle 
     * @param weight The weight of the observation 
     * @param kind   Kind 
     * @param part   Partition
     */
    virtual void fill(Real       x,
		      Real       phi,
		      Real       weight,
		      Kind       kind=correlations::REF,
		      Partition  part=0)
    {
      for (auto& b : _bins) b.fill(x,phi,weight,kind,part);
    }
    /** 
     * Fill in observations.  
     *
     * @param x      The independent variable values 
     * @param phi    The observed @f$\varphi@f$ angle 
     * @param weight The weight of the observation 
     * @param kind   Kind 
     * @param part   Partition
     */
    virtual void fill(const RealVector& x,
		      const RealVector& phi,
		      const RealVector& weight,
		      const Kinds       kind=0,
		      const Partitions& part=0)
    {
      for (auto& b : _bins) b.fill(x,phi,weight,kind,part);
    }
    /** 
     * Get JSON representation
     */
    json::JSON toJson() const
    {
      json::JSON bins = json::Array();
      for (auto& b : _bins) bins.append(b.toJson());

      json::JSON json = { "bounds", _bounds,
			  "stat",   _s.toJson(),
			  "bins",   bins };
      return json;
      
    }
    /** Load state from JSON */
    void fromJson(const json::JSON& json)
    {
      json["bounds"].toArray(_bounds);
      _s.fromJson(json["stat"]);
      auto b = _bins.begin();
      for (auto& e : json["bins"].arrayRange()) {
	b->fromJson(e);
	b++;
      }
    }
    /** Merge state from JSON */
    void mergeJson(const json::JSON& json)
    {
      json["bounds"].toArray(_bounds);
      _s.mergeJson(json["stat"]);
      auto b = _bins.begin();
      for (auto& e : json["bins"].arrayRange()) {
	b->mergeJson(e);
	b++;
      }
    }
    /** Get result as JSON */
    json::JSON result() const
    {
      json::JSON values = json::Array();
      for (auto& b : _bins) values.append(b.result());
      json::JSON json = { "edges", _bounds,
			  "profile", { "mean", _s.mean(),
				       "uncer", _s.sem() },
			  "values", values };
      return json;
    }
  protected:
    /** Type of bin vector */
    using BinVector=std::vector<LHBin>;

    /** Bins */
    BinVector _bins;
    /** Boundaries */
    RealVector _bounds;
    /** Statistics */
    stat::WelfordVar<Real> _s;
  };
}
#endif
//
// EOF
//
