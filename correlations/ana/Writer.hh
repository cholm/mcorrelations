/**
 * @file   correlations/ana/Writer.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Write data
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_WRITER_HH
#define ANA_WRITER_HH
#include <correlations/gen/Header.hh>
#include <correlations/ana/Analyzer.hh>
#include <iostream>
#include <iomanip>

/** 
 * @defgroup ana Analysis tools 
 *
 * Various tools used to define some analysers 
 */
/** 
 * Namespace for analyses 
 *
 * @ingroup ana 
 */
namespace ana
{
  //====================================================================
  /** 
   * Write events to disk. 
   *
   * The data is written in the following format 
   *
   * @verbatim 
   HEADER_COMMENTS 
   HEADER 
   RECORDS_COMMENTS
   EVENT_RECORDS 
   @endverbatim 
   *
   * `HEADER_COMMENTS` and `RECORDS_COMMENTS` are 8 and 10 lines,
   * respectively, of comments - i.e., lines starting with `#`.
   *
   * `HEADER` is 5 lines of information, formatted as 
   *
   * @verbatim
   NUMBER_OF_EVENTS MIN_MULTIPLICITY MAX_MULTIPLICITY 
   FLOW_COEFFICENTS 
   PT_BINS 
   PT_COEFFICIENTS 
   MEAN_PT
   @endverbatim 
   * 
   * `NUMBER_OF_EVENTS`, `MIN_MULTIPLICITY`, and `MAX_MULTIPLICITY`
   * are integers that give the number of events, and least and
   * largest requested number of observations per event, respectively.
   *
   * `FLOW_COEFFICENTS`, `PT_BINS`, and `PT_COEFFICIENTS` are arrays
   * formatted as
   *
   * @verbatim 
   NUMBER_OF_ELEMENTS ELEMENTS
   @endverbatim 
   *
   * That is, and integer value followed by that many real numbers.
   * `FLOW_COEFFICIENTS` describes the simulated flow harmonic values
   * @f$ v_n@f$ that determine the probability density function of
   * @f$\varphi@f$ as
   *
   * @f[ f(\varphi) = 1+\sum_{n=1} v_n\cos(n\varphi)\quad.@f]
   *
   * `PT_BINS` says the range of transverse momentum values sampled
   * and the binning used for the sampling.
   *
   * `PT_COEFFICIENTS` describes the modulation of the @f$\varphi@f$
   * distribution according to the transverse momentum
   *
   * @f{eqnarray*}{
   g(p_{\\mathrm{T}}) &=& \sum_{i=1} a_i p_{\\mathrm{T}}^i\\
   f(\varphi) &\rightarrow& g(p_{\\mathrm{T}})f(\varphi)
   @f}
   *
   * where @f$ a_i@f$ are the coefficients. 
   *
   * Finally. `MEANPT` is the average transverse momentum used when
   * sampling the transverse momentum distribution.
   * 
   * The `EVENT_RECORDS` consist of `NUMBER_OF_EVENTS` individual
   * `EVENT_RECORDS`.  Each of these have the format
   *
   @verbatim 
   COUNT 
   COUNT_LINES_OF_OBSERVATIONS 
   @endverbatim 
   *
   * `COUNT_LINES_OF_OBSERVATIONS` are the observations where each of
   * the `COUNT` observations is a single line.  Each `OBSERVATION`
   * consists of
   *
   * @f[\eta,p_{\mathrm{T}},\varphi,w\quad,@f]
   *
   * formatted with spaces between i.e., 
   *
   * @verbatim
   ETA PT PHI W
   @endverbatim
   *
   * @ingroup ana 
   * @headerfile "" <correlations/ana/Writer.hh>
   */
  struct Writer : public Analyzer
  {
    /** Base type */
    using Base=Analyzer;
    /** Real type */
    using Real=typename Base::Real;
    /** Particle vector type */
    using ParticleVector=typename Base::ParticleVector;
    /** Header type */
    using Header=typename Base::Header;
    /** 
     * Constructor 
     *
     * @param out Output stream
     */
    Writer(std::ostream& out) : _out(out) {}
    /** 
     * Before an event.  If the header hasn't been output yet, output
     * it.
     */
    void pre()
    {
      if (_header_written) return;
      _header.write(_out);
      _header_written = true;
    }
    /** 
     * Process an event.  Event record is written to output. 
     *
     * An event record consists of 
     @verbatim 
     COUNT 
     COUNT_LINES_OF_OBSERVATIONS 
     @endverbatim 
     * 
     * Each observation consists of 
     *
     * @f[\eta,p_{\mathrm{T}},\varphi,w\quad,@f]
     *
     * formatted with spaces between i.e., 
     *
     * @verbatim
     ETA PT PHI W
     @endverbatim
     *
     * @param p Particles 
     */
    void event(const ParticleVector& p)
    {
      _out << p.size() << '\n';
      for (auto& pp : p) {
	_out << std::left 
	    << std::setw(18) << std::get<0>(pp) << " "
	    << std::setw(18) << std::get<1>(pp) << " "
	    << std::setw(18) << std::get<2>(pp) << " "
	    << std::setw(18) << std::get<3>(pp) << std::endl;
      }
    }
    /** Get results - returns null */
    json::JSON result() const { return json::JSON(); }
    /** Convert state to JSON - returns null */
    json::JSON toJson() const { return json::JSON(); }
    /** Reads state from JSON - nothing is read */
    void fromJson(const json::JSON&) {}
    /** Merge state from JSON - nothing is merged */
    void mergeJson(const json::JSON&) {}
    /** Whether the header as been output */
    bool _header_written = false;
    /** Output stream */
    std::ostream& _out;
  };
}
#endif
//
// EOF
//
