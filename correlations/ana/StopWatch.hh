/**
 * @file   correlations/ana/StopWatch.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A stopwatch 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_STOPWATCH_HH
#define ANA_STOPWATCH_HH
#include <chrono>
#include <tuple>

namespace ana
{
  /** 
   * A simple stop-watch 
   *
   * @ingroup ana
   */
  struct StopWatch
  {
    /** The type of clock */
    using Clock=std::chrono::system_clock;
    /** The type of time point */
    using Time=typename Clock::time_point;
    /** the type of parts */
    using Parts=std::tuple<size_t,size_t,double>;
    /** Constructor. Sets start time */
    StopWatch() : _start(Clock::now()) {}
    /** Destructor */
    ~StopWatch() {}
    /** Reset stopwatch.  Sets start time to now and returns
	previously elapsed time */
    double reset()
    {
      Time stop = Clock::now();
      std::chrono::duration<double> elapsed = stop-_start;
      _start = stop;
      return elapsed.count();
    }
    /** Do a split time */
    double split() const
    {
      Time stop = Clock::now();
      std::chrono::duration<double> elapsed = stop-_start;
      return elapsed.count();
    }
    /** Get the various parts (hours, minutes, seconds) from the
	passed time */
    Parts parts(double elap) const
    {
      size_t nn   = size_t(elap);
      size_t hh   = nn / 3600;
      size_t mm   = (nn - 3600 * hh) / 60;
      double ss   = (elap - 3600 * hh - 60 * mm);
      return std::make_tuple(hh,mm,ss);
    }
    /** Start time */
    Time _start;
  };
}
#endif
//
// EOF
//

