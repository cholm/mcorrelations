/**
 * @file   correlations/ana/Analyzer.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief An analyzer 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef ANA_ANALYZER_HH
#define ANA_ANALYZER_HH
#include <correlations/ana/Profile.hh>
#include <correlations/ana/Reader.hh>
#include <correlations/gen/Generator.hh>
#include <correlations/ana/Progress.hh>

namespace ana
{
  //====================================================================
  /** 
   * Base class for analysers 
   *
   * @ingroup ana 
   * @headerfile "" <correlations/progs/ana.hh>
   */
  struct Analyzer
  {
    using Real           = gen::Generator::Real;
    using EtaPtPhiW      = gen::Generator::EtaPtPhiW;
    using ParticleVector = gen::Generator::ParticleVector;
    using Header         = gen::Header;

    /** 
     * Before event processing 
     */
    virtual void pre() {}
    /** 
     * Analyze a single event 
     *
     * @param p Particle data 
     */
    virtual void event(const ParticleVector& p) = 0;
    /** 
     * After event processing 
     */
    virtual void post() {}
    /** 
     * Finalize the analysis 
     *
     * @param fini finalize
     */
    virtual json::JSON end(bool fini=true)
    {
      if (!fini) return toJson();

      return result();
    }

    /** 
     * Run the test using in-memory generation of particle data 
     * 
     * @param nev  Number of events
     * @param g    Generator of events 
     * @param fini If true, finalize, otherwise dump 
     */
    virtual json::JSON run(gen::Generator& g,
			    size_t          nev,
			    bool            fini=true)
    {
      _header = g.header(nev);
      
      ParticleVector p;
      Progress       pm(std::cerr,nev);
      for (size_t i = 0; i < nev; i++) {
	pm.print(i+1);
	g(p);
	pre();
	event(p);
	post();
      }
    
      return end(fini);
    }

    /** 
     * Run the test reading data from a stream 
     *
     * @param in     Input stream
     * @param maxEv  Maximum number of events (if 0, read all)
     * @param read   If true, load state 
     * @param fini   If true, finalize, otherwise dump 
     */
    virtual json::JSON run(std::istream& in,
			   size_t        maxEv=0,
			   bool          read=false,
			   bool          fini=true)
    {
      if (!in.good())
	throw std::runtime_error("Bad input stream for reading data");

      if (read) {
	merge(in);
	return end(fini);
      }

      _header      = Header::read(in);
      size_t  nev  = _header._nev;
      maxEv        = maxEv > 0 ? std::min(maxEv,nev) : nev;
      _header._nev = maxEv;
	
      Reader         r;
      ParticleVector p;
      size_t         i = 0;
      Progress       pm(std::cerr,maxEv);
      while (r.event(in,p) > 0 && (maxEv == 0 || i < maxEv)) {
	pm.print(i+1);
	pre();
	event(p);
	post();
	i++;
      }
      return end(fini);
    }

    /** 
     * Run the test reading data from a file 
     *
     * @param filename The name of the file to read from 
     * @param maxEv  Maximum number of events to read (if 0 read all)
     * @param read   If true, load state 
     * @param fini   If true, finalize, otherwise dump 
     */
    json::JSON run(const std::string& filename,
		   size_t             maxEv=0,
		   bool               read=false,
		   bool               fini=true)
    {
      if (filename.empty())
	throw std::runtime_error("No input file specified");
      
      std::ifstream in(filename);
      auto json = run(in, maxEv, read, fini);
      in.close();
      return json;
    }  
    /** Destructor */
    virtual ~Analyzer() = default;
    /** Copy constructor */
    Analyzer(const Analyzer&) = delete;
    /** Assignment operator */
    Analyzer& operator=(const Analyzer&) = delete;
  protected:
    /** 
     * Constructor 
     */
    Analyzer()
      : _header()
    {}
    /** 
     * Move copy constructor 
     *
     * @param o Object to move from 
     */
    Analyzer(Analyzer&& o)
      : _header(std::move(o._header))
    {}
    /** 
     * Get results.  Default implementation returns the header only
     *
     */
    virtual json::JSON result() const
    {
      json::JSON json = { "header",  _header.toJson() };
      return json;
    }
    /** 
     * Other stuff to add 
     */
    virtual json::JSON other() const { return json::JSON(); }
    /** 
     * Get state as JSON 
     */
    virtual json::JSON toJson() const
    {
      json::JSON json = { "header",   _header.toJson(),
			  "analyzer", toJson() };
      return json;
    }
    /** 
     * Decode state from JSON
     */
    virtual void fromJson(const json::JSON& json)
    {
      _header.fromJson(json["header"]);
    }
    /** 
     * Merge state from JSON 
     */
    virtual void mergeJson(const json::JSON& json)
    {
      _header.mergeJson(json["header"]);
    }
    /** 
     * Merge from other object 
     */
    virtual void merge(const Analyzer& o)
    {
      _header.merge(o._header);
    }
    /** 
     * Merge states from stream 
     */
    void merge(std::istream& in)
    {
      size_t cnt = 0;
      while (!in.eof()) {
	json::JSON json;
	in >> json;

	mergeJson(json);
	cnt++;
      }
    }
    /** Header */
    gen::Header _header;
  };
}
#endif
//
// EOF
//
#if 0

std::cout << "f(42) = " << fibonacci(42) << '\n';

std::time_t end_time = std::chrono::system_clock::to_time_t(end);

std::cout << "finished computation at " << std::ctime(&end_time)
<< "elapsed time: " << elapsed_seconds.count() << "s\n";
#endif
