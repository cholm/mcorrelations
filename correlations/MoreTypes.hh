#ifndef CORRELATIONS_MORETYPES_HH
#define CORRELATIONS_MORETYPES_HH
/**
 * @file   correlations/MoreTypes.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Declarations of more types
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <vector>
#include <valarray>
#include <algorithm>
#include <type_traits>
#include <iostream>
#include <iterator>

namespace correlations
{
  /** Type of real array 
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  using RealArray=std::vector<Real>;

  /** Type of real array 
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  using RealVector=std::valarray<Real>;
  
  /** Type definition of our internal storage 
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  // typedef std::vector<Complex> ComplexVector;
  using ComplexVector=std::valarray<Complex>;

  /** Type of Sizes 
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  using Size=unsigned short;

  /** Type of Powers 
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  using Power=unsigned short;

  /**
   * Type definition of vector of harmonics
   *
   * @code
   * correlations::Result  r;
   * correlations::HarmonicVector h(n);
   * for (size_t i = 0; i < n; i++) h[i] = ...;
   * correlations::Correlator c = ...;n
   *
   * while (moreEvents) {
   *
   *   while (moreObservations) {
   *     ...
   *   }
   *
   *   r += c.calculate(h);
   * }
   * std::cout << r.eval() << std::endl;
   * @endcode
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  using HarmonicVector=std::valarray<Harmonic>;

  /** Vector of partition identifiers. 
   *
   * Suppose we want to calculate the 6-particle correlator with harmonics 
   *
   * @f[ \mathbf{h} = \{h_1,h_2,h_3,h_4,h_5,h_6\}\quad,@f] 
   *
   * and we want to do this in two sub-events corresponding to 
   *
   * @f[ \mathbf{h}_L=\{h_1,h_2,h_3\}\quad\mathbf{h}_R=\{h_4,h_5,h_6\}@f]
   * 
   * Then, we'd construct the partition vector 
   *
   * @f[ \mathbf{p} = \{1,1,1,2,2,2\}\quad,@f]
   *
   * and pass that along with the vector @f$ \mathbf{h}@f$ to the
   * calculate method.
   */
  using Partitions=std::valarray<Partition>;

  /** Kinds of observations */
  enum { NONE=0x0, REF=0x1, POI=0x2 };

  /** Type of kind of observations */
  using Kind=unsigned short;
  
  /** 
   * Vector of class of observation identifiers 
   */
  using Kinds=std::valarray<unsigned short>;
  
  /** 
   * Vector of harmonics 
   *
   *
   * @headerfile "" <correlations/Types.hh>
   * @ingroup correlations_types 
   */
  using PartitionHarmonicVector=std::valarray<PartitionHarmonic>;

  /** 
   * Make a vector of partition and harmonics 
   *
   * @param h Initializer list of harmonics 
   * @param p Initializer list of partitions 
   */
  PartitionHarmonicVector makeHV(std::initializer_list<Harmonic> h,
				 std::initializer_list<Partition> p)
  {
    PartitionHarmonicVector r(h.size());
    auto hb = h.begin();
    auto pb = p.begin();
    for (size_t i = 0; i < h.size(); i++, ++hb, ++pb) 
      r[i] = PartitionHarmonic(*hb, *pb);
    return r;
  }
  /** 
   * Make a vector of partition and zero harmonics 
   *
   * @param v Vector to base it on
   */
  PartitionHarmonicVector zeroHV(const PartitionHarmonicVector& v)
  {
    PartitionHarmonicVector r(v.size());
    for (size_t i = 0; i < v.size(); i++) 
      r[i] = PartitionHarmonic(0, v[i].partition());
    return r;
  }
  /** 
   * Make a vector of partition and zero harmonics 
   *
   * @param v Vector to base it on
   */
  HarmonicVector zeroHV(const HarmonicVector& v)
  {
    HarmonicVector r(v.size());
    std::fill(std::begin(r),std::end(r),Harmonic());
    return r;
  }
}

namespace {
  template <typename Container>
  struct _Cwrap
  {
    _Cwrap(const Container& c, char sep=',') : _c(c), _s(sep) {}
    const Container& _c;
    char _s;
  };
}
namespace correlations
{
  template <typename T>
  _Cwrap<T> o(const T& c,char sep=',')
  {
    return _Cwrap<T>(c,sep);
  }
}

namespace std
{
  template <typename T>
  ostream& operator<<(ostream& o, const _Cwrap<T>& c)
  {
    using std::begin;
    using std::end;
    
    o << "[";
    for (auto& e : c._c) o << e << c._s;
    o << "]";
    return o;
  }
  
}  
#endif
//
// EOF
//

