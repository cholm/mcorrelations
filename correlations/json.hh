#ifndef JSON_HH
#define JSON_HH
#include <cmath>
#include <string>
#include <deque>
#include <map>
#include <initializer_list>
#include <iostream>
#include <sstream>
#include <cassert>
#include <valarray>

/**
 * @page usage Use of C++ JSON library 
 *
 * Heavily inspired by https://github.com/nbsdx/SimpleJSON.git 
 *
 * @section basics A basic example of usage.  
 *
 * Here we create an object of each possible kind.
 *
 * @dontinclude examples.cc 
 * @skip basic()
 * @until json::Object
 *
 * The type of an object is not fixed.  By assigning a different
 * value the type is changed.
 *
 * @until b = std::string
 *
 * We can easily append elements to an array 
 *
 * @until a.append(false)
 *
 * Elements of an array are accessed by index 
 *
 * @until a[0]
 *
 * Initializing an array directly requires using json::Array 
 *
 * @until json::Array
 *
 * Object properties are assigned by string keys 
 *
 * @until a2
 *
 * Objects can be nested within other objects 
 *
 * @until o2
 *
 * Finally, we can print out any JSON object 
 *
 * @until } 
 *
 * @section to Extracting value 
 *
 * We can use member functions to extract values of basic types (@c
 * bool, @c string, @c float, and @c int)
 *
 * @until }
 *
 * @section init Fany initialisation 
 *
 * We can initialize and object using _almost_ JSON syntax.  Note, however, that we must use @c , as key-value separator in objects, and arrays must explicity use json::Array. 
 *
 * @until // end_init 
 *
 * @section iter Iteration over arrays and objects 
 *
 * We can iterate over array elements via json::JSON::arrayRange and
 * objects via json::JSON::objectRange.
 *
 * @until }
 *
 * @section array Array assignment 
 *
 * Assigning to an object using index keys turns the object into an
 * array.  The array is padded up to the assigned index (but not
 * truncated).  Sub-indexes are also padded as necessary (with `null`
 * values).
 *
 * @until } 
 *
 * @section load Loading from string or stream
 *
 * JSON objects can be loaded from a string or stream. 
 *
 * @until } // end_load
 */

/** 
 * Namespac for simple JSON library 
 */
namespace json
{

  namespace
  {
    /** 
     * @{ 
     * @name Short-hands 
     */
    template <typename T>  using is_bool  = std::is_same<T,bool>;
    template <typename T>  using is_float = std::is_floating_point<T>;
    template <typename T>  using is_string= std::is_convertible<T,std::string>;
    template <typename T>  using is_int   = std::is_integral<T>;
    template <typename...> using void_tt  = void;
    /* @} */
    /** 
      * @{ 
      * @name other type deductin 
      */
    /** 
     * Detect if a type is iterable 
     */
    template <typename T, typename = void>
    struct is_iterable : std::false_type {};

    /**
     * Detect if a type is iterable 
     */
    template <typename T>
    struct is_iterable<T, void_tt<decltype(std::begin(std::declval<T>())),
				  decltype(std::end(std::declval<T>()))>>
       : std::true_type {};

    /** Detect if a type is an iterator */
    template<typename T, typename = void>
    struct is_iterator : std::false_type {};

    template<typename T>
    struct is_iterator<T,
		       typename std::enable_if<!std::is_same<typename
							     std::iterator_traits<T>::value_type, void>::value>::type> : std::true_type {};
    /** 
     * Detect if type has @c key_type sub-type 
     */
    template<typename, typename = void_tt<> >
    struct has_key_type : std::false_type { };
 
    /** 
     * Detect if type has @c key_type sub-type 
     */
    template<typename T>
    struct has_key_type<T, void_tt<typename T::key_type>>
      : std::true_type { };

    
    /* @} */

    /** Helper to turn character into string */
    std::string ctos(char c)
    {
      std::stringstream s;
      s << c;
      return s.str();
    }
    /** 
     * Escape special characters in a string 
     *
     * @param str String to escape in 
     *
     * @return String with special characters escaped 
     */
    std::string escape(const std::string &str)
    {
      std::string output;
      for( unsigned i = 0; i < str.length(); ++i )
	switch (str[i]) {
	case '\"': output += "\\\""; break;
	case '\\': output += "\\\\"; break;
	case '\b': output += "\\b";  break;
	case '\f': output += "\\f";  break;
	case '\n': output += "\\n";  break;
	case '\r': output += "\\r";  break;
	case '\t': output += "\\t";  break;
	default  : output += str[i]; break;
	}
      return output;
    }
  }

  /** 
   * Any object in the JSON tree.
   *
   * @see usage 
   */
  class JSON
  {
  public:
    /** The type object */
    enum class Class {
		      NONE,
		      OBJECT,
		      ARRAY,
		      STRING,
		      FLOATING,
		      INTEGRAL,
		      BOOLEAN
    };
    using Mapping=std::map<std::string,JSON>;
    using Array=std::deque<JSON>;

    /** 
     * @{ 
     * @name Options 
     */
    size_t default_tab = 2;
    bool   nonfinite_is_null = false;
    /* @} */
  private:
    /** 
     * The actual data stored in a union 
     */
    union Store {
      /** Construct from double */
      Store(double d) : _float(d){}
      /** Construct from integer */
      Store(long   l) : _int(l){}
      /** Construct from boolean */
      Store(bool   b) : _bool(b){}
      /** Construct from string */
      Store(std::string s) : _string(new std::string(s)) {}
      /** Construct from nothing */
      Store()           : _int(0) {}

      /** When an array */
      Array* _list;
      /** When a mapping (object) */
      Mapping* _map;
      /** When a string */
      std::string* _string;
      /** When an double */
      double _float;
      /** When an integer */
      long _int;
      /** When a boolean */
      bool _bool;

      /** Reset data */
      void clear(Class type)
      {
	/* beware: only call if YOU know that _internal is
	   allocated. No checks performed here.  This function should be
	   called in a constructed JSON just before you are going to
	   overwrite _internal...
	*/
	switch(type) {
	case Class::OBJECT: delete _map;    _map    = nullptr; break;
	case Class::ARRAY:  delete _list;   _list   = nullptr; break;
	case Class::STRING: delete _string; _string = nullptr; break;
	default:;
	}
      }
    } _internal;
    /** Type type of object */
    Class _type = Class::NONE;
  public:
    /** A wrapper around a container */
    template <typename Container>
    class Wrapper {
    private:
      /** The container */
      Container* _object;
    public:
      using const_iterator=typename Container::const_iterator;
      using iterator=typename Container::iterator;

      /** Constructor. Wraps value container */
      Wrapper(Container *val) : _object(val) {}
      /** Constructor. Wraps null container */
      Wrapper(std::nullptr_t)  : _object(nullptr) {}

      /**
       * Get iterator point to beginning of container 
       */
      iterator begin() { return _object ? _object->begin() : iterator(); }
      /** 
       * Get iterator pointing just beyond end of container 
       */
      iterator end() { return _object ? _object->end() : iterator(); }
      /**
       * Get iterator point to beginning of container 
       */
      const_iterator begin() const
      {
	return _object ? _object->begin() : iterator();
      }
      /** 
       * Get iterator pointing just beyond end of container 
       */
      const_iterator end() const
      {
	return _object ? _object->end() : iterator();
      }
    };
 
    /** A wrapper around a container */
    template <typename Container>
    class ConstWrapper {
    private:
      /** Wrapped container */
      const Container* _object;

    public:
      using const_iterator=typename Container::const_iterator;

      /** Constant wrapper around container value */
      ConstWrapper(const Container *val) : _object(val) {}
      /** Constant wrapper around null container */
      ConstWrapper(std::nullptr_t)  : _object(nullptr) {}
      /**
       * Get iterator point to beginning of container 
       */
      const_iterator begin() const
      {
	return _object ? _object->begin() : const_iterator();
      }
      /** 
       * Get iterator pointing just beyond end of container 
       */
      const_iterator end() const
      {
	return _object ? _object->end() : const_iterator();
      }
    };

    /** Constructor - empty object */
    JSON() : _internal(), _type(Class::NONE) {}
    /** Construct from initializer list */
    JSON(std::initializer_list<JSON> list) 
      : JSON() 
    {
      setType(Class::OBJECT);
      for(auto i = list.begin(); i != list.end(); ++i, ++i ) 
	operator[](i->toString()) = *std::next(i);
      
    }

#if 0
    /** Construct from initializer list */
    JSON(std::initializer_list<std::pair<std::string,JSON>> list) 
      : JSON() 
    {
      setType(Class::OBJECT);
      for( auto i = list.begin(), e = list.end(); i != e; ++i) 
	operator[](i->first) = i->second;
      
    }
#endif
    
    /** Move constructor from other object */
    JSON(JSON&& other)
      : _internal( other._internal )
      , _type(other._type)
    {
      other._type          = Class::NONE;
      other._internal._map = nullptr;
    }

    /** Copy constructor */
    JSON(const JSON &other)
      : JSON()
    {
      switch( other._type ) {
      case Class::OBJECT:
	_internal._map = 
	  new Mapping(other._internal._map->begin(),
		      other._internal._map->end());
	break;
      case Class::ARRAY:
	_internal._list = 
	  new Array(other._internal._list->begin(),
		    other._internal._list->end() );
	break;
      case Class::STRING:
	_internal._string = new std::string(*other._internal._string);
	break;
      default:
	_internal = other._internal;
      }
      _type = other._type;
    }
    /** Move assignment operator */
    JSON& operator=(JSON&& other)
    {
      _internal.clear(_type);
      _internal            = other._internal;
      _type                = other._type;
      other._internal._map = nullptr;
      other._type          = Class::NONE;
      return *this;
    }
    /** Assignment operator */
    JSON& operator=(const JSON &other)
    {
      _internal.clear(_type);
      switch(other._type) {
      case Class::OBJECT:
	_internal._map = 
	  new Mapping(other._internal._map->begin(),
		      other._internal._map->end());
	break;
      case Class::ARRAY:
	_internal._list = 
	  new Array(other._internal._list->begin(),
		    other._internal._list->end());
	break;
      case Class::STRING:
	_internal._string = new std::string(*other._internal._string);
	break;
      default:
	_internal = other._internal;
      }
      _type = other._type;
      return *this;
    }
    /** Destructor */
    ~JSON()
    {
      _internal.clear(_type);
    }

    /** 
     * SFINAE constructor from boolean type 
     * @param b Boolean value
     */
    template <typename T,
	      typename std::enable_if<is_bool<T>::value,int>::type = 0>
    JSON(T b)
      : _internal(b), _type(Class::BOOLEAN)
    {}

    /** 
     * SFINAE constructor from integer type 
     *
     * @param i Integer value 
     */
    template <typename T,
	      typename std::enable_if<is_int<T>::value &&
				      !is_bool<T>::value,int>::type = 0>
    JSON(T i)
      : _internal(long(i)), _type(Class::INTEGRAL)
    {}
    
    /** 
     * SFINAE constructor from float type 
     *
     * @param f Floating point value
     */
    template <typename T,
	      typename std::enable_if<is_float<T>::value,int>::type = 0>
    JSON(T f)
      : _internal(double(f)), _type(Class::FLOATING)
    {}

    /** 
     * SFINAE constructor from string type 
     *
     * @param s String value 
     */
    template <typename T,
	      typename std::enable_if<is_string<T>::value,int>::type = 0>
    JSON(T s)
      : _internal(std::string(s)), _type(Class::STRING){}

    /** 
     * SFINAE constructor from iterable
     *
     * @param c An iterable 
     */
    template <typename T,
	      typename std::enable_if<is_iterable<T>::value &&
				      !has_key_type<T>::value &&
				      !is_string<T>::value,int>::type = 0>
    JSON(T c)
      : _internal(), _type(Class::NONE)
    {
      auto b = std::begin(c);
      auto e = std::end(c);
      setType(Class::ARRAY,std::distance(b,e));
      for (auto i = b; i != e; ++i)
	this->operator[](std::distance(b,i)) = *i;
    }
    
    /** SFINAE assignment operator from map iterable */
    template <typename T,
	      typename std::enable_if<is_iterable<T>::value &&
				      has_key_type<T>::value &&
				      is_string<typename T::key_type>::value &&
				      !is_string<T>::value,int>::type = 0>
    JSON(T m)
      : _internal(), _type(Class::NONE)
    {
      setType(Class::OBJECT);

      for (auto& kv : m) 
	this->operator[](kv.first) = kv.second;
    }
    /** 
     * Constructor from null 
     *
     */
    JSON(std::nullptr_t) : _internal(), _type(Class::NONE){}

    /** Make an object of a specific type */
    static JSON make(Class type, size_t n=0)
    {
      JSON ret;
      ret.setType(type,n);
      return ret;
    }

    /** 
     * @{ 
     * @name Queries 
     */
    /** 
     * Get length of object (if array), -1 otherwise 
     *
     * @returns number of elements 
     */
    int length() const
    {
      return _type == Class::ARRAY ? _internal._list->size() : -1;
    }

    /** 
     * Check if object has a key (if an object) 
     *
     * @param key Key to check for 
     *
     * @return true if object contains key 
     */
    bool hasKey(const std::string& key) const
    {
      if (_type != Class::OBJECT)
	return false;
      return
	_type == Class::OBJECT ? 
	_internal._map->find( key ) != _internal._map->end() :
	false;
    }

    /** 
     * Get size (in elements) of this if an array or object 
     *
     * @return Number of elements or values, or -1
     */
    int size() const
    {
      return
	_type == Class::OBJECT ?  _internal._map->size() :
	_type == Class::ARRAY  ?  _internal._list->size() : 
	-1;
    }

    /** 
     * Get the type 
     *
     * @param type specifier 
     */
    Class type() const { return _type; }
    /* @} */

    /** 
     * @{ 
     * @name Assigning values 
     */
    /** Append an object */
    template <typename T>
    JSON& append(T arg)
    {
      setType(Class::ARRAY);
      _internal._list->emplace_back(arg);
      return *this;
    }

    /** Append objects - variadic, using tail recursion to set elements */
    template <typename T, typename... U>
    JSON& append(T arg, U... args)
    {
      append(arg);
      return append(args...);
    }

    /** SFINAE assignment operator from boolean */
    template <typename T,
	      typename std::enable_if<is_bool<T>::value,int>::type = 0>
    JSON& operator=(T b)
    {
      setType(Class::BOOLEAN);
      _internal._bool = b;
      return *this;
    }

    /** SFINAE assignment operator from integer */
    template <typename T,
	      typename std::enable_if<is_int<T>::value &&
				      !is_bool<T>::value,int>::type = 0>
    JSON& operator=(T i)
    {
      setType(Class::INTEGRAL);
      _internal._int = i;
      return *this;
    }

    /** SFINAE assignment operator from float */
    template <typename T,
	      typename std::enable_if<is_float<T>::value,int>::type = 0>
    JSON& operator=(T f)
    {
      setType(Class::FLOATING);
      _internal._float = f;
      return *this;
    }

    /** SFINAE assignment operator from string */
    template <typename T,
	      typename std::enable_if<is_string<T>::value,int>::type = 0>
    JSON& operator=(T s)
    {
      setType(Class::STRING);
      *_internal._string = std::string(s);
      return *this;
    }

    /** SFINAE assignment operator from iterable */
    template <typename T,
	      typename std::enable_if<is_iterable<T>::value &&
				      !has_key_type<T>::value &&
				      !is_string<T>::value,int>::type = 0>
    JSON& operator=(T c)
    {
      auto b = std::begin(c);
      auto e = std::end(c);
      
      setType(Class::ARRAY,std::distance(b,e));
	
      for (auto i = b; i != e; ++i)
	this->operator[](std::distance(b,i)) = *i;
      return *this;
    }

    /** SFINAE assignment operator from map iterable */
    template <typename T,
	      typename std::enable_if<is_iterable<T>::value &&
				      has_key_type<T>::value &&
				      is_string<typename T::key_type>::value &&
				      !is_string<T>::value,int>::type = 0>
    JSON& operator=(T m)
    {
      setType(Class::OBJECT);

      for (auto& kv : m) 
	this->operator[](kv.first) = kv.second;

      return *this;
    }
    /* @} */

    /** 
     * @{
     * @name Accessing value 
     */
    /** 
     * Value access with key
     *
     * @param key Key 
     *
     * @returns Value 
     */
    JSON& operator[](const std::string& key)
    {
      setType( Class::OBJECT );
      return _internal._map->operator[](key);
    }
    /** 
     * Value access with key
     *
     * @param key Key 
     *
     * @returns Value 
     */
    const JSON& operator[](const std::string& key) const
    {
      if (!hasKey(key)) return null();
      return _internal._map->operator[](key);
    }

    /** 
     * Element access with integer index 
     *
     * @param index Index 
     *
     * @returns Element 
     */
    JSON& operator[](unsigned index)
    {
      setType(Class::ARRAY);
      if (index >= _internal._list->size())
	_internal._list->resize(index + 1);
      return _internal._list->operator[](index);
    }
    /** 
     * Element access with integer index 
     *
     * @param index Index 
     *
     * @returns Element 
     */
    const JSON& operator[](unsigned index) const
    {
      if (index >= _internal._list->size()) null();
      return _internal._list->operator[](index);
    }

    /** 
     * Value access with key
     *
     * @param key Key 
     *
     * @returns Value 
     */
    JSON& at(const std::string& key) { return operator[](key); }

    /** 
     * Value access with key
     *
     * @param key Key 
     *
     * @returns Value 
     */
    const JSON &at(const std::string& key) const
    {
      return _internal._map->at(key);
    }

    /** 
     * Element access with integer index 
     *
     * @param index Index 
     *
     * @returns Element 
     */
    JSON& at(unsigned index) { return operator[](index); }

    /** 
     * Element access with integer index 
     *
     * @param index Index 
     *
     * @returns Element 
     */
    const JSON& at(unsigned index) const { return _internal._list->at(index); }


    /**
     * Check if this is a null object
     */
    bool isNull() const { return _type == Class::NONE; }

    /** 
     * Access as a string 
     *
     * @returns requested value 
     */
    std::string toString() const { bool b; return std::move(toString(b)); }
    /** 
     * Access as string, with check 
     *
     * @param ok true on return if access is good 
     * 
     * @return as requested type
     */
    std::string toString( bool &ok ) const
    {
      ok = (_type == Class::STRING);
      return ok ? std::move(escape(*_internal._string)): std::string("");
    }
    /** 
     * Access as a float 
     *
     * @returns requested value 
     */
    double toFloat() const { bool b; return toFloat(b); }
    /** 
     * Access as floating point, with check 
     *
     * @param ok true on return if access is good 
     * 
     * @return as requested type
     */
    double toFloat( bool &ok ) const
    {
      ok = (_type == Class::FLOATING);
      return ok ? _internal._float : 0.0;
    }
    /** 
     * Access as integer 
     *
     * @returns requested value 
     */
    long toInt() const { bool b; return toInt(b); }
    /** 
     * Access as integer, with check 
     *
     * @param ok true on return if access is good 
     * 
     * @return as requested type
     */
    long toInt(bool &ok) const
    {
      ok = (_type == Class::INTEGRAL);
      return ok ? _internal._int : 0;
    }
    /** 
     * Access as boolean
     *
     * @returns requested value 
     */
    bool toBool() const { bool b; return toBool( b ); }
    /** 
     * Access as boolean, with check 
     *
     * @param ok true on return if access is good 
     * 
     * @return as requested type
     */
    bool toBool(bool &ok) const
    {
      ok = (_type == Class::BOOLEAN);
      return ok ? _internal._bool : false;
    }

    /**
     * Extract values into an array 
     */
    template <typename C,
	      typename std::enable_if<is_iterable<C>::value &&
				      is_float<typename C::value_type>::value,
				      int>::type = 0>
    void toArray(C& c) const
    {
      int n = length();
      if (n <= 0) return;

      c.resize(n);
      size_t j = 0;
      for (auto& i : arrayRange())
	c[j++] = i.toFloat();
    }      
    /**
     * Extract values into an array 
     */
    template <typename C,
	      typename std::enable_if<is_iterable<C>::value &&
				      is_int<typename C::value_type>::value,
				      int>::type = 0>
    void toArray(C& c) const
    {
      int n = length();
      if (n <= 0) return;

      c.resize(n);
      size_t j = 0;
      for (auto& i : arrayRange())
	c[j++] = i.toInt();
    }      
    /**
     * Extract values into an array 
     */
    template <typename C,
	      typename std::enable_if<is_iterable<C>::value &&
				      is_string<typename C::value_type>::value,
				      int>::type = 0>
    void toArray(C& c) const
    {
      int n = length();
      if (n <= 0) return;

      c.resize(n);
      size_t j = 0;
      for (auto& i : arrayRange())
	c[j++] = i.toString();
    }      
      
    /* @} */

    /** 
     * @{ 
     * @name Iteration 
     */
    /** 
     * Get view of an object
     *
     * @returns a container wrapper that allows iteration over object
     * properties. .
     */
    Wrapper<Mapping> objectRange()
    {
      if (_type == Class::OBJECT)
	return Wrapper<Mapping>(_internal._map);
      return Wrapper<Mapping>(nullptr);
    }
    /** 
     * Get view of an object
     *
     * @returns a container wrapper that allows iteration over object
     * properties. .
     */
    ConstWrapper<Mapping> objectRange() const
    {
      if(_type == Class::OBJECT)
	return ConstWrapper<Mapping>(_internal._map);
      return ConstWrapper<Mapping>(nullptr);
    }
    /** 
     * Get view of an array 
     *
     * @returns a container wrapper that allows iteration over array
     * elements.
     */
    Wrapper<Array> arrayRange()
    {
      if (_type == Class::ARRAY)
	return Wrapper<Array>(_internal._list);
      return Wrapper<Array>(nullptr);
    }
    /** 
     * Get view of an array 
     *
     * @returns a container wrapper that allows iteration over array
     * elements.
     */
    ConstWrapper<Array> arrayRange() const
    { 
      if (_type == Class::ARRAY)
	return ConstWrapper<Array>(_internal._list);
      return ConstWrapper<Array>(nullptr);
    }
    /* @} */

    /** 
     * @{ 
     * @name I/O
     */
    /** 
     * Read object (and nested objects) from stream 
     *
     * @param in Input stream to read from 
     *
     * @return @a in 
     */
    std::istream& read(std::istream& in)
    {
      read_ws(in);
      if (in.eof()) return in;

      char value = in.peek();
      switch (value) {
      case '[' : return read_array (in);
      case '{' : return read_object(in);
      case '\"': return read_string(in);
      case 't' :
      case 'f' : return read_bool(in);
      case 'n' : return read_null(in);
      default  : if ((value <= '9' && value >= '0')
		     || value == 'N'
		     || value == 'I'
		     || value == '-')
	  return read_number(in);
      }
      throw std::runtime_error("Unknown starting character: '"
			       +ctos(value)+"' ("
			       +std::to_string(int(value))+")");;
      return in;
    }
    /** 
     * Write object (and nested objects) to stream 
     *
     * @param out Output stream 
     * @param cur Current indent 
     * @param tab Indention step
     *
     * @return @a out 
     */
    std::ostream& write(std::ostream& out,
			const std::string& cur="",
			const std::string& tab="  ") const
    {
      switch(_type) {
      case Class::BOOLEAN:  return write_bool(out);
      case Class::INTEGRAL: return write_int(out);
      case Class::FLOATING: return write_float(out);
      case Class::STRING:   return write_string(out);
      case Class::ARRAY:    return write_array(out, cur+tab);
      case Class::OBJECT:   return write_object(out, cur+tab);
      case Class::NONE:
	break;
      }
      return out << "null";
    }
    /** 
     * Load an object from stream 
     *
     * @param in Stream to read from 
     *
     * @return Read JSON object. 
     */
    static JSON load(std::istream& in)
    {
      JSON j;
      j.read(in);
      return j;
    }
    /** 
     * Load an object from string
     *
     * @param str String to read from 
     *
     * @return Read JSON object. 
     */
    static JSON load(const std::string& str)
    {
      std::stringstream s(str);
      return load(s);
    }
    /** 
     * Dump a JSON object to stream 
     *
     * @param o   Stream to write to
     * @param tab Indention per level 
     */
    void dump(std::ostream& o, const std::string& tab="  ")
    {
      write(o,"",tab);
    }
    /** 
     * Dump a JSON object to a string
     *
     * @param tab Indention per level 
     *
     * @return String of JSON object
     */
    std::string dump(const std::string& tab="  ")
    {
      std::stringstream s;
      dump(s,tab);
      return s.str();
    }
    /* @} */
  private:
    /** 
     * Set type of object 
     *
     * @param type Type to set to.  Note, if the object is currently
     * not if this type, then this operation will clear it's content.
     * @param n If initialize to an array, reserve this many places 
     *
     */
    void setType(Class type,size_t n=0)
    {
      if (type == _type) return;

      _internal.clear(_type);
          
      switch (type) {
      case Class::NONE:      _internal._map    = nullptr;            break;
      case Class::OBJECT:    _internal._map    = new Mapping();      break;
      case Class::ARRAY:     _internal._list   = new Array(n);       break;
      case Class::STRING:    _internal._string = new std::string();  break;
      case Class::FLOATING:  _internal._float  = 0.0;                break;
      case Class::INTEGRAL:  _internal._int    = 0;                  break;
      case Class::BOOLEAN:   _internal._bool   = false;              break;
      }

      _type = type;
    }
    /** 
     * @{ 
     * @name Writing out 
     */
    /** 
     * Write a boolean 
     *
     * @param out Output stream 
     *
     * @return @a out 
     */
    std::ostream& write_bool(std::ostream& out) const
    {
      return out << (_internal._bool ? "true" : "false");
    }
    /** 
     * Write an integer
     *
     * @param out Output stream 
     *
     * @return @a out 
     */
    std::ostream& write_int(std::ostream& out) const
    {
      return out << _internal._int;
    }
    /** 
     * Write a float
     *
     * @param out Output stream 
     *
     * @return @a out 
     */
    std::ostream& write_float(std::ostream& out) const
    {
      if (std::isnan(_internal._float) or
	  std::isnan(-_internal._float))
	return out << (nonfinite_is_null ? "null" : "NaN");
      if (std::isinf(_internal._float)) {
	if (nonfinite_is_null)
	  return out << "null";
	return out << (_internal._float < 0 ? "-" : "")
		   << "Infinity";
      }
      return out << _internal._float;
    }
    /** 
     * Write a string
     *
     * @param out Output stream 
     *
     * @return @a out 
     */
    std::ostream& write_string(std::ostream& out) const
    {
      return out << "\"" + escape(*_internal._string) + "\"";
    }
    /** 
     * Write an array
     *
     * @param out Output stream 
     * @param cur Current indent 
     *
     * @return @a out 
     */
    std::ostream& write_array(std::ostream& out, const std::string& cur) const
    {
      bool sep = false;
      out << '[';
      for(auto& p : *_internal._list) {
	if (sep) out << ", ";
	sep = true;
	p.write(out,cur);
      }
      return out << ']';
    }
    /** 
     * Write a object
     *
     * @param out Output stream 
     * @param cur Current indent 
     *
     * @return @a out 
     */
    std::ostream& write_object(std::ostream& out, const std::string& cur) const
    {
      bool sep = false;
      out << "{\n";
      for(auto& p : *_internal._map) {
	if (sep) out << ",\n";
	sep = true;
	out << cur << '\"' << p.first << "\": ";
	p.second.write(out,cur);
      }
      return out << '\n' << cur << "}";
    }
    /** 
     * @{ 
     * @name Reading in 
     */
    /** 
     * Skip over whitespace 
     *
     * @param in Input stream 
     * 
     * @returns @a in Input stream 
     */
    std::istream& read_ws(std::istream& in)
    {
      while (std::isspace(in.peek())) in.get();
      return in;
    }
    /** 	
     * Read in as an array 
     *
     * @param in Input stream 
     * 
     * @returns @a in Input stream 
     */
    std::istream& read_array(std::istream& in)
    {
      assert(in.get() == '[');
      setType(Class::ARRAY);
      do {
	read_ws(in);
	char n = in.peek();
	if (n == ']') {
	  in.get();
	  break;
	}

	read_ws(in);
	n = in.peek();
	if (n == ',') in.get();
	
	JSON e;
	e.read(in);
	append(e);
      } while(true);
      return in;
    }
    /** 	
     * Read in as an object 
     *
     * @param in Input stream 
     * 
     * @returns @a in Input stream 
     */
    std::istream& read_object(std::istream& in)
    {
      assert(in.get() == '{');
      setType(Class::OBJECT);

      do {
	read_ws(in);
	char n = in.peek();
	if (n == '}') {
	  in.get();
	  break;
	}

	read_ws(in);
	n = in.peek();
	if (n == ',') in.get();

	JSON k;
	k.read(in);
	assert (k.type() == Class::STRING);

	read_ws(in);
	assert(in.get() == ':');

	JSON v;
	v.read(in);
	operator[](k.toString()) = v;
      } while (true);
      return in;
    }
    /** 	
     * Read in as a string
     *
     * @param in Input stream 
     * 
     * @returns @a in Input stream 
     */
    std::istream& read_string(std::istream& in)
    {
      assert(in.get() == '"');

      std::string v;
      do {
	char n = in.peek();
	if (n == '\"') {
	  in.get();
	  break;
	}

	n = in.get();
	if (n == '\\') {
	  n = in.get();
	  switch (n) {
	  case '\"': v += '\"'; break;
	  case '\\': v += '\\'; break;
	  case '/' : v += '/' ; break;
	  case 'b' : v += '\b'; break;
	  case 'f' : v += '\f'; break;
	  case 'n' : v += '\n'; break;
	  case 'r' : v += '\r'; break;
	  case 't' : v += '\t'; break;
	  case 'u' : 
	    /* Unicode */
	    v += "\\u" ;
	    for (unsigned i = 1; i <= 4; ++i) {
	      n = in.get();
	      if ((n >= '0' && n <= '9') ||
		  (n >= 'a' && n <= 'f') ||
		  (n >= 'A' && n <= 'F'))
		v += n;
	      else
		throw std::runtime_error("Invalid unicode digit "+
					 std::string(1,n));
	    }
	    break;
	  default: v += '\\'; break;
	  }
	}
	else
	  v += n;
      } while (true);

      this->operator=(v);
      return in;
    }
    /** 	
     * Read in as a boolean
     *
     * @param in Input stream 
     * 
     * @returns @a in Input stream 
     */
    std::istream& read_bool(std::istream& in)
    {
      std::string s;
      for (size_t i = 0; i < 4; i++) s += in.get();

      if (s == "true") {
	this->operator=(true);
	return in;
      }

      s += in.get();
      if (s == "false") {
	this->operator=(false);
	return in;
      }
      
      throw std::runtime_error("Invalid boolean value: "+s);
      return in;
    }
    /** 	
     * Read in as null
     *
     * @param in Input stream 
     * 
     * @returns @a in Input stream 
     */
    std::istream& read_null(std::istream& in)
    {
      std::string s;
      for (size_t i = 0; i < 4; i++) s += in.get();

      if (s != "null")
	throw std::runtime_error("Invalid null value: "+s);

      setType(Class::NONE);
      return in;
    }
    /** 	
     * Read in as a number
     *
     * @param in Input stream 
     * 
     * @returns @a in Input stream 
     */
    std::istream& read_number(std::istream& in)
    {
      char p = in.peek();
      if (p == 'N') {
	std::string s = {char(in.get()), char(in.get()), char(in.get())};
	if (s == "NaN" and !nonfinite_is_null) {
	  this->operator=(NAN);
	  return in;
	}

	throw std::runtime_error("Invalid number value: "+s);
      }

      double sign = 1;
      if (p == '-') {
	sign = -1;
	in.get();
      }

      p = in.peek();
      if (p == 'I') {
	// Infinity is 8 characters 
	std::string s = {char(in.get()), char(in.get()),
			 char(in.get()), char(in.get()),
			 char(in.get()), char(in.get()),
			 char(in.get()), char(in.get())};
	if (s == "Infinity" and !nonfinite_is_null) {
	  this->operator=(sign*INFINITY);
	  return in;
	}

	throw std::runtime_error("Invalid number value: "+s);
      }
	
	  
      double v;
      in >> v;

      if (std::nearbyint(v) == v) 
	this->operator=(long(sign*v));
      else 
	this->operator=(sign*v);

      return in;
    }
    /* @} */
  private:
    static const JSON& null()
    {
      static JSON* _null = 0;
      if (!_null) _null = new JSON();
      return *_null;
    }
  };

  /** 
   * Make an array 
   *
   * @return JSON object
   */
  JSON Array(size_t n=0)
  {
    return std::move(JSON::make(JSON::Class::ARRAY,n));
  }

  /** 
   * Make an array 
   *
   * @param args Arguments (variadic)
   *
   * @return JSON object
   */
  template <typename... T>
  JSON Array(T... args)
  {
    JSON arr = JSON::make(JSON::Class::ARRAY);
    arr.append(args...);
    return arr;
  }
  /** 
   * Make an array 
   * 
   * @param c Iterable 
   *
   * @return JSON object 
    */
  template <typename T,
            typename std::enable_if<is_iterable<T>::value &&
                                    !has_key_type<T>::value &&
                                    !is_string<T>::value,int>::type = 0>
  JSON Array(T c)
  {
    auto b = std::begin(c);
    auto e = std::end(c);
      
    JSON arr = JSON::make(JSON::Class::ARRAY, std::distance(b,e));
    for (auto i = b; i != e; ++i)
      arr[std::distance(b,i)] = *i;
    
    return arr;
  }  
  
  /** 
   * Make an array 
   * 
   * @param begin Iterator
   * @param end   Iterator
   *
   * @return JSON object 
   */
  template <typename Iterator,
	    typename std::enable_if<is_iterator<Iterator>::value,int>::type = 0>
  JSON Array(Iterator begin, Iterator end)
  {
    JSON arr = JSON::make(JSON::Class::ARRAY, std::distance(begin,end));
    for (Iterator i = begin; i != end; ++i)
      arr[std::distance(begin,i)] = *i;

    return arr;
  }
  
  /** 
   * Make an object 
   *
   * @return JSON object
   */
  JSON Object()
  {
    return std::move(JSON::make(JSON::Class::OBJECT));
  }

  /** 
   * Output operator 
   *
   * @param out Output stream 
   * @param json JSON object 
   *
   * @return @a out 
   */
  std::ostream& operator<<(std::ostream &out, const JSON &json)
  {
    std::string tab;
    for (size_t i = 0; i < json.default_tab; i++) tab += " ";
    
    return json.write(out,"",tab);
  }
  /** 
   * Input operator 
   *
   * @param in Input stream 
   * @param json JSON object 
   *
   * @return @a in 
   */
  std::istream& operator>>(std::istream &in, JSON &json)
  {
    return json.read(in);;
  }
}
#endif 
//
// EOF
//
