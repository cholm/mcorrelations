/**
 * @file   correlations/stat/Trait.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Trait for statistics type
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_TRAIT_HH
#define STAT_TRAIT_HH
#include <cassert>
#include <valarray>
#include <complex>
#include <type_traits>
#include <limits>
#include <iostream>
#include <iomanip>

namespace stat
{
  /** 
   * Print a vector 
   *
   * @param name Name of matrix
   * @param v Vector
   * @param w Field width
   * @param p Precision 
   */
  template <typename T>
  void pv(const std::string& name, const T& v, size_t w=8, size_t p=4)
  {
    std::cout << name << ": ";
    std::cout << std::scientific << std::setprecision(p);
    for (auto& vv : v) std::cout << std::setw(w) << vv << ' ';
    std::cout << std::defaultfloat << std::setprecision(p) << std::endl;
  }
  /** 
   * Print a square matrix 
   *
   * @param name Name of matrix
   * @param m Matrix
   * @param w Field width
   * @param p Precision 
   */
  template <typename T>
  void pm(const std::string& name, const T& m, size_t w=8, size_t p=4)
  {
    std::cout << name << ": " << std::scientific << std::setprecision(p); 
    size_t n = std::sqrt(m.size());
    for (size_t i = 0; i < n; i++) {
      if (i != 0)
	std::cout << "\n" << std::setw(name.size()+2) << " ";
      for (size_t j = 0; j < n; j++)
	std::cout << std::setw(w) << m[i*n+j] << ' ';
    }
    std::cout << std::defaultfloat << std::setprecision(0) << std::endl;
  }
  /** 
   * Check if two numbers are close.
   *
   * Returns true if 
   * 
   * @f[ |a-b| \leq \epsilon + \delta |b|\quad,@f]
   *
   * where @f$\epsilon@f$ is the absolute tolerance, and
   * @f$\delta@f$ is the relative tolerance.  Perhaps the easiest
   * way to understand this condition is to first consider the case
   * of @f$\epsilon=0@f$.  In that case, the condition reads 
   *
   * @f[ \frac{|a-b|}{|b|} \leq \delta\quad.@f]
   *
   * That is, true if the relative change of @f$a@f$ relative to
   * @f$b@f$ is smaller or equal to @f$\delta@f$.  If we on the
   * other hand set @f$\delta=0@f$, then the condition is simply
   *
   * @f[|a-b|\leq\epsilon\quad,@f] 
   *
   * or that the absolute distance is smaller than or equal to
   * @f$\epsilon@f$.   
   *
   * By combining adding both conditions, we end up with a mix
   * between the two conditions.  Of course, we may always pass
   * @f$\epsilon=0@f$ _or_ @f$\delta=@f$ to eliminate one or the
   * other condition.
   *
   * @param a    @f$a@f$
   * @param b    @f$b@f$
   * @param atol @f$\epsilon@f$
   * @param rtol @f$\delta@f$
   *
   * @return true if the condition above is met. 
   */
  template <typename T>
  bool isclose(const T& a, const T& b,
	       double atol=std::numeric_limits<double>::epsilon(),
	       double rtol=std::numeric_limits<double>::epsilon())
  {
    return std::fabs(a-b) <= (atol + rtol * std::fabs(b));
  }
  /** 
   * Check if a matrix is asymmetric with tolerances. 
   *
   * See isclose for definition of tolerances.  This will return the
   * number of asymmetric elements (discarding the diagonal) as
   * 
   * @f[ \sum_{i=1}^{N} \sum_{j=i+1}^{N} f(m_{ij}, m_{ji})\quad,@f]
   *
   * with 
   *
   * @f[f(a,b) = \begin{cases} 1 & a \ne b\\ 0 & \mathrm{otherwise}
   * \end{cases}\quad,@f]
   *
   * where "non-equality" is set by the tolerances 
   *
   * @param m    The matrix @f$ m@f$ 
   * @param atol @f$\epsilon@f$
   * @param rtol @f$\delta@f$
   *
   * @return true if the condition above is met. 
   */
  template <typename T>
  size_t isasym(const T& m, double atol=0, double rtol=1e-6)
  {
    size_t n   = std::sqrt(m.size()); 
    size_t cnt = 0;
    for (size_t i = 0; i < n; i++) {
      for (size_t j = i+1; j < n; j++) {
	if (!isclose(m[i*n+j],m[j*n+i],atol,rtol)) cnt++;
      }
    }
    return cnt;
  }
  /** 
   * Trait for computing various quantities on statistics type @a T.
   *
   * This models operations on elements @f$ x\in X@f$ or vectors
   * @f$v\in X^N@f$.  The set @f$X@f$ may or may not be
   * @f$\mathbb{R}@f$, but since we will deal mainly will real valued
   * estimators in the end, we define a number of mappings
   * @f$X\rightarrow\mathbb{R}@f$ and vice verse.
   * 
   * Typically, we have that 
   *
   * @f[X=\mathbb{R}\quad\mathrm{or}\quad X=\mathbb{C}\quad.@f]
   *
   * Since the standard library does not give completely transparent
   * and homogenious manipulation of real and complex numbers, we
   * define a number of static helper methods.
   *
   * - Outer product @f$ uv^T@f$ (or @f$uv^H@f$ for
   *   @f$u,v\in\mathbb{C}@f$) of vectors 
   *
   * - Conversion from @f$ v\in\mathbb{R}^N@f$ to @f$ x\in X^N@f$
   *
   * - Element-by-element division for @f$u,v\in\mathbb{R}@f$ and
   *   @f$x,y\in X^N@f$
   *
   * @f{eqnarray}{
   *   x/y &:& X^N,X^N\rightarrow X^N\\
   *   x/u &:& X^N,\mathbb{R}^N\rightarrow X^N\\
   *   u/v &:& \mathbb{R}^N,\mathbb{R}^N\rightarrow X^N
   * @f}
   *
   *   Of these, the last may seem unnatural, but we need that mapping
   *   for some operations, especially for complex numbers.
   *
   *   Note, if any divisor element @f$ |y_i|,|u_i|,|v_i|<\epsilon@f$
   *   then we replace the result by a fixed number (typically 0 or
   *   1).  This allows us to have "holes" in our observation vectors.
   *
   * - Element-by-element multiplication or @f$u\in\mathbb{R}@f$ and
   *   @f$x\in X^N@f$
   *
   *   @f{eqnarray}{
   *      u\cdot x &:& \mathbb{R}^N,X^N \rightarrow X^N
   *   @f}
   *
   * - Conjugation of vectors @f$ x\in X^N@f$.  Note, this really only
   *   makes sense if @f$ X=\mathbb{C}@f$.  In all other cases, that
   *   operation is simply the identity.
   *
   * - Mapping of @f$ x\in X^N@f$ to a real vector
   *   @f$u\in\mathbb{R}^N@f$.  Again, for @f$ X=\mathbb{R}@f$ this is
   *   just the identity operation.
   *
   * Note, if @f$X=\mathbb{R}@f$, then most operations are simply
   * aliases for the standard operations and does no additional
   * computation.
   *    
   *
   * @tparam T The value type			
   *
   * @ingroup stat
   */
  template <typename T>
  struct Trait
  {
    /** Value type */
    using Value=T;
    /** Real type */
    using Real=Value;
    /** Array of values */
    using ValueVector=std::valarray<Value>;
    /** Array of reals */
    using RealVector=ValueVector;
    static std::string id() { return "R"; }
    /** 
     * Calculate the outer product of two vectors
     * @f$ v,u\in\mathbb{R}^N @f$ resulting in a matrix 
     * @f$ m\in\mathbb{R}^N\times\mathbb{R}^N @f$.
     *
     * @f{eqnarray}{
     m &= uv^T\\
     &= \begin{bmatrix}u_1\\u_2\\\vdots\\u_N\end{bmatrix}
     \begin{bmatrix} v_1&v_2&\ldots& v_N\end{bmatrix}\\
     &= \begin{bmatrix} 
     u_1v_1 & u_1v_2 & \ldots &u_1v_N\\
     u_2v_1 & u_2v_2 & \ldots &u_2v_N\\
     \vdots & \vdots & \ddots & \vdots\\
     u_Nv_1 & u_Nv_2 & \ldots &u_Nv_N\\
     \end{bmatrix}
     @f}
     *
     * @param u the vector @f$ u@f$ 
     * @param v the vectir @f$ v@f$ 
     *
     * @return Outer product @f$ m@f$ 
     */
    static ValueVector outer(const ValueVector& u, const ValueVector& v)
    {
      size_t d1 = u.size();
      size_t d2 = v.size();
      // assert(v.size() == d1);
      
      ValueVector r(d1*d2);
      for (size_t i = 0; i < d2; i++)
	r[std::slice(i*d1,d1,1)] = v[i]*u;
      return r;
    }
    static ValueVector allpow(const ValueVector& base, const ValueVector& ex)
    {
      size_t d2 = base.size();
      size_t d1 = ex.size();

      ValueVector r(d1*d2);
      for (size_t i = 0; i < d2; i++)
	r[std::slice(i*d1,d1,1)] = std::pow(base[i],ex);
      return r;
    }
    /** 
     * Turn real vector into value vector 
     *
     * @param r Input real vector 
     *
     * @return Output value vector 
     */
    static const ValueVector& value(const RealVector& r)  { return r; }
    /** 
     * Element-by-element division.  Note, if a given denominator is
     * close to zero, then we return @a def for that element.
     *
     * @param n   Numerator 
     * @param d   Denominator 
     * @param def Default value if denominator close to 0 
     * @param eps Threshold for 0 value 
     *
     * @return @a n divided by @a d element-wise 
     */
    static ValueVector div(const ValueVector& n, const ValueVector& d,
			   Value def=Value(),
			   Real eps=std::numeric_limits<Real>::epsilon())
    {
      ValueVector r(def, n.size());
      for (size_t i = 0; i < n.size(); i++) {
	if (isclose(d[i], 0., eps, 0.)) continue;
	r[i] = n[i] / d[i];
      }
      return r;
    }
    /** 
     * Element-by-element multiplication 
     * 
     * @param l Left-hand side 
     * @param r Right-hand side 
     *
     * @return Element-by-element product 
     */
    static ValueVector mul(const RealVector& l, const ValueVector& r)
    {
      return l * r;
    }
    /** 
     * Get the conjugate of a value (the same for this specialisation) 
     *
     * @param x Value 
     * @return @a x 
     */
    static const Value& conj(const Value& x) { return x; }
    /** 
     * Get the conjugate of a value (the same for this specialisation) 
     *
     * @param x Value 
     * @return @a x 
     */
    static const ValueVector& conj(const ValueVector& x) { return x; }
    /** 
     * Get the real part of a value (the same for this specialisation) 
     *
     * @param x Value 
     * @return @a x 
     */
    static const RealVector& real(const ValueVector& x) { return x; }
	      
  };    
  /** 
   * (Partial) specialisation of the trait for complex numbers
   * @f$X=\mathbb{C}@f$.
   */
  template <typename T>
  struct Trait<std::complex<T>>
  {
    /** Value type */
    using Value=std::complex<T>;
    /** Real type */
    using Real=T;
    /** Array of values */
    using ValueVector=std::valarray<Value>;
    /** Array of reals */
    using RealVector=std::valarray<Real>;

    static std::string id() { return "Z"; }
    /** 
     * Calculate the outer product of two vectors
     * @f$v,u\in\mathbb{C}^N @f$ resulting in a matrix
     * @f$m\in\mathbb{C}^N\times\mathbb{C}^N@f$.
     *
     * @f{eqnarray}{
     m &= uv^H\\
     &= \begin{bmatrix}u_1\\u_2\\\vdots\\u_N\end{bmatrix}
     \begin{bmatrix} v_1&v_2&\ldots& v_N\end{bmatrix}\\
     &= \begin{bmatrix} 
     u_1v_1^\dagger & u_1v_2^\dagger & \ldots &u_1v_N^\dagger\\
     u_2v_1^\dagger & u_2v_2^\dagger & \ldots &u_2v_N^\dagger\\
     \vdots & \vdots & \ddots & \vdots\\
     u_Nv_1^\dagger & u_Nv_2^\dagger & \ldots &u_Nv_N^\dagger\\
     \end{bmatrix}
     @f}
     * 
     * Note, for @f$ uu^H@f$ the diagnoal is real because 
     *
     * @f[ m_{jj} = u_i u_i^\dagger = (x_j+iy_j)(x_j-iy_j) = x^2+y^2\quad.@f]
     *
     * @param u the vector @f$ u@f$ 
     * @param v the vectir @f$ v@f$ 
     *
     * @return Outer product @f$ m@f$ 
     */
    static ValueVector outer(const ValueVector& u, const ValueVector& v)
    {
      size_t d = u.size();
      assert(v.size() == d);
      
      ValueVector r(d*d);
      for (size_t i = 0; i < d; i++)
	r[std::slice(i*d,d,1)] = v[i]*conj(u);
      return r;
    }
    /** 
     * Calculate the outer product of two vectors
     * @f$v,u\in\mathbb{R}^N@f$ resulting in a matrix
     * @f$m\in\mathbb{R}^N\times\mathbb{R}^N@f$.
     *
     * @f{eqnarray}{
     m &= uv^T\\
     &= \begin{bmatrix}u_1\\u_2\\\vdots\\u_N\end{bmatrix}
     \begin{bmatrix} v_1&v_2&\ldots& v_N\end{bmatrix}\\
     &= \begin{bmatrix} 
     u_1v_1 & u_1v_2 & \ldots &u_1v_N\\
     u_2v_1 & u_2v_2 & \ldots &u_2v_N\\
     \vdots & \vdots & \ddots & \vdots\\
     u_Nv_1 & u_Nv_2 & \ldots &u_Nv_N\\
     \end{bmatrix}
     @f}
     *
     * @param u the vector @f$ u@f$ 
     * @param v the vectir @f$ v@f$ 
     *
     * @return Outer product @f$ m@f$ 
     */
    static RealVector outer(const RealVector& u, const RealVector& v)
    {
      return Trait<Real>::outer(u,v);
    }
    /** 
     * Turn real vector into value vector 
     *
     * @param r Input real vector 
     *
     * @return Output value vector 
     */
    static ValueVector value(const RealVector& r)
    {
      ValueVector v(r.size());
      for (size_t i = 0 ; i < r.size(); i++) v[i] = Value(r[i]);
      return v;
    }
    /** 
     * Element-by-element division.  Note, if a given denominator is
     * close to zero, then we return @a def for that element.
     *
     * @param n   Numerator 
     * @param d   Denominator 
     * @param def Default value if denominator close to 0 
     * @param eps Threshold for 0 value 
     *
     * @return @a n divided by @a d element-wise 
     */
    static ValueVector div(const ValueVector& n, const ValueVector& d,
			   Value def=Value(), Real eps=1e-9)
    {
      ValueVector r(def, n.size());
      for (size_t i = 0; i < n.size(); i++) {
	if (std::fabs(d[i]) < eps) continue;
	r[i] = n[i] / d[i];
      }
      return r;
    }
    /** 
     * Element-by-element division.  Note, if a given denominator is
     * close to zero, then we return @a def for that element.
     *
     * @param n   Numerator 
     * @param d   Denominator 
     * @param def Default value if denominator close to 0 
     * @param eps Threshold for 0 value 
     *
     * @return @a n divided by @a d element-wise 
     */
    static ValueVector div(const ValueVector& n, const RealVector& d,
			   Value def=Value(), Real eps=1e-9)
    {
      ValueVector r(def, n.size());
      for (size_t i = 0; i < n.size(); i++) {
	if (std::fabs(d[i]) < eps) continue;
	r[i] = n[i] / d[i];
      }
      return r;
    }
    /** 
     * Element-by-element division.  Note, if a given denominator is
     * close to zero, then we return @a def for that element.
     *
     * @param n   Numerator 
     * @param d   Denominator 
     * @param def Default value if denominator close to 0 
     * @param eps Threshold for 0 value 
     *
     * @return @a n divided by @a d element-wise 
     */
    static ValueVector div(const RealVector& n, const ValueVector& d,
			   Value def=Value(), Real eps=1e-9)
    {
      ValueVector r(def, n.size());
      for (size_t i = 0; i < n.size(); i++) {
	if (std::fabs(d[i]) < eps) continue;
	r[i] = n[i] / d[i];
      }
      return r;
    }
    /** 
     * Element-by-element division.  Note, if a given denominator is
     * close to zero, then we return @a def for that element.
     *
     * @param n   Numerator 
     * @param d   Denominator 
     * @param def Default value if denominator close to 0 
     * @param eps Threshold for 0 value 
     *
     * @return @a n divided by @a d element-wise 
     */
    static ValueVector div(const RealVector& n, const RealVector& d,
			   Real def=Value(), Real eps=1e-9)
    {
      return value(Trait<Real>::div(n,d,def,eps));
    }
    /** 
     * Element-by-element multiplication 
     * 
     * @param l Left-hand side 
     * @param r Right-hand side 
     *
     * @return Element-by-element product 
     */
    static ValueVector mul(const RealVector& l, const ValueVector& r)
    {
      ValueVector rr(r);
      for (size_t i = 0; i < rr.size(); i++) rr[i] *= l[i];
      return rr;
    }
    /** 
     * Get the conjugate of a value (the same for this specialisation) 
     *
     * @param z Value @f$ z\in\mathbb{C}@f$ 
     * @return @f$ z^\dagger@f$ 
     */
    static Value conj(const Value& z) { return std::conj(z); }
    /** 
     * Get the conjugate of a value (the same for this specialisation) 
     *
     * @param z Array of values @f$ z\in\mathbb{C}@f$ 
     * @return @f$ z^\dagger@f$ for each value
     */
    static ValueVector conj(const ValueVector& z) { return z.apply(std::conj); }
    /** 
     * Get the real part of a value (the same for this specialisation) 
     *
     * @param z Array of @f$ z=\mathbb{C}@f$ 
     * @return @f$ \mathcal{R}(z)@f$ for all values
     */
    static RealVector real(const ValueVector& z)
    {
      RealVector x(z.size());
      std::transform(std::begin(z),std::end(z),std::begin(x),
		     [](const Value& z) { return z.real(); });
      return x;
    }
  };    
}
#endif
//
// EOF
//
