/**
 * @file   correlations/stat/Stat.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Generators of particle data. 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_STAT_HH
#define STAT_STAT_HH
#include <correlations/stat/Trait.hh>
#include <correlations/json.hh>

/** 
 * @defgroup stat Statistics 
 *
 * On-line calculations of means, variances, and covariances with or
 * without (component, non-frequency) weights.
 */
/** 
 * Namespace for statistics code.
 * 
 * @ingroup stat
 */
namespace stat
{
  //====================================================================
  /** 
   * Base class for statistics with online updates 
   *
   * @ingroup stat 
   * @headerfile "" <correlations/stat/Stat.hh>
   */
  template <typename T=double>
  struct Stat 
  {
    /** Size type */
    using Size=size_t;
    /** Value type */
    using Trait=stat::Trait<T>;
    using Value=typename Trait::Value;
    /** Vector real type */
    using ValueVector=typename Trait::ValueVector;
    /** Weight type */
    using Weight=typename Trait::Real;
    /** Vector real type */
    using WeightVector=typename Trait::RealVector;
    /** Real type */
    using Real=typename Trait::Real;
    /** Vector real type */
    using RealVector=typename Trait::RealVector;

    /** Destructor */
    virtual ~Stat() {}
    /** Reset state */
    virtual void reset()
    {
      std::fill(std::begin(_mean),std::end(_mean),0);
      _n = 0;
    }
    /** 
     * Fill in observation 
     *
     * @param x  Observations 
     * @param w  Weights 
     */
    virtual void fill(const ValueVector& x, const WeightVector& w) = 0;
    /** 
     * Fill in observation 
     *
     * @param x  Observations 
     */
    virtual void fill(const ValueVector& x) = 0;

    /** @return Size of statistics */
    Size size() const { return _mean.size(); }

    /** @return the variances */
    virtual RealVector variance() const = 0;
  
    /** @return the mean */
    const ValueVector& mean() const { return _mean; }

    /** @return the standard deviation */
    RealVector std() const  { return std::sqrt(variance());  }

    /** @return the standard mean uncertainty */
    virtual RealVector sem() const  {  return std() / sqrt(_n); }

    /** @return the number of fills */
    Size count() const { return _n; }

    /** 
     * @return @f$\Delta_{\nu}@f$ bias correction for @f$\nu@f$ - the
     * number of degrees of freedom
     */
    Size ddof() const { return _ddof; }

    /** 
     * Print statistics to output stream 
     * 
     * @param o Output stream 
     * @param f Flags
     *
     * @return @a o 
     */
    virtual std::ostream& print(std::ostream& o, unsigned int f=0x0) const = 0;

    /** 
     * Merge other statistics into this.
     *
     * Note, this is not implemented here 
     *
     * @tparam T1 Type of other statistcs 
     * @param o Other statistics 
     *
     * @return Reference to this 
     */
    template <typename T1> Stat<T1>& merge(const T1& o);
    /** 
     * Extract JSON object from this. 
     *
     * @return A JSON representation of this object.
     */
    virtual json::JSON toJson() const
    {
      json::JSON json = {"size",  size(),
			 "ddof",  _ddof,
			 "n",     _n,
			 "means", toJsonArray(_mean) };
      return json;
    }
    /** 
     * Assign this object from a JSON object 
     *
     * @param json JSON object to extract state from 
     */
    virtual void fromJson(const json::JSON& json)
    {
      Size s = json["size"].toInt();
      _ddof  = json["ddof"].toInt();
      _n     = json["n"].toInt();
      _mean.resize(s);
      fromJsonArray(json["means"],_mean);
    }
    /** 
     * Merge from a JSON object 
     *
     * @param json JSON object to merge from 
     */
    virtual void mergeJson(const json::JSON& json) = 0;
  protected:
    /** 
     * Constructor 
     *
     * @param n  Size of statistics 
     * @param ddof @f$\Delta_{\nu}@f$ bias correction for @f$\nu@f$ -
     * the number of degress of freedom.
     */
    Stat(Size n, Size ddof=0)
      : _mean(n),
	_n(0),
	_ddof(ddof)
    {}
    /** Move constructor */
    Stat(Stat&& o) : _mean(std::move(o._mean)), _n(o._n), _ddof(o._ddof) {}
    /** Deleted copy constructor */
    Stat(const Stat&) = delete;
    /** Deleted assignment operator */
    Stat& operator=(const Stat&) = delete;

    /** 
     * Make JSON array from array (non-complex case)
     *
     * @tparam C   Container type 
     *
     * @param c Array to convert 
     *
     * @return JSON array 
     */
    template <typename C,
	      typename std::enable_if<!std::is_same<typename C::value_type,
						    std::complex<double>>
				      ::value,int>::type = 0>
    json::JSON toJsonArray(const C& c) const
    {
      return json::JSON(c);
    }
    
    /** 
     * Assign content of JSON array to array (non-complex case)
     *
     * @tparam C   Container type 
     *
     * @param json JSON array to read rom 
     * @param c    Array to assign to 
     */
    template <typename C,
	      typename std::enable_if<!std::is_same<typename C::value_type,
						    std::complex<double>>
				      ::value,int>::type = 0>
    void fromJsonArray(const json::JSON& json, C& c)
    {
      json.toArray(c);
    }

    /** 
     * Make JSON array from array (complex case).  The complex array
     * is flattened to an array of single values. 
     *
     * @tparam C   Container type 
     *
     * @param c Array to convert 
     *
     * @return JSON array 
     */
    template <typename C,
	      typename std::enable_if<std::is_same<typename C::value_type,
						   std::complex<double>>::value,
				      int>::type = 0>
    json::JSON toJsonArray(const C& c) const
    {
      RealVector r(2*c.size());
      for (size_t i = 0; i < c.size(); i++) {
	r[2*i+0] = c[i].real();
	r[2*i+1] = c[i].imag();
      }
      return json::JSON(r);
    }

    /** 
     * Assign content of JSON array to array (complex case).  The
     * complex array is assumed to be flattened.
     *
     * @tparam C   Container type 
     *
     * @param json JSON array to read rom 
     * @param c    Array to assign to 
     */
    template <typename C,
	      typename std::enable_if<std::is_same<typename C::value_type,
						   std::complex<double>>
				      ::value,int>::type = 0>
    void fromJsonArray(const json::JSON& json, C& c)
    {
      RealVector r;
      json.toArray(r);

      c.resize(r.size()/2);
      for (size_t i = 0; i < c.size(); i++) 
	c[i] = std::complex<double>(r[2*i+0],r[2*i+1]);
    }
    

    virtual std::string id() const { return ""; }
    /** Means */
    ValueVector _mean;
    /** Number of fills */
    Size       _n;
    /** @f$ \Delta_{\nu}@f$ the bias correction for @f$\nu@f$ */
    Size       _ddof;
  };
}
#endif
//
// EOF
//
