/**
 * @file   correlations/stat/West.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Utilities used by West updates
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_WEST_HH
#define STAT_WEST_HH
#include <correlations/stat/Stat.hh>

namespace stat
{
  /** 
   * Utilities used for West (weighted) updates.  This assumes
   * component, non-frequency weights.
   *
   * @tparam T The value type 
   *
   * @ingroup stat 
   * @headerfile "" <correlations/stat/West.hh>
   */
  template <typename T=double>
  struct West
  {
    /** Size type */
    using Size=typename Stat<T>::Size;    
    /** Value type */
    using Value=typename Stat<T>::Value;
    /** Value vector type */
    using ValueVector=typename Stat<T>::ValueVector;
    /** Type of weights */
    using Weight=typename Stat<T>::Weight;
    /** Type of weights */
    using WeightVector=typename Stat<T>::WeightVector;
    /** Trait type */
    using Trait=typename Stat<T>::Trait;

    /** Destructor */
    virtual ~West() {}
  protected:
    /** Constructor - does nothing */
    West() {}
    /** Move constructor - does nothing */
    West(West&&) {}
    /** Deleted copy constructor */
    West(const West&) = delete;
    /** Deleted assignment operator */
    West& operator=(const West&) = delete;
    /** 
     * Calculate the denominator for updates 
     *
     * @f[ D = W - \Delta_{\nu} \frac{W^2}{W}\quad, @f]
     *
     * where @f$ W@f$ is the sum of weights  
     *
     * @f[ W = \sum_i w_iw_i^{T}\quad, @f]
     *
     * @f$ W^2@f$ is the sum of square weights 
     *
     * @f[ W^2 = \sum_i \left(w_iw_i^{T}\right)^2\quad, @f]
     *
     * and @f$\Delta_{\nu}@f$ is the change for the number degrees of
     * freedom (1 for unbiased estimator of the covariance)
     *
     * @param sumW @f$ W@f$ 
     * @param sumW2 @f$ W^2@f$ 
     * @param ddof @f$\Delta_{\nu}@f$ bias correction to @f$\nu@f$ 
     *
     * @return @f$ D@f$ 
     */ 
    ValueVector denom(const WeightVector& sumW,
		      const WeightVector& sumW2, Size ddof) const
    {
      ValueVector s(sumW.size());
      for (size_t i = 0; i < sumW.size(); i++) s[i] = sumW[i];
      
      if (ddof == 0) return s;

      if (sumW.apply([](Weight w) {return w*w;}).sum() < 1e-6)
	return ValueVector(sumW.size());
    
      return s - Value(ddof) * Trait::div(sumW2, sumW, Weight());
    }
    WeightVector scor(const WeightVector& sumW, const WeightVector& sumW2) const
    {
      return (sumW2 / (sumW*sumW))
	.apply([](Weight x) { return (x>1e-9 ? std::sqrt(x) : 0); });
    }
  };
}
#endif
//
// EOF
//

