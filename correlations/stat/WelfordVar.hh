/**
 * @file   correlations/stat/WelfordVar.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Welford update for mean and variance
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_WELFORDVAR_HH
#define STAT_WELFORDVAR_HH
#include <correlations/stat/Var.hh>

namespace stat
{
  /** 
   * Online calculation of means and variances without weights.
   *
   *
   * @sa 
   *
   * - http://doi.org/10.1145/3221269.3223036
   * - https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm
   *
   * @code 
   #include <correlations/stat/WelfordVar.hh>
   #include <random>
   int main()
   {
      std::random_device         rnd;
      std::default_random_engine gen(rnd());
      std::normal_distribution<> xdist(0,1);
      std::valarray<double>      x(4);
      stat::WelfordVar<>         s(x.size());
      
      for (size_t i = 0; i < 100; i++) {
        for (auto& xx : x) xx = xdist(gen);
        s.fill(x);
      }
      s.print();
      return 0;
   }
   @endcode 
   *
   * Possible output:
   *
   * @verbatim 
   n=100	(ddof=0)
        |     mean | variance |      std |   uncer.
   -----+----------+----------+----------+----------
      0 |    -0.05 |     0.90 |     0.95 |     0.09
      1 |    -0.09 |     0.88 |     0.94 |     0.09
      2 |    -0.05 |     0.83 |     0.91 |     0.09
      3 |     0.00 |     0.86 |     0.93 |     0.09
   @endverbatim 
   *
   * @tparam T The value type 
   *
   * @ingroup stat 
   * @headerfile "" <correlations/stat/WelfordVar.hh>
   */
  template <typename T=double>
  struct WelfordVar : public Var<T>
  {
    using Base=Var<T>;
    /** Trait type */
    using Trait=typename Base::Trait;
    /** Size type */
    using Size=typename Base::Size;
    /** Value type */
    using Value=typename Base::Value;
    /** Value vector type */
    using ValueVector=typename Base::ValueVector;
    /** Weight type */
    using Weight=typename Base::Weight;
    /** Weight vector type */
    using WeightVector=typename Base::WeightVector;
    /** Real type */
    using Real=typename Base::Real;
    /** Real vector type */
    using RealVector=typename Base::RealVector;
    /** 
     * Constructor 
     *
     * @param n    Size of statistcs 
     * @param ddof @f$\Delta_{\nu}@f$ 
     */
    WelfordVar(Size n, Size ddof=0) : Base(n, ddof) {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */    
    WelfordVar(WelfordVar&& o) : Base(std::move(o)) {}
    /** Deleted copy constructor */
    WelfordVar(const WelfordVar&) = delete;
    /** Deleted assignment operator */
    WelfordVar& operator=(const WelfordVar&) = delete;
    /** 
     * Fill in observation 
     *
     * @param x  Observations 
     */
    void fill(const ValueVector& x)
    {
      // assert(x.size() == _mean.size());
    
      if (_n < _ddof) {
	_mean = x;
	_n    = 1;
	return;
      }
      _n++;
      ValueVector dx =  x - _mean;
      _mean          += Value(1. / _n) * dx;
      _var           *= Real(_n - 1 - _ddof) / _n;
      _var           += Value(1. / (_n - _ddof)) * dx * Trait::conj(x-_mean);
    }
    /** 
     * Fill in observation 
     *
     * @param x  Observations 
     */
    void fill(const ValueVector& x, const WeightVector&) { fill(x); }
    /** 
     * Merge another statistics object into this 
     *
     * @param o Other 
     *
     * @return Reference to this 
     */
    WelfordVar& merge(const WelfordVar& o)
    {
      assert(o.size() == this->size());
      if (o._n <= 0) return *this;
      if (_n == 0) {
	_n    = o._n;
	_mean = o._mean;
	_var  = o._var;
	return *this;
      }

      Real        fac =  Real(_n-_ddof);
      _n              += o._n;
      Real        on  =  Real(o._n);
      ValueVector dx  =  o._mean - _mean;
      _mean           += Value(on / _n) * dx;
      ValueVector dy  =  Trait::conj(o._mean - _mean);
      _var            *= fac / _n;
      _var            += Value(on / (_n - _ddof)) * (o._var + dx * dy);
      
      return *this;
    }
    /** 
     * Merge in from JSON object 
     */
    void mergeJson(const json::JSON& json)
    {
      WelfordVar<T> w(this->size());
      w.fromJson(json);
      merge(w);
    }
  protected:
    /** Clarify scope */
    using Base::_n;
    /** Clarify scope */
    using Base::_ddof;
    /** Clarify scope */
    using Base::_mean;
    /** Clarify scope */
    using Base::_var;

    std::string id() const { return "WelfordVar"; }
  };
}
#endif
//
// EOF
//

