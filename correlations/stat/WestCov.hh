/**
 * @file   correlations/stat/WestCov.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief West update for mean and covariance
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_WESTCOV_HH
#define STAT_WESTCOV_HH
#include <correlations/stat/Cov.hh>
#include <correlations/stat/West.hh>
#include <correlations/stat/WelfordVar.hh>

namespace stat
{
  /**
   * Online calculation of means and covariance with component,
   * non-frequency weights.
   *
   *
   * @sa 
   *
   * - http://doi.org/10.1145/3221269.3223036
   *
   * @code 
   #include <correlations/stat/WestCov.hh>
   #include <random>
   int main()
   {
      std::random_device               rnd;
      std::default_random_engine       gen(rnd());
      std::normal_distribution<>       xdist(0,1);
      std::uniform_real_distribution<> wdist(0,1);
      std::valarray<double>            x(4);
      std::valarray<double>            w(4);
      stat::WestCov<>                  s(x.size());
      
      for (size_t i = 0; i < 100; i++) {
        for (auto& xx : x) xx = xdist(gen);
        for (auto& ww : w) ww = wdist(gen);
        s.fill(x);
      }
      s.print();
      return 0;
   }
   @endcode 
   *
   * Possible output:
   *
   * @verbatim 
   n=100	(ddof=0)
        |     mean |   uncer. |        0 |        1 |        2 |        3
   -----+----------+----------+----------+----------+----------+----------
      0 |     0.07 |     0.09 |     0.99 |     0.01 |    -0.10 |    -0.11
      1 |    -0.19 |     0.09 |     0.01 |     0.80 |    -0.11 |    -0.00
      2 |    -0.09 |     0.10 |    -0.10 |    -0.11 |     0.94 |     0.11
      3 |     0.18 |     0.11 |    -0.11 |    -0.00 |     0.11 |     1.19
   @endverbatim 
   *
   * @tparam T The value type 
   *
   * @ingroup stat 
   * @headerfile "" <correlations/stat/WestCov.hh>
   */
  template <typename T=double>
  struct WestCov : public Cov<T>, public West<T> 
  {
    /** Base type  */
    using Base=Cov<T>;
    /** Utility */
    using Util=West<T>;
    /** Size type */
    using Size=typename Base::Size;
    /** Trait type */
    using Trait=typename Base::Trait;
    /** Value type */
    using Value=typename Base::Value;
    /** Vector of reals type */
    using ValueVector=typename Base::ValueVector;
    /** Weight type */
    using Weight=typename Base::Weight;
    /** Vector of reals type */
    using WeightVector=typename Base::WeightVector;
    /** Real type */
    using Real=typename Base::Real;
    /** Vector of reals type */
    using RealVector=typename Base::RealVector;
    /** Unweighted Variance calculator type */
    using Var=WelfordVar<T>;
  
    /** 
     * Constructor 
     *
     * @param n number of parameters 
     * @param ddof Delta degrees of freedom (1 for unbiased)
     */
    WestCov(Size n=3,Size ddof=0)
      : Base(n,ddof),
	_sumww(n*n),
	_sumw2(n*n),
	_sumw(n),
	_var(n)
    {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */
    WestCov(WestCov&& o)
      : Base(std::move(o)),
	_sumww(std::move(o._sumww)),
	_sumw2(std::move(o._sumw2)),
	_sumw(std::move(o._sumw)),
	_var(std::move(o._var))
    {}
    /** Deleted copy constructor */
    WestCov(const WestCov&) = delete;
    /** Deleted assignment operator */
    WestCov& operator=(const WestCov&) = delete;
    /** Reset state */
    virtual void reset()
    {
      Base::reset();
      std::fill(std::begin(_sumww),  std::end(_sumww),  0);
      std::fill(std::begin(_sumw2), std::end(_sumw2), 0);
      std::fill(std::begin(_sumw), std::end(_sumw), 0);
      _var.reset();
    }
    /** 
     * Fill in an observation.  This assumes all weights are 1. 
     *
     * @param x Obervations 
     */
    void fill(const ValueVector& x)
    {
      const WeightVector w(1,x.size());
      fill(x,w);
    }
    /** 
     * Fill in an observation 
     *
     * @param x Obervations 
     * @param w Weights
     */
    void fill(const ValueVector& x, const WeightVector& w)
    {
      // Fill unweighted observation
      _var.fill(x,w);
      WeightVector ww = Trait::outer(w,w);
      _fill(ww,ww*ww,w,x,Value(0),1);
    }
    /** 
     * Merge other statistics object into this 
     *
     * @param o Other object to merge in 
     *
     * @return Reference to this 
     */
    WestCov& merge(const WestCov& o)
    {
      assert(o.size() == this->size());
      if (o._n == 0) return *this;
      _var.merge(o._var);
      _fill(o._sumww,o._sumw2,o._sumw,o._mean,o._cov,o._n);
      return *this;
    }
    /** 
     * Get the uncertainty on the weighted mean
     *
     * @f[
     \mathrm{Var}[\overline{x_i}_w] = 
     \frac{\sum_j w_{ij}^2}{\left(\sum_j w_{ij}\right)^2}\mathrm{Var}[x_i]
     @f]
     * 
     * @return standard uncertainty on the mean 
     */
    RealVector sem() const
    {
      // Take diagonal of correction matrix and multiply on standard
      // deviation of unweighted sample.
      WeightVector c = cor()[std::slice(0,size(),size()+1)];
      return c*_var.std();
      // The below is _not_ correct 
      // return std() / std::sqrt(_sumww);
    }
    // Clarify scope
    using Base::toJsonArray;
    using Base::fromJsonArray;
    /** 
     * Extract JSON object from this. 
     *
     * @return A JSON representation of this object.
     */
    json::JSON toJson() const
    {
      json::JSON json = Base::toJson();
      json["sumw"]  = toJsonArray(_sumw);
      json["sumw2"] = toJsonArray(_sumw2);
      json["sumww"] = toJsonArray(_sumww);
      json["var"]   = _var.toJson();
      
      return json;
    }
    /** 
     * Assign this object from a JSON object 
     *
     * @param json JSON object to extract state from 
     */
    void fromJson(const json::JSON& json)
    {
      Base::fromJson(json);
      fromJsonArray(json["sumw"],_sumw);
      fromJsonArray(json["sumw2"],_sumw2);
      fromJsonArray(json["sumww"],_sumww);
      _var.fromJson(json["var"]);
    }
    /** 
     * Merge in from JSON object 
     */
    void mergeJson(const json::JSON& in)
    {
      WestCov<T> w(this->size());
      w.fromJson(in);
      merge(w);
    }
    /** Clarify scope */
    using Base::size;    
  protected:
    /** Clarify scope */
    using Base::_n;
    /** Clarify scope */
    using Base::_ddof;
    /** Clarify scope */
    using Base::_mean;
    /** Clarify scope */
    using Base::_cov;
    /** Clarify scope */
    using Util::denom;
    /** Clarify scope */
    using Util::scor;

    /** 
     * Actual fill method - works for both regular files and merges. 
     *
     * Here, we take @c dy _before_ the mean update.  If we took the
     * @c dy _after_ the mean update, we would end up with an
     * asymmetric covariance matrix.  This is because the weight
     * vector sumw is not the same throughout.  Thus, if we calculate
     * the outer product
     *
     * @f[ \delta \delta'^T = (x-\bar{x})(x-\bar{x}')^T\quad,@f]
     *
     * where @f$\bar{x}'@f$ is the mean after the update, then we
     * would have an asymmetric matrix which we will update by
     *
     * This means that we take @f$\delta'=\delta@f$, and the update of
     * partition @f$A@f$ by partition @f$B@f$ with (small letters
     * vectors, capital matrixes) for @f$ P=A,B@f$ 
     *
     * @f{eqnarray}{
     w_{P} &=& \sum_i w_{P,i}\\
     U_{P} &=& \sum_i w_{P,i} w_{P,i}^T\\
     V_{P} &=& \sum_i (w_{P,i} w_{P,i}^T)^2\\
     W_{P} &=& U_{P} - \delta_\nu\frac{V_{P}}{U_P}\\
     \overline{x}_P &=& \frac{\sum_i w_{P,i}x_{P,i}}{w_P}\\
     \mathrm{Cov}_P &=& \frac{\sum_i w_{P,i}[x_{P_i}-\overline{x}_P]
     \left(w_{P,i}[x_{P_i}-\overline{x}_P]\right)^T}{W_{P}}\quad,
     @f}
     *
     * (the last term in the fourth line is the bias correction, with
     * @f$\delta_\nu=0@f$ corresponding to the _biased_ estimator, and
     * @f$\delta_\nu=1@f$ to the _unbiased_ estimator)
     *
     * is done by
     *
     * @f{eqnarray}{
     w_{AB} &=& w_A + w_B\\
     U_{AB} &=& U_A + U_B\\
     V_{AB} &=& V_A + V_B\\
     W_{AB} &=& W_A + W_B - \delta_\nu\frac{V_{AB}}{U_{AB}}\\
     \delta &=& \overline{x}_A - \overline{x}_B\\
     \overline{x}_{AB} &=& \frac{\overline{x}_A + w_B \delta}{w_{AB}}\\
     \Delta &=& \frac{W_A}{W_{AB}}\delta\delta^T\\
     \mathrm{Cov}_{AB} &=& \frac{W_A\mathrm{Cov}_A+W_B\left(\mathrm{Cov}_B+\Delta\right)}{W_{AB}}\quad. 
     @f}
     *
     * Here @f$\mathrm{Cov}_P@f$ is the covariance over partition
     * @f$P@f$.
     * 
     * The above also holds for a partition @f$ B@f$ of size 1 (i.e.,
     * a single observation) with @f$\mathrm{Cov}_B=0@f$. This is the
     * primary reason why this is implemented as a template member
     * function: In case of a single observation we can pass in a
     * scalar constant instead of constructing a full null matrix.
     *
     * Clearly, @f$w_B,U_B,V_B,W_B,\overline{x}_B,@f$ and
     * @f$\mathrm{Cov}_B@f$ can be calculated using this algorithm.
     *
     * Note, for complex numbers we replace the transpose
     * @f$\cdot^T@f$ with the hermitian conjugate @f$\cdot^H@f$.
     *
     * @param sumww @f$ U_B@f$ to update by 
     * @param sumw2 @f$ V_B@f$ to update by 
     * @param sumw  @f$ w_B@f$ to update by 
     * @param mean  @f$ x@f$ or sample mean @f$\bar{x}_B@f$ to update by 
     * @param cov   0 or sample covariance @f$ \mathrm{Cov}_B@f$ to update by 
     * @param n     number of fills to update by
     */
    template <typename C>
    void _fill(const WeightVector& sumww,
	       const WeightVector& sumw2,
	       const WeightVector& sumw,
	       const ValueVector&  mean,
	       const C&            cov,
	       size_t              n)
    {
      if (_n == 0) {
	_mean  = mean;
	_cov   = cov;
	_sumww = sumww;
	_sumw2 = sumw2;
	_sumw  = sumw;
	_n     = n;
	return;
      }
#if 1
      ValueVector  fac =  denom(_sumww, _sumw2, _ddof);
      _sumww           += sumww;
      _sumw2           += sumw2;
      _sumw            += sumw;
      ValueVector dx   =  mean - _mean;
      _mean            += Trait::div(Trait::value(sumw)*dx, _sumw);
      ValueVector div  =  denom(_sumww, _sumw2, _ddof);
      ValueVector dxy  =  Trait::div(fac,div,Value(1))*Trait::outer(dx,dx);
      _cov             *= fac; 
      _cov             += Trait::value(sumww)*(cov+dxy);
      _cov             =  Trait::div(_cov,div,Value(1));
      _n               += n;
#else      
      ValueVector  fac =  denom(_sumww, _sumw2, _ddof);
      _sumww           += sumww;
      _sumw2           += sumw2;
      _sumw            += sumw;
      ValueVector dx   =  mean - _mean;
      ValueVector dy   =  mean - _mean; // Hmm, why not after mean up?
      _mean            += Trait::div(Trait::value(sumw)*dx, _sumw);
      ValueVector dxy  =  Trait::outer(dy,dx);
      _cov             *= fac; 
      _cov             += Trait::value(sumww)*(cov+dxy);
      ValueVector div  =  denom(_sumww, _sumw2, _ddof);
      _cov             =  Trait::div(_cov,div,Value(1));
      _n               += n;
#endif
    }
      
    /** Get correction on standard deviation */
    WeightVector cor() const
    {
      return scor(_sumww,_sumw2);
    }
    std::string id() const { return "WestCov"; }
    /** Sum of w w^T */
    WeightVector _sumww;
    /** Sum of weights squared */ 
    WeightVector _sumw2;
    /** Sum of weights times means */ 
    WeightVector _sumw;
    /** The unweighted mean and variance */
    Var _var;
  };
}
#endif
//
// EOF
//

