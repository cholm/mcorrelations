/**
 * @file   correlations/stat/Cov.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Online calculations of mean and covariance 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_COV_HH
#define CORRELATIONS_STAT_COV_HH
#include <correlations/stat/Stat.hh>

namespace stat
{
  //====================================================================
  /** 
   * Base class for online calculation means and covariance 
   *
   * @tparam T The value type 
   *
   * @ingroup stat 
   * @headerfile "" <correlations/progs/cov.hh>
   */
  template <typename T=double>
  struct Cov : public Stat<T>
  {
    /** Base type */
    using Base=Stat<T>;
    /** Trait type */
    using Trait=typename Base::Trait;
    /** Size type */
    using Size=typename Base::Size;
    /** Value type */
    using Value=typename Base::Value;
    /** Value vector type */
    using ValueVector=typename Base::ValueVector;
    /** Weight type */
    using Weight=typename Base::Weight;
    /** Weight vector type */
    using WeightVector=typename Base::WeightVector;
    /** Real type */
    using Real=typename Base::Real;
    /** Real vector type */
    using RealVector=typename Base::RealVector;

    /** Destructor */
    virtual ~Cov() {}

    virtual void reset()
    {
      Base::reset();
      std::fill(std::begin(_cov), std::end(_cov), 0);
    }
    /** @return the covariance */
    virtual const ValueVector& covariance() const { return _cov; }

    /** @return the variance */
    RealVector variance() const
    {
      return Trait::real(_cov[std::slice(0,size(),size()+1)]);
    }

    /** @return the correlation */
    ValueVector correlation() const
    {
      // RealVector var = variance();
      // RealVector den = std::sqrt(Trait::outer(var,var));
      RealVector sd  = std();
      RealVector den = Trait::outer(sd,sd);
      return Trait::div(covariance(),den);
    }
    /** 
     * Print statistics to output stream 
     * 
     * @param o Output stream 
     * @param f Flags 
     *
     * @return @a o 
     */
    std::ostream& print(std::ostream& o=std::cout, unsigned int f=0x0) const
    {
      size_t w = o.width(0);
      o << "n=" << _n << "\t(ddof=" << _ddof << ")\n";

      if (f < 1) return o;
      
      size_t n = _mean.size();
      auto e = sem();
      ValueVector c = (f & 0x4 ? correlation() : covariance());
      
      o << "     | "
	<< std::setw(w) << "mean" << " | "
	<< std::setw(w) << "uncer.";
      
      for (size_t i = 0; i < n; i++)  o << " | " << std::setw(w) << i;

      o << "\n-----";
      for (size_t i = 0; i < n+2; i++) {
	o << "+";
	for (size_t j = 0; j < w+2; j++) o << '-';
      }
      o << '\n';
      for (size_t i = 0; i < n; i++) {
	o << std::setw(4) << i        << " | "
	  << std::setw(w) << _mean[i] << " | "
	  << std::setw(w) << e[i];
	for (size_t j = 0; j < n; j++)
	  o << " | " << std::setw(w) << c[i*n+j];
	o << std::endl;
      }

      return o;
    }	
    // Clarify scope
    using Base::toJsonArray;
    using Base::fromJsonArray;
    /** 
     * Extract JSON object from this. 
     *
     * @return A JSON representation of this object.
     */
    virtual json::JSON toJson() const
    {
      json::JSON json = Base::toJson();
      json["cov"] = toJsonArray(_cov);

      return json;
    }
    /** 
     * Assign this object from a JSON object 
     *
     * @param json JSON object to extract state from 
     */
    virtual void fromJson(const json::JSON& json)
    {
      Base::fromJson(json);
      fromJsonArray(json["cov"],_cov);
    }
    /** claify scope */
    using Base::size;
    /** claify scope */
    using Base::sem;
    /** claify scope */
    using Base::std;
  protected:
    /** claify scope */
    using Base::_mean;
    /** claify scope */
    using Base::_n;
    /** claify scope */
    using Base::_ddof;
    /** 
     * Constructor 
     *
     * @param n number of parameters 
     * @param ddof Delta degrees of freedom (1 for unbiased)
     */
    Cov(Size n,Size ddof=0)
      : Base(n, ddof),
	_cov(n*n)
    {}
    Cov(Cov&& o) : Base(std::move(o)), _cov(std::move(o._cov)) {}


    /** Covariance */
    ValueVector _cov;
  };
}

#endif 
//
// EOF
//
