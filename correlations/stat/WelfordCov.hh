/**
 * @file   correlations/stat/WelfordCov.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Welford update for mean and covariance
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_WELFORDCOV_HH
#define STAT_WELFORDCOV_HH
#include <correlations/stat/Cov.hh>

namespace stat
{
  /**
   * Online calculation of means and covariance without weights.
   *
   * @sa 
   *
   * - http://doi.org/10.1145/3221269.3223036
   *
   * @code 
   #include <correlations/stat/WelfordCov.hh>
   #include <random>
   int main()
   {
      std::random_device         rnd;
      std::default_random_engine gen(rnd());
      std::normal_distribution<> xdist(0,1);
      std::valarray<double>      x(4);
      stat::WelfordCov<>         s(x.size());
      
      for (size_t i = 0; i < 100; i++) {
        for (auto& xx : x) xx = dist(gen);
        s.fill(x);
      }
      s.print();
      return 0;
   }
   @endcode 
   *
   * Possible output:
   *
   * @verbatim 
   n=100	(ddof=0)
       |    mean |  uncer. |       0 |       1 |       2 |       3 
   ----+---------+---------+---------+---------+---------+---------
     0 |   -0.05 |    0.09 |    0.90 |    0.07 |   -0.13 |   -0.04 
     1 |   -0.09 |    0.09 |    0.07 |    0.88 |   -0.05 |    0.04 
     2 |   -0.05 |    0.09 |   -0.13 |   -0.05 |    0.83 |    0.01 
     3 |    0.00 |    0.09 |   -0.04 |    0.04 |    0.01 |    0.86 
   @endverbatim 
   *
   * @tparam T The value type 
   *
   * 
   * @ingroup stat 
   * @headerfile "" <correlations/stat/WelfordCov.hh>
   */
  template <typename T=double>
  struct WelfordCov : public Cov<T>
  {
    /** Base type */
    using Base=Cov<T>;
    /** Trait type */
    using Trait=typename Base::Trait;
    /** Size type */
    using Size=typename Base::Size;
    /** Value type */
    using Value=typename Base::Value;
    /** Value vector type */
    using ValueVector=typename Base::ValueVector;
    /** Weight type */
    using Weight=typename Base::Weight;
    /** Weight vector type */
    using WeightVector=typename Base::WeightVector;
    /** Real type */
    using Real=typename Base::Real;
    /** Real vector type */
    using RealVector=typename Base::RealVector;

    /** 
     * Constructor 
     *
     * @param n number of parameters 
     * @param ddof Delta degrees of freedom (1 for unbiased)
     */
    WelfordCov(Size n=3,Size ddof=0) : Base(n,ddof) {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */
    WelfordCov(WelfordCov&& o) : Base(std::move(o)) {}
    /** Deleted copy constructor */
    WelfordCov(const WelfordCov&) = delete;
    /** Deleted assignment operator */
    WelfordCov& operator=(const WelfordCov&) = delete;
    /** 
     * Fill in an observation 
     *
     * @param x Obervations 
     */
    void fill(const ValueVector& x)
    {
      assert(x.size() == _mean.size());

      if (_n < _ddof) {
	_mean = x;
	_n    = 1;
	return;
      }
      
      _n++;
      ValueVector dx =  x - _mean;
      _mean          += Value(1. / _n) * dx;
      ValueVector dy =  x - _mean;
      _cov           *= Real(_n-1) / (_n - _ddof);
      _cov           += Value(1. / (_n - _ddof)) * Trait::outer(dy,dx);
    }
    /** 
     * Fill in an observation 
     *
     * @param x Obervations 
     */
    void fill(const ValueVector& x, const WeightVector&)
    {
      fill(x);
    }
    /** 
     * Merge another statistics object into this 
     *
     * @param o Other 
     *
     * @return Reference to this 
     */
    WelfordCov& merge(const WelfordCov& o)
    {
      assert(o.size() == this->size());
      if (_n == 0) {
	_n    = o._n;
	_mean = o._mean;
	_cov  = o._cov;
	return *this;
      }
      if (o._n <= 0) return *this;

      Size        nn =  _n + o._n;
      Real        on =  Real(o._n);
      ValueVector dx =  o._mean - _mean;
      _mean          += Value(on / nn) * dx;
      ValueVector dy =  o._mean - _mean;
      _cov           *= Real(_n - _ddof) / nn;
      _cov           += Value(on / (nn - _ddof)) * (o._cov+Trait::outer(dy,dx));
      _n             =  nn;

      return *this;
    }
    /** 
     * Merge in from JSON object 
     */
    void mergeJson(const json::JSON& json)
    {
      WelfordCov<T> w(this->size());
      w.fromJson(json);
      merge(w);
    }
  protected:
    /** claify scope */
    using Base::_mean;
    /** claify scope */
    using Base::_n;
    /** claify scope */
    using Base::_ddof;
    /** claify scope */
    using Base::_cov;

    std::string id() const { return "WelfordCov"; }
  };
}
#endif
//
// EOF
//

