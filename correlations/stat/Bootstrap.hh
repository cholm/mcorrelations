/**
 * @file   correlations/stat/Bootstrap.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_BOOTSTRAP_HH
#define CORRELATIONS_STAT_BOOTSTRAP_HH
#include <correlations/stat/SubSamples.hh>

namespace stat
{
  // -----------------------------------------------------------------
  /** 
   * Calculator using bootstrap estimate of uncertainties.  
   * 
   * Note that this method generally overestimates the uncertainties.
   * See for example plot in the description of the base class
   * Estimator.
   * 
   * This overestimate of uncertainty also applies when doing
   * differential analysis.  See the following two plots: In the first
   * plot, we use full error propagation (i.e., Derivatives
   * estimators).  Note, results with a relative uncertainty
   * @f$\delta{}x/x>40\%@f$ are ignored. 
   *
   * @image html gen_eta_medium_diff.png 
   *
   * In the second plot, we use the bootstrap method. Again, results
   * with a relative uncertainty @f$\delta{}x/x>40\%@f$ are ignored.
   * Thus, missing points signify very large relative uncertainties.
   *
   * @image html gen_eta_medium_diff_bootstrap.png 
   *
   *
   * @tparam T The value type 
   *
   * @ingroup stat
   * @headerfile "" <correlations/stat/Bootstrap.hh>
   */
  template<typename T=double,unsigned short NSUB=10,unsigned short NSIM=1000>
  struct Bootstrap : public SubSamples<T,NSUB>
  {
    /** Base class */
    using Base=SubSamples<T>;
    /** Statistics calculator for weighted mean and covariance */
    using Var=WestVar<T>;
    /** Value value type */
    using Value=typename Base::Value;
    /** A value vector */
    using ValueVector=typename Base::ValueVector;
    /** Size value type */
    using Size=typename Base::Size;

    /** Destructor */
    virtual ~Bootstrap() {}
    /** 
     * Move copy constructor 
     *
     * @param o Object to move from 
     */
    Bootstrap(Bootstrap&& o) : Base(std::move(o)) {}
    /** 
     * Normal copy constructor (deleted)
     */
    Bootstrap(const Bootstrap&) = delete;
    /** 
     * Assignment operator (deleted)
     */
    Bootstrap& operator=(const Bootstrap&) = delete;
    /** 
     * Write out information on the estimator 
     *
     * @param o Output stream 
     * @param f Flags
     *
     * @return The output stream @a o
     */
    virtual std::ostream& print(std::ostream& o, unsigned int f=0x0) const
    {
      size_t w = o.width(0);
      if (f >= Estimator<T>::MORE) 
	o << std::setw(0) << "T=" << NSIM << " ";

      o  << std::setw(w);
      return Base::print(o, f);
    }
  protected:
    /** Clarify scope */
    using Base::_sub;
    /** Clarify scope */
    using Base::_s;
    /** 
     * Constructor 
     *
     * @param n Number of input parameters 
     */
    Bootstrap(Size n)
      : Base(n)
    {}
    /** 
     * Calculate the uncertainty of the measurements using a bootstrap 
     *
     * @return The uncertainty 
     */
    virtual Value simulate() const
    {
      WelfordVar<T> s(1);
      for (size_t i = 0; i < NSIM; i++) {
	Size       k = this->select();
	Value      r = this->value(this->_sub[k].mean());
	s.fill({r},{1.});
      }
      return s.std()[0];
    }
  };
}
#endif
//
// EOF
//

  
