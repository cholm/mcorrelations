/**
 * @file   correlations/stat/SubSamples.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_SUBSAMPLES_HH
#define CORRELATIONS_STAT_SUBSAMPLES_HH
#include <correlations/stat/Estimator.hh>
#include <correlations/stat/WestVar.hh>
#include <correlations/stat/WelfordVar.hh>
#include <vector>

namespace stat
{
  // -----------------------------------------------------------------
  /** 
   * Base class for calculators using sub-samples to estimate
   * uncertainties
   *
   *
   * @tparam T The value type 
   *
   * @ingroup stat
   * @headerfile "" <correlations/stat/SubSamples.hh>
   */
  template<typename T=double, unsigned short NSUB=10>
  struct SubSamples : public Estimator<T>
  {
    /** base class */
    using Base=Estimator<T>;
    /** Statistics calculator for weighted mean and covariance */
    using Var=WestVar<T>;
    /** Value value type */
    using Value=typename Base::Value;
    /** A value vector */
    using ValueVector=typename Base::ValueVector;
    /** Weight weight type */
    using Weight=typename Base::Weight;
    /** A weight vector */
    using WeightVector=typename Base::WeightVector;
    /** Size value type */
    using Size=typename Base::Size;

    /** Destructor */
    virtual ~SubSamples() {}
    /** 
     * Move copy constructor 
     *
     * @param o Object to move from 
     */
    SubSamples(SubSamples&& o)
      : _s(std::move(o._s)),
	_sub(std::move(o._sub)),
	_calc(o._calc),
	_std(o._std)
    {}
    /** 
     * Normal copy constructor (deleted)
     */
    SubSamples(const SubSamples&) = delete;
    /** 
     * Assignment operator (deleted)
     */
    SubSamples& operator=(const SubSamples&) = delete;
    /** 
     * Number of input parameters expected 
     */
    Size size() const { return _s.size(); }
    /** 
     * Write out information on the estimator 
     *
     * @param o Output stream 
     * @param f Flags
     *
     * @return The output stream @a o
     */
    virtual std::ostream& print(std::ostream& o, unsigned int f=0x0) const
    {
      size_t w = o.width(0);
      o << std::setw(w);

      if (f >= Base::MORE) {
	o << std::setw(0) << "N=" << _sub.size() << " " << std::setw(w);
	_s.print(o, f);
      }

      if (f >= Base::DETAILS) {
	for (auto& t : _sub) {
	  o << std::setw(w);
	  t.print(o, f);
	}
      }

      o << std::setw(w);
      Base::print(o);

      return o << std::endl << std::setw(w);
    }
    /** 
     * Fill in an observation 
     *
     * @param x  Observation 
     * @param w  Weights 
     */
    void fill(const ValueVector& x, const WeightVector& w)
    {
      _s.fill(x,w);
      _sub[select()].fill(x,w);
      _calc = false;
    }
    /** Dummy */
    Value rho(size_t, size_t) const { return 0; }
    /** 
     * Get JSON object representation 
     *
     * @return JSON object 
     */
    virtual json::JSON toJson() const
    {
      json::JSON json;
      json["nsub"] = _sub.size();
      json["stat"] = _s.toJson();
      json["subs"] = json::Array();
      for (auto& s : _sub) json["subs"].append(s.toJson());

      return json;
    }
    /** 
     * Read from JSON object representation 
     *
     * @param json JSON object 
     */
    virtual void fromJson(const json::JSON& json)
    {
      size_t nsub = json["nsub"].toInt();
      assert(nsub == _sub.size());
      for (size_t i = 0; i < _sub.size(); i++)
	_sub[i].fromJson(json["subs"][i]);
    }
    /**
     * Merge from JSON 
     *
     * @param json 
     */
    virtual void mergeJson(const json::JSON& json)
    {
      _s.mergeJson(json["stat"]);
      size_t nsub = json["nsub"].toInt();
      assert(nsub == _sub.size());
      for (size_t i = 0; i < _sub.size(); i++)
	_sub[i].mergeJson(json["subs"][i]);
    }
    virtual void merge(const SubSamples<T,NSUB>& o)
    {
      assert(o._sub.size() == _sub.size());
      _s.merge(o._s);
      for (size_t i = 0; i < _sub.size(); i++)
	_sub[i].merge(o._sub[i]);
    }
      
    /** 
     * Get means 
     *
     * @return vector of means 
     */
    const ValueVector& mean() const { return _s.mean(); }
  protected:
    /** 
     * Constructor 
     *
     * @param n Number of input parameters 
     */
    SubSamples(Size n)
      : Base(),
	_s(n),
	_sub(),
	_calc(false),
	_std(0)
    {
      for (size_t i = 0; i < NSUB; i++) _sub.emplace_back(n);
    }
    /**
     * Get a random number between 0 and the number of samples
     * (exclusive)
     *
     * @return Random number between 0 and the number of samples 
     */
    Size select() const
    {
      return Size(float(rand()) / RAND_MAX * _sub.size());
    }
    /** 
     * Calculate the uncertainty of the measurements using a sub-samples 
     *
     * @return The uncertainty 
     */
    virtual Value uncertainty(const ValueVector&) const
    {
      if (!_calc) {
	_std = simulate();
	_calc = true;
      }
      return _std;
    }
    /** 
     * Do simulation over sub-samples 
     *
     * @return Standard deviation 
     */
    virtual Value simulate() const = 0;
    /** Statistics  */
    Var _s;
    /** Sub-sample statistics */
    std::vector<Var> _sub;
    /** Flag if we've done the calculation */
    mutable bool _calc;
    /** Calculated standard deviation */
    mutable Value _std;
  };
}
#endif
//
// EOF
//

  
