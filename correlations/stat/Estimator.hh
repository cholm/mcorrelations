/**
 * @file   correlations/stat/Estimator.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_ESTIMATOR_HH
#define CORRELATIONS_STAT_ESTIMATOR_HH
#include <correlations/stat/Stat.hh>
#include <iostream>
#include <iomanip>

namespace stat
{
  /** 
   * Base class for an estimator.  Sub-classes implements specific
   * ways of estimating the uncertainty on the estimator.  Defined
   * subclasse are
   *
   * - Derivatives - full uncertainty propagation via covariance 
   * - Bootstrap - Simulated estimate using Bootstrapping 
   * - Jackknife - Simulated estimate using Jackknife prescription 
   *
   * @image html gen_phi_medium_intg_uncr.png 
   *
   * @include tests/testEstimator.cc 
   *
   * Possible output 
   *
   * @verbatim 
      Jackknife:   2.0645 +/-   0.3475
      Bootstrap:   2.0645 +/-   0.2261
    Derivatives:   2.0645 +/-   0.1344
   @endverbatim
   *
   * @tparam T The value type 
   */
  template <typename T=double>
  struct Estimator
  {
    enum {
	  MORE = 0x1,
	  DETAILS = 0x2
    };
    /** Statistics base class */
    using Stat=stat::Stat<T>;
    /** Traits */
    using Trait=typename Stat::Trait;
    /** Real value type */
    using Value=typename Stat::Value;
    /** A value vector */
    using ValueVector=typename Stat::ValueVector;
    /** Real weight type */
    using Weight=typename Stat::Weight;
    /** A weight vector */
    using WeightVector=typename Stat::WeightVector;
    /** Real real type */
    using Real=typename Stat::Real;
    /** A real vector */
    using RealVector=typename Stat::RealVector;
    /** Size value type */
    using Size=typename Stat::Size;
    /** Result */
    using Result=std::pair<Value,Value>;

    /**
     * Destructor 
     */
    virtual ~Estimator() {}
    /** 
     * Move copy constructor 
     */
    Estimator(Estimator&&) {}
    /** 
     * Normal copy constructor (deleted)
     */
    Estimator(const Estimator&) = delete;
    /** 
     * Assignment operator (deleted)
     */
    Estimator& operator=(const Estimator&) = delete;
    /** 
     * Calculate the final result and uncertainty 
     *
     * @return Result and uncertainty 
     */
    virtual Result eval() const
    {
      Value val = value(mean());
      Value unc = uncertainty(mean());
      return std::make_pair(val,unc);
    }
    /** 
     * Fill in an observation 
     *
     * @param x  Observation 
     * @param w  Weights 
     */
    virtual void fill(const ValueVector& x, const WeightVector& w) = 0;
    /** 
     * Number of input parameters expected 
     */
    virtual Size size() const = 0;
    /** 
     * Write out information on the estimator 
     *
     * @param o Output stream 
     *
     * @return The output stream @a o
     */
    virtual std::ostream& print(std::ostream& o, unsigned int =0x0) const
    {
      size_t w = o.width(0);

      auto res = eval();
      return o << std::setw(w) << res.first << " +/- "
	       << std::setw(w) << res.second;
    }
    /** 
     * Get JSON object representation 
     *
     * @return JSON object 
     */
    virtual json::JSON toJson() const = 0;
    /** 
     * Read from JSON object representation 
     *
     * @param json JSON object 
     */
    virtual void fromJson(const json::JSON& json) = 0;
    /**
     * Merge from JSON 
     *
     * @param json 
     */
    virtual void mergeJson(const json::JSON& json) = 0;
    /** 
     * Get means 
     *
     * @return vector of means 
     */
    virtual const ValueVector& mean() const = 0;
  protected:
    /** Constructor */
    Estimator() {}
    /** 
     * Virtual interface for calculating the estimator 
     *
     * @param means Means of each component 
     *
     * @return The estimator evaluated at the means 
     */
    virtual Value value(const ValueVector& means) const = 0;
    /** 
     * Calculate the uncertainty of the estimator 
     *
     * @return The uncertainty 
     */
    virtual Value uncertainty(const ValueVector& means) const = 0;
  };
}
#endif
//
// EOF
//

    
    
   
    
    
