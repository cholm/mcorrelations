/**
 * @file   correlations/stat/Jackknife.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_JACKKNIFE_HH
#define CORRELATIONS_STAT_JACKKNIFE_HH
#include <correlations/stat/SubSamples.hh>

namespace stat
{
  // -----------------------------------------------------------------
  /** 
   * Calculator using jackknife estimate of uncertainties 
   *
   * Note that this method generally overestimates the uncertainties.
   * See for example plot in the description of the base class
   * Estimator.
   * 
   * This overestimate of uncertainty also applies when doing
   * differential analysis.  See the following two plots: In the first
   * plot, we use full error propagation (i.e., Derivatives
   * estimators).  Note, results with a relative uncertainty
   * @f$\delta{}x/x>40\%@f$ are ignored. 
   *
   * @image html gen_eta_medium_diff.png 
   *
   * In the second plot, we use the jackknife method. Again, results
   * with a relative uncertainty @f$\delta{}x/x>40\%@f$ are ignored.
   * Thus, missing points signify very large relative uncertainties.
   *
   * @image html gen_eta_medium_diff_jackknife.png 
   *
   *
   * @tparam T The value type 
   *
   * @ingroup stat
   * @headerfile "" <correlations/stat/Jackknife.hh>
   */
  template<typename T=double, unsigned short NSUB=10>
  struct Jackknife : public SubSamples<T,NSUB>
  {
    /** Base class */
    using Base=SubSamples<T>;
    /** Statistics calculator for weighted mean and covariance */
    using Var=WestVar<T>;
    /** Real value type */
    using Value=typename Base::Value;
    /** A value vector */
    using ValueVector=typename Base::ValueVector;
    /** Size value type */
    using Size=typename Base::Size;

    /** Destructor */
    virtual ~Jackknife() {}
    /** 
     * Move copy constructor 
     *
     * @param o Object to move from 
     */
    Jackknife(Jackknife&& o)
      : Base(std::move(o))
    {}
    /** 
     * Normal copy constructor (deleted)
     */
    Jackknife(const Jackknife&) = delete;
    /** 
     * Assignment operator (deleted)
     */
    Jackknife& operator=(const Jackknife&) = delete;
  protected:
    /** Clarify scope */
    using Base::_sub;
    /** Clarify scope */
    using Base::_s;
    /** 
     * Constructor 
     *
     * @param n Number of input parameters 
     */
    Jackknife(Size n)
      : Base(n)
    {
    }
    /** 
     * Calculate the uncertainty of the measurements using a jackknife 
     *
     * @return The uncertainty 
     */
    virtual Value simulate() const
    {
      WelfordVar<T> s(1);
      for (size_t i = 0; i < _sub.size(); i++) {
	WelfordVar<T> t(1);
	for (size_t j = 0; j < _sub.size(); j++) {
	  if (i == j) continue;
	  t.fill({this->value(this->_sub[j].mean())});
	}
	s.fill({t.mean()[0]});
      }
      return (_sub.size()-1)*s.std()[0];
    }	    
  };
}
#endif
//
// EOF
//

  
