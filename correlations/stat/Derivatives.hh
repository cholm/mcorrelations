/**
 * @file   correlations/stat/Derivatives.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief A simple class that calculates the 2-particle
 * integrated 2nd, 3rd, 4th, 5th, ... flow harmonics.
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_DERIVATIVES_HH
#define CORRELATIONS_STAT_DERIVATIVES_HH
#include <correlations/stat/Estimator.hh>
#include <correlations/stat/WestCov.hh>
#include <numeric>

namespace stat
{
  // -----------------------------------------------------------------
  /** 
   * Calculator using derivatives for uncertainties 
   *
   * @tparam T The value type 
   *
   * @ingroup stat
   * @headerfile "" <correlations/stat/Derivatives.hh>
   */
  template <typename T=double>
  struct Derivatives : public Estimator<T>
  {
    using Base=Estimator<T>;
    /** Traits */
    using Trait=typename Base::Trait;
    /** Statistics calculator for weighted mean and covariance */
    using Cov=WestCov<T>;
    /** Value value type */
    using Value=typename Base::Value;
    /** A value vector */
    using ValueVector=typename Base::ValueVector;
    /** Weight weight type */
    using Weight=typename Base::Weight;
    /** A weight vector */
    using WeightVector=typename Base::WeightVector;
    /** Real real type */
    using Real=typename Base::Real;
    /** A real vector */
    using RealVector=typename Base::RealVector;
    /** Size value type */
    using Size=typename Base::Size;

    /** Destructor */
    virtual ~Derivatives() {}
    /** 
     * Move copy constructor 
     *
     * @param o Object to move from 
     */
    Derivatives(Derivatives&& o) : Base(std::move(o)), _s(std::move(o._s)){}
    /** 
     * Normal copy constructor (deleted)
     */
    Derivatives(const Derivatives&) = delete;
    /** 
     * Assignment operator (deleted)
     */
    Derivatives& operator=(const Derivatives&) = delete;
    /** 
     * Number of input arguments to average over 
     */
    Size size() const { return _s.size(); }
    /** 
     * Fill in an observation 
     *
     * @param x  Observation 
     * @param w  Weights 
     */
    void fill(const ValueVector& x, const WeightVector& w)
    {
      this->_s.fill(x,w);
    }
    /** 
     * Write out information on the estimator 
     *
     * @param o Output stream 
     * @param f Flags
     *
     * @return The output stream @a a
     */
    virtual std::ostream& print(std::ostream& o, unsigned int f=0x0) const
    {
      size_t w = o.width(0);
      o << std::setw(w);

      if (f >= Base::MORE) {
	_s.print(o, f);
	o << std::setw(w);
      }
      
      Base::print(o, f);
      return o << std::endl << std::setw(w);
    }
    /** 
     * Get the weighted correlation between two components 
     *
     * @f[
     \rho_{ij} = 
     \frac{\mathrm{Cov}[x_i,x_j]_w}{
     \sqrt{\mathrm{Var}[x_i]_w\mathrm{Var}[x_j]_w}}
     @f]
     *
     * @param i Index 
     * @param j Index 
     *
     * @return @f$\rho_{ij}@f$ 
     */
    Value rho(size_t i, size_t j) const
    {
      return rho()[i * _s.size() + j];
    }
    /** 
     * Get JSON object representation 
     *
     * @return JSON object 
     */
    virtual json::JSON toJson() const
    {
      json::JSON json = {"stat", _s.toJson() };
      return json;
    }
    /** 
     * Read from JSON object representation 
     *
     * @param json JSON object 
     */
    virtual void fromJson(const json::JSON& json)
    {
      _s.fromJson(json["stat"]);
    }
    /** 
     * Merge state from input stream 
     *
     * Note, we cannot use the merge function because we cannot
     * instantise this class.
     *
     * @param json JSON object
     */
    void mergeJson(const json::JSON& json)
    {
      _s.mergeJson(json["stat"]);
    }
    /** 
     * Merge state from other object 
     *
     * @param o Other object  
     *
     */
    virtual void merge(const Derivatives<T>& o)
    {
      _s.merge(o._s);
    }
    /** 
     * Get means 
     *
     * @return vector of means 
     */
    const ValueVector& mean() const { return _s.mean(); }
  protected:
    /** 
     * Constructor 
     *
     * @param n Number of input parameters 
     */
    Derivatives(Size n) : Base(), _s(n) {}
    /** 
     * Get the mean of a component 
     * 
     * @f[ \overline{x_i}_w = \frac{\sum_j w_{ij}x_{ij}}{\sum_j w_{ij}}@f]
     *
     * @param i Index 
     *
     * @return @f$\overline{x}_w@f$ 
     */
    Value mean(size_t i) const { return mean()[i]; }
    /** 
     * Get variances 
     *
     * @return vector of means 
     */
    RealVector var() const { return _s.variance(); }
    /** 
     * Get the weighted variance of a component 
     * 
     * @param i Index 
     *
     * @return @f$\mathrm{Var}[x]_w@f$ 
     */
    Real var(size_t i) const { return var()[i]; }
    /** 
     * @return the uncertainty on mean of each element 
     */
    RealVector uncer() const  { return _s.sem(); }
    /** 
     * Get the uncertainty on the weighted mean of a component 
     * 
     * @param i Index 
     *
     * @return @f$\sqrt{\mathrm{Var}[\overline{x}_w]}@f$ 
     */
    Real uncer(size_t i) const { return uncer()[i]; }
    /** 
     * The weighted correlation matrix 
     */
    ValueVector rho() const { return _s.correlation(); }
    /** 
     * Propagate the uncertainty 
     *
     * @f[ e = \sqrt{d^T C d}\quad,@f]
     *
     * where @f$ d@f$ is the partial derivatives of the flow
     * coefficient with respect to each component
     *
     * @f[ d_i = \frac{\partial v}{\partial x_i}\quad, @f]
     *
     * and @f$ C@f$ is the covariance matrix of the elements 
     *
     * @f[ C_{ij} = \rho_{ij}\delta_i\delta_j\quad.@f]
     *
     * @param df The derivatives
     *
     * @return @f$ e@f$ 
     */
    Value propagate(const ValueVector& df) const
    {
      size_t  m = _s.size();
      assert(df.size() == _s.size());

      // Calculate covariance using correlation and uncertainties.  We
      // do this, rather than taking the weighted covariance directly,
      // because we need the uncertainties on the _weighted_ mean, which
      // is _not_ what is stored in the covariance matrix.
      RealVector  u     = uncer();
      RealVector  e     = Trait::outer(u,u);
      ValueVector covar = Trait::mul(e, rho());
      // RealVector  sd    = _s.std();
      // RealVector  den   = Trait::outer(sd,sd);
      
      // Calculate the matrix-vector product (a vector) C*d 
      ValueVector cdf(m);
      for (size_t i = 0; i < m; i++)
	for (size_t j = 0; j < m; j++) 
	  cdf[i] += covar[i * m + j] * df[j];
      
      
      // Calculate the vector-vector product (a scalar) d^T*(C*d)
      Value t = 0;
      t = std::inner_product(std::begin(df),std::end(df),std::begin(cdf),t);
      // if (std::isnan(t) || t < 0) {
      // 	std::cout << "=== t: " << t << " ===" << std::endl;
      // 	pv("u  ", u);
      // 	pm("e  ", e);
      // 	pv("var",_s.variance());
      // 	pm("cov",_s.covariance());
      // 	pm("den",den);
      // 	pm("rho",rho());
      // 	pm("c  ", covar);
      // 	pv("df ",df);
      // 	pv("cdf",cdf);
      // }
	
      return t;
    }
    /** 
     * Calculate derivatives with respect to each input variable. 
     *
     * Derived classes can overload this member function for explicit
     * expression for the derivatives.  The default implementation
     * does a numeric differentiation using a mid-point approximation.
     *
     * @param means Where to evaluate the derivatives. 
     *
     * @returns derivatives with respect to each variable
     */
    virtual ValueVector derivatives(const ValueVector& means) const
    {
      auto        s = _s.std();
      ValueVector d(means.size());
      ValueVector dx(0., means.size());
      for (size_t i = 0; i < means.size(); i++) {
	dx[i] = s[i];
	d[i]  = (s[i] > 0 ?
		 (this->value(means+dx)-this->value(means-dx)) / (2*s[i]) :
		 0);
	dx[i] = 0;
      }
      return d;
    }
    /** 
     * Calculate the uncertainty of the measurements using a bootstrap 
     *
     * @return The uncertainty 
     */
    virtual Value uncertainty(const ValueVector& means) const
    {
      ValueVector df = derivatives(means);
      return std::sqrt(propagate(df));
    }
    /** The statistics */
    Cov _s;
  };
}
#endif
//
// EOF
//


