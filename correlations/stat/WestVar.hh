/**
 * @file   correlations/stat/WestVar.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief West update for mean and variance
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef STAT_WESTVAR_HH
#define STAT_WESTVAR_HH
#include <correlations/stat/Var.hh>
#include <correlations/stat/West.hh>

namespace stat
{
  /** 
   * Online calculation of means and variances with component,
   * non-frequency weights.
   *
   *
   * @sa 
   *
   * - http://doi.org/10.1145/3221269.3223036
   * - https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Weighted_incremental_algorithm
   * 
   * @code 
   #include <correlations/stat/WestVar.hh>
   #include <random>
   int main()
   {
      std::random_device               rnd;
      std::default_random_engine       gen(rnd());
      std::normal_distribution<>       xdist(0,1);
      std::uniform_real_distribution<> wdist(0,1);
      std::valarray<double>            x(4);
      std::valarray<double>            w(4);
      stat::WestVar<>                  s(x.size());
      
      for (size_t i = 0; i < 100; i++) {
        for (auto& xx : x) xx = xdist(gen);
        for (auto& ww : w) ww = wdist(gen);
        s.fill(x);
      }
      s.print();
      return 0;
   }
   @endcode 
   *
   * Possible output:
   *
   * @verbatim 
   n=100	(ddof=0)
        |     mean | variance |      std |   uncer.
   -----+----------+----------+----------+----------
      0 |     0.07 |     0.90 |     0.95 |     0.10
      1 |    -0.19 |     0.78 |     0.88 |     0.09
      2 |    -0.09 |     0.92 |     0.96 |     0.10
      3 |     0.18 |     1.10 |     1.05 |     0.11
   @endverbatim
   *
   * @tparam T The value type 
   *
   * @ingroup stat 
   * @headerfile "" <correlations/stat/WestVar.hh>
   */
  template <typename T=double>
  struct WestVar : public Var<T>, public West<T>
  {
    /** Base type */
    using Base=Var<T>;
    /** Utility type */
    using Util=West<T>;
    /** Trait type */
    using Trait=typename Base::Trait;
    /** Size type */
    using Size=typename Base::Size;
    /** Value type */
    using Value=typename Base::Value;
    /** Value vector type */
    using ValueVector=typename Base::ValueVector;
    /** Weight type */
    using Weight=typename Base::Weight;
    /** Weight vector type */
    using WeightVector=typename Base::WeightVector;
    /** Real type */
    using Real=typename Base::Real;
    /** Real vector type */
    using RealVector=typename Base::RealVector;
    /** 
     * Constructor 
     *
     * @param n    Size of statistcs 
     * @param ddof @f$\Delta_{\nu}@f$ 
     */
    WestVar(Size n, Size ddof=0)
      : Base(n, ddof),
	_sumw(n),
	_sumw2(n)
    {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */
    WestVar(WestVar&& o)
      : Base(std::move(o)),
	_sumw(std::move(o._sumw)),
	_sumw2(std::move(o._sumw2))
    {}
    /** Deleted copy constructor */
    WestVar(const WestVar&) = delete;
    /** Deleted assignment operator */
    WestVar& operator=(const WestVar&) = delete;
    virtual void reset()
    {
      Base::reset();
      std::fill(std::begin(_sumw),  std::end(_sumw),  0);
      std::fill(std::begin(_sumw2), std::end(_sumw2), 0);
    }
    /** 
     * Fill in observation 
     *
     * @param x  Observations 
     * @param w  Weights 
     */
    void fill(const ValueVector& x, const WeightVector& w)
    {
      _fill(w,w*w,x,Value(0),1);
    }
    /** 
     * Merge other statistics object into this 
     *
     * @param o Other object to merge in 
     *
     * @return Reference to this 
     */
    WestVar& merge(const WestVar& o)
    {
      assert(o.size() == this->size());
      if (o._n <= 0) return *this;
      _fill(o._sumw,o._sumw2,o._mean,o._var,o._n);
      return *this;
    }
    /** 
     * Merge in from JSON object 
     */
    void mergeJson(const json::JSON& json)
    {
      WestVar<T> w(this->size());
      w.fromJson(json);
      merge(w);
    }
    /** 
     * Fill in observation 
     *
     * @param z  Observations 
     */
    void fill(const ValueVector& z)
    {
      const WeightVector w(1,z.size());
      fill(z,w);
    }
    /** @return standard uncertainty on the mean */
    RealVector sem() const
    {
      return std() / std::sqrt(_sumw);
    }
    // Clarify scope
    using Base::toJsonArray;
    using Base::fromJsonArray;
    /** 
     * Extract JSON object from this. 
     *
     * @return A JSON representation of this object.
     */
    json::JSON toJson() const
    {
      json::JSON json = Base::toJson();
      json["sumw"]  = toJsonArray(_sumw);
      json["sumw2"] = toJsonArray(_sumw2);

      return json;
    }
    /** 
     * Assign this object from a JSON object 
     *
     * @param json JSON object to extract state from 
     */
    void fromJson(const json::JSON& json)
    {
      Base::fromJson(json);
      fromJsonArray(json["sumw"],_sumw);
      fromJsonArray(json["sumw2"],_sumw2);
    }
    /** Clarify scope */
    using Base::std;
  protected:
    /** Clarify scope */
    using Base::_n;
    /** Clarify scope */
    using Base::_ddof;
    /** Clarify scope */
    using Base::_mean;
    /** Clarify scope */
    using Base::_var;
    /** Clarify scope */
    using Util::denom;

    std::string id() const { return "WestVar"; }

    template <typename C>
    void _fill(const WeightVector& sumw,
	       const WeightVector& sumw2,
	       const ValueVector&  mean,
	       const C&            var,
	       size_t              n)
    {
      if (_n == 0) {
	_mean  = mean;
	_var   = var;
	_sumw  = sumw;
	_sumw2 = sumw2;
	_n     = n;
	return;
      }
      ValueVector  fac   =  denom(_sumw, _sumw2, _ddof);//old sumw
      _sumw              += sumw;
      _sumw2             += sumw2;
      ValueVector  dx    =  mean - _mean;
      _mean              += Trait::div(Trait::mul(sumw,dx), _sumw);
      ValueVector  dy    =  Trait::conj(mean - _mean);
      ValueVector  dxy   =  dx*dy;
      ValueVector  div   =  denom(_sumw, _sumw2, _ddof); // new sumw
      _var               *= Trait::div(fac, div, Value(1));//old / new
      _var               += Trait::div(Trait::mul(sumw,var+dxy),div);
      _n                 += n;
    }      

    /** Sum of component weights */
    WeightVector _sumw;
    /** Sum of squared component weights */
    WeightVector _sumw2;
  };
}
#endif
//
// EOF
//

