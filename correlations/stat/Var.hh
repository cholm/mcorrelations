/**
 * @file   correlations/stat/Var.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief On-line calculations of means and variances 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef CORRELATIONS_STAT_VAR_HH
#define CORRELATIONS_STAT_VAR_HH
#include <correlations/stat/Stat.hh>

/** 
 * Namespace for statistics code.
 */
namespace stat
{
  //====================================================================
  /** 
   * Base class for calculating means and variances 
   *
   * @tparam T The value type 
   *
   * @ingroup stat 
   * @headerfile "" <correlations/stat/Var.hh>
   */
  template <typename T=double>
  struct Var : public Stat<T>
  {
    /** Base type */
    using Base=Stat<T>;
    /** Trait type */
    using Trait=typename Base::Trait;
    /** Size type */
    using Size=typename Base::Size;
    /** Value type */
    using Value=typename Base::Value;
    /** Value vector type */
    using ValueVector=typename Base::ValueVector;
    /** Weight type */
    using Weight=typename Base::Weight;
    /** Weight vector type */
    using WeightVector=typename Base::WeightVector;
    /** Real type */
    using Real=typename Base::Real;
    /** Real vector type */
    using RealVector=typename Base::RealVector;
    
    /** Destructor */
    virtual ~Var() {}

    virtual void reset()
    {
      Base::reset();
      std::fill(std::begin(_var), std::end(_var), 0);
    }
    /** @return the variance */
    virtual RealVector variance() const { return Trait::real(_var); }

    /** 
     * Print statistics to output stream 
     * 
     * @param o Output stream 
     * @param f Flags
     *
     * @return @a o 
     */
    std::ostream& print(std::ostream& o=std::cout, unsigned int f=0x0) const
    {
      size_t w = o.width(0);
      o << "n=" << _n << "\t(ddof=" << _ddof << ")\n";

      if (f < 1) return o;
      size_t n = _mean.size();
      
      o << "     | "
	<< std::setw(w) << "mean"     << " | "
	<< std::setw(w) << "variance" << " | "
	<< std::setw(w) << "std"      << " | "
	<< std::setw(w) << "uncer."   << "\n";

      auto& m = _mean;
      auto& v = _var;
      auto  s = std();
      auto  e = sem();
      o << "-----";
      for (size_t i = 0; i < 4; i++) {
	o << "+-";
	for (size_t j = 0; j < w+1; j++) o << '-';
      }
      o << '\n';
      for (size_t i = 0; i < n; i++) 
	o << std::setw(4) << i << " | "
	  << std::setw(w) << m[i] << " | "
	  << std::setw(w) << v[i] << " | "
	  << std::setw(w) << s[i] << " | "
	  << std::setw(w) << e[i] << std::endl;

      return o;
    }
    // Clarify scope
    using Base::toJsonArray;
    using Base::fromJsonArray;
    /** 
     * Extract JSON object from this. 
     *
     * @return A JSON representation of this object.
     */
    virtual json::JSON toJson() const
    {
      json::JSON json = Base::toJson();
      json["var"] = toJsonArray(_var);

      return json;
    }
    /** 
     * Assign this object from a JSON object 
     *
     * @param json JSON object to extract state from 
     */
    virtual void fromJson(const json::JSON& json)
    {
      Base::fromJson(json);
      fromJsonArray(json["var"],_var);
    }
    /** Clarify scope */
    using Base::std;
    /** Clarify scope */
    using Base::sem;
  protected:
    /** Clarify scope */
    using Base::_n;
    /** Clarify scope */
    using Base::_mean;
    /** Clarify scope */
    using Base::_ddof;
    /** 
     * Constructor 
     *
     * @param n    Size of statistics 
     * @param ddof @f$\Delta_{\nu}@f$ bias correction to @f$\nu@f$ 
     */
    Var(Size n, Size ddof=0)
      : Base(n, ddof),
	_var(n)
    {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */
    Var(Var&& o) : Base(std::move(o)), _var(std::move(o._var)) {}
    /** Deleted copy constructor */
    Var(const Var&) = delete;
    /** Deleted assignment operator */
    Var& operator=(const Var&) = delete;


    /** Variances */
    ValueVector _var;
  };
}
#endif
//
// EOF
//
