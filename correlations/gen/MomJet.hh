/**
 * @file   correlations/gen/MomJet.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Jet-like implementation of generator of particles
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your oetaption) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef GEN_MOMJET_HH
#define GEN_MOMJET_HH
#include <correlations/gen/Generator.hh>

namespace gen
{
  //__________________________________________________________________
  /** 
   * A generator _may_ produce pairs of particles where 
   *
   * @f{eqnarray}{ 
   * p_{\mathrm{T,2}} &=& p_{\mathrm{T,1}}\\
   * \eta_2 &=& -\eta_1\\
   * \varphi_2 &=& -\varphi_1\quad.		       
   * @f}
   *
   * The probability of such pair production parameterized on the
   * transverse momentum so that the
   *
   * @f[ P = p \frac{p_{\mathrm{T}}^4}{\max(p_{\mathrm{T}})^4}\quad.@f]
   *
   * This introduces a pair-wise correlation of particles which is
   * stronger for higher transverse momentum which means that those
   * particles are not independent, identically distributed (iid). 
   *						
   * @image html jet_eta_medium_dist.png
   *
   * @headerfile "" <correlations/gen/MomJet.hh>
   * @ingroup gen
   */
  struct MomJet : public Generator
  {
    using ParticleVector=Generator::ParticleVector;
    using Real=Generator::Real;
    /** 
     * Constructor 
     *
     * @param e        Random number engine to use 
     * @param s        Sampler to use 
     * @param min      Least number of particles 
     * @param max      Largest number of particles 
     * @param hungry   Insist on exact number of particles 
     * @param ranpsi   Random event plane angle (if true)
     * @param f        Probability at max pT
     */
    MomJet(RandomEngine& e,
	    Sampler&      s,
	    size_t        min,
	    size_t        max,
	    bool          hungry=false,
	    bool          ranpsi=false,
	    Real          f=.5)
      : Generator(e, s, min, max, hungry, ranpsi),
	_j(0,1),
	_f(f)
    {}
    MomJet(MomJet&& o)
      : Generator(std::move(o)),
	_j(std::move(o._j)),
	_f(o._f)
    {}
    /** Destructor */
    virtual ~MomJet() {}
    /** 
     * Generate a single particle 
     *
     * @param l   Where to push particles 
     * @param psi Event plane 
     *
     * @return true on success 
     */
    virtual bool single(Real psi, ParticleVector& l)
    {
      auto ptphiw = _s(psi);
      // Real eta  = std::get<0>(ptphiw);
      Real pt      = std::get<1>(ptphiw);
      Real phi     = std::get<2>(ptphiw);
      Real w       = std::get<3>(ptphiw);
      if (pt < 0 || phi < 0 || w < 0) 
	return false;

      l.push_back(ptphiw);

      const int    p   = 3;
      const double max = pow(5,p);
      double       jp  = pow(pt,p) * _f / max;
      if (_j(_e) > jp) return true;

      // Store particle
      l.push_back(_s.mirror(ptphiw));
      return true;
    }
  protected:
    /** Event plane angle distribution */
    std::uniform_real_distribution<> _j;
    /** Factor */
    Real _f;
  };
}
#endif
//
// EOF
//
