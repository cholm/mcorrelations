/**
 * @file   correlations/gen/OldPhiW.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Old class for sampling Particle PDFs
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef GEN_OLDPHIWSAMPLER_HH
#define GEN_OLDPHIWSAMPLER_HH
#include <correlations/gen/Sampler.hh>

namespace gen
{
  //====================================================================
  /**
   * The @f$\phi@f$ distribution.
   *
   * @f[
   *  f(\phi) = 1 + 2\sum_{i=1}^{6} p_i\cos(i(\phi-\Phi_R))
   * @f]
   *
   * The indefinite integral is given by
   * @f[
   * \int d\phi f(\phi) = 2\pi + 
   *    2 \sum_{i=1}^{6} p_i \frac{\sin(i(\phi-\Phi_R))}{i}
   * @f]
   *
   * or for the definte integral over @f$[a,b]@f$
   * @f[
   * \int_a^b f(\phi) = (b-a) + 2 \sum_{i=1}^6
   *   p_i \frac{\sin(i(b-\Phi_R))-\sin(i(a-\Phi_R))}{i}
   * @f]
   *
   * @headerfile "" <correlations/gen/OldPhiW.hh>
   * @deprecated Use PhiW 
   * @ingroup gen
   */
  struct OldPhiW : public Sampler
  {
    /** 
     * Constructor 
     *
     * @param e     Random number engine 
     * @param vs    Flow parameters @f$ v_n@f$ 
     */
    OldPhiW(RandomEngine& e,
	    std::initializer_list<Real>& vs)
      :  Sampler(e,vs), _u(0.,1.)
    {
    }
    /** 
     * Constructor 
     *
     * @param e    Random number engine 
     * @param vs   Flow parameters @f$ v_n@f$ 
     */
    OldPhiW(RandomEngine& e, const RealVector& vs)
      :  Sampler(e,vs), _u(0.,1.)
    {
    }
    /** Copy constructor */
    OldPhiW(const OldPhiW&) = delete;
  protected:
    /** 
     * Generate pseudorapdidity @f$ \eta@f$
     *
     * @return @f$ \eta@f$
     */
    virtual Real genEta() { return 0; }
    /** 
     * Generate transverse momentum @f$ p_{\mathrm{T}}@f$
     *
     * @return @f$ p_{\mathrm{T}}@f$
     */
    virtual Real genPt(Real) { return 0.5; }
    /** 
     * Generate azimuthal angle @f$ \varphi@f$ 
     *
     * @return @f$ \varphi@f$ 
     */
    virtual Real genPhi(Real, Real) { return inverseCdf(_u(_e) * 2 * M_PI); }
    /** 
     * Generate weight @f$ w@f$ 
     *
     * @return @f$ w@f$ 
     */
    virtual Real genW(Real, Real, Real) { return 1+_u(_e)*0.144; }
    /**
     * Evaluate the integral of the function from @a a to @a b
     *
     * @param a Lower limit
     * @param b Upper limit
     *
     * @return Integral from @a a to @a b
     */
    Real integral(Real a, Real b) const
    {
      Real ret = (b-a);
      for (size_t n = 1; n <= _vs.size(); n++)
	ret += 2./n * _vs[n-1] * (sin(n*b)-sin(n*a));
      return ret;
    }

    /**
     * Evaluate the CDF at @f$\varphi@f$ 
     *
     * @param phi Where to evaluate the CDF 
     *
     * @return The CDF evaluated at @f$\varphi@f$ 
     */
    Real cdf(Real phi) const { return integral(0,phi); }

    /**
     * Evaluate the inverse CDF (Quantile function) at @f$ y@f$ 
     *
     * @param y Where to evaluate the inverse CDF 
     *
     * @return The inverse CDF evaluated at @f$ y@f$ 
     */
    Real inverseCdf(Real y) const
    {
      const Real   eps = 1e-12;
      const size_t max = 100;
      Real a  = 0;
      Real b  = 2*M_PI;
      Real ia = 0;
      Real ib = cdf(b);
      if (y <= ia) return a;
      if (y >= ib) return b;
    
      Real   x   = (b+a)/2;
      Real   ix  = cdf(x);
      size_t itr = 0;
      while (std::abs(ix - y) > eps && itr < max) {
	if   (ix < y) a = x;
	else          b = x;
	x  = (b+a) / 2;
	ix = cdf(x);
	itr++;
      }
      if (itr >= max)
	throw std::runtime_error("inverseCDF: reached max iterations");
      return x;
    }
    /** Weight test distributions */
    std::uniform_real_distribution<> _u;
  };
}
#endif
//
// EOF
//
