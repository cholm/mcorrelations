/**
 * @file   correlations/gen/Generator.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Base implementation of generator of particles
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your oetaption) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef GEN_GENERATOR_HH
#define GEN_GENERATOR_HH
#include <correlations/gen/Sampler.hh>

namespace gen
{
  //====================================================================
  /** 
   * Generate events.  
   *
   * An event is a list of particles where each particle is 
   *     
   * - @f$ p_{\mathrm{T}}@f$
   * - @f$\varphi@f$
   * - weight
   *
   * (could be expanded to include @f$\eta@f$ too)
   *
   * @image html gen_eta_medium_dist.png 
   *
   * @headerfile "" <correlations/gen/Generator.hh>
   * @ingroup gen
   */
  struct Generator
  {
    using Real=Sampler::Real;
    using RealVector=Sampler::RealVector;
    using EtaPtPhiW=Sampler::EtaPtPhiW;
    using ParticleVector=std::vector<EtaPtPhiW>;
    
    /** 
     * Constructor 
     *
     * @param e          Random number engine to use 
     * @param s          Sampler to use 
     * @param min        Least number of particles 
     * @param max        Largest number of particles 
     * @param hungry     Insist on exact number of particles 
     * @param ranpsi     Random event plane angle (if true)
     * @param acceptance If true, simulate accceptance 
     */
    Generator(RandomEngine& e,
	      Sampler&      s,
	      size_t        min,
	      size_t        max,
	      bool          hungry=false,
	      bool          ranpsi=false,
	      bool          acceptance=true)
      : _e(e),
	_s(s),
	_n(min,max),
	_r(0, ranpsi ? 2*M_PI : 0),
	_x(hungry),
	_a(acceptance)
    {
      if (_n.min() < _n.max()) _x = false;
    }
    /** Deleted copy constructor */
    Generator(const Generator&) = delete;
    /** Deleted copy constructor */
    Generator(Generator&& o)
      : _e(o._e),
	_s(o._s),
	_n(std::move(o._n)),
	_r(std::move(o._r)),
	_x(o._x),
	_a(o._a)
    {}
    /** Destructor */
    virtual ~Generator() {}
    /** @return Least number of particles to generate */
    size_t minN() const { return _n.min(); }
    /** @return Largest number of particles to generate */
    size_t maxN() const { return _n.max(); }
    /** 
     * Generate a single particle 
     *
     * @param l   Where to push particles 
     * @param psi Event plane 
     *
     * @return true on success 
     */
    virtual bool single(Real psi, ParticleVector& l)
    {
      auto ptphiw = _s(psi);
      // Real eta  = std::get<0>(ptphiw);
      Real pt      = std::get<1>(ptphiw);
      Real phi     = std::get<2>(ptphiw);
      Real w       = std::get<3>(ptphiw);
      if (pt < 0 || phi < 0 || w < 0) 
	return false;

      if (_a and _r(_e) > w)
	return false;
	       
      // Store particle
      l.push_back(ptphiw);
      return true;
    }
    /** 
     * Generate @a n bulk particles 
     *
     * @param n   Number of particles 
     * @param l   Where to push particles 
     * @param psi Event plane 
     *
     */
    virtual size_t bulk(size_t n, Real psi, ParticleVector& l)
    {
      // Reserve space
      l.reserve(n);

      // Loop until we've produced enough particles 
      size_t i = 0;
      while (i < n) {
	size_t j = l.size();
	if (!single(psi,l)) 
	  if (_x) continue;
	i += l.size() - j;
      }
      return l.size();
    }
    /** 
     * Generate phi's and weights.  For each generated sample, evaluate
     * the inverse weight as the survival probability.  Then draw a
     * random number @f$ r@f$.  If that random number is larger than the
     * survival probability, then we throw away the observation.
     *
     * Note, if @c _x is true, then @e always generate @a m particles
     * (possibly a random number).
     */
    virtual size_t generate(ParticleVector& l)
    {
      // Clear output vector
      l.clear();
    
      // Number of particles to produce 
      size_t n = 0;
      if (_n.min() >= _n.max()) n = _n.min();
      else                      n = _n(_e);

      // Event plane
      Real psi = 0;
      if (_r.min() >= _r.max()) psi = _r.min();
      else                      psi = _r(_e);

      return bulk(n, psi, l);
    }
    /** 
     * Alternative to the method generate 
     *
     * @param p Vector to fill with particle data 
     *
     * @return number of particles generated 
     */
    size_t operator()(ParticleVector& p) { return generate(p); }
    /** Generate a header */
    Header header(size_t nev=0) const
    {
      Header h;
      h._nev  = nev;
      h._minN = _n.min();
      h._maxN = _n.max();
      _s.fill(h);

      return h;
    }
    static std::tuple<RealVector,RealVector,RealVector,RealVector>
    toVectors(const ParticleVector& p)
    {
      RealVector etas   (p.size());
      RealVector pts    (p.size());
      RealVector phis   (p.size());
      RealVector weights(p.size());

      for (size_t i = 0; i < p.size(); i++) {
	auto pp = p[i];
	etas[i]    = std::get<0>(pp);
	pts [i]    = std::get<1>(pp);
	phis[i]    = std::get<2>(pp);
	weights[i] = std::get<3>(pp);
      }
      return std::make_tuple(etas,pts,phis,weights);
    }
  protected:
    /** Random number engine */
    RandomEngine& _e;
    /** Sampling object */
    Sampler& _s;
    /** Number of particles random distribution */
    std::uniform_int_distribution<size_t> _n;
    /** Event plane angle distribution */
    std::uniform_real_distribution<> _r;
    /** Whether to be hungry */
    bool _x;
    /** Whether to simulate acceptance */
    bool _a;
  };
}
#endif
//
// EOF
//
