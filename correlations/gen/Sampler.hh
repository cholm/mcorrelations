/**
 * @file   correlations/gen/Sampler.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Base class for sampling Particle PDFs
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef GEN_SAMPLER_HH
#define GEN_SAMPLER_HH
#include <tuple>
#include <correlations/gen/Header.hh>

namespace gen
{
  //====================================================================
  /** 
   * Base class for samplers.  A sampler samples a @f$\varphi@f$ PDF to
   * generate angles.  The PDF can be parameterised on
   * @f$p_{\mathrm{pt}}@f$, for example
   *
   * @image html gen_phi_medium_dist.png 
   *
   * @headerfile "" <correlations/gen/Sampler.hh>
   * @ingroup gen
   */
  struct Sampler
  {
    /** Real value */
    using Real=correlations::Real;
    /** Vector of real values */
    using RealVector=correlations::RealVector;
    /** Returned value type */
    using EtaPtPhiW=std::tuple<Real,Real,Real,Real>;

    /** 
     * Constructor 
     *
     * @param e   Random number engine 
     * @param vs  List of flow coefficents 
     */
    Sampler(RandomEngine& e, std::initializer_list<Real> vs)
      : _e(e), _vs(vs), _psi(0)
    {}
    /** 
     * Constructor 
     *
     * @param e   Random number engine 
     * @param vs  List of flow coefficents 
     */
    Sampler(RandomEngine& e, const RealVector& vs)
      : _e(e), _vs(vs), _psi(0)
    {}
    /** Destructor */
    virtual ~Sampler() {}
    /** Copy constructor */
    Sampler(const Sampler&) = delete;
    /** Move constrictor */
    Sampler(Sampler&& o)
      : _e(o._e), _vs(std::move(o._vs)), _psi(o._psi)
    {}
    /** 
     * Fill in header information 
     *
     * @param h Header to fill into 
     */
    virtual void fill(Header& h) const
    {
      h._vn = _vs;
    }
    /** 
     * Generate a single observation 
     *
     * @param psi Current event plane angle @f$\Psi@f$
     *
     * @return Tuple @f$(\eta,p_{\mathrm{T}},\varphi,w)@f$ 
     */
    EtaPtPhiW operator()(Real psi=0) { return gen(psi); }
    /** 
     * Normalize an angle to the range @f$[0,2\pi]@f$ 
     *
     * @param phi Angle @f$ \varphi@f$ 
     *
     * @return @f$\varphi@f$ normalized to @f$[0,2\pi]@f$
     */
    static Real normalize(Real phi)
    {
      if (phi > 2*M_PI) phi -= 2*M_PI;
      if (phi < 0)      phi += 2*M_PI;
      return phi;
    }
    /** 
     * Mirror a particle. 
     *
     * That is, flip the azimuthal angle @f$\varphi@f$ and
     * pseudorapidity @f$\eta@f$ so that
     * 
     * @f{eqnarrya}{
     *   \varphi &\rightarrow -\varphi\\
     *   \eta    &\rightarrow -\eta\\
     *   p_{\mathrm{T}} &\rightarrow p_{\mathrm{T}}
     * @f} 
     * 
     * We may either copy the weight @f$ w@f$ so that 
     *
     * @f[w\rightarrow w\quad,@f] 
     *
     * or draw a new random weight.  This is selected by the argument @a neww 
     *
     * @param o Observation to flip 
     * @param neww if true, create a new random weight 
     *
     * @return mirrored observation 
     */
    EtaPtPhiW mirror(const EtaPtPhiW& o, bool neww=false)
    {
      EtaPtPhiW r              =  o;
      std::get<0>(r)           *= -1;
      std::get<2>(r)           =  normalize(std::get<2>(r)+M_PI);
      if (neww) std::get<2>(r) =  genW(std::get<0>(r),std::get<1>(r),std::get<2>(r));
	
      return r;
    }
  protected:
    /**
     * Generate a tuple of transverse momentum (always 0), azimuthal
     * angle, and weight
     *
     * @param psi Event plane angle 
     *
     * @return Tuple of @f$(\eta,p_{\mathrm{T}},\varphi,w)@f$ 
     */
    virtual EtaPtPhiW gen(Real psi)
    {
      Real eta = genEta();
      Real pt  = genPt(eta);
      if (pt < 0) return std::make_tuple(0.,-1.,-1.,-1.);
      Real phi = genPhi(eta,pt)+psi;
      if (phi < 0) return std::make_tuple(0.,-1.,-1.,-1.);
      Real w   = genW(eta,pt,phi);
      if (w < 0) return std::make_tuple(0.,-1.,-1.,-1.);
      
      return std::make_tuple(eta, pt,phi,w);
    }
    /** 
     * Generate pseudorapdidity @f$ \eta@f$
     *
     * @return @f$ \eta@f$
     */
    virtual Real genEta() { return 0; }
    /** 
     * Generate transverse momentum @f$ p_{\mathrm{T}}@f$
     *
     * @param eta Current pseudorapidity @f$\eta@f$ 
     *
     * @return @f$ p_{\mathrm{T}}@f$
     */
    virtual Real genPt(Real eta) = 0;
    /** 
     * Generate azimuthal angle @f$ \varphi@f$ 
     *
     * @param eta Current pseudorapidity @f$\eta@f$ 
     * @param pt  Current transverse momentum @f$ p_{\mathrm{T}}@f$ 
     *
     * @return @f$ \varphi@f$ 
     */
    virtual Real genPhi(Real eta, Real pt) = 0;
    /** 
     * Generate weight @f$ w@f$ 
     *
     * @param eta Current pseudorapidity @f$\eta@f$ 
     * @param pt  Current transverse momentum @f$ p_{\mathrm{T}}@f$ 
     * @param phi Current azimuthal angle @f$ \varphi@f$ 
     *
     * @return @f$ w@f$ 
     */
    virtual Real genW(Real eta, Real pt, Real phi) = 0;
    /**
     * The @f$\varphi@f$ probability density function
     *
     * @f[ 1+\sum_{n} 2v_{n}\cos\left(n[\varphi-\Psi]\right)@f]
     *
     * @param phi @f$\varphi@f$ 
     *
     * @return Probability 
     */
    Real phiPdf(Real phi) const
    {
      Real ret = 0;
      for (size_t n = 1; n <= _vs.size(); n++)
	ret += 2 * _vs[n-1] * cos(n * (phi-_psi));
    
      return 1 + ret;
    }
    /** Random number engine */
    RandomEngine& _e;
    /** Flow coefficients */
    RealVector _vs;
    const Real _psi;
  };
}
#endif
//
// EOF
//
