/**
 * @file   correlations/gen/PhiW.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Class for sampling Particle PDFs
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef GEN_PHIWSAMPLER_HH
#define GEN_PHIWSAMPLER_HH
#include <correlations/gen/Sampler.hh>

namespace gen
{
  //====================================================================
  /** 
   * Sample @f$\varphi@f$ distribution parameterised by flow coefficents 
   *
   * @f[ v_1,\ldots,v_n @f]
   *
   * @image html gen_phi_medium_dist.png
   *
   * @headerfile "" <correlations/gen/PhiW.hh>
   * @ingroup gen
   */
  struct PhiW : public Sampler
  {
    /** Random number distribution for @f$\varphi@f$ */
    using PhiDist = std::piecewise_linear_distribution<>;
    /** Random number distribution for weights */
    using WeightDist = std::uniform_real_distribution<>;
    /** 
     * Constructor.
     *
     * Initialise the random number generators 
     *
     * @param e    Random number engine 
     * @param vs   Flow coefficients 
     * @param nphi Number of sampling points along @f$\varphi@f$ for the PDF 
     */
    PhiW(RandomEngine&                e,
	 std::initializer_list<Real>& vs,
	 size_t                       nphi=100)
      :  Sampler(e,vs),
	 _phiDist(nphi,0,2*M_PI,[this](Real x) { return phiPdf(x); }),
	 _wDist(1,1+0.144) // Flat between 1 and 1.44
    {
    }
    /** 
     * Constructor.
     *
     * Initialise the random number generators 
     *
     * @param e    Random number engine 
     * @param vs   Flow coefficients 
     * @param nphi Number of sampling points along @f$\varphi@f$ for the PDF 
     */
    PhiW(RandomEngine&     e,
	 const RealVector& vs,
	 size_t            nphi=100)
      :  Sampler(e,vs),
	 _phiDist(nphi,0,2*M_PI,[this](Real x) { return phiPdf(x); }),
	 _wDist(1,1+0.144)  // Flat between 1 and 1.44
    {
    }
    /** Copy constructor */
    PhiW(const PhiW&) = delete;
    /** Move constructor */
    PhiW(PhiW&& o)
      : Sampler(std::move(o)),
	_phiDist(std::move(o._phiDist)),
	_wDist(std::move(o._wDist))
    {}
  protected:
    /** 
     * Generate pseudorapdidity @f$ \eta@f$
     *
     * @return @f$ \eta@f$
     */
    virtual Real genEta() { return 0; }
    /** 
     * Generate transverse momentum @f$ p_{\mathrm{T}}@f$
     *
     * @return @f$ p_{\mathrm{T}}@f$
     */
    virtual Real genPt(Real) { return 0.5; }
    /** 
     * Generate azimuthal angle @f$ \varphi@f$ 
     *
     * @return @f$ \varphi@f$ 
     */
    virtual Real genPhi(Real, Real) { return _phiDist(_e); }
    /** 
     * Generate weight @f$ w@f$ 
     *
     * @return @f$ w@f$ 
     */
    virtual Real genW(Real, Real, Real) { return _wDist(_e); }
    /** The @f$ \varphi@f$ distribution */
    PhiDist _phiDist;
    /** The weight @f$ w@f$ distribution */
    WeightDist _wDist;
  };
}
#endif
//
// EOF
//
