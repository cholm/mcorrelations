/**
 * @file   correlations/gen/Header.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Header for particle data. 
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef GEN_HEADER_HH
#define GEN_HEADER_HH
#include <random>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <correlations/Types.hh>
#include <correlations/json.hh>

/** 
 * @defgroup gen Generate events 
 *
 * This module contains code to generate events.
 */
/** 
 * Namespace for generators 
 * @ingroup gen
 */
namespace gen
{
  /** The random engine we'll use */
  // using RandomEngine=std::mt19937;
  using RandomEngine=std::default_random_engine;

  //====================================================================
  /**
   * Header helper 
   *
   * @ingroup gen
   * @headerfile "" <correlations/gen/Header.hh>
   */
  struct Header
  {
    using RealVector = correlations::RealVector;
    using Real = correlations::Real;
    /** Constructor */
    Header()
      : _nev(0),
	_minN(0),
	_maxN(0),
	_vn(),
	_ptbin(),
	_ptcoef(),
	_meanpt(0)
    {}
    /** 
     * Constructor 
     * 
     * @param nev       Number of events 
     * @param minN      Least number of particles 
     * @param maxN      Largest number of particles 
     * @param vn        Flow coefficents 
     * @param ptbin     Transverse momentum bins (GeV/C) 
     * @param ptcoef    Transverse momentum modulation coefficients 
     * @param meanpt    Mean transverse momentum (GeV/C)
     */
    Header(size_t            nev,
	   size_t            minN,
	   size_t            maxN,
	   const RealVector& vn,
	   const RealVector& ptbin,
	   const RealVector& ptcoef,
	   Real              meanpt)
      : _nev(nev),
	_minN(minN),
	_maxN(maxN),
	_vn(vn),
	_ptbin(ptbin),
	_ptcoef(ptcoef),
	_meanpt(meanpt)
    {}
    /** 
     * Constructor 
     * 
     * @param nev       Number of events 
     * @param minN      Least number of particles 
     * @param maxN      Largest number of particles 
     * @param vn        Flow coefficents 
     * @param ptbin     Transverse momentum bins (GeV/C) 
     * @param ptcoef    Transverse momentum modulation coefficients 
     * @param meanpt    Mean transverse momentum (GeV/C)
     */
    Header(size_t                      nev,
	   size_t                      minN,	   
	   size_t                      maxN,
	   std::initializer_list<Real> vn,
	   std::initializer_list<Real> ptbin,
	   std::initializer_list<Real> ptcoef,
	   Real                        meanpt)
      : _nev(nev),
	_minN(minN),
	_maxN(maxN),
	_vn(vn),
	_ptbin(ptbin),
	_ptcoef(ptcoef),
	_meanpt(meanpt)
    {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from 
     */
    Header(Header&& o)
      : _nev(o._nev),
	_minN(o._minN),
	_maxN(o._maxN),
	_vn(std::move(o._vn)),
	_ptbin(std::move(o._ptbin)),
	_ptcoef(std::move(o._ptcoef)),
	_meanpt(o._meanpt)
    {}
    /** 
     * Deleted copy constructor 
     */
    Header(const Header&) = delete;
    /** Deleted assignment operator */
    Header& operator=(const Header& o)
    {
      if (this == &o) return *this;

      _nev    = o._nev;
      _minN   = o._minN;
      _maxN   = o._maxN;
      _vn     = o._vn;
      _ptbin  = o._ptbin;
      _ptcoef = o._ptcoef;
      _meanpt = o._meanpt;
      
      return *this;      
    }

    
    /** 
     * Write an array to output.  Format is 
     *
     * @verbatim 
     * SIZE ELEMENTS...
     * @endverbatim
     *
     * @param array Array to write 
     * @param out   Output stream 
     */
    static void writeArray(const RealVector& array, std::ostream& out)
    {
      out << array.size();
      for (auto& a : array) out << "\t" << a;
      out << std::endl;
    }
    /** 
     * Read in an array.  Expected format as 
     *
     * @verbatim 
     * SIZE ELEMENTS...
     * @endverbatim 
     *
     * @param in    Input stream 
     * @param array Array to read into 
     */
    static void readArray(std::istream& in, RealVector&   array)
    {      
      std::string l;
      std::getline(in, l);
      std::stringstream s(l);
      size_t n = 0;
      s >> n;
      if (s.fail())
	throw std::runtime_error("Failed to read array size in header: '"
				 + l + "'");
      
      array.resize(n);
      for (auto& a : array) {
	s >> a;
	if (s.fail())
	  throw std::runtime_error("Failed to read array element in header: '"
				   + l + "'");
      }
    }
    /** 
     * Write comments 
     */
    static void writeComment(std::ostream& out)
    {
      out << "# Next 5 data lines is the header.\n"
	  << "# The lines are:\n"
	  << "# - Number of events\n"
	  << "# - Number of particles range\n"
	  << "# - Flow coefficents: N V1 V2 ... VN\n"
	  << "# - pT bin edges:     N B1 B2 ... BN\n"
	  << "# - pT coefficents:   N C1 C2 ... CN\n"
	  << "# - Mean pT (in GeV/c)" << std::endl;
    }
    static void writeEventComment(std::ostream& out) 
    {
      out << "# Following records are for each event.\n"
	  << "# \n"
	  << "# Event records start with the number of particles in the event\n"
	  << "# followed by that many lines of particle information.\n"
	  << "# Each line contain the 4 fields (in order):\n"
	  << "# \n"
	  << "# - Pseudorapidity (eta)\n"
	  << "# - Transverse momentum (pT)\n"
	  << "# - Azimuth angle (phi) in radians\n"
	  << "# - Weight (weight)" << std::endl;
    }
    /** 
     * Write out the header to a stream 
     *
     * @param out Output stream 
     * 
     * @return The output stream @a out 
     */
    std::ostream& write(std::ostream& out) const
    {
      writeComment(out);
      out << _nev << "\t" << _minN << "\t" << _maxN << std::endl;
      writeArray(_vn, out);
      writeArray(_ptbin, out);
      writeArray(_ptcoef, out);
      out << _meanpt << std::endl;
      writeEventComment(out);
	
      return out;
    }
    static void readComments(std::istream& in, size_t lines,
			     const std::string& where)
    {
      std::string l;
      for (size_t i = 0; i < lines; i++) {
	std::getline(in, l);
	if (in.fail())
	  throw std::runtime_error("Failed to read " + where + " comment line "
				   + std::to_string(i+1) + ": '"
				   + l +"'");
	if (l[0] != '#')
	  throw std::runtime_error("Expected " + where + " comment line # "
				   + std::to_string(i+1) + " got: '"
				   + l + "'");
      }
    }
    /** 
     * Read header 
     *
     * @param in     Input stream 
     *
     * @return the read header information 
     */
    static Header read(std::istream& in)
    {
      if (!in.good())
	throw std::runtime_error("Bad input stream for header header");
      
      // Read 8 comment lines
      readComments(in,8,"header");

      Header h;
      
      // Read the data
      std::string l;
      std::getline(in, l);
      std::stringstream s(l);
      s >> h._nev >> h._minN >> h._maxN;
      if (s.fail())
	throw std::runtime_error("Failed to read 1st header line from: '"
				 +l+"'");
	
      readArray(in, h._vn);
      readArray(in, h._ptbin);
      readArray(in, h._ptcoef);

      std::getline(in,l);
      s.clear();
      s.str(l);
      s >> h._meanpt;
      if (s.fail())
	throw std::runtime_error("Failed to read 5th header line: '"
				 + l + "' " + std::to_string(h._meanpt));

      // Read 10 comment lines 
      readComments(in,10,"event");

      return h;
    }
    static size_t copy(std::istream& in,
		       std::ostream& out,
		       size_t        nev=0)
    {
      Header h = read(in);
      if (nev > 0) h._nev = std::max(nev, h._nev);
      h.write(out);
      return h._nev;
    }
    /** 
     * Copy header from input to named file 
     *
     * @param in       Input stream 
     * @param filename Output file 
     * @param nev      Number of events to override
     *
     * @return nev - Number of events
     */
    static size_t copy(std::istream& in,
		       const std::string& filename,
		       size_t nev=0)
    {
      std::ofstream out(filename.c_str());
      nev = copy(in, out, nev);
      out.close();
      
      return nev;
    }
    /** 
     * Make JSON representation 
     *
     * @return JSON object 
     */
    json::JSON toJson() const
    {
      json::JSON json = { "nev",          _nev,
			  "multiplicity", json::Array(_minN, _maxN),
			  "flow_coef",    _vn,
			  "pt_bin",       _ptbin,
			  "pt_coef",      _ptcoef,
			  "mean_pt",      _meanpt };
      return json;
    }
    /** 
     * Get from JSON representation 
     *
     * @param json JSON object to read from 
     */
    void fromJson(const json::JSON& json)
    {
      _nev    = json["nev"] .toInt();
      _minN   = json["minN"].toInt();
      _maxN   = json["maxN"].toInt();
      json["vn"].toArray(_vn);
      json["ptbins"].toArray(_ptbin);
      json["ptcoef"].toArray(_ptcoef);
      _meanpt = json["meanpt"].toFloat();
    }
    void merge(const Header& h)
    {
      _nev += h._nev;
    }
    /** 
     * Merge from JSON representation 
     *
     * @param json JSON object to read from 
     */
    void mergeJson(const json::JSON& json)
    {
      Header h;
      h.fromJson(json);

      _nev += h._nev;
    }
    /** 
     * @{ 
     * @name Data members. These are public to allow fast access. 
     */
    /** Number of events */
    size_t _nev;
    /** Least number of particles */
    size_t _minN;
    /** Largest number of particles */
    size_t _maxN;
    /** Flow coefficients */
    RealVector _vn;
    /** Transverse momentum bins (GeV/c) */
    RealVector _ptbin;
    /** Transverse momentum modulation coefficients */
    RealVector _ptcoef;
    /** Average transverse momentum */
    Real _meanpt;
  };
}
#endif
//
// EOF
//
