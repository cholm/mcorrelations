/**
 * @file   correlations/gen/Momentum.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Pair-wise momentum conserving implementation of generator of particles
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your oetaption) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef GEN_MOMENTUM_HH
#define GEN_MOMENTUM_HH
#include <correlations/gen/Generator.hh>

namespace gen
{
  //__________________________________________________________________
  /** 
   * A generator that preserves momentum by mirroring every particle
   * in pseudorapidity and azimuthal angle
   *
   * @f{eqnarray}{ 
   * p_{\mathrm{T,2}} &=& p_{\mathrm{T,1}}\\
   * \eta_2 &=& -\eta_1\\
   * \varphi_2 &=& -\varphi_1\quad.		       
   * @f}
   *						
   * This introduces an explicit correlation which breaks the
   * assumption that all particles are independent, identically
   * distributed (iid) almost maximally.
   *
   * @image html mom_eta_medium_dist.png
   *
   * @headerfile "" <correlations/gen/Momentum.hh>
   * @ingroup gen
   */
  struct Momentum : public Generator
  {
    using ParticleVector=Generator::ParticleVector;
    using Real=Generator::Real;
    /** 
     * Constructor 
     *
     * @param e        Random number engine to use 
     * @param s        Sampler to use 
     * @param min      Least number of particles 
     * @param max      Largest number of particles 
     * @param hungry   Insist on exact number of particles 
     * @param ranpsi   Random event plane angle (if true)
     * @param accept   If true, simulate accceptance 
     */
    Momentum(RandomEngine& e,
	     Sampler&      s,
	     size_t        min,
	     size_t        max,
	     bool          hungry=false,
	     bool          ranpsi=false,
	     bool          accept=true)
      : Generator(e, s, min, max, hungry, ranpsi, accept)
    {}
    Momentum(Momentum&& o)
      : Generator(std::move(o))
    {}
    /** Destructor */
    virtual ~Momentum() {}
    /** 
     * Generate a single particle 
     *
     * @param l   Where to push particles 
     * @param psi Event plane 
     *
     * @return true on success 
     */
    virtual bool single(Real psi, ParticleVector& l)
    {
      auto ptphiw = _s(psi);
      // Real eta  = std::get<0>(ptphiw);
      Real pt      = std::get<1>(ptphiw);
      Real phi     = std::get<2>(ptphiw);
      Real w       = std::get<3>(ptphiw);
      if (pt < 0 || phi < 0 || w < 0) 
	return false;

      auto back = ptphiw;
      std::get<0>(back) *= -1;
      std::get<2>(back) = Sampler::normalize(phi+M_PI);
      
      // Store particle
      l.push_back(ptphiw);
      l.push_back(back);
      return true;
    }
  };
}
#endif
//
// EOF
//
