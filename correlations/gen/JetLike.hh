/**
 * @file   correlations/gen/JetLike.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Jet-like implementation of generator of particles
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your oetaption) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef GEN_JETLIKE_HH
#define GEN_JETLIKE_HH
#include <correlations/gen/Generator.hh>

namespace gen
{
  //__________________________________________________________________
  /** 
   * A generator _may_ produce pairs of particles where 
   *
   * @f{eqnarray}{ 
   * p_{\mathrm{T,2}} &=& p_{\mathrm{T,1}}\\
   * \eta_2 &=& -\eta_1\\
   * \varphi_2 &=& -\varphi_1\quad.		       
   * @f}
   *
   * The probability of such pair production parameterized on the
   * transverse momentum so that the
   *
   * @f[ P = p \frac{p_{\mathrm{T}}^4}{\max(p_{\mathrm{T}})^4}\quad.@f]
   *
   * This introduces a pair-wise correlation of particles which is
   * stronger for higher transverse momentum which means that those
   * particles are not independent, identically distributed (iid). 
   *						
   * @image html jet_eta_medium_dist.png
   *
   * @headerfile "" <correlations/gen/JetLike.hh>
   * @ingroup gen
   */
  struct JetLike : public Generator
  {
    using ParticleVector=Generator::ParticleVector;
    using Real=Generator::Real;
    /** Returned value type */
    using EtaPtPhiW=Sampler::EtaPtPhiW;
    /** 
     * Constructor 
     *
     * @param e        Random number engine to use 
     * @param s        Sampler to use 
     * @param min      Least number of particles 
     * @param max      Largest number of particles 
     * @param hungry   Insist on exact number of particles 
     * @param ranpsi   Random event plane angle (if true)
     * @param accept   If true, simulate accceptance 
     * @param f        Probability at max pT
     */
    JetLike(RandomEngine& e,
	    Sampler&      s,
	    size_t        min,
	    size_t        max,
	    bool          hungry=false,
	    bool          ranpsi=false,
	    bool          accept=true,
	    Real          f=.3)
      : Generator(e, s, min, max, hungry, ranpsi, accept),
	_j(0,1),
	_jn(5),                // Average one side 
	_ja(0,2 * M_PI / 180), // 2-degree spread 
	_f(f)
    {}
    JetLike(JetLike&& o)
      : Generator(std::move(o)),
	_j(std::move(o._j)),
	_jn(std::move(o._jn)),
	_ja(std::move(o._ja)),
	_f(o._f)
    {}
    /** Destructor */
    virtual ~JetLike() {}
    /** 
     * Generate a single particle 
     *
     * @param l   Where to push particles 
     * @param psi Event plane 
     *
     * @return true on success 
     */
    virtual bool single(Real psi, ParticleVector& l)
    {
      auto ptphiw = _s(psi);
      // Real eta  = std::get<0>(ptphiw);
      Real pt      = std::get<1>(ptphiw);
      Real phi     = std::get<2>(ptphiw);
      Real w       = std::get<3>(ptphiw);
      if (pt < 0 || phi < 0 || w < 0) 
	return false;

      l.push_back(ptphiw);

      const int    p   = 3;
      const double max = pow(5,p);
      double       jp  = pow(pt,p) * _f / max;
      if (_j(_e) > jp) return true;

      l.push_back(_s.mirror(ptphiw));

      size_t nj = _jn(_e);
      for (size_t j = 0; j < nj; j++) {
	auto p = _s(psi);
	std::get<2>(p) = phi + _ja(_e);
	l.push_back(p);
	l.push_back(_s.mirror(p));
      }
		   
      return true;
    }
  protected:
    /** Event plane angle distribution */
    std::uniform_real_distribution<> _j;
    /** Number of particles in jet */
    std::poisson_distribution<unsigned short> _jn;
    /** Number of particles in jet */
    std::normal_distribution<> _ja;
    /** Factor */
    Real _f;
  };
}
#endif
//
// EOF
//
