/**
 * @file   correlations/gen/PtPhiW.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Class for sampling Particle PDFs
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef GEN_PTPHIWSAMPLER_HH
#define GEN_PTPHIWSAMPLER_HH
#include <correlations/gen/PhiW.hh>

namespace gen
{
  //====================================================================
  /** 
   * Sample @f$ p_{\mathrm{T}}@f$ and @f$\varphi@f$ distributions. 
   *
   * The transverse momentum is sampled from an exponential distribution 
   *
   * @f[ p_{\mathrm{T}}\sim \operatorname{Exp}[\overline{p_{\mathrm{T}}}]@f]
   *
   * with the mean @f$ p_{\mathrm{T}}@f$ as parameter.  
   *
   * The azimuthal angle is sampled from a Fourier expansion with given
   * harmonic parameters
   *
   * @f[\varphi\sim 1+A\sum_{n} 2v_{n}\cos\left(n[\varphi-\Psi]\right)@f]
   *
   * where the flow parameters @f$ v_{n}@f$ and event plane angle
   * @f$\Psi@f$ are parameters, and the prefactor @f$ A@f$ depends on
   * the selected @f$ p_{\mathrm{T}}@f$ as 
   *
   * @f[ A = a p_{\mathrm{T}} + b p_{\mathrm{T}}^2@f] 
   *
   * where the parameters @f$ a,b@f$ are tuned to the Pb-Pb at @f$
   * 2.76\mathrm{TeV}@f$ results (Phys.Rev.Lett. 105 (2010) 252302) so
   * that the flow parameters @f$ v_{n}@f$ are roughly recovered at the
   * mean @f$ p_{\mathrm{T}}@f$.
   *
   * @image html gen_pt_medium_dist.png
   *
   * @headerfile "" <correlations/gen/PtPhiW.hh>
   * @ingroup gen
   */
  struct PtPhiW : public PhiW
  {
    /** Random number distribution for @f$ p_{\mathrm{T}}@f$ */
    using PtDist  = std::exponential_distribution<>;
    /** Random number distribution for @f$\varphi@f$ */
    using PhiDist = std::piecewise_linear_distribution<>;
    /** List of @f$ \varphi@f$ distributions per @f$ p_{\mathrm{T}}@f$ bin */
    using DistVector = std::vector<PhiDist>;

    /** 
     * Constructor 
     *
     * @param e      Random number engine to use 
     * @param vs     Flow coefficients @f$ v_{n}@f$ at mean 
     *               @f$ p_{\mathrm{T}}@f$ 
     * @param pts    @f$ p_{\mathrm{T}}@f$  bin edges 
     * @param pc     @f$ p_{\mathrm{T}}@f$ modulation coefficients
     * @param meanpt @f$ \overline{p_{\mathrm{T}}}@f$
     * @param nphi   Number of sampling points for @f$\varphi@f$ distributions 
     */
    PtPhiW(RandomEngine& e,
	   std::initializer_list<Real> vs,
	   std::initializer_list<Real> pts,
	   std::initializer_list<Real> pc,
	   Real   meanpt=2,
	   size_t nphi=100)
      : PhiW(e, vs),
	_ptCoeff(pc),       // Coefficients for pT modulation 
	_phiDists(),        // phi distribution
	_ptBins(pts),       // pT bins 
	_ptDist(1/meanpt)  // Exponential with tau=1/meanpt
    {
      init(nphi);
    }
    /** 
     * Constructor 
     *
     * @param e      Random number engine to use 
     * @param vs     Flow coefficients @f$ v_{n}@f$ at mean 
     *               @f$ p_{\mathrm{T}}@f$ 
     * @param pts    @f$ p_{\mathrm{T}}@f$  bin edges 
     * @param pc     @f$ p_{\mathrm{T}}@f$ modulation coefficients
     * @param meanpt @f$ \overline{p_{\mathrm{T}}}@f$
     * @param nphi   Number of sampling points for @f$\varphi@f$ distributions 
     */
    PtPhiW(RandomEngine& e,
	   const RealVector& vs,
	   const RealVector& pts,
	   const RealVector& pc,
	   Real   meanpt=2,
	   size_t nphi=100)
      : PhiW(e, vs, nphi),
	_ptCoeff(pc),        // Coefficients for pT modulation 
	_phiDists(),	     // phi distribution		      
	_ptBins(pts),	     // pT bins 			      
	_ptDist(1/meanpt)    // Exponential with tau=1/meanpt  
    {
      init(nphi);
    }
    /** Copy constructor */
    PtPhiW(const PtPhiW&) = delete;
    /** Move constructor */
    PtPhiW(PtPhiW&& o)
      : PhiW(std::move(o)),
	_ptCoeff(std::move(o._ptCoeff)),
	_phiDists(std::move(o._phiDists)),
	_ptBins(std::move(o._ptBins)),
	_ptDist(std::move(o._ptDist))
    {}
		 
    /** 
     * Fill in header information 
     *
     * @param h Header to fill into 
     */
    virtual void fill(Header& h) const
    {
      PhiW::fill(h);
      h._ptbin  = _ptBins;
      h._ptcoef = _ptCoeff;
      h._meanpt = 1 / _ptDist.lambda();
    }
  protected:
    /** 
     * Initialise the random number generators 
     *
     * @param nphi Number of sampling points along @f$\varphi@f$ for the PDF 
     */
    void init(size_t nphi)
    {
      for (auto pt : _ptBins) {
	auto f = [this,pt](Real phi) { return pdf(phi,pt); };
	_phiDists.emplace_back(nphi, 0, 2 * M_PI, f);
      }
    }
    /**
     * The @f$\varphi@f$ probability density function 
     *
     * @f[ 1+A(p_{\mathrm{T}})\sum_{n} 2v_{n}\cos\left(n[\varphi-\Psi]\right)@f]
     *
     * @param phi @f$\varphi@f$ 
     * @param pt @f$ p_{\mathrm{T}}@f$ 
     *
     * @return Probability 
     */
    Real pdf(Real phi, Real pt) const
    {
      Real f   = 0;
      for (size_t i = 0; i < _ptCoeff.size(); i++)
	f += _ptCoeff[i] * pow(pt,i);

      return 1 + f*(phiPdf(phi)-1);
    }
    /** 
     * Generate transverse momentum @f$ p_{\mathrm{T}}@f$
     *
     * @return @f$ p_{\mathrm{T}}@f$
     */
    virtual Real genPt(Real) { return _ptDist(_e); }
    /** 
     * Generate azimuthal angle @f$ \varphi@f$ 
     *
     * @return @f$ \varphi@f$ 
     */
    virtual Real genPhi(Real, Real pt)
    {
      auto pti = std::lower_bound(std::begin(_ptBins),std::end(_ptBins),pt);
      if (pti == std::end(_ptBins) || pt < _ptBins[0]) return -1;

      auto    idx = std::max(std::distance(std::begin(_ptBins), pti)-1,0l);
      Real    phi = _phiDists[idx](_e);
      return phi;
    }
    /** Coefficients of the transverse momentum dependence */
    RealVector _ptCoeff;
    /** Randon number distributions for each transverse momentum bin */
    DistVector _phiDists;
    /** Transverse momentum bin edges */
    RealVector _ptBins;
    /** The distribution of transverse momentum */
    PtDist     _ptDist;
  };
}
#endif
//
// EOF
//
