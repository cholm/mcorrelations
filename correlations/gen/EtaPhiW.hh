/**
 * @file   correlations/gen/EtaPtPhiW.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Tue Mar 13 15:23:24 2018
 * 
 * @brief Class for sampling Particle PDFs
 */
/*
 * Multi-particle correlations 
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 * 
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your oetaption) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#ifndef GEN_ETAPHIWSAMPLER_HH
#define GEN_ETAPHIWSAMPLER_HH
#include <correlations/gen/PhiW.hh>

namespace gen
{
  //====================================================================
  /** 
   * Sample @f$ \eta@f$, @f$ p_{\mathrm{T}}@f$, and @f$\varphi@f$
   * distributions.
   *
   * The pseudorapidity is sampled from a inverse triangular distribution 
   *
   * @f[\eta\sim a|\eta|+b@f]
   *
   * over the range @f$ -1\leq\eta\leq 1@f$ with @f$ a@f$ as a
   * parameter, and @f$ b=(1-a)/2 @f$, so that the integral over the
   * sampled range is 1.
   *
   * The transverse momentum is sampled from an exponential distribution 
   *
   * @f[ p_{\mathrm{T}}\sim \operatorname{Exp}[\overline{p_{\mathrm{T}}}]@f]
   *
   * with the mean @f$ p_{\mathrm{T}}@f$ as parameter.  
   *
   * The azimuthal angle is sampled from a Fourier expansion with given
   * harmonic parameters
   *
   * @f[\varphi\sim 1+A\sum_{n} 2v_{n}\cos\left(n[\varphi-\Psi]\right)@f]
   *
   * where the flow parameters @f$ v_{n}@f$ and event plane angle
   * @f$\Psi@f$ are parameters, and the prefactor @f$ A@f$ depends on
   * the selected @f$ p_{\mathrm{T}}@f$ as 
   *
   * @f[ A = a p_{\mathrm{T}} + b p_{\mathrm{T}}^2@f] 
   *
   * where the parameters @f$ a,b@f$ are tuned to the Pb-Pb at @f$
   * 2.76\mathrm{TeV}@f$ results (Phys.Rev.Lett. 105 (2010) 252302) so
   * that the flow parameters @f$ v_{n}@f$ are roughly recovered at the
   * mean @f$ p_{\mathrm{T}}@f$.
   *
   * @image html gen_eta_medium_dist.png
   *
   *
   * @headerfile "" <correlations/gen/EtaPtPhiW.hh>
   * @ingroup gen
   */
  struct EtaPhiW : public PhiW
  {
    /** Random number distribution for @f$\eta@f$ */
    using EtaDist = std::piecewise_linear_distribution<>;

    /** 
     * Constructor 
     *
     * @param e      Random number engine to use 
     * @param vs     Flow coefficients @f$ v_{n}@f$ 
     * @param etaA   Slope of @f$\eta@f$ distribution 
     * @param nphi   Number of sampling points for @f$\varphi@f$ distributions 
     */
    EtaPhiW(RandomEngine& e,
	    std::initializer_list<Real> vs,
	    Real   etaA=.1,
	    size_t nphi=100)
      : PhiW(e, vs, nphi),
	_etaDist({-1.,0.,1},
		 [etaA](Real x){return etaA*std::fabs(x)+(1-etaA)/2;})
    {
    }
    /** 
     * Constructor 
     *
     * @param e      Random number engine to use 
     * @param vs     Flow coefficients @f$ v_{n}@f$
     * @param etaA   Slope of @f$\eta@f$ distribution 
     * @param nphi   Number of sampling points for @f$\varphi@f$ distributions 
     */
    EtaPhiW(RandomEngine& e,
	    const         RealVector& vs,
	    Real          etaA=.1,
	    size_t        nphi=100)
      : PhiW(e, vs, nphi),
	_etaDist({-1.,0.,1},
		 [etaA](Real x){return etaA*std::fabs(x)+(1-etaA)/2;})
    {
    }
    /** Copy constructor */
    EtaPhiW(const EtaPhiW&) = delete;
    /** Move constructor */
    EtaPhiW(EtaPhiW&& o)
      : PhiW(std::move(o)),
	_etaDist(std::move(o._etaDist))
    {}
  protected:
    /** 
     * Generate pseudorapidity @f$ \eta@f$
     *
     * @return @f$ \eta@f$
     */
    virtual Real genEta() { return _etaDist(_e); }
    /** The distribution of pseudorapidity */
    EtaDist     _etaDist;
  };
}
#endif
//
// EOF
//
