#ifndef CORRELATIONS_FROMQVECTOR_HH
#define CORRELATIONS_FROMQVECTOR_HH
/**
 * @file   correlations/FromQVector.hh
 * @author Christian Holm Christensen <cholm@nbi.dk>
 * @date   Thu Oct 24 23:45:40 2013
 *
 * @brief  Base class for cumulant code
 */
/*
 * Multi-particle correlations
 * Copyright (C) 2013 K.Gulbrandsen, A.Bilandzic, C.H. Christensen.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 */
#include <correlations/QStore.hh>
#include <correlations/Correlator.hh>
#include <functional>

namespace correlations
{
  //____________________________________________________________________
  /**
   * Base class to calculate Cumulants of a given Q vector.
   *
   * See also @ref doc/_Overview.md
   *
   @code
   correlations::Result          r;
   correlations::HarmonicVector  h{n,n,-n,-n};
   correlations::QVector<>       q(h);
   correlations::FromQVector<>   c(q);
   
   while (moreEvents) {
     q.reset();
   
     while (moreObservations) {
       correlations::Real phi    = NextObservation();
       correlations::Real Weight = GetWeight(phi);
   
       q.fill(phi, weight);
     }
   
     r += c.calculate(h);
   }
   std::cout << r.eval() << std::endl;
   @endcode
   *
   * @tparam Harmonics The type of harmonic vector to use 
   *
   * @headerfile ""  <correlations/FromQVector.hh>
   * @ingroup correlations 
   */
  template <typename Harmonics=HarmonicVector>
  struct FromQVector : public Correlator<Harmonics>
  {
    /** Base type */
    using Base=Correlator<Harmonics>;
    /** The type of vectors of harmonics to use */
    using HarmonicVector=typename Base::HarmonicVector;
    /** The harmonics to use */
    using HarmonicElement=typename Base::HarmonicElement;
    /** The Q-vector to use */
    using QVector=correlations::QVector;
    /** The complex type */
    using Complex=typename QVector::Complex;
    /** Storage type */
    using QStore=correlations::QStore<HarmonicVector>;

    /** Destructor */
    virtual ~FromQVector() {}
  protected:
    /**
     * Constructor
     *
     * @param q Storage of @f$ Q@f$-vectors
     */
    FromQVector(const QStore& q)
      : Base(),
	_s(q)
    {}
    /** 
     * Move constructor 
     *
     * @param o Object to move from
     */
    FromQVector(FromQVector&& o)
      : Base(std::move(o)),
	_s(std::move(o._s))
    {}
    /** 
     * Delete copy constructor 
     */
    FromQVector(const FromQVector&) = delete;
    /** 
     * Delete assignment operator 
     */
    FromQVector& operator=(const FromQVector&) = delete;
    /** 
     * Get @f$ Q@f$-vector component @f$ r_{h,p}@f$. 
     *
     * @param h @f$ h@f$ 
     * @param p @f$ p@f$ 
     *
     * @return @f$ r_{h,p}@f$. 
     */
    const Complex _r(const HarmonicElement& h, Power p) const
    {
      return _s.r(h,p);
    }
    /** 
     * Get @f$ Q@f$-vector component @f$ p_{h,p}@f$. 
     *
     * @param h @f$ h@f$ 
     * @param p @f$ p@f$ 
     *
     * @return @f$ p_{h,p}@f$. 
     */
    const Complex _p(const HarmonicElement& h, Power p) const
    {
      return _s.p(h,p);
    }
    /** 
     * Get @f$ Q@f$-vector component @f$ q_{h,p}@f$. 
     *
     * @param h @f$ h@f$ 
     * @param p @f$ p@f$ 
     *
     * @return @f$ q_{h,p}@f$. 
     */
    const Complex _q(const HarmonicElement& h, Power p) const
    {
      return _s.q(h,p);
    }
    /**
     * Calculate the @f$ m@f$-particle correlation using harmonics
     * @f$\mathbf{h}@f$.
     *
     * For integrated observations (i.e., @f$Q=p=r=q@f$)
     *
     * @f[ C_{\mathbf{h}} = \frac{N_{\mathbf{h}}}{D_{\mathbf{h}}}\quad,@f]
     * 
     * and for differential observations 
     *
     * @f[ C_{\mathbf{h}} = \frac{N'_{\mathbf{h}}}{D'_{\mathbf{h}}}\quad.@f]
     * 
     * @param m @f$ m@f$ - How many particles to correlate
     * @param h @f$\mathbf{h}@f$ - Harmonic of each term
     *
     * @return The correlator and the summed weights
     */
    virtual Result cN(const Size m, const HarmonicVector& h) const
    {
      static HarmonicVector null(0);
      if (null.size() <= h.size()) null.resize(h.size(), 0);

      return Result(eval(m, h), eval(m, null).real());
    }
    /** 
     * The real evaluator 
     *
     * @param m number of particles to correlate 
     * @param h Harmonic coefficients 
     *
     * @return the @f$ m@f$-particle correlator 
     */
    virtual Complex eval(const Size m, const HarmonicVector& h) const = 0;
    /** Storage of Q-vectors */
    QStore _s;
  };
  template <>
  Result
  FromQVector<PartitionHarmonicVector>::cN(const Size m,
					   const HarmonicVector& h) const
  {
    static HarmonicVector null(0);
    if (null.size() <= h.size()) null.resize(h.size(), HarmonicElement());
    for (size_t i = 0; i < h.size(); i++)
      null[i] = HarmonicElement(0,h[i].partition());

    return Result(eval(m, h), eval(m, null).real());
  }
}
#endif
// Local Variables:
//  mode: C++
// End:
