from sympy import *
init_printing()

cm, dm = symbols("c_m d_m",real=True)
m = symbols("m",int=True,positive=True)
M = symbols("M_m",real=True,positive=True)

vm   = ((-1)**(m/2-1)*1/M*cm)**(1/m)
vpm  = dm / cm * vm
vpm2 = dm / ((1/M)**(-1/m) * ((-1)**(m/2-1)*cm)**(1 - 1/m)) #/ M**(1/(m/2))
vpm4 = (-1)**(m/2-1) * 1 / M**(1/m) * dm / ((-1)**(m/2-1)*cm)**(1 - 1/m)
vpm3 = dm / ((-1)**(m/2-1)*1/M*cm)**(1-1/m) / M * (-1)**(m/2-1)

x = vpm3
r = x / vm / dm * cm
t = (vpm / x).simplify()

for i in [2,4,6,8]:
    print('=== {} ==='.format(i))
    pprint((vpm.subs({m:i}),
            vpm2.subs({m:i}),
            vpm3.subs({m:i}),
            (vpm4 / vpm).subs({m:i}).simplify(),
            r.subs({m:i}).simplify(),
            t.subs({m:i}).simplify()))

print('=== {} ==='.format('r'))
pprint(r.simplify())
print('=== {} ==='.format('t'))
pprint(t.simplify())
print('=== {} ==='.format('t'))
