import numpy as np
import math
import matplotlib.pyplot as plt
import scipy as sc
import scipy.special

plt.ion()

def fac(known=[1,1,4,33, 456, 9460, 274800, 10643745]):

    fig = plt.figure(num='plot',clear=True)
    ns = range(2,2*len(known)+2,2)
    plt.plot(ns,known,label='Knonwn')
    plt.yscale('log')
    y = [0]*len(ns)
    
    for j, f in enumerate(known):
        k = j + 1
        n = 2 * k
        #m = n
        
        #tt = math.factorial(m)/(math.factorial(m-2*k)*math.factorial(k)**2)
        # t  = int(math.factorial(k)**2)
        # tt = int(t/k**2)-
        # tt   = sc.special.binom(n,n)*sc.special.binom(n, k)
        # tt = math.factorial(n-2)/sc.special.binom(n,n-1) if k > 2 else 1
        # tt = sc.special.binom(n,n-2)
        t    = 0
        tt   = math.factorial(k)**2 / (2*k)
        y[j] = tt;
        s = (-1)**(k-1)

        print('{:3d} {:3d}: s={:+2d} {:10} {:10} {:10} {:10}'
              .format(n,k,int(s),t,tt,f,tt/f))

    plt.plot(ns,y,label='Computed')
    plt.legend()
    
if __name__ == "__main__":
    fac() 

        
        
