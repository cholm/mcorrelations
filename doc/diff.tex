\documentclass[11pt]{article}

\usepackage[a4paper,margin=2cm]{geometry}
\usepackage{amsmath}
\usepackage{amstext}
\usepackage{amssymb}
\usepackage{xcolor}
\usepackage[bookmarks,colorlinks=true,linkcolor=blue!70!black]{hyperref}
\newcommand\cN[1]{\ensuremath N_{#1}}
\newcommand\cD[1]{\ensuremath D_{#1}}
\newcommand\Qv[2]{\ensuremath Q_{#1}}
\newcommand\pv[2]{\ensuremath p_{#1}}
\newcommand\qv[2]{\ensuremath q_{#1}}
\newcommand\rv[2]{\ensuremath r_{#1}}
\begin{document}

\section{General $m$-particle correlators}

Let us define the harmonic vector
\begin{align}
  \label{eq:harmonicvector}
  \mathbf{h} &= \left(h_1,h_2,\ldots,h_m\right)\quad h_i\in\mathbb{Z}
\end{align}
and the vector of observations
\begin{align}
  \label{eq:obsvector}
  \boldsymbol\varphi_{k_1,\ldots,k_m}
  &= \left(\varphi_{k_1},\varphi_{k_2},\ldots,\varphi_{k_m}\right)\quad,
\end{align}
where $k_1,\ldots,k_n$ labels different observations, and both vectors
are of size $m$.  We then have that
\begin{align}
  \langle m'\rangle_{\mathbf{h}}
  &= \frac{\sum^{M}_{k_1,\ldots,k_m} w_{k_1}\cdots
    w_{k_m}
    e^{\mathbf{h}\bullet\boldsymbol\varphi_{k_1,\ldots,k_m}}}{
    \sum_{k_1,\ldots,k_m}^{M} w_{k_1}w_{k_2}\cdots
    w_{k_m}}\quad\text{with}\ k_1\neq k_2\neq \ldots \neq
    k_m\notag\\
  \label{eq:<m'>}
  &= \frac{\cN{\mathbf{h}}}{\cD{\mathbf{h}}}
    = \frac{\cN{\mathbf{h}}}{\cN{\mathbf{0}}}
    \quad,
\end{align}
where the sums run over all unique tuples of $m$ particles, meaning no
two particles are the same, and where we in the last equations have
identified the numerator $\cN{\mathbf{h}}$ and denominator
$\cD{\mathbf{h}}=\cN{\mathbf{0}}$.

Following the developments of Bilandzic \emph{et
  al}~\cite{prc89.064904}, we can write the numerator and denominator
of \eqref{eq:<m'>} in terms of the $Q$-vector defined by
\begin{align}
  \label{eq:Qvector}
  Q_{h,p} &= \sum_{k=1}^{M}w_k^pe^{ih\varphi_k}
\end{align}
for specific number of harmonics (i.e., dimension of the $\mathbf{h}$
vector).   We will slightly alter this definition to accommodate a
more compact notation and write
\begin{align}
  \label{eq:Qvector'}
  \Qv{\mathbf{h}}{}
  &= Q_{\sum h_i,\mathrm{dim}(\mathbf{h})}
    = \sum_{k=1}^{M}w_k^{\mathrm{dim}(\mathbf{h})}e^{i\varphi_k\sum h_i}
\end{align}
where $\mathrm{dim}(\mathbf{h})$ gives the number of dimensions of the
vector $\mathrm{h}$.  Using the notation $a||b$ to mean concatenation
of vectors, so that
\begin{align*}
  \left[
  \begin{array}{c}
    a_1\\
    \vdots\\
    a_n
  \end{array}
  \right]
  ||
  \left[
  \begin{array}{c}
    b_1\\
    \vdots\\
    b_m
  \end{array}
  \right]
  &=
    \left[
    \begin{array}{c}
      a_1\\
      \vdots\\
      a_n\\
      b_1\\
      \vdots\\
      b_m
  \end{array}
  \right]\quad,
\end{align*}
we can rewrite the explicit expressions for $\cN{\mathrm{h}}$ up to 4
dimensions as
\begin{align}
  \cN{h_1} &= \Qv{h_1}{1}\notag\\
  \cN{h_1,h_2}
  &= \Qv{h_1}{1}\Qv{h_2}{1}-\Qv{h_1||h_2}{2}\notag\\
  \cN{h_1,h_2,h_3}
  &= \Qv{h_1}{1} \Qv{h_2}{1} \Qv{h_3}{1}
    -\Qv{h_1||h_2}{2} \Qv{h_3}{1}
    -\Qv{h_1}{1} \Qv{h_2||h_3}{2}
    -\Qv{h_2}{1} \Qv{h_1||h_3}{2}
    +2\Qv{h_1||h_2||h_3}{3}\notag\\
  &=
    \cN{h_1,h_2}\Qv{h_3}{1}
    -\Qv{h_1}{1}\Qv{h_2||h_3}{2}
    -\Qv{h_2}{1}\Qv{h_1||h_3}{2}
    +2\Qv{h_1||h_2||h_3}{3}\notag\\
  &= \cN{h_1,h_2}\Qv{h_3}{1}
    -\cN{h_1,h_2||h_3}
    -\cN{h_2,h_1||h_3}\notag\\
  \cN{h_1,h_2,h_3,h_4}
  &=
    \Qv{h_1}{1} \Qv{h_2}{1} \Qv{h_3}{1} \Qv{h_4}{1}
    - \Qv{h_1||h_2}{2} \Qv{h_3}{1} \Qv{h_4}{1}
    - \Qv{h_2}{1} \Qv{h_1||h_3}{2} \Qv{h_4}{1}
    - \Qv{h_1}{1} \Qv{h_2||h_3}{2} \Qv{h_4}{1}
    + 2 \Qv{h_1||h_2||h_3}{3} \Qv{h_4}{1}\notag\\
  &\quad
    - \Qv{h_1}{1} \Qv{h_2}{1} \Qv{h_3||h_4}{2}
    + \Qv{h_1||h_2}{2} \Qv{h_3||h_4}{2}\notag\\
  &\quad
    - \Qv{h_1}{1} \Qv{h_3}{1} \Qv{h_2||h_4}{2}
    + \Qv{h_1||h_3}{2} \Qv{h_2||h_4}{2}\notag\\
  &\quad
    - \Qv{h_2}{1} \Qv{h_3}{1} \Qv{h_1||h_4}{2}
    + \Qv{h_2||h_3}{2} \Qv{h_1||h_4}{2}\notag\\
  &\quad
    + 2 \Qv{h_3}{1} \Qv{h_1||h_2||h_4}{3}
    + 2 \Qv{h_2}{1} \Qv{h_1||h_3||h_4}{3}
    + 2 \Qv{h_1}{1} \Qv{h_2||h_3||h_4}{3}\notag\\
  &\quad
    - 6 \Qv{h_1||h_2||h_3||h_4}{4}\notag\\
  &=
    \cN{h_1,h_2,h_3}\Qv{h_4}{1}
    -\cN{h_1,h_2,h_3||h_4}
    -\cN{h_1,h_3,h_2||h_4}
    -\cN{h_2,h_3,h_1||h_4}\quad.
    \label{eq:Nout}
\end{align}
Note, that we concatenate vectors in the sub-scripts of $\cN{}$, so
that
\begin{align*}
  \cN{h_1,h_2,h_3||h_4}
  &= \cN{h_1,h_2}\Qv{h_3||h_4}{1}
    -\cN{h_1,h_2||h_3||h_4}
    -\cN{h_2,h_1||h_3||h_4}\\
  &=
    \Qv{h_1}{1}\Qv{h_2}{1}\Qv{h_3||h_4}{2}-\Qv{h_1||h_2}{2}\Qv{h_3||h_4}{2}\\
  &\quad-\Qv{h_1}{1}\Qv{h_2||h_3||h_4}{3}+\Qv{h_1||h_2||h_3||h_4}{4}\\
  &\quad-\Qv{h_2}{1}\Qv{h_1||h_3||h_4}{3}+\Qv{h_1||h_2||h_3||h_4}{4}\quad,
\end{align*}
and we recognise the contributions to $\cN{h_1,h_2,h_3,h_4}$.   Note,
that the complex conjugate of our $Q$-vector is simply given by
\begin{align*}
  Q^*_{h,p}=Q_{-h,p}
\end{align*}
and thus we need to calculate our $Q$-vector up to the harmonic
$H=\sum^p_{i=1} h_i$ and the power $p=\mathrm{dim}(\mathbf{h})$ for
a given harmonic vector $\mathbf{h}$.

It is easy to see how the equations in \eqref{eq:Nout} generalises to
higher orders of $\mathbf{h}$.  In general, we calculate the product
of the one-lower $\cN{}$ and the last $Q$ vector, and then subtract
off all one-lower $\cN{}$ of all possible combinations with this last
$Q$-vector. 

\section{Differential $m$-particle correlators}

Again, following the developments of A.Bilandzic \emph{et
  al}~\cite{prc89.064904} we can define \emph{differential}
correlators by redefining our $Q$-vectors.  First, let us define the
$Q$-vector of particles-of-interest (POI) as
\begin{align}
  \label{eq:pvector}
  p_{\mathrm{h}}
  &= \sum_{k\in P}w^{\mathrm{dim}(\mathbf{h})}e^{i\varphi_k\sum h_i}\quad,
\end{align}
where $k\in P$ selects all particles in the set $P$.  Next, we define
the $Q$-vector of reference particles (RP) as
\begin{align}
  \label{eq:rvector}
  r_{\mathrm{h}}
  &= \sum_{k\in R}w^{\mathrm{dim}(\mathbf{h})}e^{i\varphi_k\sum h_i}\quad,
\end{align}
where $k\in R$ selects all particles in the set $R$.  This set of
particles typically represents a large sample so that $|R|>|P|$.
Finally, we define a $Q$-vector for all particles that are \emph{both}
considered reference particles and particles of interest
\begin{align}
  \label{eq:qvector}
  q_{\mathrm{h}}
  &= \sum_{k\in P\cap R}w^{\mathrm{dim}(\mathbf{h})}e^{i\varphi_k\sum h_i}\quad,
\end{align}
where $k\in P\cap R$ selects all particles that are \emph{both} in $P$
\emph{and} $R$.   This set may be empty in which case all
$q_{\mathrm{h}}=0$.

Let us write our the differential correlators
\begin{align}
  \cN{h_1,h_2}^p
  &= \pv{h_1}{1}\rv{h_2}{1} - \qv{h_1||h_2}{2}\notag\\
  \cN{h_1,h_2,h_3}^p
  &= \pv{h_1}{1}\rv{h_2}{1}\rv{h_3}{1}
    - \qv{h_1||h_2}{2}\rv{h_3}{1}
    - \pv{h_1}{1}\rv{h_2||h_3}{2}
    - \qv{h_1||h_3}{2}\rv{h_2}{1} 
    + 2\qv{h_1||h_2||h_3}{3}\notag\\
  &= \cN{h_1,h_2}^p\rv{h_3}{1} - \cN{h_1,h_2||h_3}^p-\cN{h_1||h_3,h_2}^q\quad.
    \label{eq:ndiff3}
\end{align}
Let us stop here and consider the above result for a moment.  The
parallel to \eqref{eq:Nout} is striking.  However, we note that we
must take care which $Q$-vector we evaluate when.  In the above
\begin{align*}
  \cN{h_1,h_2}^q
  &= \qv{h_1}{1}\rv{h_2}{1} - \qv{h_1||h_2}{2}\notag\quad,
\end{align*}
which is not the same as
\begin{align*}
  \cN{h_1,h_2}^p&=\pv{h_1}{1}\rv{h_2}{1} - \qv{h_1||h_2}{2}\quad.  
\end{align*}
Continuing with the
4 particle expression, we get
\begin{align}
  \cN{h_1,h_2,h_3,h_4}^p
  &= \pv{h_1}{1} \rv{h_2}{1} \rv{h_3}{1} \rv{h_4}{1}
    - \qv{h_1||h_2}{2} \rv{h_3}{1} \rv{h_4}{1}
    - \qv{h_1||h_3}{2} \rv{h_2}{1} \rv{h_4}{1}
    - \pv{h_1}{1} \rv{h_2||h_3}{2} \rv{h_4}{1}
    + 2 \qv{h_1||h_2||h_3}{3} \rv{h_4}{1}\notag\\
  &\quad
    - \qv{h_1||h_4}{2} \rv{h_2}{1} \rv{h_3}{1} 
    + \qv{h_1||h_4}{2} \rv{h_2||h_3}{2} 
    - \pv{h_1}{1} \rv{h_3}{1} \rv{h_2||h_4}{2}\notag\\
  &\quad
    + \qv{h_1||h_3}{2} \rv{h_2||h_4}{2}
    + 2 \qv{h_1||h_2||h_4}{3}\rv{h_3}{1} 
    - \pv{h_1}{1} \rv{h_2}{1} \rv{h_3||h_4}{2}\notag\\
  &\quad
  + \qv{h_1||h_2}{2} \rv{h_3||h_4}{2}
  + 2 \qv{h_1||h_3||h_4}{3} \rv{h_2}{1} 
  + 2 \pv{h_1}{1} \rv{h_2||h_3||h_4}{3}
    - 6 \qv{h_1||h_2||h_3||h_4}{4}\notag\\
  &= \cN{h_1,h_2,h_3}^p\rv{h_4}{1}
    - \cN{h_1,h_2,h_3||h_4}^p
    - \cN{h_1,h_3,h_2||h_4}^p
    - \cN{h_1||h_4,h_2,h_3}^q\quad.
    \label{eq:ndiff4}
\end{align}
It is worth nothing the differences between \eqref{eq:Nout} and
\eqref{eq:ndiff3} and \eqref{eq:ndiff4}.  In \eqref{eq:Nout} the
generalisation to higher dimensions of $\mathbf{h}$ is relatively
easy, while in \eqref{eq:ndiff3} and \eqref{eq:ndiff4} one must be
specific about the evaluation of the lower order $\cN{}$. That is,
every time we have $h_1$ un-concatenated with other $h_x$ we can use
$\cN{h_1,\ldots}^p$, but if $h_1$ is concatenated we must use
$\cN{h_1||\ldots,\ldots}^q$. 

To see that \eqref{eq:Nout} is a special case of \eqref{eq:ndiff3} and
\eqref{eq:ndiff4} we consider the case where $P=R=Q$ so that the
$Q$-vectors $p=q=r$
\begin{align}
  \cN{h_1,h_2}^p
  &= \pv{h_1}{1}\pv{h_2}{1} - \pv{h_1||h_2}{2}\notag\\
  \cN{h_1,h_2,h_3}^p
  &= \pv{h_1}{1}\pv{h_2}{1}\pv{h_3}{1}
    - \pv{h_1||h_2}{2}\pv{h_3}{1}
    - \pv{h_1}{1}\pv{h_2||h_3}{2}
    - \pv{h_1||h_3}{2}\pv{h_2}{1} 
    + 2\pv{h_1||h_2||h_3}{3}\notag\\
  &= \cN{h_1,h_2}^p\pv{h_3}{1} -
    \cN{h_1,h_2||h_3}^p-\cN{h_1||h_3,h_2}^p\notag\\
  \cN{h_1,h_2,h_3,h_4}^p
  &= \pv{h_1}{1} \pv{h_2}{1} \pv{h_3}{1} \pv{h_4}{1}
    - \pv{h_1||h_2}{2} \pv{h_3}{1} \pv{h_4}{1}
    - \pv{(h_1,h_3}{2} \pv{h_2}{1} \pv{h_4}{1}
    - \pv{h_1}{1} \pv{h_2||h_3}{2} \pv{h_4}{1}
    + 2 \pv{h_1||h_2||h_3}{3} \pv{h_4}{1}\notag\\
  &\quad
    - \pv{h_1||h_4}{2} \pv{h_2}{1} \pv{h_3}{1} 
    + \pv{h_1||h_4}{2} \pv{h_2||h_3}{2} 
    - \pv{h_1}{1} \pv{h_3}{1} \pv{h_2||h_4}{2}\notag\\
  &\quad
    + \pv{h_1||h_3}{2} \pv{h_2||h_4}{2}
    + 2 \pv{h_1||h_2||h_4}{3}\pv{h_3}{1} 
    - \pv{h_1}{1} \pv{h_2}{1} \pv{h_3||h_4}{2}\notag\\
  &\quad
  + \pv{h_1||h_2}{2} \pv{h_3||h_4}{2}
  + 2 \pv{h_1||h_3||h_4}{3} \pv{h_2}{1} 
  + 2 \pv{h_1}{1} \pv{h_2||h_3||h_4}{3}
    - 6 \pv{h_1||h_2||h_3||h_4}{4}\notag\\
  &= \cN{h_1,h_2,h_3}^p\pv{h_4}{1}
    - \cN{h_1,h_2,h_3||h_4}^p
    - \cN{h_1,h_3,h_2||h_4}^p
    - \cN{h_1||h_4,h_2,h_3}^p\quad,
  \label{eq:ndiff_special}
\end{align}
which of course is the same as \eqref{eq:Nout} with $Q=p$. 

\begin{thebibliography}{}
  
\bibitem{prc89.064904} A.Bilandzic, \emph{et al}, Generic
  framework for anisotropic flow analyses with multiparticle
  azimuthal correlations, \textit{Phys.Rev.}\textbf{C89} (2014)
  064904. 
\end{thebibliography}
\end{document}
%% Local Variables:
%% ispell-dictionary: "british"
%% End:

%  LocalWords:  correlators
