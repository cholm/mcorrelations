# Using the framework for symbolic calculations 

In general, the code is set up to use the types correlations::Real,
correlations::Complex, correlations::Harmonic, correlations::Power,
and correlations::Size which are normally type defined to be aliases
of `double`, `std::complex<double>`, `short`, `unsigned short`, and
`unsigned short`, respectively.  That is, we have

- correlations::Real represents $`\mathbb{R}`$ 
- correlations::Complex represents $`\mathbb{C}`$ 
- correlations::Harmonic represents $`\mathbb{Z}`$ 
- correlations::Power and correlations::Size both represent
  $`\mathbb{N}_0`$

However, by redefining these types, in particular correlations::Real,
correlations::Complex, and correlations::Harmonic, we can turn the
_numeric_ calculations into _symbolic_ calculations, and we can use
some external Computer Algebra System (CAS, such as [Maxima][maxima],
[Mathematica][mathematica], [Maple][maple], [SageMath][sagemath], or
[SymPy][sympy]) to evaluate these expression and otherwise manipulate
them.

We do this by defining the templated base class
correlations::symbolic::Number which holds a string representation
of a given type of number, and then derive particular types from this.
We then define all standard operations (and some specific functions)
on these types to return a new string representation of that operation
(or function).

The code lives in the namespace correlation::symbolic, so all we
need to do is to include the relevant header files.  These header
files, in particular `correlations/symbolic/QVector.hh`, before any
other header file from this framework.  The headers then contains the
declarations

	namespace correlation {
		using symbolic::Real;
		using symbolic::Complex;
		using symbolic::Harmonic;
		using symbolic::QVector;
	}

thus allowing us to use the rest of the code unmodified, and without
penalty when doing numeric computations.

## Example program 

The program 

- [symbolic.cc](correlations/progs/symbolic.cc) is an example which
  symbolically evaluates the $`Q`$-vector expression up to some number
  of particles using all three available methods. The expressions are
  then compared for equality.
  
  Currently, the program can write output suitable for
  [Maxima][maxima], [SymPy][sympy] (either regular Python or as a
  Jupyter notebook), and [Sage][sagemath] (again, either as regular
  script or as Jupyter notebook).  The format is selected by the `-f`
  option which can be given one of `maxima`, `sympy`, `jsympy`,
  `sage`, or `jsage` as argument.  One can often pass the output
  directly to the interpreter i.e., 
  
	  ./symbolic -f maxima | maxima 
	  ./symbolic -f sympy  | python 
	  ./symbolic -f sage   | sage 
  
  (this is what the `symtest` _Makefile_ target does), or one can
  redirect the output to a file
  
	  ./symbolic -f jsympy > jsympy.ipynb 
	  ./symbolic -f jsage  > jsage.ipynb 

  for example for opening in later in a Jupyter session.
  
[maxima]: http://maxima.sourceforge.net/
[mathematica]: https://www.wolfram.com/mathematica/
[maple]: https://www.maplesoft.com/products/Maple/
[sagemath]: http://www.sagemath.org/
[sympy]: http://www.sympy.org/en/index.html
