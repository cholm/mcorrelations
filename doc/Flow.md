# Practical use of Generic Framework

## Q-vectors 

The first thing we need for our analysis of data is to define the
`$Q$`-vectors we will use. Exactly which `$Q$`-vectors we need depends
on the kind of analysis we will do 

### Integrated Q-vector

For integrated flow we need only one `$Q$`-vectors.  This we define
by 

~~~~~~{.cc}
correlations::QVector q(maxN, maxM, useWeights) 
~~~~~~

where `maxN` is the maximum harmonic order (i.e., the largest $`n`$
in $`v_n\{m\}`$) we want to calculate, and `maxM` is the maximum
number of particles (i.e., the largest $`m`$ in $`v_n\{m\}`$) we
want to correlate.
  
`useWeigts` should evaluate to `true` if we want to use weights.
Otherwise we should set this to `false`.

### Differential Q-vectors

For differential measurements without partitioning (sub-events), we
need three distinct $`Q`$ vectors
  
- $`p`$ for particles-of-interest (POI).  That is, the particles on
  which we will use some property to make the differential
  measurement of flow (for example, $`p_{\mathrm{T}}`$ or $`\eta`$).
    
- $`r`$ for reference particles.  These particles are the ones we
  will correlate the particles-of-interest to. 
    
- $`q`$ for the overlap between particles-of-interest and reference
  particles. That is, particles which we both use for the
  differential measurement and for correlating to. 
    
We can declare these $`Q`$-vectors as 

~~~~~~{.cc}
correlations::QVector p(maxN, maxM, useWeights);
correlations::QVector r(maxN, maxM, useWeights);
correlations::QVector q(maxN, maxM, useWeights);
~~~~~~

with the same meaning of `maxN`, `maxM`, and `useWeights` as above. 
  
We must of course have one such set for each "bin" of the
differential independent variable (e.g., $`p_{\mathrm{T}}`$) - or at
the very least, one for each of $`p`$ and $`q`$ - but here we will
do one example only.  Later on, we will see how the framework allows
for a higher level abstraction and easier application.

## Correlators 

To calculate the $`m`$-particle correlators as explain above, we need
to make a correlations::Correlator object.  Here, we have a number of
choices (see also [Methods](Methods)).  Here, as an illustration, we
will opt for the `correlations::Recursive` method.  

To make the correlator, we first need to create a storage of our
$`Q`$-vectors.  This is done by making an object of the class
`correlations::QStore`.  Exactly how that object is instantised
depends on the type of flow we are calculating.

### Integrated storage and correlator

For integrated flow 

~~~~~~{.cc}
correlations::QStore s(q,q,q);
~~~~~~

Now we can create our `correlations::Correlator` object and pass the
storage to that 

~~~~~~{.cc}
correlations::Recursive<> c(s);
~~~~~~

This object will calculate the $`m`$-particle correlators
$`C_{\mathbf{h}}`$

### Differential storage and correlators 

For differential flow we need two stores - one for the differential
correlator and one for the integrated correlator 

~~~~~~{.cc}
correlations::QStore sd(r,p,q);
correlations::QStore si(r,r,r);
~~~~~~

For differential measurements, we need an
additional correlator to calculate $`C'_{\mathbf{h}}`$ 

~~~~~~{.cc}
correlations::Recursive<> c(si);
correlations::Recursive<> d(sd);
~~~~~~

Note, we will not use the store(s) in our code, so we could shorten
this to for example 

~~~~~~{.cc}
correlations::Recursive<> c(correlations::QStore(r,r,r));
correlations::Recursive<> d(correlations::QStore(r,p,q));
~~~~~~

## Mean m-particle correlators 

As outlined above, we need to evaluate the mean of these correlators
over many events.  The library provides code for doing this.  

### Mean integrated correlators 

For example, if we are calculating the 4-particle second flow harmonic
$`v_n\{m\}`$ we need to evaluate the 4-particle cumulant

```math
c_n\{4\} = \overline{C}_{n,n,-n,-n} - 2\overline{C}_{n,-n}^2\quad,
```

and thus we need to average the 4- and 2-particle correlators
$`C_{n,n,-n,-n}`$ and $`C_{n,-n}`$, respectively.  We also want to
store the covariance between these, so we define an object to do this
for us

~~~~~~{.cc}
stat::WestCov we(2);
~~~~~~

This object will calculate means and covariances between the needed
correlators.

### Mean differential correlators 


If we are doing the differential 4-particle second flow
harmonic $`v_n\{m\}`$ we need to evaluate the different 4-particle
cumulant

```math
d_n\{4\} = \overline{C}_{n,n,-n,-n} 
- 2\overline{C'}_{n,-n}\overline{C}_{n,-n}\quad,
```

and thus we need to also calculate the average of the different 4- and
2-particle correlators.  In this case, we will create our statistics
object as 

~~~~~~{.cc}
stat::WestCov we(4)
~~~~~~

## Filling the Q-vector(s)

For _each_ event, we need to fill in the $`\varphi`$ observations into
our $`Q`$-vectors.   We will do that directly on the
`correlations::QVector` objects we declared above.  Thus, assuming
`NextObserveration` fetches the next observation, and `GetPhi` and
`GetWeight` fetches the azimuth angle and weight of that observation,
we do 

### Filling integrated Q-vector 

~~~~~~{.cc}
q.reset();
while (NextObservation()) { 
  correlations::Real phi = GetPhi();
  correlations::Real w   = GetWeight();
  
  q.fill(phi, w);
}
~~~~~~

### Filling differential Q-vectors 

For differential measurements we need to figure out if a particular
observation belongs to a particle-of-interest or is a reference
particle, or will be used as both
  
~~~~~~{.cc}
 p.reset();
 r.reset();
 q.reset();
 while (NextObservation()) {
   correlations::Real phi    = GetPhi();
   correlations::Real weight = GetWeight();
   bool               isPOI  = IsParticleOfInterest();
   bool               isREF  = IsReference();
   
   if (isPOI)          p->fill(phi,weight);
   if (isREF)          r->fill(phi,weight);
   if (isPOI && isREF) q->fill(phi,weight);
}
~~~~~~

## Calculating the m-particle correlators 

At the end of _each_ event, we need to calculate the $`m`$-particle
correlators we need for our cumulant(s).

### Calculating integrated m-particle correlators 

Let us take the integrated 4-particle second harmonic flow
$`v_2\{4\}`$ as an example. Using the `correlations::Correlator`
object `c` and statistics object `we` defined above, we calculate 

~~~~~~{.cc}
correlations::Result c4 = c.calculate({2,2,-2,-2});
correlations::Result c2 = c.calculate({2,-2});
we.fill({c4.eval.real(), c2.eval().real()},
        {c4.weights(),   c2.weights()});
~~~~~~

Thus, we have added the 4- and 2-particle correlators for this event
to our means and covariances in `we`. 

### Calculating differential m-particle correlators 

For the differential 4-particle second flow harmonic $`v'_2\{4\}`$ we
will do 

~~~~~~{.cc}
correlations::Result d4 = c.calculate({2,2,-2,-2});
correlations::Result d2 = c.calculate({2,-2});
correlations::Result c4 = c.calculate({2,2,-2,-2});
correlations::Result c2 = c.calculate({2,-2});
we.fill({d4.eval.real(), d2.eval().real()},
         c4.eval.real(), c2.eval().real()},
        {d4.weights(),   d2.weights(),
         c4.weights(),   c2.weights()});
~~~~~~

to add our differential and integrated 4-particle correlators to the
statistics.

## Calculate the final results 

To evaluate the final result of the flow harmonics of interest, we
need to evaluate the relevant cumulants.

### Calculating final integrated result 

Let us take the integrated 4-particle second harmonic as our first
example.  We need to calculate

```math
\sqrt[4]{-c_2\{4\}} = 
\sqrt[4]{-\left(\overline{C_{2,2,-2,-2}}-2\overline{C}^2_{2,-2}\right)}\quad,
```

In code this becomes 

~~~~~~{.cc}
correlations::Real avgC4 = we.mean()[0];
correlations::Real avgC2 = we.mean()[1];
correlations::Real c24   = avgC4 - 2*avgC2*avgC2;
correlations::Real v24   = pow(-c24,0.25);
~~~~~~

### Calculating final differential result 
    
For the differential measurements, we need
    
```math
\begin{aligned}
c_2\{4\} &= \overline{C}_{n,n,-n,-n} - 2\overline{C}_{n,-n}^2\\
d_2\{4\} &= \overline{C}_{n,n,-n,-n} - 2\overline{C'}_{n,-n}\overline{C}_{n,-n}\\
v_2\{4\} &= \sqrt[4]{-c_2\{4\}} = 
\sqrt[4]{-\left(\overline{C_{2,2,-2,-2}}-2\overline{C}^2_{2,-2}\right)}\\
v'_2\{4\} &= \frac{d_2\{4\}}{c_2\{4\}}v_2\{4\}\quad.
\end{aligned}
```

In code 

~~~~~~{.cc}
correlations::Real avgD4 = we.mean()[0];
correlations::Real avgD2 = we.mean()[1];
correlations::Real avgC4 = we.mean()[2];
correlations::Real avgC2 = we.mean()[3];
correlations::Real d24   = avgD4 - 2*avgD2*avgC2;
correlations::Real c24   = avgC4 - 2*avgC2*avgC2;
correlations::Real v24   = pow(-c24,0.25);
correlations::Real vp24  = d24 / c24 * v24;
~~~~~~

## Complete walk-through 

Below is the example code from above collected.  Note, these are not
examples of real-life applications - for that we need also to
calculate uncertainties and so on. 

For truly operational real-life code base, see [the simplified
use](#Flow_Full_Flow_Framework_)

### Integrated flow 

This is a (pseeudo-)example of doing integrated second order,
4-particle flow measurement.

~~~~~~{.cc}
correlations::QVector q(2, 4, useWeights)
correlations::QStore s(q,q,q);
correlations::Recursive<> c(s);
stat::WestCov we(2);

while (NextEvent()) {
  q.reset();
  while (NextObservation()) { 
    correlations::Real phi = GetPhi();
    correlations::Real w   = GetWeight();
    
    q.fill(phi, w);
  }
  
  correlations::Result c4 = c.calculate({2,2,-2,-2});
  correlations::Result c2 = c.calculate({2,-2});
  we.fill({c4.eval.real(), c2.eval().real()},
          {c4.weights(),   c2.weights()});
}
  
correlations::Real avgC4 = we.mean()[0];
correlations::Real avgC2 = we.mean()[1];
correlations::Real c24   = avgC4 - 2*avgC2*avgC2;
correlations::Real v24   = pow(-c24,0.25);
~~~~~~

Here, we assume `NextEvent` fetches the next event. 

### Differential flow 

This is a (pseudo-)example of doing integrated flow measurement of
$`v'_2\{4\}`$.  Here we only have one differential bin.  In real-life
code we would have many differential bins. 

~~~~~~{.cc}
correlations::QVector p(2, 4, useWeights);
correlations::QVector r(2, 4, useWeights);
correlations::QVector q(2, 4, useWeights);
correlations::Recursive<> c(correlations::QStore(r,r,r));
correlations::Recursive<> d(correlations::QStore(r,p,q));
stat::WestCov we(4)

while (NextEvent()) {
  p.reset();
  r.reset();
  q.reset();
  while (NextObservation()) {
    correlations::Real phi    = GetPhi();
    correlations::Real weight = GetWeight();
    bool               isPOI  = IsOfInterest();
    bool               isREF  = IsReference();
    
    if (isPOI)          p->fill(phi,weight);
    if (isREF)          r->fill(phi,weight);
    if (isPOI && isREF) q->fill(phi,weight);
  }
  correlations::Result d4 = c.calculate({2,2,-2,-2});
  correlations::Result d2 = c.calculate({2,-2});
  correlations::Result c4 = c.calculate({2,2,-2,-2});
  correlations::Result c2 = c.calculate({2,-2});
  we.fill({d4.eval.real(), d2.eval().real()},
           c4.eval.real(), c2.eval().real()},
          {d4.weights(),   d2.weights(),
           c4.weights(),   c2.weights()});
}

correlations::Real avgD4 = we.mean()[0];
correlations::Real avgD2 = we.mean()[1];
correlations::Real avgC4 = we.mean()[2];
correlations::Real avgC2 = we.mean()[3];
correlations::Real d24   = avgD4 - 2*avgD2*avgC2;
correlations::Real c24   = avgC4 - 2*avgC2*avgC2;
correlations::Real v24   = pow(-c24,0.25);
correlations::Real vp24  = d24 / c24 * v24;
~~~~~~

Here, we assume that `IsOfInterest` returns true if the current
observation is _of interest_ and `IsReference` returns true if the
observation is a _reference_ observation (can overlap). 

## Full Flow Framework

The namespace `correlations::flow` implements many examples of using
the Generic Framework for calculation flow results.  

### Flow Estimators 

The two class templates

- `correlations::flow::VNM` 
- `correlations::flow::VPNM` 

implements bases classes for integrated and differential flow
measurements, respectively.  The classes calculates the mean of the
per-event correlators needed as well as the covariance between these
correlators.  This will later be used for propagation of
uncertainty. On each event, these classes expect the appropriate
correlation values calculated on the event. 

In the end, the classes calculates the needed cumulants and derives
the flow coefficients $`v_n\{m\}`$ (or $`v'_n\{m\}`$ for differential
measurements) as well as the statistical uncertainties on these. 

The classes are templated on the estimator type.  We can choose
between 

- `correlations::stat::Derivatives` (default) which will use the full
  derivatives to do full error propagation and estimate the
  uncertainties. 
  
- `correlations::stat::Jackknife` which uses a Jackknife simulation to
  estimate the uncertainties. 

- `correlations::stat::Bootstra` which uses a Bootstrap simulation to
  estimate the uncertainties. 

Thus, we can switch between the methods of estimating uncertainties by 

~~~~~~{.cc}
correlations::flow::VNM<Bootstrap>   v2bootstrap;
correlations::flow::VNM<Jackknife>   v2jackknife;
correlations::flow::VNM<Derivatives> v2full;
~~~~~~

### Bins of Flow 

The interface provided by the above classes is a bit tedious, and thus
we provide the classes

- `correlations::flow::Reference` for calculation of the integrated
  flow coefficients
- `correlations::flow::Ofinterest` for calculating the differential
  flow coefficients in a single differential bin.
  
Sometimes we may use the same reference correlator for multiple
differential bins.  in that case, one can pass the same
`correlations::flow::Reference` object to all
`correlations::flow::Ofinterest` objects. 

See also the examples 

- `correlations::ana::Integrated` for integrated flow measurements 
- `correlations::ana::Differential` for transverse momentum
  differential flow measurements. 

These example analyses uses the service classes
`correlations::ana::XBin` and `correlations::ana::XBinned` to ease the
set-up even further. 

#### Integrated example 

~~~~~~{.cc}
correlations::flow::Reference<> i(4,4,useWeights);
while (NextEvent()) {
  i.reset();
  while (NextObservation()) { 
    correlations::Real phi = GetPhi();
    correlations::Real w   = GetWeight();
    
    i.fill(phi, w);
  }
  i.update();
}

i.result();
~~~~~~

#### Differential example 

~~~~~~{.cc}
correlations::flow::Reference<>  i(4,4,useWeights);
correlations::flow::OfInterest<> d(i);

while (NextEvent()) {
  i.reset();
  d.reset();
  while (NextObservation()) { 
    correlations::Real eta = GetEta();
    correlations::Real pt  = GetPt();
    correlations::Real phi = GetPhi();
    correlations::Real w   = GetWeight();
    
    i.fill(phi, w);
	
	if (IsOfInterest(eta,pt,phi,w)) d.fill(phi,w);
  }
  i.update();
  d.update();
}

d.result();
~~~~~~


## Some results 

### Integrated flow results

In this example, we generate particles with explicit flow coefficients 

```math
v_1=0.02\quad v_2=0.07\quad v_3=0.03
``` 

and analyse those for integrated flow using 2- and 4-particle
correlations.  The plot below shows the result.  Here the
uncertainties are calculated using full error propagation (including
covariance terms).

![Result of integrated flow measurements](gen_phi_medium_intg.png "Results")

Using other estimators for calculating the uncertainties results in
larger uncertainties.  See also `stat::Estimator` 

### Differential flow results

    
Here, we generate particles with explicit flow coefficients 

```math
v_1=0.02\quad v_2=0.07\quad v_3=0.03\quad.
``` 

However, we modulate the flow of the particles based on the transverse
momentum of the particles by the factor 

```math
A(p_{\mathrm{T}}) = 1.77157p_{\mathrm{T}} -
0.305679p_{\mathrm{T}}^2\quad,
```

(the parameters are from a fit to the ALICE $`v'_2\{2\}`$
wrt. transverse momentum measurements). We analyse the produced
particles for transverse momentum differential flow using 2- and
4-particle correlations.  The plot below shows the result.  Here the
uncertainties are calculated using full error propagation (including
covariance terms).

![Result of differential flow measurements](gen_eta_medium_diff.png "Results")

Using, for example, a bootstrap method (see `stat::Boostrap`) to
estimate the uncertainties will result in larger uncertainties.  The
plot below shows the same analysis as above but using a bootstrap
method.

![Result of differential flow measurements w/Bootstrap](gen_eta_medium_diff_bootstrap.png "Results")


### Differential flow results in 2-sub-events

As above, particles are produced with explicit flow parameters 

```math
v_1=0.02\quad v_2=0.07\quad v_3=0.03\quad,
``` 

modulated by 

```math
A(p_{\mathrm{T}}) = 1.77157p_{\mathrm{T}} -
0.305679p_{\mathrm{T}}^2\quad,
```

The particles are analysed for 4-particle differential flow in 2
sub-events $`v'_n\{4|12,34\}`$ separated by

```math
|\Delta\eta| > 0.5\quad.
```

The plot below shows the result.  Here
the uncertainties are calculated using full error propagation
(including covariance terms).

![Result of differential flow measurements in sub-events](gen_eta_medium_pdif.png "Results")


