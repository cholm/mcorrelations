# Methods for calculating the correlators

The correlation is calculated using the concrete sub-classes of
correlations::Correlator:

- `correlations::NestedLoops` uses nested loops (or rather, recursive
  function calls.  This relies on storing all the observations of
  $`\varphi`$ and (possibly) the corresponding weights $`w`$.  This
  way of calculating the correlation scales like $`O(M^N)`$ where
  $`M`$ is the event multiplicity and $`N`$ is the correlation order
  to calculate.  It is indeed rather slow.

- `correlations::FromQVector` uses a $`Q`$-vector to calculate the
  correlations.  Calculating the $`Q`$-vector is an order $`O(N M)`$
  operation, while calculating the correlation is only $`O(N)`$.
  Still - it is much faster generally, then the nested loop method.

Both algorithms are interfaced in the same way through the member
function `correlations::Correlator::calculate`.

The member function `correlations::Correlator::calculate` returns a
`correlations::Result` object, which contains the sum and the sum of
the weights.  The sum and the sum of weights can be accumulated in an
object of this time, and the final event average result can be
extracted using `correlations::Result::eval`.

## Recursive versus closed-form

The class `correlations::FromQVector` is again an abstract base class
(cannot make objects of that type), and three different implementations
are provided.

- `correlations::recurrence::FromQVector` which used recursion to
  simplify the expression, and has the added feature that it can
  calculate any order correlator - provided enough computing time and
  memory.
- `correlations::recursive::FromQVector` which uses a recursive
  algorithm to calculate the expressions. As above, it can calculate
  any order correlator  - provided enough computing time and
  memory.
- `correlations::closed::FromQVector<>` which uses closed-form
  expression for the correlators.  This means that we are limited to
  the order for which we have generated these closed forms.  Note, one
  can use the program print.cc to generate this, should it be needed.

Similarly to `correlations::recursive::FromQVector`, there is a
sub-class of `correlations::NestedLoops` -
`correlations::recursive::NestedLoops` which does the nested loops
using recursion.

Note that `correlations::closed::FromQVector<M>` can be slow to
compile if `M` is too large. The largest possible value of `M` is 8,
but that can take infinitely long to compile, so the default value of
`M` is set to 5.   Should you need 6, 7, or 8 particle correlators you
should increase `M` or choose one of the other methods. 

The recursive algorithms could probably be implemented using template
recursion to let the compiler (and not run-time) deal with branching,
etc., which could speed up this tremendously.

![Comparison of methods](gen_phi_small_corr.png "Comparison of methods")

## Differential correlators

The classes `correlations::recurrence::FromQVector` and
`correlations::closed::FromQVector<M>` both allow calculation of
differential correlators.  This is achieved by passing 3 Q-vectors to
the constructor or to the `correlations::Factory::make` generator. 

For both `correlations::recurrence::FromQVector` and
`correlations::closed::FromQVector<M>` up to 4-particle correlators
can be calculated differentially. 
