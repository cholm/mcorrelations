# Anisotropic Azimuthal Flow Measurements

The measurement of the anisotropic azimuthal particle production (or
["flow"][olli]) in heavy-ion, nucleon-nucleus, and even nucleus-nucleus
collision at relativistic energies is of great interest in the
heavy-ion community.  The measurements have provided great insight
into the possibly deconfined phase of matter created in these
collisions, allowing the study of the strong nuclear force below the
perturbative regime.

A myriad of methods have been developed to investigate flow, ranging
from the original _even plane_ method to more sophisticated methods
such as _Lee-Yang Zeros_.  In all cases, the basic premise is to
expand the azimuthal distribution in a Fourier series around some
common angle $`\Psi`$

```math
\frac{\mathrm{d}N}{\mathrm{d}\varphi}\propto
1 + 2\sum_{n}v_n\cos\left(n[\varphi - \Psi]\right)\quad.
```

The _flow coefficients_ $`v_n`$ are then estimates of the flow of the
produced particles.  The history of flow measurements is too long to
get into here, and we refer the interested reader to the
[literature][gfin]. 

## Generic framework 


The paper [Generic framework for anisotropic flow analyses with
multiparticle azimuthal correlations][gfin] describes a particular
method of estimating flow from experimental data.  The method has the
benefit that it allows for easy generalisation and extension (e.g.,
_symmetric cumulants_).  This software library provides the _reference
implementation_ of the method described in that paper.

In the generic framework, the flow coefficients $`v_n`$ are estimated
using _multi-particle_ correlations 

```math
v_n \rightarrow v_n\{m\}
``` 

where $`m`$ is the number of particles (an even number) that are
correlated.  Sometimes, we will estimate the dependence of $v_n$ on
some other quantity $`x`$ such as the transverse momentum
$`p_{\mathrm{T}}`$ or pseudorapidity $`\eta`$.  For such
_differential_ measurements, we will use the notation 

```math
v_n(x) \rightarrow v^{x}_n\{m\}\quad.
```

Often the context gives the quantity we are interested in dependence
of, and we write 

```math
v_n(x) \rightarrow v'_n\{m\}\quad.
```

Experimentally, $`v_n\{m\}`$ is some function the _cumulants_ 

```math
v_n\{m\} = f_m(c_1\{m\},\ldots)\quad,
```

where the expression for $`f_m`$ depends on the number of correlated
particles $`m`$, and where each cumulant is a function of the event
averaged _correlators_

```math
c_n\{m\} = 
g_m(\overline{C}_{n_1}\{1\},\ldots,\overline{C}_{\mathbf{n}}\{m\})\quad.
```

Here $`\mathbf{n}`$ specifies the _order_ and _power_ of the
correlator, For $`n`$ equal to a non-zero, whole number, we often have
that

```math
\mathbf{n} = \begin{bmatrix}
    n\\
    \vdots\\
    n\\
    -n\\
    \vdots\\
    -n\end{bmatrix}\in \mathbb{N}^m\quad,
```
where the first $`m/2`$ entries are positive $`n`$, and the other half
os negative $`n`$.  We will use this notation, so that 

```math 
\begin{aligned}
\mathbf{2} &= \begin{bmatrix}
  2\\
  \vdots\\
  -2
  \end{bmatrix}\\
\mathbf{4} &= \begin{bmatrix}
  4\\
  \vdots\\
  -4
  \end{bmatrix}\quad.
\end{aligned}
``` 

Furthermore, since the _power_ $`m`$ of each correlator
$`C_\mathbf{n}\{m\}`$ is given by the dimension of $`\mathbf{n}`$, we
can shorten the notation and leave out the $`\{m\}`$ and specify the
vector $`\mathbf{n}`$ elements. Thus, we have that 

```math
c_2\{m\} = g_m(\overline{C}_{2},\overline{C}_{\mathbf{2}})
=g_m(\overline{C}_{2},\ldots,\overline{C}_{2,\ldots,-2})\quad.

```

### Flow expressions

Let us write up some expression for $`f_n`$.  In particular, we have for
_integrated_ flow with 2-particle correlations

```math
v_n\{2\} = f_2(c_n\{2\}) = \sqrt{c_n\{2\}}
```

while for 4, 6, and 8 particles we have 

```math
\begin{aligned}
v_n\{4\} &= f_4(c_n\{4\}) = \sqrt[{\textstyle 4}]{-c_n\{4\}}\\
v_n\{6\} &= f_6(c_n\{6\}) = \sqrt[{\textstyle 6}]{\frac{1}{4}c_n\{6\}}\\
v_n\{8\} &= f_8(c_n\{8\}) = \sqrt[{\textstyle 8}]{-\frac{1}{33}c_n\{8\}}
\end{aligned}
```

More generically, with $`m=2k`$, 

```math
v_{n}\{2k\} =
\sqrt[\textstyle{2k}]{(-1)^{k-1}\frac{1}{M_k}c_{n}\{2km\}}\quad,
```

with the sequence 

```math
M = \{1, 1, 4, 33, 456, 9460, 274800, 10643745, \ldots\}\quad.
```

The sequence above can in principle be calculated using the
expectation values of cumulants in terms of flow harmonics or from
generating functions. 

A generic expression for the sequence is given by 

```math
\begin{aligned}
M_k &= \sum_{i=1}^{k-1}\binom{k}{i}\binom{k}{k-i}M_{i}M_{k-i}\\
M_1 &= 1
\end{aligned}
```

and is related to Bessel coefficients, [see
here](https://oeis.org/A002190) and
[here](https://oeis.org/A101981). The code, however, encodes up to
16-particle correlators as constants, which should suffice in most
practical applications, and only calculates higher numbers if
needed. Note, since we only evaluate this sequence at the very end of
the analysis, there's little performance penalty paid for this
calculation. 

The generic formula for $`m`$-particle integrated flow is implemented
in the member function `correlations::flow::Calculations::vnm`.

_Differential_ flow is defined in terms of _differential_ cumulants
$`d_{n}\{m\}`$ which in turn are functions of the average differential
and integrated correlators

```math
d_{n}\{m\} = 
h_m\left
(\overline{C'}_{n_1}\{1\},\ldots,\overline{C'}_{\mathbf{n}}\{m\},
 \overline{C}_{n_1}\{1\}, \ldots,\overline{C}_{\mathbf{n}}\{m\}\right)\quad.
```

For 2-, 4-, and 6-particle correlations, we find that 

```math
\begin{aligned}
v'_n\{2\} &= \frac{d_n\{2\}}{\sqrt{c_n\{2\}}}\\
v'_n\{4\} &= \frac{-d_n\{4\}}{(-c_n\{4\})^{3/4}}\\
v'_n\{6\} &= \frac{d_n\{2\}}{(1/4)^{1/6}(c_n\{6\})^{5/6}}
\end{aligned}
``` 

In general, we have that 

```math
v'_n\{2k\} = 
\frac{(-1)^{k-1}}{M^{1/(2k)}}
\frac{d_n\{2k\}}{\left[(-1)^{k-1}c_{n}\{2k\}\right]^{1-1/(2k)}}
= \frac{d_n\{2k\}}{c_{n}\{2k\}}v_n\{2k\}
\quad,
```

where $`m=2k`$ and $`M_k`$ is given by the sequence above. 

The generic formula for $`m`$-particle differential flow is
implemented in the member function
`correlations::flow::Calculations::vpnm`.

### Cumulant expressions

The expressions for the $`m`$-particle cumulants can be derived from
the general definition of cumulants.  However, in this library, they
are implemented for specific number of particles - that is
$`m\in\{2,4,6,8,\ldots\}`$. The $`m`$-particle cumulant is expressed
in terms of the event-averaged $`(2k)`$-particle correlators (for
$`1\leq k\leq m/2`$).

For integrated cumulants these are 

```math
\begin{aligned}
c_n\{2\} &= \overline{C}_{\mathbf{n}}\{2\}\\
c_n\{4\} &= \overline{C}_{\mathbf{n}}\{4\}-2\overline{C}_{\mathbf{n}}\{2\}^2\\
c_n\{6\} &= 
\overline{C}_{\mathbf{n}}\{6\}
-9\overline{C}_{\mathbf{n}}\{4\}\overline{C}_{\mathbf{n}}\{2\}
+12\overline{C}_{\mathbf{n}}\{2\}^3\\
c_n\{8\} &= \overline{C}_{\mathbf{n}}\{8\}
       - 16 \overline{C}_{\mathbf{n}}\{6\}\overline{C}_{\mathbf{n}}\{2\}
       - 18\overline{C}_{\mathbf{n}}\{2\}^2
       + 144\overline{C}_{\mathbf{n}}\{4\} \overline{C}_{\mathbf{n}}\{2\}^2
       - 144\overline{C}_{\mathbf{n}}\{2\}^4
\end{aligned}
```

It is easy to see that 

```math
\begin{aligned}
c_n\{4\} & = \overline{C}_{\mathbf{n}}\{4\}
             -2c_n\{2\}\overline{C}_{\mathbf{n}}\{2\}\\
c_n\{6\} &=  \overline{C}_{\mathbf{n}}\{6\}
             -6c_n\{4\}\overline{C}_{\mathbf{n}}\{2\}			 
             -3c_n\{2\}\overline{C}_{\mathbf{n}}\{4\}\\			 
c_n\{8\} &=  \overline{C}_{\mathbf{n}}\{8\}
             -12c_n\{6\}\overline{C}_{\mathbf{n}}\{2\}			 
             -18c_n\{4\}\overline{C}_{\mathbf{n}}\{4\}			 
             -3c_n\{2\}\overline{C}_{\mathbf{n}}\{6\}\quad,\\		 
\end{aligned}			 
```			 

which clearly suggests a recursive relation ship between the
cumulants. Thus, we find that 

```math
\begin{aligned}
c_n\{2n\} &= \overline{C}_{\mathbf{n}}\{2n\} - 
			 \sum_{k=1}^{n-1} \binom{n}{k}\binom{n-1}{k}
			 c_n\{2(n-1)\}\overline{C}_{\mathbf{n}}\{2(n-k)\}\\
c_n\{2\} &= \overline{C}_{\mathbf{n}}\{2\}\quad,
\end{aligned}
```
where $`m=2n`$, and thus allowing us to calculate the cumulant of any
number of pairs of  particles.  This is implemented in
`correlations::flow::Calculations::cnm` and the gradient in
`correlations::flow::Calculations::gcnm`. 
			 
For differential cumulants we have 

```math
\begin{aligned}
d_n\{2\} &= \overline{C'}_{\mathbf{n}}\{2\}\\
d_n\{4\} &= 
      \overline{C'}_{\mathbf{n}}\{4\}
  - 2 \overline{C'}_{\mathbf{n}}\{2\} 
      \overline{C}_{\mathbf{n}}\{2\}\\
d_n\{6\} &= 
       \overline{C'}_{\mathbf{n}}\{6\}
  - 6  \overline{C'}_{\mathbf{n}}\{4\} 
       \overline{C}_{\mathbf{n}}\{2\}
  - 3  \overline{C'}_{\mathbf{n}}\{2\} 
       \overline{C}_{\mathbf{n}}\{4\}
  + 12 \overline{C'}_{\mathbf{n}}\{2\}
       \overline{C}_{\mathbf{n}}\{2\}^2\\
d_n\{8\} &= 
  \overline{C'}_{\mathbf{n}}\{8\}
  - 12  \overline{C'}_{\mathbf{n}}\{6\} 
        \overline{C}_{\mathbf{n}}\{2\}
  - 4   \overline{C'}_{\mathbf{n}}\{2\} 
        \overline{C}_{\mathbf{n}}\{6\}
  - 18  \overline{C'}_{\mathbf{n}}\{4\} 
        \overline{C}_{\mathbf{n}}\{4\}
  + 72  \overline{C'}_{\mathbf{n}}\{4\} 
        \overline{C}_{\mathbf{n}}\{2\}^2
  + 72  \overline{C'}_{\mathbf{n}}\{2\}
        \overline{C}_{\mathbf{n}}\{4\} 
        \overline{C}_{\mathbf{n}}\{2\}
  - 144 \overline{C'}_{\mathbf{n}}\{2\}
        \overline{C}_{\mathbf{n}}\{2\}^3
  \quad.
\end{aligned}
```

Note, if $`\forall x: C'_n\{x\}=C_n\{x\}`$ then $`d_n\{m\}=c_n\{m\}`$
as expected. 

Again, it is easy to see that 

```math
\begin{aligned}
d_n\{4\} & = \overline{C'}_{\mathbf{n}}\{4\}
             -2d_n\{2\}\overline{C}_{\mathbf{n}}\{2\}\\
d_n\{6\} &=  \overline{C'}_{\mathbf{n}}\{6\}
             -6d_n\{4\}\overline{C}_{\mathbf{n}}\{2\}			 
             -3d_n\{2\}\overline{C}_{\mathbf{n}}\{4\}\\			 
d_n\{8\} &=  \overline{C'}_{\mathbf{n}}\{8\}
             -12d_n\{6\}\overline{C}_{\mathbf{n}}\{2\}			 
             -18d_n\{4\}\overline{C}_{\mathbf{n}}\{4\}			 
             -3c_n\{2\}\overline{C}_{\mathbf{n}}\{6\}\quad,\\		 
\end{aligned}			 
```			 

and thus we can express the cumulants recursively as 

```math
\begin{aligned}
d_n\{2n\} &= \overline{C'}_{\mathbf{n}}\{2n\} - 
			 \sum_{k=1}^{n-1}\binom{n}{k}\binom{n-1}{k}
			 d_n\{2(n-1)\}\overline{C}_{\mathbf{n}}\{2(n-k)\}\\
d_n\{2\} &= \overline{C'}_{\mathbf{n}}\{2\}\quad,
\end{aligned}
```

and we have again a generic expression for the cumulants of any number
of particle pair correlations. This is implemented in
`correlations::flow::Calculations::dnm` and the gradient in
`correlations::flow::Calculations::gdnm`.

Note, the factor 

```math
\binom{n}{k}\binom{n-1}{k}
```
is easily understood as the product of combinations in
$`d_n\{2(n-1)\}`$ times the number of combinations for
$`\overline{C}_{\mathbf{n}}\{2(n-k)\}`$.  The sequence of numbers is
related to the number of [Dyke prefixes](https://oeis.org/A124428). 

Note, we only evaluate the cumulant expression at the very end of the
analysis, and thus there is little performance penalty in evaluating
these expression. 


### Correlator expressions 

The integrated and differential correlators, $`C_{\mathbf{n}}\{m\}`$
and $`C'_{\mathbf{n}}\{m\}`$, respectively, are calculated
event-by-event from the corresponding $`Q`$-vector (or -vectors in
case of differential correlators).  The expression generally becomes
tedious after $`m>4`$, and indeed the focus of the [paper](gfin) is
indeed to show these expression.  Here we will give a few examples.
One can use the program [symbolic.cc](symbolic.cc) to write out these
expression to any number of particles (as long as processing time
permits). 

In general, we have 

```math
C_{\mathrm{h}}\{m\} = \frac{N_{\mathrm{h}}\{m\}}{D_{\mathrm{h}}\{m\}}
= \frac{N_{\mathrm{h}}\{m\}}{N_{\mathrm{0}}\{m\}}\quad,
```

where 

```math
\mathbf{h} = \begin{bmatrix}
  h_1\\
  \vdots\\
  h_m
  \end{bmatrix} \in \mathbf{Z}^m\quad,
```

is a vector of flow harmonics with the condition that 

```math
\sum_{i=1}^m h_i = 0\quad.
```

For the $`n^{\mathrm{th}}`$ order flow harmonic we set 

```math
\mathbf{h} = \mathbf{n} \equiv \begin{bmatrix}
  n\\
  \vdots\\
  n\\
  -n\\
  \vdots\\
  -n
  \end{bmatrix}\quad,
```

so that the number of negatives and positives are equal.  Note the
denominator $`D_{\mathbf{h}}\{m\}`$ is the same as the numerator but
with all $`h_i=0`$. 

For integrated correlators, using the single $`Q`$-vector $`Q`$, we
have 

```math
\begin{aligned}
N_{\mathbf{n}}\{2\} &= 
Q_{n,1} 
Q_{-n,1} - 
Q_{n - n,2} = Q_{n}Q_{-n} - Q_{0,2}\\
N_{\mathbf{n}}\{4\} &= 
Q_{n,1} Q_{n,1} Q_{-n,1} Q_{-n,1}\\
&\quad - Q_{n,1} Q_{n,1} Q_{-n - n,2}\\
&\quad - Q_{n,1} Q_{-n,1} Q_{n - n,2}\\
&\quad - Q_{n,1} Q_{-n,1} Q_{n - n,2}\\
&\quad + 2 Q_{n,1} Q_{n - n - n,3}\\
&\quad - Q_{n,1} Q_{-n,1} Q_{n - n,2}\\
&\quad - Q_{n,1} Q_{-n,1} Q_{n - n,2}\\
&\quad + 2 Q_{n,1} Q_{n - n - n,3}\\
&\quad - Q_{-n,1} Q_{-n,1} Q_{n + n,2}\\
&\quad + 2 Q_{-n,1} Q_{n + n - n,3}\\
&\quad + 2 Q_{-n,1} Q_{n + n - n,3}\\
&\quad + Q_{n + n,2} Q_{-n - n,2}\\
&\quad + Q_{n - n,2} Q_{n - n,2}\\
&\quad + Q_{n - n,2} Q_{n - n,2}\\
&\quad - 6 Q_{n + n - n - n,4}\\
&= 2 Q^{2}_{0,2}\\
&\quad  - 4 Q_{0,2} Q_{-n,1} Q_{n,1}\\
&\quad  - 6 Q_{0,4}\\
&\quad  - Q_{-2n,2} Q^{2}_{n,1}\\
&\quad  + Q_{-2n,2} Q_{2n,2}\\
&\quad  + Q^{2}_{-n,1} Q^{2}_{n,1}\\
&\quad  - Q^{2}_{-n,1} Q_{2 n,2}\\
&\quad  + 4 Q_{-n,1} Q_{n,3}\\
&\quad  + 4 Q_{-n,3} Q_{n,1}
\end{aligned}
```

When calculating differential correlators, we still follow the form
above, and have 

```math
C'_{\mathrm{h}}\{m\} = \frac{N'_{\mathrm{h}}\{m\}}{D'_{\mathrm{h}}\{m\}}
= \frac{N'_{\mathrm{h}}\{m\}}{N'_{\mathrm{0}}\{m\}}\quad,
```

where $`\mathbf{h}`$ have the same meaning as before.  However, for
differential correlators, we need three $`Q`$-vectors: 

- $`p`$ - the $`Q`$-vector for particles (measurements) of interest $`P`$
- $`r`$ - the $`Q`$-vector for reference particles (measurements) $`R`$
- $`q`$ - the $`Q`$-vector for particles (measurements) in
  $`P\cap R`$. 
  
We have 

```math
\begin{aligned}
N'_{\mathbf{n}}\{2\} &= 
p_{n,1} r_{-n,1} - 
q_{n -n,2}
= p_{n,1} r_{- n,1} - q_{0,2}\\
N'_{\mathbf{n}}\{4\} &= 
p_{n,1} r_{n,1} r_{-n,1} r_{-n,1}\\
&\quad - p_{n,1} r_{n,1} r_{-n -n,2}\\
&\quad - p_{n,1} r_{-n,1} r_{n -n,2}\\
&\quad - p_{n,1} r_{-n,1} r_{n -n,2}\\
&\quad + 2 p_{n,1} r_{n -n -n,3}\\
&\quad - q_{n + n,2} r_{-n,1} r_{-n,1}\\
&\quad + q_{n + n,2} r_{-n -n,2}\\
&\quad - q_{n -n,2} r_{n,1} r_{-n,1}\\
&\quad + q_{n -n,2} r_{n -n,2}\\
&\quad - q_{n -n,2} r_{n,1} r_{-n,1}\\
&\quad + q_{n -n,2} r_{n -n,2}\\
&\quad + 2 q_{n + n -n,3} r_{-n,1}\\
&\quad + 2 q_{n + n -n,3} r_{-n,1}\\
&\quad + 2 q_{n -n -n,3} r_{n,1}\\
&\quad - 6 q_{n + n -n -n,4}\\
&= - 2 p_{n,1} r_{0,2} r_{-n,1}\\
&\quad - p_{n,1} r_{-2n,2} r_{n,1}\\
&\quad + p_{n,1} r^{2}_{-n,1} r_{n,1}\\
&\quad + 2 p_{n,1} r_{-n,3}\\
&\quad + 2 q_{0,2} r_{0,2}\\
&\quad - 2 q_{0,2} r_{-n,1} r_{n,1}\\
&\quad - 6 q_{0,4}\\
&\quad + 2 q_{-n,3} r_{n,1}\\
&\quad + 4 q_{n,3} r_{-n,1}\\
&\quad + q_{2n,2} r_{-2n,2}\\
&\quad - q_{2n,2} r^{2}_{-n,1}
\quad.
\end{aligned}
```

The classes `correlations::FromQVector` implements the evaluation of
these expressions (to what order depends on the
[method](doc/Methods.md)). 

If $`q=0`$ above, we find that 

```math
\begin{aligned}
N'_{\mathbf{n}}\{2\} &= p_{n,1} r_{- n,1}\\
N'_{\mathbf{n}}\{4\} &= 
- 2 p_{n,1} r_{0,2} r_{-n,1}\\
&\quad - p{_n,1} r_{-2n,2} r_{n,1}\\
&\quad + p_{n,1} r^{2}_{- n,1} r_{n,1}\\
&\quad + 2 p_{n,1} r_{- n,3}\\
&= - 2 W^2_r p_{n,1} r_{-n,1}\\
&\quad - p{_n,1} r_{-2n,2} r_{n,1}\\
&\quad + p_{n,1} r^{2}_{- n,1} r_{n,1}\\
&\quad + 2 p_{n,1} r_{- n,3}\quad,
\end{aligned}
```

where $`W^2_r=r_{0,2}`$

### Q-vector expression 

We have observations of the azimuthal angle $`\varphi`$ as the set
$`\Phi`$ for which we want to evaluate the $`Q`$ vector to some order
$`n`$ and power $`p`$.  Assuming each observation of $`\varphi`$ has
some weight $`w\in\mathbb{R}`$, so that we have the pairs

```math 
    \varphi_j,w_j\quad.
```
We then evaluate 

```math
Q_{n,p} = \sum_{\varphi_j\in\Phi} w_j^p e^{in\varphi_j}\in\mathbb{C}\quad.
```

We note that for the complex conjugate 

```math
Q_{n,p}^{\dagger} = Q_{-n,p}\quad,
```
and for $`n=0`$ 

```math
Q_{0,p} = \sum_{\varphi_j\in\Phi} w_j^p 
``` 

we get the sum of weights to the $`p`$ power. In particular, if all
weights are unity, then $`Q_{0,p}=M`$ where $`M`$ is the number of
observations in $`\Phi`$. 

For integrated measurements the set $`\Phi`$ is the set of all
observations (within cuts). 

For differential measurements, we have three sets 

- $`\Phi`$ The reference observations.  Typically this is the set of
  all observations (within cuts). 
- $`\Phi'`$ The observations of interest.  This could be observations
  at a particular transverse momentum, pseudorapidity, or the like
- $`\Phi^u = \Phi\cap \Phi'`$ The intersect of the two sets above.  That
  is, observations that are _both_ reference observations and
  observations of interest. 

For the differential measurements we must calculate the $`Q`$ vector
over _each_ of these sets for every single event. 










[olli]: https://inspirehep.net/record/31918?ln=en
[fexp]: https://inspirehep.net/record/374777?ln=en
[gfin]: https://inspirehep.net/record/1269018?ln=en
