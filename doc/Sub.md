# Sub-events and pseudorapidity gap 

Suppose we have two sets of observations 

```math
\begin{aligned}
A &= \{(\varphi^A_1,w^A_1),\ldots\}\\
B &= \{(\varphi^B_1,w^B_1),\ldots\}\quad.
\end{aligned}
```

We can then calculate the $`m`$-particle correlator of observations in
$`A`$ with respect to $`B`$.  Let us write this as 

```math
C_{\mathbf{n}}\{m|A,B\} = f(p,r,q)\quad,
``` 

where 

- $`p`$ is the $`Q`$-vector of observations in $`A`$,
- $`r`$ is the $`Q`$-vector of observations in $`B`$, and 
- $`q`$ is the $`Q`$-vector of observations in $`A\cap B`$ 

We recognise this as the _differential_ correlator of observations of
interest in $`A`$ with respect to the reference $`B`$.  Thus, we have
that the integrated and differential cumulants are given by 

```math
\begin{aligned}
c_{n}\{m|B\} &= 
g\left(\left\{\overline{C}_{\mathbf{n}}\{i|B\}|i=1,\ldots,m\right\}\right)\\
d_{n}\{m|A,B\} &=
g\left(\left\{\overline{C'}_{\mathbf{n}}\{i|A,B\}|i=1,\ldots,m\right\}\right)
\quad,
\end{aligned}
```

and 

```math
v'_n\{2k\} = 
\frac{d_n\{2k|A,B\}}{
\left[(-1)^{k-1}\frac{1}{M_k}c_{n}\{2k|B\}\right]^{1-1/(2k)}}
= \frac{d_n\{2k|A,B\}}{c_{n}\{2k|B\}}v_n\{2k\}
\quad.
```

Suppose now, that $`A`$ and $`B`$ are distinct - that is 

```math
A \cap B = \emptyset\quad \Rightarrow\quad q = 0\quad,
```

for example by introducing a pseudorapidity gap between the
observations in $`A`$ and $`B`$. Thus, to determine the integrated
flow in $`A`$ with respect to $`B`$ is the same as calculating the
_differential_ flow of $`A`$ selecting reference observations from
$`B`$. 

## Example 

The example `exa::GFlow` calculates the integrated flow over all
transverse momentum using 2-particle correlations where
particles-of-interested are those with 

```math
\eta < -0.5\quad,
```

and the reference particles are those with 

```math
\eta > +0.5\quad.
```

Thus the overlap is empty and $`q=0`$. 

![Result of 2 and 4-particle integrated flow measurements in
sub-events](gen_et0_medium_pint.png "Results")


## 4-particle correlators 

For 4 particle correlations we can sub-divide our event in three
distinct ways 

- Correlate, differentially, 2 particles from the $`A`$ region with
  reference particles from the $`B`$ region, which we label
  $`\{12,34\}`$. 
- Correlate, differentially 1 particle from $`A`$ and 1 from $`B`$
  with reference particles from $`B`$ and $`A`$, respectively,
  labelled $`\{13,24\}`$. 
- Correlate 1 particle from region $`A`$ with 3 reference particles
  from region $`B`$. 
  
Of these, the first is of most interest.  If we go through the
mathemathics we find that 

```math
\begin{aligned}
C_{n,n,-n,-n}\{12,34\} &= C_{n,n}\{12\}C_{-n,-n}\{34\}\\
C'_{n,n,-n,-n}\{12,34\} &= C'_{n,n}\{12\}C_{-n,-n}\{34\}\quad,
\end{aligned}
```

and we get 

```math
\begin{aligned}
v'_n\{12,34\} &= 
\frac{d_n\{12,34\}}{c_n\{12,34\}}{v_n\{4\}}\\
&= 
\frac{\overline{C'_{n,n}\{12\}C_{-n,-n}\{34\}}
  -2\overline{C'_{n,n}\{12\}}\ \overline{C_{-n,-n}\{34\}}}{
  \overline{C_{n,n}\{12\}C_{-n,-n}\{34\}}
  -2\overline{C_{n,n}\{12\}}\ \overline{C_{-n,-n}\{34\}}}{v_n\{4\}}
\end{aligned}
```

If the two sides are uncorrelated, then

```math
\begin{aligned}
\overline{C'_{n,n}\{12\}C_{-n,-n}\{34\}} &= 
\overline{C'_{n,n}\{12\}}\,\overline{C_{-n,-n}\{34\}}\\
\overline{C_{n,n}\{12\}C_{-n,-n}\{34\}} &= 
\overline{C_{n,n}\{12\}}\,\overline{C_{-n,-n}\{34\}}\quad,
\end{aligned}
```

and the above reduces further to 

```math
\begin{aligned}
v'_n\{12,34\} &= 
\frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}v_n\{4\}
\end{aligned}
```

and thus the differential flow is determined solely by the region
$`A`$. Note, that this is in general _not_ the same as 

```math
\begin{aligned}
v'_n\{12,34\} &= 
\frac{\overline{C'_{n,-n}\{12\}}}{\overline{C_{n,-n}\{12\}}}v_n\{4\}
= \frac{d_n\{12\}}{c_n\{12\}}v_n\{4\}
= \frac{v'_n\{12\}}{v_n\{2\}}v_n\{4\}\\
\end{aligned}
```

which would imply 

```math
\begin{aligned}
\frac{v'_n\{12,34\}}{v'_n\{12\}} &= \frac{v_n\{4\}}{v_n\{2\}} = \mathrm{const.}
\end{aligned}
```

The integrated flow is given by 

```math
v_n\{4\} = \sqrt[{\scriptstyle 4}]{-c_n\{4\}} 
= \sqrt[{\scriptstyle 4}]{-\left(C_{n,n,-n,-n}-2C^2_{n,-n}\right)}\quad,
```

and thus the partial derivatives wrt. to the correlators then becomes 

```math
\begin{aligned}
\frac{\partial v'_n\{12,34\}}{\partial \overline{C'_{n,n}\{12\}}}
&= \frac{v_n\{4\}}{\overline{C_{n,n}\{12\}}}\\

\frac{\partial v'_n\{12,34\}}{\partial \overline{C_{n,n}\{12\}}}
&=
-\overline{C'_{n,n}\{12\}}\frac{v_n\{4\}}{\overline{C_{n,n}\{12\}}^2}\\

\frac{\partial v'_n\{12,34\}}{\partial v_n\{4\}}
&=\frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}\\

\frac{\partial v'_n\{12,34\}}{\partial c_n\{4\}}
&= \frac{\partial v'_n\{12,34\}}{\partial v_n\{4\}}

\frac{\partial v_n\{4\}}{\partial c_n\{4\}}
= \frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}
\frac{1}{4\,c_n\{4\}}v_n\{4\}\\

\frac{\partial v'_n\{12,34\}}{\partial \overline{C_{n,n,-n,-n}}}
&= \frac{\partial v'_n\{12,34\}}{\partial v_n\{4\}}
\frac{\partial v_n\{4\}}{\partial c_n\{4\}}
\frac{\partial c_n\{4\}}{\partial \overline{C_{n,n,-n,-n}}}
= \frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}
\frac{1}{4\,c_n\{4\}}v_n\{4\}\cdot1\\

\frac{\partial v'_n\{12,34\}}{\partial \overline{C_{n,-n}}}
&= \frac{\partial v'_n\{12,34\}}{\partial v_n\{4\}}
\frac{\partial v_n\{4\}}{\partial c_n\{4\}}
\frac{\partial c_n\{4\}}{\partial \overline{C_{n,-n}}}
= -4\frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}
\frac{1}{4\,c_n\{4\}}v_n\{4\}\overline{C_{n,-n}}\quad,
\end{aligned}
```

and we can evaluate the full error propagation simply as 

```math
\begin{aligned}
\delta^2 v'_n\{12,34\} 
&= 
    \left(\frac{\partial v'_n\{12,34\}}{\partial\overline{C'_{n,n}\{12\}}}\right)^2
	\delta^2C'_{n,n}\{12\} +
    \left(\frac{\partial v'_n\{12,34\}}{\partial\overline{C_{n,n}\{12\}}}\right)^2
    \delta^2C_{n,n}\{12\}\\
&\quad +
	\frac{\partial v'_n\{12,34\}}{\partial\overline{C'_{n,n}\{12\}}}	
	\frac{\partial v'_n\{12,34\}}{\partial\overline{C_{n,n}\{12\}}}	
	\rho_{C'_{n,n}\{12\},C_{n,n}\{12\}}
	\delta^2C'_{n,n}\{12\}\delta^2C_{n,n}\{12\}\\
&\quad + 
    \left(\frac{\partial v'_n\{12,34\}}{\partial\overline{C_{n,n,-n,-n}}}\right)^2
    \delta^2 C_{n,n,-n,-n} + 
	\left(\frac{\partial v'_n\{12,34\}}{\partial\overline{C_{n,-n}}}\right)^2
    \delta^2 C_{n,-n}\\
&= \left(\frac{v_n\{4\}}{\overline{C_{n,n}\{12\}}}\right)^2
	\delta^2C'_{n,n}\{12\} +
	\left(-\overline{C'_{n,n}\{12\}}\frac{v_n\{4\}}{\overline{C_{n,n}\{12\}}^2}\right)^2
	\delta^2C_{n,n}\{12\}\\
&\quad -
   \frac{v_n\{4\}}{\overline{C_{n,n}\{12\}}}
   \overline{C'}_{n,n}\{12\}\frac{v_n\{4\}}{\overline{C_{n,n}\{12\}}^2}
   \rho_{C'_{n,n}\{12\},C_{n,n}\{12\}}
   \delta^2C'_{n,n}\{12\} 
   \delta^2C_{n,n}\{12\}\\
&\quad +
   \left(\frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}\frac{1}{4\,c_n\{4\}}v_n\{4\}\right)^2
   \delta^2 C_{n,n,-n,-n} +
   \left(-\frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}}\frac{1}{c_n\{4\}}v_n\{4\}\overline{C_{n,-n}}\right)^2
   \delta^2 C_{n,-n}\\
\end{aligned}
```

Dividing by the integrated flow we get 

```math
\begin{aligned}
\frac{\delta^2 v'_n\{12,34\}}{v_n^2\{4\}} &= 
  \frac{1}{\overline{C_{n,n}\{12\}}}\delta^2C'_{n,n}\{12\} +
  \frac{\overline{C'}^2_{n,n}\{12\}}{\overline{C_{n,n}\{12\}}^4}\delta^2C_{n,n}\{12\}\\
&\quad +
  \frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}^3}
  \rho_{C'_{n,n}\{12\},C_{n,n}\{12\}}
  \delta^2C'_{n,n}\{12\} 
  \delta^2C_{n,n}\{12\}\\
&\quad +
   \frac{1}{16c^2_n\{4\}}\frac{\overline{C'_{n,n}\{12\}}^2}{\overline{C_{n,n}\{12\}}^2}
   \delta^2 C_{n,n,-n,-n}  + 
   \frac{\overline{C'_{n,n}\{12\}}^2}{\overline{C_{n,n}\{12\}}^2}\frac{1}{c_n^2\{4\}}\overline{C_{n,-n}}^2
   \delta^2 C_{n,-n}\\
   
&=
  \frac{1}{\overline{C_{n,n}\{12\}}}\delta^2C'_{n,n}\{12\}\\
&\quad +
  \frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}^3}
  \left(\frac{\overline{C'}_{n,n}\{12\}}{\overline{C_{n,n}\{12\}}}+
      \rho_{C'_{n,n}\{12\},C_{n,n}\{12\}}
      \delta^2C'_{n,n}\{12\}\right)\delta^2C_{n,n}\{12\}\\
&\quad +
   \frac{1}{c^2_n\{4\}}\frac{\overline{C'_{n,n}\{12\}}^2}{\overline{C_{n,n}\{12\}}^2}
   \left(\frac{1}{16}\delta^2 C_{n,n,-n,-n} +
   \overline{C_{n,-n}}^2\delta^2 C_{n,-n}\right)\\
   
&= \frac{1}{\overline{C_{n,n}\{12\}}}\\
&\quad\times  \left[\delta^2C'_{n,n}\{12\}+
   \frac{\overline{C'_{n,n}\{12\}}}{\overline{C_{n,n}\{12\}}^2}
       \left(\frac{\overline{C'}_{n,n}\{12\}}{\overline{C_{n,n}\{12\}}}+
            \rho_{C'_{n,n}\{12\},C_{n,n}\{12\}}
            \delta^2C'_{n,n}\{12\}\right)  \delta^2C_{n,n}\{12\}\right.\\
&\qquad+
   \left.\frac{1}{c^2_n\{4\}}\frac{\overline{C'_{n,n}\{12\}}^2}{\overline{C_{n,n}\{12\}}}
   \left(\frac{1}{16}\delta^2 C_{n,n,-n,-n} +
   \overline{C_{n,-n}}^2\delta^2 C_{n,-n}\right)\right]
   
\end{aligned}
```

Note, in the above, 

```math
\overline{C_{n,n}\{12\}}\neq \overline{C_{n,-n}}\quad,
```

since the left hand side is correlated over one sub-event and the
right hand side is correlated over the full event.
	
## Example of 4-particle 2 sub-events


In this example we generate particles with a explicitly modulated
$`\varphi`$ distribution and an explicit $`p_{\mathrm{T}}`$
modulation.  No other correlations are introduced.  In pseudorapidity
we generate particles according to a simple "V" distribution. 

In the example we calculate $`v_n'\{12,34\}`$ using dressed
harmonics.  That is, we assign a partition to each harmonic and if we
are accessing $`Q`$-vector components from mixed partitions, we return
zero. 

The results are shown in the plot below.

![Result of differential flow measurements in sub-events](gen_eta_medium_pdif.png "Results")

### Example with additional correlations 


Here, we introduce additional, transverse momentum dependent,
correlations.  For a given particle we determine the probability of
creating a partner particle by 

```math
P = p\frac{p_{\mathrm{T}}^3}{\max(p_{\mathrm{T}})^3}\quad.
```

A partner particle is one with 

```math
\begin{aligned}
p_{\mathrm{T,2}} &= p_{\mathrm{T,1}}\\
\eta_2 &= -\eta_1\\
\varphi_2 &= -\varphi_1\quad.
\end{aligned}
```

![Result of differential flow measurements in sub-events w/additional correlations](jet_eta_medium_pdif.png "Results")

