# Tests of the framework

The package comes with a number of examples and tests

## Test programs 

There are some predefined examples that uses these classes.  The code
is in the sub-directory [correlations/progs](correlations/progs)

- [benchmark.cc](correlations/ana/Benchmark.hh) analyses the data file
  generated for correlations
- [integrated.cc](correlations/ana/Integrated.hh) analysis the data
  file for integrated flow (with or without partitions)
- [differential.cc](correlations/ana/Differential.hh) analysis the data
  file for differential flow (with or without partitions)

To build and run the tests, do

	make tests

By default, only up to 6-particle correlators are defined for the
so-called 'fixed-form' (see [this page](doc/Methods.md)), but 7 and
8-particle correlators can be enabled by passing 7 or 8, respectively,
as the template argument for Tester in
[benchmark.cc](correlations/progs/benchmark.cc). Do note, that
compilation will take a very long time in this case (in fact, it may
never really finish).

All of these programs allow for a set of options.  To see which
options are accepted, do

	./prog -h

where `prog` is one of the above programs.

## Running the examples yourself 

The example programs `benchmark.cc`, `integrated.cc`, and
`differential.cc` all accepts a number of command line arguments.
Pass the option `-h` to get an overview of the available options.  For
example 

    Usage: ./differential
    
    Options:
    	-c KIND          Correlator kind (closed)
    	-e N             Number of events (10000)
    	-f               Finalize calculations (true)
    	-g GAP           Pseudorapidity gap (-1.000000)
    	-h               Show help (true)
    	-i FILE          Input filename ()
    	-l               Load state from input (false)
    	-m M             Maximum correlator (4)
    	-n M             Maximum harmonic (4)
    	-o FILE          Output filename ()
    	-t PT[,PT[,...]] pT bins (0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.2,1.4,1.6,1.8,2,2.5,3,3.5,4,4.5,5)
    	-u UNCER         Uncertainty kind (full)
    	-w               Use weights (true)

All three example, `benchmark.cc`, `integrated.cc`, and
`differential.cc` require an input file with data.  The data format is

    input file             ::= header events
	header                 ::= n_of events min_observations max_observations
	                           flow_coefficients
		                       pt_bins 
		                       pt_coefficents 
		                       mean_pt
	n_of_events            ::= <<integer>>
	min_observations       ::= <<integer>>
	max_observations       ::= <<integer>>
	flow_coefficents       ::= number coefficients 
	pt_bins                ::= number edges
	pt_coefficients        ::= number coefficients
	number                 ::= <<integer>>
	coefficients           ::= 
	                       |   coefficent coefficients
    coefficient            ::= <<floating point number>>
	edges                  ::= 
	                       |   edge edges
	edge                   ::= <<floating point number>>
	mean_pt                ::= <<floating point numbe (GeV?)r>>
    events                 ::= 
	                       |   event
		                   |   event events
	event                  ::= number_of_observations 
	                           observations 
	number_of_observations ::= <<integer>>
    observerations         ::=
		                   |   observation observations 
	observation            ::= eta pt phi weigth
    eta                    ::= <<floating point number>>
	pt                     ::= <<floating point number (GeV?)>>
	phi                    ::= <<floating point number (radians)>>
	weight                 ::= <<floating point number>>

The header is 5 lines, as shown above.  The number of observations per
event is one line and each observation is on a line of its own.  Lines
starting with `#` are ignored (comments). 

One can generate such a data file using the program `writer.cc`.  This
program, again, take a number of options (use the option `-h` to get a
summary). One can illustrate the various distributions in a data file
using the Python script `correlations/scripts/distributions.py`.

To run f.ex. the `integrated.cc` example, one needs to first
generate a data file, and then run that example on that data: 

    ./writer -e 1000 -G base -o data.dat
	./integrated -i data.dat -o data_flow.json 
	
If one want to see the distributions, one can do 

	./distributions -i data.dat
    python -i correlations/scripts/distributions.py data_dist.json

To see the results of the above integrated analysis, do 

	python -i correlatdions/scripts/integrated.py data_flow.json 
	
If no input file name is specified for `benchmark.cc`,
`integrated.cc`, `differential.cc`, and `distributions.cc` then the
applications will read from _standard input_.  This allows one to
create a pipeline of the applications, like 

    ./writer -e 1000 -G base | ./integrated -o data_flow.json 

or do parallel jobs using f.ex. [GNU
parallel](https://gnu.org/software/parallel) (see also the top-level
`Makefile` for some examples). 

## Symbolic expressions tests 

Please refer to [this page](doc/Symbolic.md)

